package org.doctordiabet.di.graph

import org.doctordiabet.di.components.artifacts.ArtifactsDataComponent
import org.doctordiabet.di.components.categories.CategoriesDataComponent
import org.doctordiabet.di.components.comments.CommentsDataComponent
import org.doctordiabet.di.components.environment.EnvironmentComponent
import org.doctordiabet.di.components.environment.EnvironmentComponentImpl
import org.doctordiabet.di.components.identities.IdentitiesDataComponent
import org.doctordiabet.di.components.roles.RolesDataComponent
import org.doctordiabet.di.components.sessions.SessionsDataComponent
import org.doctordiabet.di.components.smsru.SmsDataComponent
import org.doctordiabet.di.components.topics.TopicsDataComponent
import org.doctordiabet.di.components.verification_codes.VerificationCodesDataComponent

class DependencyGraph {

    private val environment: EnvironmentComponent

    constructor() {
        environment = EnvironmentComponentImpl.create()
    }

    fun resolveCategoriesDataComponent(): CategoriesDataComponent = environment.resolveCategoriesDataComponent()

    fun resolveTopicsDataComponent(): TopicsDataComponent = environment.resolveTopicsDataComponent()

    fun resolveCommentsDataComponent(): CommentsDataComponent = environment.resolveCommentsDataComponent()

    fun resolveIdentitiesDataComponent(): IdentitiesDataComponent = environment.resolveIdentitiesDataComponent()

    fun resolveSmsDataComponent(): SmsDataComponent = environment.resolveSmsDataComponent()

    fun resolveSessionsDataComponent(): SessionsDataComponent = environment.resolveSessionsDataComponent()

    fun resolveVerificationCodesDataComponent(): VerificationCodesDataComponent = environment.resolveVerificationCodesDataComponent()

    fun resolveRolesDataComponent(): RolesDataComponent = environment.resolveRolesDataComponent()

    fun resolveArtifactsDataComponent(): ArtifactsDataComponent = environment.resolveArtifactsDataComponent()
}