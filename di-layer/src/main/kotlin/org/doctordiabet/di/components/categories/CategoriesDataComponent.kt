package org.doctordiabet.di.components.categories

import org.doctordiabet.datalayer.services.categories.CategoriesCacheService
import org.doctordiabet.datalayer.services.categories.CategoriesPersistenceService
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class CategoriesDataComponentImpl private constructor(
        parent: EnvironmentComponent
): CategoriesDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): CategoriesDataComponent = CategoriesDataComponentImpl(parent)
    }

    override val persistence: CategoriesPersistenceService by lazy {
        CategoriesPersistenceService(parent.postgres)
    }

    override val cache: CategoriesCacheService by lazy {
        CategoriesCacheService(parent.redis)
    }

    override fun resolveCategoriesRepositoryComponent(): CategoriesRepositoryComponent = CategoriesRepositoryComponentImpl.create(this)
}

interface CategoriesDataComponent {

    val persistence: CategoriesPersistenceService

    val cache: CategoriesCacheService

    fun resolveCategoriesRepositoryComponent(): CategoriesRepositoryComponent
}