package org.doctordiabet.di.components.topics

import org.doctordiabet.datalayer.services.topics.TopicsPersistenceService
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class TopicsDataComponentImpl private constructor(parent: EnvironmentComponent): TopicsDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): TopicsDataComponent = TopicsDataComponentImpl(parent)
    }

    override val persistence: TopicsPersistenceService = TopicsPersistenceService(parent.postgres)

    override fun resolveTopicsRepositoryComponent(): TopicsRepositoryComponent = TopicsRepositoryComponentImpl.create(this)
}

interface TopicsDataComponent {

    val persistence: TopicsPersistenceService

    fun resolveTopicsRepositoryComponent(): TopicsRepositoryComponent
}