package org.doctordiabet.di.components.verification_codes

import org.doctordiabet.datalayer.services.verification_codes.VerificationCodesCacheService
import org.doctordiabet.di.components.environment.EnvironmentComponent
import org.doctordiabet.di.components.smsru.SmsDataComponent

internal class VerificationCodesDataComponentImpl private constructor(
        parent: EnvironmentComponent
): VerificationCodesDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): VerificationCodesDataComponent = VerificationCodesDataComponentImpl(parent)
    }

    override val cache: VerificationCodesCacheService by lazy {
        VerificationCodesCacheService(parent.redis)
    }

    override fun resolveVerificationCodesRepositoryComponent(dependencies: SmsDataComponent): VerificationCodesRepositoryComponent = VerificationCodesRepositoryComponentImpl.create(this, dependencies)

}

interface VerificationCodesDataComponent {

    val cache: VerificationCodesCacheService

    fun resolveVerificationCodesRepositoryComponent(dependencies: SmsDataComponent): VerificationCodesRepositoryComponent
}