package org.doctordiabet.di.components.sessions

import org.doctordiabet.datalayer.repository.sessions.SessionsRepositoryImpl
import org.doctordiabet.domainlayer.repository.session.SessionsRepository

internal class SessionsRepositoryComponentImpl private constructor(
        parent: SessionsDataComponent
): SessionsRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: SessionsDataComponent): SessionsRepositoryComponent = SessionsRepositoryComponentImpl(parent)
    }

    override val repository: SessionsRepository by lazy {
        SessionsRepositoryImpl(parent.persistentTokenPersistence, parent.persistentTokenCache, parent.sessionTokenCache)
    }

    override fun resolveSessionsInterfaceAdapterComponent(): SessionsInterfaceAdapterComponent = SessionsInterfaceAdapterComponentImpl.create(this)
}

interface SessionsRepositoryComponent {

    val repository: SessionsRepository

    fun resolveSessionsInterfaceAdapterComponent(): SessionsInterfaceAdapterComponent
}
