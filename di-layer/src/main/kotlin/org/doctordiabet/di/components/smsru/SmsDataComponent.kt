package org.doctordiabet.di.components.smsru

import org.doctordiabet.datalayer.processing.sms.SmsProcessor
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class SmsDataComponentImpl private constructor(
        parent: EnvironmentComponent
): SmsDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): SmsDataComponent = SmsDataComponentImpl(parent)
    }

    override val processor: SmsProcessor by lazy {
        SmsProcessor()
    }
}

interface SmsDataComponent {

    val processor: SmsProcessor
}