package org.doctordiabet.di.components.identities

import org.doctordiabet.domainlayer.usecase.identities.*

internal class IdentitiesInterfaceAdapterComponentImpl private constructor(
        parent: IdentitiesRepositoryComponent
): IdentitiesInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: IdentitiesRepositoryComponent): IdentitiesInterfaceAdapterComponent =
                IdentitiesInterfaceAdapterComponentImpl(parent)
    }

    override val identitiesFetch: IdentitiesFetchUseCase         = IdentitiesFetchUseCase(parent.repository)

    override val identitiesGet: IdentitiesGetUseCase             = IdentitiesGetUseCase(parent.repository)

    override val identityCreate: IdentityCreateUseCase           = IdentityCreateUseCase(parent.repository)

    override val identityDelete: IdentityDeleteUseCase           = IdentityDeleteUseCase(parent.repository)

    override val identityGet: IdentityGetUseCase                 = IdentityGetUseCase(parent.repository)

    override val identityUpdate: IdentityUpdateUseCase           = IdentityUpdateUseCase(parent.repository)

    override val identityPwdValidate: IdentityPwdValidateUseCase = IdentityPwdValidateUseCase(parent.repository)
}

interface IdentitiesInterfaceAdapterComponent {

    val identitiesFetch: IdentitiesFetchUseCase

    val identitiesGet: IdentitiesGetUseCase

    val identityCreate: IdentityCreateUseCase

    val identityDelete: IdentityDeleteUseCase

    val identityGet: IdentityGetUseCase

    val identityUpdate: IdentityUpdateUseCase

    val identityPwdValidate: IdentityPwdValidateUseCase
}