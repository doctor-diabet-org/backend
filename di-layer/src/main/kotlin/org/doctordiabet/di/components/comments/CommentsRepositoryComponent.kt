package org.doctordiabet.di.components.comments

import org.doctordiabet.datalayer.repository.comments.CommentsRepositoryImpl
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository

internal class CommentsRepositoryComponentImpl private constructor(parent: CommentsDataComponent): CommentsRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: CommentsDataComponent): CommentsRepositoryComponent = CommentsRepositoryComponentImpl(parent)
    }

    override val repository: CommentsRepository = CommentsRepositoryImpl(parent.persistence)

    override fun resolveCommentsInterfaceAdapterComponent(): CommentsInterfaceAdapterComponent = CommentsInterfaceAdapterComponentImpl.create(this)
}

interface CommentsRepositoryComponent {

    val repository: CommentsRepository

    fun resolveCommentsInterfaceAdapterComponent(): CommentsInterfaceAdapterComponent
}