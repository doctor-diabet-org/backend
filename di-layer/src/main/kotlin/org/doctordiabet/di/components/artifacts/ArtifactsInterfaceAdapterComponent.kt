package org.doctordiabet.di.components.artifacts

import org.doctordiabet.domainlayer.usecase.artifacts.*

internal class ArtifactsInterfaceAdapterComponentImpl(parent: ArtifactsRepositoryComponent): ArtifactsInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: ArtifactsRepositoryComponent): ArtifactsInterfaceAdapterComponent =
                ArtifactsInterfaceAdapterComponentImpl(parent)
    }

    override val subscribe: SubscribeUseCase by lazy {
        SubscribeUseCase(parent.repository)
    }

    override val unsubscribe: UnsubscribeUseCase by lazy {
        UnsubscribeUseCase(parent.repository)
    }

    override val downvote: DownvoteUseCase by lazy {
        DownvoteUseCase(parent.repository)
    }

    override val tagsAttach: TagsAttachUseCase by lazy {
        TagsAttachUseCase(parent.repository)
    }

    override val tagsCreate: TagsCreateUseCase by lazy {
        TagsCreateUseCase(parent.repository)
    }

    override val tagsDetach: TagsDetachUseCase by lazy {
        TagsDetachUseCase(parent.repository)
    }

    override val tagsRead: TagsReadUseCase by lazy {
        TagsReadUseCase(parent.repository)
    }

    override val upvote: UpvoteUseCase by lazy {
        UpvoteUseCase(parent.repository)
    }

    override val subscribersGet: SubscribersGetUseCase by lazy {
        SubscribersGetUseCase(parent.repository)
    }
}

interface ArtifactsInterfaceAdapterComponent {

    val subscribe: SubscribeUseCase
    val unsubscribe: UnsubscribeUseCase
    val downvote: DownvoteUseCase
    val tagsAttach: TagsAttachUseCase
    val tagsCreate: TagsCreateUseCase
    val tagsDetach: TagsDetachUseCase
    val tagsRead: TagsReadUseCase
    val upvote: UpvoteUseCase
    val subscribersGet: SubscribersGetUseCase
}