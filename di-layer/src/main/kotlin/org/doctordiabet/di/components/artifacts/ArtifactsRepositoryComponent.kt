package org.doctordiabet.di.components.artifacts

import org.doctordiabet.datalayer.repository.artifacts.ArtifactsRepositoryImpl
import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository

internal class ArtifactsRepositoryComponentImpl(parent: ArtifactsDataComponent): ArtifactsRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: ArtifactsDataComponent): ArtifactsRepositoryComponent = ArtifactsRepositoryComponentImpl(parent)
    }

    override val repository: ArtifactsRepository by lazy {
        ArtifactsRepositoryImpl(parent.persistence)
    }

    override fun resolveArtifactsInterfaceAdapterComponent(): ArtifactsInterfaceAdapterComponent =
            ArtifactsInterfaceAdapterComponentImpl.create(this)
}

interface ArtifactsRepositoryComponent {

    val repository: ArtifactsRepository

    fun resolveArtifactsInterfaceAdapterComponent(): ArtifactsInterfaceAdapterComponent
}