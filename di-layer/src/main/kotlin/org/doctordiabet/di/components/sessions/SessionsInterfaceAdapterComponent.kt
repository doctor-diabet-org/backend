package org.doctordiabet.di.components.sessions

import org.doctordiabet.domainlayer.usecase.session.PersistentTokenCreateUseCase
import org.doctordiabet.domainlayer.usecase.session.PersistentTokenGetUseCase
import org.doctordiabet.domainlayer.usecase.session.SessionTokenCreateUseCase
import org.doctordiabet.domainlayer.usecase.session.SessionTokenGetUseCase

internal class SessionsInterfaceAdapterComponentImpl private constructor(
        parent: SessionsRepositoryComponent
): SessionsInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: SessionsRepositoryComponent): SessionsInterfaceAdapterComponent = SessionsInterfaceAdapterComponentImpl(parent)
    }

    override val persistentTokenCreate: PersistentTokenCreateUseCase by lazy {
        PersistentTokenCreateUseCase(parent.repository)
    }

    override val persistentTokenGet: PersistentTokenGetUseCase by lazy {
        PersistentTokenGetUseCase(parent.repository)
    }

    override val sessionTokenCreate: SessionTokenCreateUseCase by lazy {
        SessionTokenCreateUseCase(parent.repository)
    }

    override val sessionTokenGet: SessionTokenGetUseCase by lazy {
        SessionTokenGetUseCase(parent.repository)
    }
}

interface SessionsInterfaceAdapterComponent {

    val persistentTokenCreate: PersistentTokenCreateUseCase

    val persistentTokenGet: PersistentTokenGetUseCase

    val sessionTokenCreate: SessionTokenCreateUseCase

    val sessionTokenGet: SessionTokenGetUseCase
}