package org.doctordiabet.di.components.categories

import org.doctordiabet.domainlayer.usecase.categories.*

internal class CategoriesInterfaceAdapterComponentImpl private constructor(
        parent: CategoriesRepositoryComponent
): CategoriesInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: CategoriesRepositoryComponent): CategoriesInterfaceAdapterComponent = CategoriesInterfaceAdapterComponentImpl(parent)
    }

    override val categoriesFetch: CategoriesFetchUseCase = CategoriesFetchUseCase(parent.repository)

    override val categoriesGet: CategoriesGetUseCase     = CategoriesGetUseCase(parent.repository)

    override val categoryCreate: CategoryCreateUseCase   = CategoryCreateUseCase(parent.repository)

    override val categoryDelete: CategoryDeleteUseCase   = CategoryDeleteUseCase(parent.repository)

    override val categoryGet: CategoryGetUseCase         = CategoryGetUseCase(parent.repository)

    override val categoryUpdate: CategoryUpdateUseCase   = CategoryUpdateUseCase(parent.repository)
}

interface CategoriesInterfaceAdapterComponent {

    val categoriesFetch: CategoriesFetchUseCase

    val categoriesGet: CategoriesGetUseCase

    val categoryCreate: CategoryCreateUseCase

    val categoryDelete: CategoryDeleteUseCase

    val categoryGet: CategoryGetUseCase

    val categoryUpdate: CategoryUpdateUseCase
}