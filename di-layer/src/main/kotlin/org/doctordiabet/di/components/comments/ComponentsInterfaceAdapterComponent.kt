package org.doctordiabet.di.components.comments

import org.doctordiabet.domainlayer.usecase.comments.*

internal class CommentsInterfaceAdapterComponentImpl private constructor(parent: CommentsRepositoryComponent): CommentsInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: CommentsRepositoryComponent): CommentsInterfaceAdapterComponent = CommentsInterfaceAdapterComponentImpl(parent)
    }

    override val commentCreate: CommentCreateUseCase = CommentCreateUseCase(parent.repository)

    override val commentDelete: CommentDeleteUseCase = CommentDeleteUseCase(parent.repository)

    override val commentGet:    CommentGetUseCase    = CommentGetUseCase(parent.repository)

    override val commentsFetch: CommentsFetchUseCase = CommentsFetchUseCase(parent.repository)

    override val commentsGet:   CommentsGetUseCase   = CommentsGetUseCase(parent.repository)

    override val commentUpdate: CommentUpdateUseCase = CommentUpdateUseCase(parent.repository)
}

interface CommentsInterfaceAdapterComponent {

    val commentCreate: CommentCreateUseCase

    val commentDelete: CommentDeleteUseCase

    val commentGet:    CommentGetUseCase

    val commentsFetch: CommentsFetchUseCase

    val commentsGet:   CommentsGetUseCase

    val commentUpdate: CommentUpdateUseCase
}