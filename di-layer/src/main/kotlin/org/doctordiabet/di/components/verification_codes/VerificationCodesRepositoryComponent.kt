package org.doctordiabet.di.components.verification_codes

import org.doctordiabet.datalayer.repository.verification_codes.VerificationCodesRepositoryImpl
import org.doctordiabet.di.components.smsru.SmsDataComponent
import org.doctordiabet.domainlayer.repository.verification_codes.VerificationCodesRepository

internal class VerificationCodesRepositoryComponentImpl private constructor(
        parent: VerificationCodesDataComponent,
        dependencies: SmsDataComponent
): VerificationCodesRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: VerificationCodesDataComponent, dependencies: SmsDataComponent): VerificationCodesRepositoryComponent = VerificationCodesRepositoryComponentImpl(parent, dependencies)
    }

    override val repository: VerificationCodesRepository by lazy {
        VerificationCodesRepositoryImpl(parent.cache, dependencies.processor)
    }

    override fun resolveVerificationCodesInterfaceAdapterComponent(): VerificationCodesInterfaceAdapterComponent = VerificationCodesInterfaceAdapterComponentImpl.create(this)
}

interface VerificationCodesRepositoryComponent {

    val repository: VerificationCodesRepository

    fun resolveVerificationCodesInterfaceAdapterComponent(): VerificationCodesInterfaceAdapterComponent
}