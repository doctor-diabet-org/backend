package org.doctordiabet.di.components.categories

import org.doctordiabet.datalayer.repository.categories.CategoriesRepositoryImpl
import org.doctordiabet.domainlayer.repository.categories.CategoriesRepository

internal class CategoriesRepositoryComponentImpl private constructor(
        parent: CategoriesDataComponent
): CategoriesRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: CategoriesDataComponent): CategoriesRepositoryComponent = CategoriesRepositoryComponentImpl(parent)
    }

    override val repository: CategoriesRepository by lazy {
        CategoriesRepositoryImpl(parent.persistence, parent.cache)
    }

    override fun resolveCategoriesInterfaceAdapterComponent(): CategoriesInterfaceAdapterComponent = CategoriesInterfaceAdapterComponentImpl.create(this)
}

interface CategoriesRepositoryComponent {

    val repository: CategoriesRepository

    fun resolveCategoriesInterfaceAdapterComponent(): CategoriesInterfaceAdapterComponent
}