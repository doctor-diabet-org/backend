package org.doctordiabet.di.components.topics

import org.doctordiabet.domainlayer.usecase.topics.*

internal class TopicsInterfaceAdapterComponentImpl private constructor(parent: TopicsRepositoryComponent): TopicsInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: TopicsRepositoryComponent): TopicsInterfaceAdapterComponent = TopicsInterfaceAdapterComponentImpl(parent)
    }

    override val topicCreate: TopicCreateUseCase = TopicCreateUseCase(parent.repository)

    override val topicDelete: TopicDeleteUseCase = TopicDeleteUseCase(parent.repository)

    override val topicGet:    TopicGetUseCase    = TopicGetUseCase(parent.repository)

    override val topicsFetch: TopicsFetchUseCase = TopicsFetchUseCase(parent.repository)

    override val topicsGet:   TopicsGetUseCase   = TopicsGetUseCase(parent.repository)

    override val topicUpdate: TopicUpdateUseCase = TopicUpdateUseCase(parent.repository)
}

interface TopicsInterfaceAdapterComponent {

    val topicCreate: TopicCreateUseCase

    val topicDelete: TopicDeleteUseCase

    val topicGet:    TopicGetUseCase

    val topicsFetch: TopicsFetchUseCase

    val topicsGet:   TopicsGetUseCase

    val topicUpdate: TopicUpdateUseCase
}