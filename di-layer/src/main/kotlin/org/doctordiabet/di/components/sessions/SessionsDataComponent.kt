package org.doctordiabet.di.components.sessions

import org.doctordiabet.datalayer.services.sessions.PersistentTokenCacheService
import org.doctordiabet.datalayer.services.sessions.PersistentTokenPersistenceService
import org.doctordiabet.datalayer.services.sessions.SessionTokenCacheService
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class SessionsDataComponentImpl private constructor(
        parent: EnvironmentComponent
): SessionsDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): SessionsDataComponent = SessionsDataComponentImpl(parent)
    }

    override val persistentTokenPersistence: PersistentTokenPersistenceService by lazy {
        PersistentTokenPersistenceService(parent.postgres)
    }

    override val persistentTokenCache: PersistentTokenCacheService by lazy {
        PersistentTokenCacheService(parent.redis)
    }

    override val sessionTokenCache: SessionTokenCacheService by lazy {
        SessionTokenCacheService(parent.redis)
    }

    override fun resolveSessionsRepositoryComponent(): SessionsRepositoryComponent = SessionsRepositoryComponentImpl.create(this)
}

interface SessionsDataComponent {

    val persistentTokenPersistence: PersistentTokenPersistenceService

    val persistentTokenCache: PersistentTokenCacheService

    val sessionTokenCache: SessionTokenCacheService

    fun resolveSessionsRepositoryComponent(): SessionsRepositoryComponent
}