package org.doctordiabet.di.components.roles

import org.doctordiabet.datalayer.services.roles.RolesPersistenceService
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class RolesDataComponentImpl private constructor(
        parent: EnvironmentComponent
): RolesDataComponent {

    companion object {

        @JvmStatic
        fun create(parent: EnvironmentComponent): RolesDataComponent = RolesDataComponentImpl(parent)
    }

    override val persistence: RolesPersistenceService = RolesPersistenceService(parent.postgres)

    override fun resolveRolesRepositoryComponent(): RolesRepositoryComponent = RolesRepositoryComponentImpl.create(this)
}

interface RolesDataComponent {

    val persistence: RolesPersistenceService

    fun resolveRolesRepositoryComponent(): RolesRepositoryComponent
}