package org.doctordiabet.di.components.identities

import org.doctordiabet.datalayer.services.identities.IdentitiesCacheService
import org.doctordiabet.datalayer.services.identities.IdentitiesPersistenceService
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class IdentitiesDataComponentImpl private constructor(
        parent: EnvironmentComponent
): IdentitiesDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): IdentitiesDataComponent = IdentitiesDataComponentImpl(parent)
    }

    override val persistence: IdentitiesPersistenceService by lazy {
        IdentitiesPersistenceService(parent.postgres)
    }

    override val cache: IdentitiesCacheService by lazy {
        IdentitiesCacheService(parent.redis)
    }

    override fun resolveIdentitiesRepositoryComponent(): IdentitiesRepositoryComponent = IdentitiesRepositoryComponentImpl.create(this)
}

interface IdentitiesDataComponent {

    val persistence: IdentitiesPersistenceService

    val cache: IdentitiesCacheService

    fun resolveIdentitiesRepositoryComponent(): IdentitiesRepositoryComponent
}