package org.doctordiabet.di.components.comments

import org.doctordiabet.datalayer.services.comments.CommentsPersistenceService
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class CommentsDataComponentImpl private constructor(parent: EnvironmentComponent): CommentsDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): CommentsDataComponent = CommentsDataComponentImpl(parent)
    }

    override val persistence: CommentsPersistenceService = CommentsPersistenceService(parent.postgres)

    override fun resolveCommentsRepositoryComponent(): CommentsRepositoryComponent = CommentsRepositoryComponentImpl.create(this)
}

interface CommentsDataComponent {

    val persistence: CommentsPersistenceService

    fun resolveCommentsRepositoryComponent(): CommentsRepositoryComponent
}