package org.doctordiabet.di.components.topics

import org.doctordiabet.datalayer.repository.topics.TopicsRepositoryImpl
import org.doctordiabet.domainlayer.repository.topics.TopicsRepository

internal class TopicsRepositoryComponentImpl private constructor(parent: TopicsDataComponent): TopicsRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: TopicsDataComponent): TopicsRepositoryComponent = TopicsRepositoryComponentImpl(parent)
    }

    override val repository: TopicsRepository = TopicsRepositoryImpl(parent.persistence)

    override fun resolveTopicsInterfaceAdapterComponent(): TopicsInterfaceAdapterComponent = TopicsInterfaceAdapterComponentImpl.create(this)
}

interface TopicsRepositoryComponent {

    val repository: TopicsRepository

    fun resolveTopicsInterfaceAdapterComponent(): TopicsInterfaceAdapterComponent
}