package org.doctordiabet.di.components.identities

import org.doctordiabet.datalayer.repository.identities.IdentitiesRepositoryImpl
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository

internal class IdentitiesRepositoryComponentImpl private constructor(
        parent: IdentitiesDataComponent
): IdentitiesRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: IdentitiesDataComponent): IdentitiesRepositoryComponent = IdentitiesRepositoryComponentImpl(parent)
    }

    override val repository: IdentitiesRepository by lazy {
        IdentitiesRepositoryImpl(parent.persistence, parent.cache)
    }

    override fun resolveIdentitiesInterfaceAdapterComponent(): IdentitiesInterfaceAdapterComponent = IdentitiesInterfaceAdapterComponentImpl.create(this)
}

interface IdentitiesRepositoryComponent {

    val repository: IdentitiesRepository

    fun resolveIdentitiesInterfaceAdapterComponent(): IdentitiesInterfaceAdapterComponent
}