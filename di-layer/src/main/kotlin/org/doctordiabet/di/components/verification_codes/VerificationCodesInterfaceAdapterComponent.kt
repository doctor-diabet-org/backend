package org.doctordiabet.di.components.verification_codes

import org.doctordiabet.domainlayer.usecase.verification_codes.VerificationCodeSendUseCase
import org.doctordiabet.domainlayer.usecase.verification_codes.VerificationCodeVerifyUseCase

internal class VerificationCodesInterfaceAdapterComponentImpl private constructor(
        parent: VerificationCodesRepositoryComponent
): VerificationCodesInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: VerificationCodesRepositoryComponent): VerificationCodesInterfaceAdapterComponent = VerificationCodesInterfaceAdapterComponentImpl(parent)
    }

    override val verificationCodeSend: VerificationCodeSendUseCase by lazy {
        VerificationCodeSendUseCase(parent.repository)
    }

    override val verificationCodeVerify: VerificationCodeVerifyUseCase by lazy {
        VerificationCodeVerifyUseCase(parent.repository)
    }
}

interface VerificationCodesInterfaceAdapterComponent {

    val verificationCodeSend: VerificationCodeSendUseCase

    val verificationCodeVerify: VerificationCodeVerifyUseCase
}