package org.doctordiabet.di.components.roles

import org.doctordiabet.datalayer.repository.roles.RolesRepositoryImpl
import org.doctordiabet.domainlayer.repository.roles.RolesRepository

internal class RolesRepositoryComponentImpl private constructor(
        parent: RolesDataComponent
): RolesRepositoryComponent {

    companion object {

        @JvmStatic fun create(parent: RolesDataComponent): RolesRepositoryComponent = RolesRepositoryComponentImpl(parent)
    }

    override val repository: RolesRepository = RolesRepositoryImpl(parent.persistence)

    override fun resolveRolesInterfaceAdapterComponent(): RolesInterfaceAdapterComponent = RolesInterfaceAdapterComponentImpl.create(this)
}

interface RolesRepositoryComponent {

    val repository: RolesRepository

    fun resolveRolesInterfaceAdapterComponent(): RolesInterfaceAdapterComponent
}