package org.doctordiabet.di.components.environment

import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.core.RedisAdapter
import org.doctordiabet.di.components.artifacts.ArtifactsDataComponent
import org.doctordiabet.di.components.artifacts.ArtifactsDataComponentImpl
import org.doctordiabet.di.components.categories.CategoriesDataComponent
import org.doctordiabet.di.components.categories.CategoriesDataComponentImpl
import org.doctordiabet.di.components.comments.CommentsDataComponent
import org.doctordiabet.di.components.comments.CommentsDataComponentImpl
import org.doctordiabet.di.components.identities.IdentitiesDataComponent
import org.doctordiabet.di.components.identities.IdentitiesDataComponentImpl
import org.doctordiabet.di.components.roles.RolesDataComponent
import org.doctordiabet.di.components.roles.RolesDataComponentImpl
import org.doctordiabet.di.components.sessions.SessionsDataComponent
import org.doctordiabet.di.components.sessions.SessionsDataComponentImpl
import org.doctordiabet.di.components.smsru.SmsDataComponent
import org.doctordiabet.di.components.smsru.SmsDataComponentImpl
import org.doctordiabet.di.components.topics.TopicsDataComponent
import org.doctordiabet.di.components.topics.TopicsDataComponentImpl
import org.doctordiabet.di.components.verification_codes.VerificationCodesDataComponent
import org.doctordiabet.di.components.verification_codes.VerificationCodesDataComponentImpl

internal class EnvironmentComponentImpl private constructor(

): EnvironmentComponent {

    companion object {

        @JvmStatic fun create(): EnvironmentComponent = EnvironmentComponentImpl()
    }

    override val redis: RedisAdapter by lazy {
        RedisAdapter()
    }

    override val postgres: PostgresAdapter by lazy {
        PostgresAdapter()
    }

    override fun resolveCategoriesDataComponent(): CategoriesDataComponent = CategoriesDataComponentImpl.create(this)

    override fun resolveTopicsDataComponent(): TopicsDataComponent = TopicsDataComponentImpl.create(this)

    override fun resolveCommentsDataComponent(): CommentsDataComponent = CommentsDataComponentImpl.create(this)

    override fun resolveIdentitiesDataComponent(): IdentitiesDataComponent = IdentitiesDataComponentImpl.create(this)

    override fun resolveSessionsDataComponent(): SessionsDataComponent = SessionsDataComponentImpl.create(this)

    override fun resolveSmsDataComponent(): SmsDataComponent = SmsDataComponentImpl.create(this)

    override fun resolveVerificationCodesDataComponent(): VerificationCodesDataComponent = VerificationCodesDataComponentImpl.create(this)

    override fun resolveRolesDataComponent(): RolesDataComponent = RolesDataComponentImpl.create(this)

    override fun resolveArtifactsDataComponent(): ArtifactsDataComponent = ArtifactsDataComponentImpl.create(this)
}

interface EnvironmentComponent {

    val redis: RedisAdapter

    val postgres: PostgresAdapter

    fun resolveCategoriesDataComponent(): CategoriesDataComponent

    fun resolveTopicsDataComponent(): TopicsDataComponent

    fun resolveCommentsDataComponent(): CommentsDataComponent

    fun resolveIdentitiesDataComponent(): IdentitiesDataComponent

    fun resolveSmsDataComponent(): SmsDataComponent

    fun resolveSessionsDataComponent(): SessionsDataComponent

    fun resolveVerificationCodesDataComponent(): VerificationCodesDataComponent

    fun resolveRolesDataComponent(): RolesDataComponent

    fun resolveArtifactsDataComponent(): ArtifactsDataComponent
}