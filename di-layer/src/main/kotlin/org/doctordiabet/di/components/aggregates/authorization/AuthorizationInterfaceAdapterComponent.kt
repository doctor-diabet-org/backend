package org.doctordiabet.di.components.aggregates.authorization

import org.doctordiabet.di.components.identities.IdentitiesRepositoryComponent
import org.doctordiabet.di.components.sessions.SessionsRepositoryComponent
import org.doctordiabet.di.components.verification_codes.VerificationCodesRepositoryComponent
import org.doctordiabet.domainlayer.usecase.authorization.AuthorizeCodeUseCase
import org.doctordiabet.domainlayer.usecase.authorization.AuthorizeCredentialsUseCase

fun resolveAuthenticationInterfaceAdapterComponent(identities: IdentitiesRepositoryComponent,
                                                  sessions: SessionsRepositoryComponent,
                                                  codes: VerificationCodesRepositoryComponent): AuthorizationInterfaceAdapterComponent =
        AuthorizationInterfaceAdapterComponentImpl.create(identities, sessions, codes)

internal class AuthorizationInterfaceAdapterComponentImpl private constructor(
        identities: IdentitiesRepositoryComponent,
        sessions: SessionsRepositoryComponent,
        codes: VerificationCodesRepositoryComponent
): AuthorizationInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(identities: IdentitiesRepositoryComponent,
                              sessions: SessionsRepositoryComponent,
                              codes: VerificationCodesRepositoryComponent): AuthorizationInterfaceAdapterComponent =
                AuthorizationInterfaceAdapterComponentImpl(identities, sessions, codes)
    }

    override val authorizeCredentials: AuthorizeCredentialsUseCase = AuthorizeCredentialsUseCase(
            identities.repository,
            sessions.repository
    )
    override val authorizeCode: AuthorizeCodeUseCase = AuthorizeCodeUseCase(
            identities.repository,
            codes.repository,
            sessions.repository
    )
}

interface AuthorizationInterfaceAdapterComponent {

    val authorizeCredentials: AuthorizeCredentialsUseCase

    val authorizeCode: AuthorizeCodeUseCase
}