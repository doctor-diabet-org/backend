package org.doctordiabet.di.components.roles

import org.doctordiabet.domainlayer.usecase.rbac.RelationshipGetUseCase
import org.doctordiabet.domainlayer.usecase.rbac.RoleGrantUseCase
import org.doctordiabet.domainlayer.usecase.rbac.RoleRevokeUseCase
import org.doctordiabet.domainlayer.usecase.rbac.RolesGetUseCase

internal class RolesInterfaceAdapterComponentImpl private constructor(
        parent: RolesRepositoryComponent
): RolesInterfaceAdapterComponent {

    companion object {

        @JvmStatic fun create(parent: RolesRepositoryComponent): RolesInterfaceAdapterComponent = RolesInterfaceAdapterComponentImpl(parent)
    }

    override val roleGrant: RoleGrantUseCase = RoleGrantUseCase(parent.repository)

    override val rolesGet: RolesGetUseCase = RolesGetUseCase(parent.repository)

    override val roleRevoke: RoleRevokeUseCase = RoleRevokeUseCase(parent.repository)

    override val relationshipGet: RelationshipGetUseCase = RelationshipGetUseCase(parent.repository)
}

interface RolesInterfaceAdapterComponent {

    val roleGrant: RoleGrantUseCase

    val rolesGet: RolesGetUseCase

    val roleRevoke: RoleRevokeUseCase

    val relationshipGet: RelationshipGetUseCase
}