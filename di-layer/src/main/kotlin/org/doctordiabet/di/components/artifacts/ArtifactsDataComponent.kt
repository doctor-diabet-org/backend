package org.doctordiabet.di.components.artifacts

import org.doctordiabet.datalayer.services.artifacts.ArtifactsPersistenceService
import org.doctordiabet.di.components.environment.EnvironmentComponent

internal class ArtifactsDataComponentImpl(parent: EnvironmentComponent): ArtifactsDataComponent {

    companion object {

        @JvmStatic fun create(parent: EnvironmentComponent): ArtifactsDataComponent = ArtifactsDataComponentImpl(parent)
    }

    override val persistence: ArtifactsPersistenceService by lazy {
        ArtifactsPersistenceService(parent.postgres)
    }

    override fun resolveArtifactsRepositoryComponent(): ArtifactsRepositoryComponent =
            ArtifactsRepositoryComponentImpl.create(this)
}

interface ArtifactsDataComponent {

    val persistence: ArtifactsPersistenceService

    fun resolveArtifactsRepositoryComponent(): ArtifactsRepositoryComponent
}