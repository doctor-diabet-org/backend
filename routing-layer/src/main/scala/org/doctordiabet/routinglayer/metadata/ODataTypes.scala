package org.doctordiabet.routinglayer.metadata

object ODataTypes {

  val COLLECTION = "Type.Collection"

  val CATEGORY = "Entity.Category"
  val TOPIC = "Entity.Topic"
  val COMMENT = "Entity.Comment"
  val IDENTITY_PUBLIC = "Entity.Identity.Public"
  val IDENTITY_PRIVATE = "Entity.Identity.Private"
  val SESSION_TOKEN = "Entity.Session"
  val REFRESH_TOKEN = "Entity.RefreshToken"
  val METADATA = "Entity.Metadata"

  val TAG = "Object.Tag"
}
