package org.doctordiabet.routinglayer.notifications

import com.fasterxml.jackson.databind.node.{JsonNodeFactory, ObjectNode}

abstract sealed class Notification {
    
    def toJson: ObjectNode
}

case class Topic(topic: String, notification: Payload, data: ObjectNode) extends Notification {
    
    override def toJson: ObjectNode = JsonNodeFactory.instance
        .objectNode()
        .put("topic", topic)
        .set("notification", notification.toJson)
        .asInstanceOf[ObjectNode]
        .set("data", data)
        .asInstanceOf[ObjectNode]
}

case class Target(token: String, notification: Payload, data: ObjectNode) extends Notification {
    
    override def toJson: ObjectNode = JsonNodeFactory.instance
        .objectNode()
        .put("token", token)
        .set("notification", notification.toJson)
        .asInstanceOf[ObjectNode]
        .set("data", data)
        .asInstanceOf[ObjectNode]
}

case class Payload(title: String, body: String) {
    
    def toJson: ObjectNode = JsonNodeFactory.instance
        .objectNode()
        .put("title", title)
        .put("body", body)
}

case class Send(private val notification: Notification) {
    
    def toJson: ObjectNode = JsonNodeFactory.instance
        .objectNode()
        .set("message", notification.toJson)
        .asInstanceOf[ObjectNode]
}