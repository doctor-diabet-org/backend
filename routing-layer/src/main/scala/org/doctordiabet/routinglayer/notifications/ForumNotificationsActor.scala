package org.doctordiabet.routinglayer.notifications

import java.util.UUID
import java.util.concurrent.{Executors, TimeUnit}

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.stream.ActorMaterializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.cache2k.integration.CacheLoader
import org.cache2k.{Cache, Cache2kBuilder}
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository.Referral
import org.doctordiabet.domainlayer.usecase.artifacts.SubscribersGetUseCase
import org.doctordiabet.routinglayer.confiuration.NotificationsConfig

import scala.collection.JavaConverters._
import scala.concurrent.duration.Duration
import scala.concurrent.{
  Await,
  ExecutionContext,
  ExecutionContextExecutorService,
  Future
}

object ForumNotificationsActor {

  def apply(subscribersGet: SubscribersGetUseCase)(
      implicit system: ActorSystem): ActorRef =
    system.actorOf(Props(new ForumNotificationsActor(subscribersGet)))
}

//The main reason this lies in presentation is that presentation is built on the top
//of akka-actors and makes use of it. So, just in case I definitely don't want to fuck with akka
//both in kotlin and scala, it would be here...
class ForumNotificationsActor(private val subscribersGet: SubscribersGetUseCase)
    extends Actor
    with ActorLogging {

  private implicit val executionContextExecutor
    : ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService(Executors.newSingleThreadExecutor())

  private implicit val materializer: ActorMaterializer = ActorMaterializer()

  private val mappingContext = new ObjectMapper()
  mappingContext.registerModule(DefaultScalaModule)

  private val topicUri = Uri.apply(
    "https://fcm.googleapis.com/v1/projects/doctor-diabet-org/messages:send")

  private val subscriptions: Cache[UUID, Traversable[UUID]] =
    new Cache2kBuilder[UUID, Traversable[UUID]]() {}
      .name("forum-subscriptions")
      .expireAfterWrite(5L, TimeUnit.MINUTES)
      .resilienceDuration(30L, TimeUnit.SECONDS)
      .refreshAhead(true)
      .entryCapacity(1000L)
      .loader(new CacheLoader[UUID, Traversable[UUID]] {
        override def load(key: UUID): Traversable[UUID] =
          subscribersGet.executeBlocking(key).asScala
      })
      .build()

  private val tokens: Cache[UUID, Traversable[String]] =
    new Cache2kBuilder[UUID, Traversable[String]]() {}
      .name("forum-tokens")
      .expireAfterWrite(1L, TimeUnit.HOURS)
      .build()
  
  override def receive: Receive = {
    case CategoryCreate(identity, artifact) => {
      println(s"Category create msg ($identity, $artifact)")
    }
    case CategoryUpdate(identity, artifact) => {
      println(s"Category update msg ($identity, $artifact)")
    }
    case CategoryDelete(identity, artifact) => {
      println(s"Category delete msg ($identity, $artifact)")
    }
    case CategoryUpvote(identity, artifact) => {
      println(s"Category upvote msg ($identity, $artifact)")
    }
    case CategoryDownvote(identity, artifact) => {
      println(s"Category downvote msg ($identity, $artifact)")
    }
    case TopicCreate(identity, artifact) => {
      println(s"Topic create msg($identity, $artifact)")
    }
    case TopicUpdate(identity, artifact) => {
      println(s"Topic update msg($identity, $artifact)")
    }
    case TopicDelete(identity, artifact) => {
      println(s"Topic delete msg($identity, $artifact)")
    }
    case TopicUpvote(identity, artifact) => {
      println(s"Topic upvote msg ($identity, $artifact)")
    }
    case TopicDownvote(identity, artifact) => {
      println(s"Topic upvote msg ($identity, $artifact)")
    }
    case CommentCreate(referral, identity, artifact) => {
      println(s"Comment create msg($referral, $identity, $artifact)")
    }
    case CommentUpdate(referral, identity, artifact) => {
      println(s"Comment update msg($referral, $identity, $artifact)")
    }
    case CommentDelete(referral, identity, artifact) => {
      println(s"Comment delete msg($referral, $identity, $artifact)")
    }
    case CommentUpvote(identity, artifact) => {
      println(s"Comment upvote msg ($identity, $artifact)")
    }
    case CommentDownvote(identity, artifact) => {
      println(s"Comment upvote msg ($identity, $artifact)")
    }
    case Subscribe(identity, artifact, owner) => {
      println(s"Subscribe msg ($identity, $artifact)")
    }
    case Unsubscribe(identity, artifact, owner) => {
      println(s"Unsubscribe msg ($identity, $artifact)")
    }
    case FirebaseToken(identity, token) => {
      println(s"Firebase token msg ($identity, $token)")
      if(tokens.containsKey(identity)) {
        tokens.put(identity, tokens.get(identity) ++ List(token))
      } else {
        tokens.put(identity, List(token))
      }
    }
    case _ => {
      //ignore all unknown messages,
      //just in case, not to kill the actor
    }
  }

  private def sendTopicBroadcast(dto: Topic): Unit = {
    val request: HttpRequest = HttpRequest(
      HttpMethods.GET,
      topicUri,
      entity = HttpEntity(contentType = ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(Send(dto).toJson)))
      .withHeaders(RawHeader("Authorization",
                             s"Bearer ${NotificationsConfig.accessToken}"))
    val response: Future[HttpResponse] =
      Http()(system = context.system).singleRequest(request)
    println(Await.result(response, Duration.Inf).entity.toString)
  }

  private def sendTargetBroadcast(dto: Target): Unit = {
    val request: HttpRequest = HttpRequest(
      HttpMethods.GET,
      topicUri,
      entity = HttpEntity(contentType = ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(Send(dto).toJson)))
      .withHeaders(RawHeader("Authorization",
                             s"Bearer ${NotificationsConfig.accessToken}"))
    val response: Future[HttpResponse] =
      Http()(system = context.system).singleRequest(request)
    println(Await.result(response, Duration.Inf).entity.toString)
  }
}

//COMMENT NOTIFICATIONS
sealed class CommentNotification(val identity: UUID, val artifact: UUID)

case class CommentUpvote(override val identity: UUID,
                         override val artifact: UUID)
    extends CommentNotification(identity, artifact)

case class CommentDownvote(override val identity: UUID,
                           override val artifact: UUID)
    extends CommentNotification(identity, artifact)

case class CommentCreate(referral: Referral,
                         override val identity: UUID,
                         override val artifact: UUID)
    extends CommentNotification(identity, artifact)

case class CommentUpdate(referral: Referral,
                         override val identity: UUID,
                         override val artifact: UUID)
    extends CommentNotification(identity, artifact)

case class CommentDelete(referral: Referral,
                         override val identity: UUID,
                         override val artifact: UUID)
    extends CommentNotification(identity, artifact)

//TOPIC NOTIFICATIONS
sealed class TopicNotification(val identity: UUID, val artifact: UUID)

case class TopicUpvote(override val identity: UUID, override val artifact: UUID)
    extends TopicNotification(identity, artifact)

case class TopicDownvote(override val identity: UUID,
                         override val artifact: UUID)
    extends TopicNotification(identity, artifact)

case class TopicCreate(override val identity: UUID, override val artifact: UUID)
    extends TopicNotification(identity, artifact)

case class TopicUpdate(override val identity: UUID, override val artifact: UUID)
    extends TopicNotification(identity, artifact)

case class TopicDelete(override val identity: UUID, override val artifact: UUID)
    extends TopicNotification(identity, artifact)

//CATEGORY NOTIFICATIONS
sealed class CategoryNotification(val identity: UUID, val artifact: UUID)

case class CategoryUpvote(override val identity: UUID,
                          override val artifact: UUID)
    extends CategoryNotification(identity, artifact)

case class CategoryDownvote(override val identity: UUID,
                            override val artifact: UUID)
    extends CategoryNotification(identity, artifact)

case class CategoryCreate(override val identity: UUID,
                          override val artifact: UUID)
    extends CategoryNotification(identity, artifact)

case class CategoryUpdate(override val identity: UUID,
                          override val artifact: UUID)
    extends CategoryNotification(identity, artifact)

case class CategoryDelete(override val identity: UUID,
                          override val artifact: UUID)
    extends CategoryNotification(identity, artifact)

//SUBSCRIPTIONS
sealed class SubscribeNotification(val identity: UUID,
                                   val artifact: UUID,
                                   val owner: UUID)

case class Subscribe(override val identity: UUID,
                     override val artifact: UUID,
                     override val owner: UUID)
    extends SubscribeNotification(identity, artifact, owner)

case class Unsubscribe(override val identity: UUID,
                       override val artifact: UUID,
                       override val owner: UUID)
    extends SubscribeNotification(identity, artifact, owner)

case class FirebaseToken(identity: UUID,
                            token: String)
