package org.doctordiabet.routinglayer.utils

import java.util.Optional

object JavaToScalaOptional {

  implicit class OptionalBridge[T](optional: Optional[T]) {

    def asScala: Option[T] = {
      if (optional.isPresent) {
        Some(optional.get())
      } else {
        None
      }
    }
  }

}
