package org.doctordiabet.routinglayer.routing.authorization

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{entity, _}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.identity.{
  IdentityEntity,
  IdentityPrivateEntity
}
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeCreateModel
import org.doctordiabet.domainlayer.exceptions.{
  BusinessExceptionAccessor,
  ExceptionBundle
}
import org.doctordiabet.domainlayer.usecase.identities.{
  IdentityGetCallback,
  IdentityGetUseCase
}
import org.doctordiabet.domainlayer.usecase.verification_codes.{
  VerificationCodeSendCallback,
  VerificationCodeSendUseCase
}
import org.doctordiabet.routinglayer.dto.authentication.VerificationCodeRequestDTO
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.Try

class AuthorizationCodeRequestController(
    private val verificationCodeSend: VerificationCodeSendUseCase,
    private val identityGet: IdentityGetUseCase,
    private val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(): Route = entity(as[String]) { (dto: String) =>
    {
      val promise: Promise[Unit] = Promise[Unit]()

      val parsedDTO: VerificationCodeRequestDTO = mappingContext
        .readValue(dto, classOf[VerificationCodeRequestDTO])

      identityGet.execute(
        IdentityGetUseCase.Type.PHONE,
        parsedDTO.getPhoneNumber,
        true,
        new IdentityGetPromiseBridge(
          promise,
          (entity: IdentityPrivateEntity, promise: Promise[Unit]) => {
            if (Try(
                  entity.getPhoneNumber.contentEquals(parsedDTO.getPhoneNumber))
                  .getOrElse(false)) {
              verificationCodeSend
                .execute(
                  new VerificationCodeCreateModel(entity.getMetadata.getId,
                                                  parsedDTO.phoneNumber),
                  new VerificationCodeSendPromiseBridge(promise))
            } else {
              security.respondForbidden(
                promise,
                BusinessExceptionAccessor.CODE_WRONG_PHONE_FORBIDDEN,
                Some("Phone numbers do not match"))
            }
          }
        )
      )

      onSuccess(promise.future) {
        complete(
          HttpResponse(
            status = StatusCodes.NoContent,
            entity = HttpEntity(ContentTypes.`application/json`, "")))
      }
    }
  }

  private class IdentityGetPromiseBridge(
      private val promise: Promise[Unit],
      private val delegate: (IdentityPrivateEntity, Promise[Unit]) => Unit)
      extends IdentityGetCallback {

    override def onSuccess(entity: IdentityEntity): Unit =
      delegate(entity.asInstanceOf[IdentityPrivateEntity], promise)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

  private class VerificationCodeSendPromiseBridge(
      private val promise: Promise[Unit])
      extends VerificationCodeSendCallback {

    override def onSuccess(): Unit = promise.success(Unit)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
