package org.doctordiabet.routinglayer.routing.comments

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{as, complete, entity, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.comments.{
  CommentUpdateCallback,
  CommentUpdateUseCase
}
import org.doctordiabet.routinglayer.dto.comments.CommentUpdateDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.mapping.comments.CommentsMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.Try

class CommentUpdateController(private val commentUpdate: CommentUpdateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String], commentId: UUID): Route =
    entity(as[String]) { (dto: String) =>
      {

        val promise: Promise[CommentEntity] = Promise[CommentEntity]()

        //User should be:
        //AUTHORIZED or SPECIALIST and and OWNER or CONTRIBUTOR to this artifact
        //or
        //User should be:
        //MODERATOR or ADMINISTRATOR
        //regardless of relationship with this artifact
        security.validate(
          promise,
          bearer.getOrElse(""),
          commentId,
          List(RoleEntity.AUTHORIZED, RoleEntity.SPECIALIST),
          List(RelationshipEntity.CREATOR, RelationshipEntity.CONTRIBUTOR),
          List(RoleEntity.MODERATOR, RoleEntity.ADMINISTRATOR),
          (identityId: UUID,
           _: java.util.List[RoleEntity],
           _: RelationshipEntity,
           promise: Promise[CommentEntity]) => {
            commentUpdate.execute(mappingContext
                                    .readValue(dto, classOf[CommentUpdateDTO])
                                    .toEntity(commentId, identityId),
                                  new CommentUpdatePromiseBridge(promise))
          }
        )

        onSuccess(promise.future) { entity: CommentEntity =>
          val body = EntityDTO(s"#/comments/${commentId.toString}/",
                               ODataTypes.COMMENT,
                               entity.toDTO)
          complete(
            HttpResponse(
              status = StatusCodes.OK,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
    }

  private class CommentUpdatePromiseBridge(
      private val promise: Promise[CommentEntity])
      extends CommentUpdateCallback {

    override def onSuccess(entity: CommentEntity): Unit =
      promise.complete(Try(entity))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
