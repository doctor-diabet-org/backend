package org.doctordiabet.routinglayer.routing.utils

import akka.http.scaladsl.model.HttpRequest
import org.doctordiabet.routinglayer.utils.JavaToScalaOptional._

object HttpMetadataUtils {

  implicit def extractBearer()(implicit request: HttpRequest): Option[String] =
    request
      .getHeader("Authorization")
      .map[String](header => header.value())
      .asScala

  implicit def extractIfModifiedSince()(
      implicit request: HttpRequest): Option[String] =
    request
      .getHeader("If-Modified-Since")
      .map[String](header => header.value())
      .asScala

  implicit def extractIfUnmodifiedSince()(
      implicit request: HttpRequest): Option[String] =
    request
      .getHeader("If-Unmodified-Since")
      .map[String](header => header.value())
      .asScala

  implicit def extractIfMatch()(implicit request: HttpRequest): Option[String] =
    request.getHeader("If-Match").map[String](header => header.value()).asScala

  implicit def extractIfNoneMatch()(
      implicit request: HttpRequest): Option[String] =
    request
      .getHeader("If-None-Match")
      .map[String](header => header.value())
      .asScala

  implicit def extractIfRange()(implicit request: HttpRequest): Option[String] =
    request.getHeader("If-Range").map[String](header => header.value()).asScala
}
