package org.doctordiabet.routinglayer.routing.comments

import java.sql.Timestamp
import java.util
import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{complete, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.comments.{
  CommentsGetCallback,
  CommentsGetUseCase
}
import org.doctordiabet.routinglayer.dto.comments.CommentDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.pagination.CollectionDTO
import org.doctordiabet.routinglayer.mapping.comments.CommentsMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.collection.JavaConverters._
import scala.concurrent.Promise

class CommentsGetController(private val commentsGet: CommentsGetUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(offset: Option[Int],
               limit: Option[Int],
               unmodifiedSince: Timestamp,
               tagIds: Iterable[UUID],
               bearer: Option[String],
               artifactReferralId: Option[UUID] = None): Route = {

    val promise: Promise[(Int, util.List[CommentEntity])] =
      Promise[(Int, util.List[CommentEntity])]

    bearer match {
      case Some(value) =>
        security.validate(
          promise,
          value,
          (identityId: UUID, promise: Promise[(Int, util.List[CommentEntity])]) => {
            artifactReferralId match {
              case Some(referralId) =>
                commentsGet.execute(
                  referralId,
                  offset.getOrElse(0),
                  limit.getOrElse(20),
                  unmodifiedSince,
                  tagIds.toList.asJava,
                  new CommentsGetPromiseBridge(promise)
                )
              case None =>
                commentsGet.execute(
                  offset.getOrElse(0),
                  limit.getOrElse(20),
                  unmodifiedSince,
                  tagIds.toList.asJava,
                  new CommentsGetPromiseBridge(promise)
                )
            }
          }
        )
      case None =>
        artifactReferralId match {
          case Some(referralId) =>
            commentsGet.execute(
              referralId,
              offset.getOrElse(0),
              limit.getOrElse(20),
              unmodifiedSince,
              tagIds.toList.asJava,
              new CommentsGetPromiseBridge(promise)
            )
          case None =>
            commentsGet.execute(
              offset.getOrElse(0),
              limit.getOrElse(20),
              unmodifiedSince,
              tagIds.toList.asJava,
              new CommentsGetPromiseBridge(promise)
            )
        }
    }

    onSuccess(promise.future) { (totalCount: Int, entities: util.List[CommentEntity]) =>
      val collection = CollectionDTO(
        entities.size(),
        totalCount,
        s"#/comments/?offset=${offset.getOrElse(0)}&limit=${limit.getOrElse(20)}",
        ODataTypes.COLLECTION,
        entities.asScala
          .map((entity: CommentEntity) => entity.toDTO)
          .map((dto: CommentDTO) =>
            EntityDTO[CommentDTO](odataType = ODataTypes.COMMENT, value = dto))
          .asJava
      )

      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity = HttpEntity(
                       ContentTypes.`application/json`,
                       new ObjectMapper().writeValueAsString(collection))))
    }
  }

  private class CommentsGetPromiseBridge(
      val promise: Promise[(Int, util.List[CommentEntity])])
      extends CommentsGetCallback {

    override def onSuccess(totalCount: Int, entities: util.List[CommentEntity]): Unit =
      promise.success((totalCount, entities))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
