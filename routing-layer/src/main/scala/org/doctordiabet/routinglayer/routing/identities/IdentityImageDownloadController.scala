package org.doctordiabet.routinglayer.routing.identities

import java.io.{File, FilenameFilter}
import java.nio.file.{Files, Path, Paths}

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import akka.stream.scaladsl.FileIO
import net.coobird.thumbnailator.Thumbnails
import org.doctordiabet.routinglayer.security.SecurityProvider

object ImageDownloadController {

  def parseSize(size: String): ImageSize = size match {
    case ImageLarge.key  => ImageLarge
    case ImageMedium.key => ImageMedium
    case ImageSmall.key  => ImageSmall
    case _               => ImageOrigin
  }
}

class ImageDownloadController()(
    private implicit val security: SecurityProvider,
    private implicit val materializer: Materializer) {

  private val dir = new File("images")

  def dispatch(filename: String, size: ImageSize = ImageLarge): Route = {
    val file: Path = dir
      .listFiles(new FilenameFilter {
        override def accept(dir: File, name: String): Boolean =
          name.startsWith(s"$filename-${size.key}")
      })
      .filter((file: File) => file.exists())
      .take(1)
      .headOption
      .map((file: File) => Paths.get(file.toURI))
      .getOrElse(Paths.get(s"images/default-${size.key}.png"))

    Some(file.toString)
      .map {
        case f if f.endsWith("png")  => (MediaTypes.`image/png`, file)
        case f if f.endsWith("jpeg") => (MediaTypes.`image/jpeg`, file)
      } match {
      case Some(data) =>
        complete(
          HttpResponse(StatusCodes.OK,
                       entity = HttpEntity(data._1, FileIO.fromPath(data._2)))
            .withHeaders(
              RawHeader("Cache-Control", s"public, max-age=31536000")
            )
        )
      case _ => complete(HttpResponse(StatusCodes.NotFound))
    }
  }
}

sealed trait ImageSize {
  val key: String
}

case object ImageOrigin extends ImageSize {
  override val key: String = "origin"
}

case object ImageLarge extends ImageSize {
  override val key: String = "large"
}

case object ImageMedium extends ImageSize {
  override val key: String = "medium"
}

case object ImageSmall extends ImageSize {
  override val key: String = "small"
}
