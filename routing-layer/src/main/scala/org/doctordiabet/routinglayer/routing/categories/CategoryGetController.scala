package org.doctordiabet.routinglayer.routing.categories

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.categories.CategoryGetUseCase
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.mapping.categories.CategoriesMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.categories.CategoryGetCallback

import scala.concurrent.Promise

private class CategoryGetController(private val categoryGet: CategoryGetUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(id: UUID, bearer: Option[String]): Route = {

    val promise: Promise[CategoryEntity] = Promise[CategoryEntity]()

    bearer match {
      case Some(value) =>
        security.validate(
          promise,
          value,
          (identityId: UUID, promise: Promise[CategoryEntity]) => {
            categoryGet.execute(id, new CategoryGetPromiseBridge(promise))
          })
      case None => {
        categoryGet.execute(id, new CategoryGetPromiseBridge(promise))
      }
    }

    onSuccess(promise.future) { entity: CategoryEntity =>
      val body = EntityDTO(s"#/categories/${id.toString}/",
                           ODataTypes.CATEGORY,
                           entity.toDTO)
      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity =
                       HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
    }
  }

  private class CategoryGetPromiseBridge(
      private val promise: Promise[CategoryEntity])
      extends CategoryGetCallback {

    override def onSuccess(entity: CategoryEntity): Unit =
      promise.success(entity)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
