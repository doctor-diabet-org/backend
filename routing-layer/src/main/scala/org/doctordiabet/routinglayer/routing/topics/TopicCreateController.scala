package org.doctordiabet.routinglayer.routing.topics

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{as, complete, entity, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.topics.{
  TopicCreateCallback,
  TopicCreateUseCase
}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.topics.TopicCreateDTO
import org.doctordiabet.routinglayer.mapping.topics.TopicsMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.Try

class TopicCreateController(private val topicCreate: TopicCreateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String], categoryId: UUID): Route =
    entity(as[String]) { (dto: String) =>
      {

        val promise: Promise[TopicEntity] = Promise[TopicEntity]()

        security.validate(
          promise,
          bearer.getOrElse(""),
          (identityId: UUID,
           roles: List[RoleEntity],
           promise: Promise[TopicEntity]) => {
            if (security.validateRole(
                  promise,
                  roles,
                  "Forbidden. Session owner have not enough privileges to perform this action",
                  RoleEntity.AUTHORIZED,
                  RoleEntity.SPECIALIST,
                  RoleEntity.MODERATOR,
                  RoleEntity.ADMINISTRATOR
                )) {
              topicCreate.execute(mappingContext
                                    .readValue(dto, classOf[TopicCreateDTO])
                                    .toEntity(categoryId, identityId),
                                  new TopicCreatePromiseBridge(promise))
            }
          }
        )

        onSuccess(promise.future) { entity: TopicEntity =>
          val body = EntityDTO(s"#/categories/${categoryId.toString}/topics/",
                               ODataTypes.TOPIC,
                               entity.toDTO)
          complete(
            HttpResponse(
              status = StatusCodes.Created,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
    }

  private class TopicCreatePromiseBridge(
      private val promise: Promise[TopicEntity])
      extends TopicCreateCallback {

    override def onSuccess(entity: TopicEntity): Unit =
      promise.complete(Try(entity))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
