package org.doctordiabet.routinglayer.routing.identities

import java.sql.Timestamp
import java.util.UUID

import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.FileInfo
import akka.stream.Materializer
import akka.stream.scaladsl.Source
import akka.util.ByteString
import org.doctordiabet.di.components.identities.IdentitiesInterfaceAdapterComponent
import org.doctordiabet.routinglayer.routing.utils.HttpMetadataUtils._
import org.doctordiabet.routinglayer.security.SecurityProvider

class IdentitiesDirective()(
    private implicit val dependencyVertex: IdentitiesInterfaceAdapterComponent,
    private implicit val security: SecurityProvider,
    private implicit val materializer: Materializer) {

  private val identitiesFetchController = new IdentitiesFetchController(
    dependencyVertex.getIdentitiesFetch)
  private val identitiesGetController = new IdentitiesGetController(
    dependencyVertex.getIdentitiesGet)
  private val identityCreateController = new IdentityCreateController(
    dependencyVertex.getIdentityCreate)
  private val identityDeleteController = new IdentityDeleteController(
    dependencyVertex.getIdentityDelete)
  private val identityGetController = new IdentityGetController(
    dependencyVertex.getIdentityGet)
  private val identityUpdateController = new IdentityUpdateController(
    dependencyVertex.getIdentityUpdate)
  private val imageUploadController = new IdentityImageUploadController()
  private val imageDownloadController = new ImageDownloadController()

  def identitiesRouting(): Route = {
    ignoreTrailingSlash {
      extractRequest { implicit request: HttpRequest =>
        pathPrefix("identities") {
          pathPrefix(JavaUUID) { id: UUID =>
            (pathPrefix("image") & get & parameters('size.?)) {
              (size: Option[String]) =>
                imageDownloadController.dispatch(
                  id.toString,
                  ImageDownloadController.parseSize(size.getOrElse("")))
            } ~
              get {
                identityGetController.dispatch(id, extractBearer)
              } ~
              patch {
                identityUpdateController.dispatch(id, extractBearer)
              } ~
              put {
                identityUpdateController.dispatch(id, extractBearer)
              } ~
              delete {
                identityDeleteController.dispatch(id, extractBearer)
              }
          } ~
            get {
              //TODO("Implement search")
              /*parameters('offset.as[Int].?, 'limit.as[Int].?, '$search.as[String]) { (offset: Option[Int], limit: Option[Int], search: String) =>

                            } ~*/
              parameters('offset.as[Int].?, 'limit.as[Int].?) {
                (offset: Option[Int], limit: Option[Int]) =>
                  extractIfModifiedSince()
                    .map(Timestamp.valueOf)
                    .map((modifiedSince: Timestamp) =>
                      identitiesFetchController
                        .dispatch(offset, limit, modifiedSince, extractBearer))
                    .getOrElse(
                      identitiesGetController.dispatch(
                        offset,
                        limit,
                        extractIfUnmodifiedSince().map(Timestamp.valueOf),
                        extractBearer)
                    )
              }
            } ~
            post {
              (pathPrefix("image") & fileUpload("image")) {
                data: (FileInfo, Source[ByteString, Any]) =>
                  imageUploadController.dispatch(extractBearer.getOrElse(""),
                    data)
              } ~
              identityCreateController.dispatch(extractBearer)
            }
        }
      }
    }
  }
}
