package org.doctordiabet.routinglayer.routing.authorization

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.authentication.{
  AuthenticateCodeModel,
  AuthenticateCredentialsModel
}
import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.authorization.{
  AuthorizationCallback,
  AuthorizeCodeUseCase,
  AuthorizeCredentialsUseCase
}
import org.doctordiabet.routinglayer.dto.authentication.{
  VerificationCodeDTO,
  VerificationCredentialsDTO
}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.mapping.authorization.AuthorizationMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes

import scala.concurrent.Promise

sealed trait AuthorizationType

case object AuthorizationCode extends AuthorizationType

case object AuthorizationCredentials extends AuthorizationType

class AuthorizationTokenRequestController(
    private val authorizeCode: AuthorizeCodeUseCase,
    private val authorizeCredentials: AuthorizeCredentialsUseCase) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(authorizeUsing: AuthorizationType): Route = entity(as[String]) {
    (dto: String) =>
      {
        val promise: Promise[PersistentTokenEntity] =
          Promise[PersistentTokenEntity]()

        authorizeUsing match {
          case AuthorizationCode => {
            val parsedDTO: VerificationCodeDTO =
              mappingContext.readValue(dto, classOf[VerificationCodeDTO])
            authorizeCode.execute(
              new AuthenticateCodeModel(parsedDTO.phone, parsedDTO.code),
              new AuthorizationPromiseBridge(promise)
            )
          }
          case AuthorizationCredentials => {
            val parsedDTO: VerificationCredentialsDTO =
              mappingContext.readValue(dto, classOf[VerificationCredentialsDTO])
            authorizeCredentials.execute(
              new AuthenticateCredentialsModel(parsedDTO.email,
                                               parsedDTO.password),
              new AuthorizationPromiseBridge(promise)
            )
          }
        }

        onSuccess(promise.future) { entity: PersistentTokenEntity =>
          val body = EntityDTO(
            "#/authorize/code/",
            ODataTypes.REFRESH_TOKEN,
            entity.toDTO
          )

          complete(
            HttpResponse(status = StatusCodes.OK,
                         entity =
                           HttpEntity(ContentTypes.`application/json`,
                                      mappingContext.writeValueAsString(body)))
              .addHeader(
                RawHeader("Cache-Control", s"private, max-age=31536000")
              )
          )
        }
      }
  }

  private class AuthorizationPromiseBridge(
      private val promise: Promise[PersistentTokenEntity])
      extends AuthorizationCallback {

    override def onSuccess(entity: PersistentTokenEntity): Unit =
      promise.success(entity)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
