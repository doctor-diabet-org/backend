package org.doctordiabet.routinglayer.routing.categories

import java.sql.Timestamp
import java.util
import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.contract.order.ArtifactFilter
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.categories.CategoriesGetUseCase
import org.doctordiabet.routinglayer.dto.categories.CategoryDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.pagination.CollectionDTO
import org.doctordiabet.routinglayer.mapping.categories.CategoriesMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.categories.CategoriesGetCallback

import scala.collection.JavaConverters._
import scala.concurrent.Promise

private class CategoriesGetController(
    private val categoriesGet: CategoriesGetUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(offset: Option[Int],
               limit: Option[Int],
               unmodifiedSince: Option[Timestamp],
               tagIds: Iterable[UUID],
               bearer: Option[String]): Route = {

    val promise: Promise[(Int, util.List[CategoryEntity])] =
      Promise[(Int, util.List[CategoryEntity])]

    bearer match {
      case Some(value) =>
        security.validate(
          promise,
          value,
          (_: UUID, promise: Promise[(Int, util.List[CategoryEntity])]) => {
            unmodifiedSince
              .map(
                (unmodified: Timestamp) =>
                  categoriesGet.execute(
                    offset.getOrElse(0),
                    limit.getOrElse(20),
                    unmodified,
                    tagIds.toList.asJava,
                    new util.ArrayList[ArtifactFilter](),
                    new CategoriesGetPromiseBridge(promise)))
              .getOrElse(
                categoriesGet.execute(offset.getOrElse(0),
                                      limit.getOrElse(20),
                                      tagIds.toList.asJava,
                                      new util.ArrayList[ArtifactFilter](),
                                      new CategoriesGetPromiseBridge(promise)))
          }
        )
      case None => {
        unmodifiedSince
          .map(
            (unmodified: Timestamp) =>
              categoriesGet.execute(offset.getOrElse(0),
                                    limit.getOrElse(20),
                                    unmodified,
                                    tagIds.toList.asJava,
                                    new util.ArrayList[ArtifactFilter](),
                                    new CategoriesGetPromiseBridge(promise)))
          .getOrElse(
            categoriesGet.execute(offset.getOrElse(0),
                                  limit.getOrElse(20),
                                  tagIds.toList.asJava,
                                  new util.ArrayList[ArtifactFilter](),
                                  new CategoriesGetPromiseBridge(promise)))
      }
    }

    onSuccess(promise.future) {
      (totalCount: Int, entities: util.List[CategoryEntity]) =>
        val tagContext = new StringBuilder()
        var counter: Int = 0
        for (tagId <- tagIds) {
          tagContext.append(s"tag=$tagId")
          counter += 1
          if (counter < tagIds.size) {
            tagContext.append("&")
          }
        }

        val collection = CollectionDTO(
          entities.size(),
          totalCount,
          s"#/categories/?offset=${offset.getOrElse(0)}&limit=${limit.getOrElse(
            20)}${if (tagContext.nonEmpty) "&" + tagContext.toString() else ""}",
          ODataTypes.COLLECTION,
          entities.asScala
            .map((entity: CategoryEntity) => entity.toDTO)
            .map((dto: CategoryDTO) =>
              EntityDTO[CategoryDTO](odataType = ODataTypes.CATEGORY,
                                     value = dto))
            .asJava
        )
        complete(
          HttpResponse(status = StatusCodes.OK,
                       entity = HttpEntity(
                         ContentTypes.`application/json`,
                         new ObjectMapper().writeValueAsString(collection))))
    }
  }

  private class CategoriesGetPromiseBridge(
      val promise: Promise[(Int, util.List[CategoryEntity])])
      extends CategoriesGetCallback {

    override def onSuccess(totalCount: Int,
                           entities: util.List[CategoryEntity]): Unit =
      promise.success((totalCount, entities))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
