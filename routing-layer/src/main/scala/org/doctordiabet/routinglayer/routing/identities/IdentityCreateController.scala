package org.doctordiabet.routinglayer.routing.identities

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.identities.{IdentityCreateCallback, IdentityCreateUseCase}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.identities.{IdentityCreateDTO, IdentityDTO}
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise

class IdentityCreateController(
    private val identityCreate: IdentityCreateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String]): Route = entity(as[String]) {
    (dto: String) =>
      {

        val promise: Promise[IdentityEntity] = Promise[IdentityEntity]

        identityCreate.execute(mappingContext
                                 .readValue[IdentityCreateDTO](dto)
                                 .toEntity(),
                               new IdentityCreatePromiseBridge(promise))

        onSuccess(promise.future) { entity: IdentityEntity =>
          val dto: IdentityDTO = entity.toDTO
          val body = EntityDTO(s"#/identities/", "", dto)
          complete(
            HttpResponse(
              status = StatusCodes.Created,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
  }

  private class IdentityCreatePromiseBridge(
      private val promise: Promise[IdentityEntity])
      extends IdentityCreateCallback {

    override def onSuccess(entity: IdentityEntity): Unit =
      promise.success(entity)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
