package org.doctordiabet.routinglayer.routing.categories

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.categories.{
  CategoryDeleteModel,
  CategoryEntity
}
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.categories.CategoryCreateUseCase
import org.doctordiabet.routinglayer.dto.categories.CategoryCreateDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.mapping.categories.CategoriesMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.categories.CategoryCreateCallback

import scala.concurrent.Promise
import scala.util.Try

private class CategoryCreateController(
    private val categoryCreate: CategoryCreateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String]): Route = entity(as[String]) {
    (dto: String) =>
      {

        val promise: Promise[CategoryEntity] = Promise[CategoryEntity]()

        security.validate(
          promise,
          bearer.getOrElse(""),
          (identityId: UUID,
           roles: List[RoleEntity],
           promise: Promise[CategoryEntity]) => {
            if (security.validateRole(
                  promise,
                  roles,
                  "Forbidden. Session owner have not enough privileges to perform this action.",
                  RoleEntity.MODERATOR,
                  RoleEntity.ADMINISTRATOR)) {
              categoryCreate.execute(
                mappingContext
                  .readValue(dto, classOf[CategoryCreateDTO])
                  .toEntity(identityId),
                new CategoryCreatePromiseBridge(promise))
            }
          }
        )

        onSuccess(promise.future) { entity: CategoryEntity =>
          val body =
            EntityDTO(s"#/categories/", ODataTypes.CATEGORY, entity.toDTO)
          complete(
            HttpResponse(
              status = StatusCodes.Created,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
  }

  private class CategoryCreatePromiseBridge(
      private val promise: Promise[CategoryEntity])
      extends CategoryCreateCallback {

    override def onSuccess(entity: CategoryEntity): Unit =
      promise.complete(Try(entity))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
