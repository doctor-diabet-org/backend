package org.doctordiabet.routinglayer.routing.artifacts

import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.usecase.artifacts.{SubscribeUseCase, UnsubscribeUseCase}
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.{Failure, Success, Try}

class ArtifactsSubscriptionController(private val subscribe: SubscribeUseCase,
                                      private val unsubscribe: UnsubscribeUseCase)
                                     (private implicit val security: SecurityProvider) {
    
    def dispatch(bearer: Option[String],
                 artifactId: UUID,
                 actionType: SubscriptionActionType): Route = {
        val promise: Promise[Boolean] = Promise[Boolean]()
    
        security.validate(
            promise,
            bearer.getOrElse(""),
            artifactId,
            List(RoleEntity.AUTHORIZED, RoleEntity.SPECIALIST),
            List(RelationshipEntity.CREATOR, RelationshipEntity.CONTRIBUTOR, RelationshipEntity.NONE),
            List(RoleEntity.MODERATOR, RoleEntity.ADMINISTRATOR),
            (identityId: UUID,
             _: java.util.List[RoleEntity],
             _: RelationshipEntity,
             promise: Promise[Boolean]) => {
                actionType match {
                    case Subscribe =>
                        Try(subscribe.executeBlocking(identityId, artifactId)) match {
                            case Success(performed)    => promise.success(performed)
                            case Failure(t: Throwable) => promise.failure(t)
                        }
                    case Unsubscribe =>
                        Try(unsubscribe.executeBlocking(identityId, artifactId)) match {
                            case Success(performed)    => promise.success(performed)
                            case Failure(t: Throwable) => promise.failure(t)
                        }
                }
            }
        )
    
        onSuccess(promise.future) { performed: Boolean =>
            complete(
                HttpResponse(
                    status = if(performed) StatusCodes.NoContent else StatusCodes.NotModified,
                    entity = HttpEntity(ContentTypes.`application/json`, "")))
        }
    }
}

sealed class SubscriptionActionType

case object Subscribe extends SubscriptionActionType

case object Unsubscribe extends SubscriptionActionType