package org.doctordiabet.routinglayer.routing.identities

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.identities.IdentityDeleteUseCase
import org.doctordiabet.routinglayer.dto.identities.IdentityDeleteDTO
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.identities.IdentityDeleteCallback

import scala.concurrent.Promise

class IdentityDeleteController(
    private val identityDelete: IdentityDeleteUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(id: UUID, bearer: Option[String]): Route =
    parameters('identity.as[String]) { identity: String =>
      val promise: Promise[UUID] = Promise[UUID]

      security.validate(
        promise,
        bearer.getOrElse(""),
        (identityId: UUID, roles: List[RoleEntity], promise: Promise[UUID]) => {
          if (security.validateRole(
                promise,
                roles,
                "Only moderator or administrator is allowed to delete identities",
                RoleEntity.MODERATOR,
                RoleEntity.ADMINISTRATOR)) {
            identityDelete.execute(IdentityDeleteDTO(id).toEntity(identityId),
                                   new IdentityDeletePromiseBridge(promise))
          }
        }
      )

      onSuccess(promise.future) { _: UUID =>
        complete(
          HttpResponse(
            status = StatusCodes.NoContent,
            entity = HttpEntity(ContentTypes.`application/json`, "")))
      }
    }

  private class IdentityDeletePromiseBridge(private val promise: Promise[UUID])
      extends IdentityDeleteCallback {

    override def onSuccess(id: UUID): Unit = promise.success(id)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
