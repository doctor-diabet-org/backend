package org.doctordiabet.routinglayer.routing.categories

import java.io.File
import java.util.concurrent.Executors
import javax.imageio.ImageIO

import java.io._
import java.nio.ByteBuffer
import java.util.UUID
import java.util.concurrent.Executors
import javax.imageio.ImageIO

import akka.Done
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.FileInfo
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import net.coobird.thumbnailator.Thumbnails
import org.doctordiabet.domainlayer.exceptions.{ExceptionBundle, ExceptionSource, InternalExceptionAccessor}
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.{Await, ExecutionContext, Future, Promise}
import scala.concurrent.duration.Duration
import scala.util.Try

/*
object CategoryImageUploadController {

  private[identities] val imageSmallSize = 64
  private[identities] val imageMediumSize = 128
  private[identities] val imageLargeSize = 256
}

class CategoryImageUploadController()(private implicit val security: SecurityProvider,
                                      private implicit val materializer: Materializer) {

  private implicit val executionContextExecutor: ExecutionContext =
    ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))

  ImageIO.setUseCache(false)

  private val rootDir = new File("categories")
  private val dir = new File("images")

  def dispatch(bearer: Option[String], data: (FileInfo, Source[ByteString, ])): Route = {
    import CategoryImageUploadController._
      
    val promise: Promise[Done] = Promise[Done]()
  
    security.validate[Done](
      promise,
      bearer.getOrElse(""),
      (id: UUID, promise: Promise[Done]) => {
        
        
        ensureDirectory()
        cleanUp(id)
      
        val image: Option[(File, File, File, File)] =
          data._1.contentType.mediaType match {
            case MediaTypes.`image/png` =>
              Some(
                new File(s"images/${id.toString}-origin.png"),
                new File(s"images/${id.toString}-small.png"),
                new File(s"images/${id.toString}-medium.png"),
                new File(s"images/${id.toString}-large.png")
              )
            case MediaTypes.`image/jpeg` =>
              Some(
                new File(s"images/${id.toString}-origin.jpeg"),
                new File(s"images/${id.toString}-small.jpeg"),
                new File(s"images/${id.toString}-medium.jpeg"),
                new File(s"images/${id.toString}-large.jpeg")
              )
            case _ => None
          }
      
        image match {
          case Some(img) =>
            if (!img._1.exists()) {
              img._1.createNewFile()
            }
          
            if (!img._2.exists()) {
              img._2.createNewFile()
            }
          
            if (!img._3.exists()) {
              img._3.createNewFile()
            }
          
            if (!img._4.exists()) {
              img._4.createNewFile()
            }
          
            val channel1 = new RandomAccessFile(img._1, "rw").getChannel
          
            val stream = new FileOutputStream(img._1)
            val writer = new BufferedOutputStream(stream)
          
            val writeResult: Future[Done] = data._2.runWith(Sink.foreach(bs => channel1.write(ByteBuffer.wrap(bs.toArray[Byte]))))
                .andThen {
                  case tryDone: Try[Done] =>
                    channel1.close()
                    tryDone
                }
            /*
              data._2.runWith(Sink.foreach(bs => writer.write(bs.toArray)))*/
            val future: Done = Await.result(writeResult, Duration.Inf)
          
            promise.success(Done)
          
            Thumbnails
                .of(img._1)
                .size(imageLargeSize, imageLargeSize)
                .toFile(img._4)
          
            Thumbnails
                .of(img._4)
                .size(imageMediumSize, imageMediumSize)
                .toFile(img._3)
          
            Thumbnails
                .of(img._3)
                .size(imageSmallSize, imageSmallSize)
                .toFile(img._2)
          case _ =>
            val bundle = new ExceptionBundle(ExceptionSource.INTERNAL)
            val accessor: InternalExceptionAccessor =
              bundle.getAccessor.asInstanceOf[InternalExceptionAccessor]
            accessor.setMessage(
              "Unsupported mime type. Please, try one of these: image/png, image/jpeg")
            promise.failure(bundle)
        }
      }
    )
  
    onSuccess(promise.future) { _: Done =>
      complete(
        HttpResponse(StatusCodes.NoContent,
          entity = HttpEntity(ContentTypes.`application/json`, "")))
    }
  }
  
  private def ensureDirectory(): Unit = if (!dir.exists()) dir.mkdir()
  
  private def cleanUp(id: UUID): Unit =
    dir
        .listFiles(new FilenameFilter {
          override def accept(dir: File, name: String): Boolean =
            name.startsWith(id.toString)
        })
        .foreach((old: File) => old.delete())
}
*/
