package org.doctordiabet.routinglayer.routing.artifacts

import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.{TagCreateModel, TagEntity}
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.exceptions.{
  BusinessExceptionAccessor,
  DatabaseExceptionAccessor,
  ExceptionBundle
}
import org.doctordiabet.domainlayer.usecase.artifacts.{
  TagsCreateUseCase,
  TagsReadUseCase
}
import org.doctordiabet.routinglayer.dto.artifacts.TagsCreateDTO
import org.doctordiabet.routinglayer.dto.categories.CategoryDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.metadata.TagDTO
import org.doctordiabet.routinglayer.dto.pagination.CollectionDTO
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

class TagsController(private val tagsCreate: TagsCreateUseCase,
                     private val tagsRead: TagsReadUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatchCreate(bearer: Option[String]): Route = {
    val promise: Promise[java.util.List[TagEntity]] =
      Promise[java.util.List[TagEntity]]()

    entity(as[String]) { json: String =>
      val dto: TagsCreateDTO = mappingContext.readValue[TagsCreateDTO](json)

      security.validate(
        promise,
        bearer.getOrElse(""),
        (_: UUID,
         roles: List[RoleEntity],
         promise: Promise[java.util.List[TagEntity]]) => {
          if (roles.contains(RoleEntity.AUTHORIZED) || roles
                .contains(RoleEntity.SPECIALIST) || roles.contains(
                RoleEntity.MODERATOR) || roles.contains(
                RoleEntity.ADMINISTRATOR)) {
            Try(
              tagsCreate.executeBlocking(
                dto.tagIds.asScala
                  .map((value: String) => new TagCreateModel(value))
                  .asJava)) match {
              case Success(entities) =>
                promise.success(entities)
              case Failure(t) =>
                promise.failure(t)
            }
          } else {
            security.respondForbidden(
              promise,
              BusinessExceptionAccessor.CODE_ACTION_FORBIDDEN)
          }
        }
      )

      onSuccess(promise.future) { entities: java.util.List[TagEntity] =>
        val collection = CollectionDTO(
          entities.size(),
          entities.size(),
          s"#/tags/",
          ODataTypes.COLLECTION,
          entities.asScala
            .map((entity: TagEntity) => TagDTO(entity.getId, entity.getValue))
            .map((tagDto: TagDTO) =>
              EntityDTO[TagDTO](odataType = ODataTypes.TAG, value = tagDto))
            .asJava
        )

        complete(
          HttpResponse(status = StatusCodes.OK,
                       entity = HttpEntity(
                         ContentTypes.`application/json`,
                         new ObjectMapper().writeValueAsString(collection))))
      }
    }
  }

  def dispatchRead(bearer: Option[String]): Route = {
    val promise: Promise[java.util.List[TagEntity]] =
      Promise[java.util.List[TagEntity]]()

    bearer match {
      case Some(value) => {
        security.validate(
          promise,
          value,
          (_: UUID, promise: Promise[java.util.List[TagEntity]]) => {
            Try(tagsRead.executeBlocking()) match {
              case Success(entities) => promise.success(entities)
              case Failure(t)        => promise.failure(t)
            }
          }
        )
      }
      case None => {
        Try(tagsRead.executeBlocking()) match {
          case Success(entities) => promise.success(entities)
          case Failure(t)        => promise.failure(t)
        }
      }
    }

    onSuccess(promise.future) { entities: java.util.List[TagEntity] =>
      val collection = CollectionDTO(
        entities.size(),
        entities.size(),
        s"#/tags/",
        ODataTypes.COLLECTION,
        entities.asScala
          .map((entity: TagEntity) => TagDTO(entity.getId, entity.getValue))
          .map((tagDto: TagDTO) =>
            EntityDTO[TagDTO](odataType = ODataTypes.TAG, value = tagDto))
          .asJava
      )

      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity = HttpEntity(
                       ContentTypes.`application/json`,
                       new ObjectMapper().writeValueAsString(collection))))
    }
  }
}
