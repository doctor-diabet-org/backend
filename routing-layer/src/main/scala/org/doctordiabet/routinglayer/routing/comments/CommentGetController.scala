package org.doctordiabet.routinglayer.routing.comments

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{complete, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.comments.{
  CommentGetCallback,
  CommentGetUseCase
}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.mapping.comments.CommentsMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.Try

class CommentGetController(private val commentGet: CommentGetUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String], commentId: UUID): Route = {

    val promise: Promise[CommentEntity] = Promise[CommentEntity]()

    security.validate(
      promise,
      bearer.getOrElse(""),
      (_: UUID, promise: Promise[CommentEntity]) => {
        commentGet.execute(commentId, new CommentGetPromiseBridge(promise))
      })

    onSuccess(promise.future) { entity: CommentEntity =>
      val body = EntityDTO(s"#/comments/${commentId.toString}/",
                           ODataTypes.COMMENT,
                           entity.toDTO)

      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity =
                       HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
    }
  }

  private class CommentGetPromiseBridge(
      private val promise: Promise[CommentEntity])
      extends CommentGetCallback {

    override def onSuccess(entity: CommentEntity): Unit =
      promise.complete(Try(entity))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
