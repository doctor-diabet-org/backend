package org.doctordiabet.routinglayer.routing.categories

import java.sql.Timestamp
import java.util.UUID

import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.Unmarshaller
import org.doctordiabet.di.components.categories.CategoriesInterfaceAdapterComponent
import org.doctordiabet.routinglayer.routing.utils.HttpMetadataUtils._
import org.doctordiabet.routinglayer.security.SecurityProvider

object CategoriesDirective {

  implicit val uuidUnmarshaller: Unmarshaller[String, UUID] =
    Unmarshaller.strict(UUID.fromString)
}

class CategoriesDirective()(
    private implicit val dependencyVertex: CategoriesInterfaceAdapterComponent,
    private implicit val security: SecurityProvider) {

  import CategoriesDirective.uuidUnmarshaller

  private val categoriesFetchController = new CategoriesFetchController(
    dependencyVertex.getCategoriesFetch)
  private val categoriesGetController = new CategoriesGetController(
    dependencyVertex.getCategoriesGet)
  private val categoryCreateController = new CategoryCreateController(
    dependencyVertex.getCategoryCreate)
  private val categoryDeleteController = new CategoryDeleteController(
    dependencyVertex.getCategoryDelete)
  private val categoryGetController = new CategoryGetController(
    dependencyVertex.getCategoryGet)
  private val categoryUpdateController = new CategoryUpdateController(
    dependencyVertex.getCategoryUpdate)

  def categoriesRouting(): Route = {
    ignoreTrailingSlash {
      extractRequest { implicit request: HttpRequest =>
        pathPrefix("categories") {
          path(JavaUUID) { id: UUID =>
            get {
              categoryGetController.dispatch(id, extractBearer)
            } ~
              patch {
                categoryUpdateController.dispatch(id, extractBearer)
              } ~
              put {
                categoryUpdateController.dispatch(id, extractBearer)
              } ~
              delete {
                categoryDeleteController.dispatch(id, extractBearer)
              }
          } ~
            get {
              parameters('offset.as[Int].?, 'limit.as[Int].?, 'tag.as[UUID].*) {
                (offset: Option[Int],
                 limit: Option[Int],
                 tagIds: Iterable[UUID]) =>
                  extractIfModifiedSince
                    .map(Timestamp.valueOf)
                    .map(
                      (modifiedSince: Timestamp) =>
                        categoriesFetchController
                          .dispatch(offset,
                                    limit,
                                    modifiedSince,
                                    tagIds,
                                    extractBearer))
                    .getOrElse(
                      categoriesGetController.dispatch(
                        offset,
                        limit,
                        extractIfUnmodifiedSince.map(Timestamp.valueOf),
                        tagIds,
                        extractBearer))
              }
            } ~
            post {
              categoryCreateController.dispatch(extractBearer)
            }
        }
      }
    }
  }
}
