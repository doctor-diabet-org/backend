package org.doctordiabet.routinglayer.routing.artifacts

import java.util.UUID

import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import org.doctordiabet.di.components.artifacts.ArtifactsInterfaceAdapterComponent
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.routinglayer.routing.utils.HttpMetadataUtils._

class ArtifactsDirective()(
    private implicit val dependencyVertex: ArtifactsInterfaceAdapterComponent,
    private implicit val security: SecurityProvider) {

  private val artifactSubscriptionsController = new ArtifactsSubscriptionController(
    dependencyVertex.getSubscribe,
    dependencyVertex.getUnsubscribe)
  
  private val artifactsTagController = new ArtifactsTagController(
    dependencyVertex.getTagsAttach,
    dependencyVertex.getTagsDetach)

  private val artifactsVoteController = new ArtifactVotesController(
    dependencyVertex.getUpvote,
    dependencyVertex.getDownvote
  )

  private val tagsController = new TagsController(
    dependencyVertex.getTagsCreate,
    dependencyVertex.getTagsRead
  )

  def artifactsRouting(): Route = {
    ignoreTrailingSlash {
      extractRequest { implicit request: HttpRequest =>
        pathPrefix("artifacts" / JavaUUID) { (id: UUID) =>
          (path("subscribe") & post) {
            artifactSubscriptionsController.dispatch(
              extractBearer,
              id,
              Subscribe
            )
          } ~
          (path("unsubscribe") & post) {
            artifactSubscriptionsController.dispatch(
              extractBearer,
              id,
              Unsubscribe
            )
          } ~
          (path("tags" / "attach") & post) {
            artifactsTagController.dispatch(
              extractBearer,
              id,
              TagAttachAction
            )
          } ~
            (path("tags" / "detach") & post) {
              artifactsTagController.dispatch(
                extractBearer,
                id,
                TagDetachAction
              )
            } ~
            (path("upvote") & post) {
              artifactsVoteController.dispatch(extractBearer, id, Upvote)
            } ~
            (path("downvote") & post) {
              artifactsVoteController.dispatch(extractBearer, id, Downvote)
            }
        } ~
          pathPrefix("tags") {
            post {
              tagsController.dispatchCreate(extractBearer)
            } ~
              get {
                tagsController.dispatchRead(extractBearer)
              }
          }
      }
    }
  }
}
