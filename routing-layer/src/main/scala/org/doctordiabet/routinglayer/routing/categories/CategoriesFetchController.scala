package org.doctordiabet.routinglayer.routing.categories

import java.sql.Timestamp
import java.util
import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{complete, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.contract.order.ArtifactFilter
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.categories.CategoriesFetchUseCase
import org.doctordiabet.routinglayer.dto.categories.CategoryDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.pagination.CollectionDTO
import org.doctordiabet.routinglayer.mapping.categories.CategoriesMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.categories.CategoriesFetchCallback

import scala.collection.JavaConverters._
import scala.concurrent.Promise

class CategoriesFetchController(
    private val categoriesFetch: CategoriesFetchUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(offset: Option[Int],
               limit: Option[Int],
               modifiedSince: Timestamp,
               tagIds: Iterable[UUID],
               bearer: Option[String]): Route = {

    val promise: Promise[Tuple2[Int, util.List[CategoryEntity]]] =
      Promise[(Int, util.List[CategoryEntity])]

    bearer match {
      case Some(value) =>
        security.validate(
          promise,
          value,
          (identityId: UUID,
           promise: Promise[Tuple2[Int, util.List[CategoryEntity]]]) => {
            categoriesFetch.execute(
              offset.getOrElse(0),
              limit.getOrElse(20),
              modifiedSince,
              tagIds.toList.asJava,
              new util.ArrayList[ArtifactFilter](),
              new CategoriesFetchPromiseBridge(promise)
            )
          }
        )
      case None => {
        categoriesFetch.execute(
          offset.getOrElse(0),
          limit.getOrElse(20),
          modifiedSince,
          tagIds.toList.asJava,
          new util.ArrayList[ArtifactFilter](),
          new CategoriesFetchPromiseBridge(promise)
        )
      }
    }

    onSuccess(promise.future) {
      (totalCount: Int, entities: util.List[CategoryEntity]) =>
        val tagContext = new StringBuilder()
        var counter: Int = 0
        for (tagId <- tagIds) {
          tagContext.append(s"tag=$tagId")
          counter += 1
          if (counter < tagIds.size) {
            tagContext.append("&")
          }
        }

        val collection = CollectionDTO(
          entities.size(),
          totalCount,
          s"#/categories/?offset=${offset.getOrElse(0)}&limit=${limit.getOrElse(
            20)}&last_modified_time=${modifiedSince.toString}${if (tagContext.nonEmpty) "&" + tagContext.toString()
          else ""}",
          ODataTypes.COLLECTION,
          entities.asScala
            .map((entity: CategoryEntity) => entity.toDTO)
            .map((dto: CategoryDTO) =>
              EntityDTO[CategoryDTO](odataType = ODataTypes.CATEGORY,
                                     value = dto))
            .asJava
        )

        complete(
          HttpResponse(status = StatusCodes.OK,
                       entity = HttpEntity(
                         ContentTypes.`application/json`,
                         new ObjectMapper().writeValueAsString(collection))))
    }
  }

  private class CategoriesFetchPromiseBridge(
      val promise: Promise[(Int, util.List[CategoryEntity])])
      extends CategoriesFetchCallback {

    override def onSuccess(totalCount: Int,
                           entities: util.List[CategoryEntity]): Unit =
      promise.success((totalCount, entities))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
