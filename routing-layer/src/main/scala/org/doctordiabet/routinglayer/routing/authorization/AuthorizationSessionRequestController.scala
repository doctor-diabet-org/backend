package org.doctordiabet.routinglayer.routing.authorization

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.session.SessionTokenEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.session.{
  SessionTokenCreateCallback,
  SessionTokenCreateUseCase
}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.session.SessionRequestDTO
import org.doctordiabet.routinglayer.mapping.authorization.AuthorizationMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes

import scala.concurrent.Promise

class AuthorizationSessionRequestController(
    private val sessionCreate: SessionTokenCreateUseCase) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(): Route = entity(as[String]) { (dto: String) =>
    {

      val promise: Promise[SessionTokenEntity] = Promise[SessionTokenEntity]

      sessionCreate.execute(
        mappingContext.readValue(dto, classOf[SessionRequestDTO]).toEntity,
        new SessionTokenCreatePromiseBridge(promise))

      onSuccess(promise.future) { entity: SessionTokenEntity =>
        val body = EntityDTO(
          "#/authorize/token/",
          ODataTypes.SESSION_TOKEN,
          entity.toDTO
        )

        complete(
          HttpResponse(status = StatusCodes.Created,
                       entity =
                         HttpEntity(ContentTypes.`application/json`,
                                    mappingContext.writeValueAsString(body)))
            .withHeaders(
              RawHeader("Cache-Control",
                        s"private, max-age=${entity.getExpiration}")
            )
        )
      }
    }
  }

  private class SessionTokenCreatePromiseBridge(
      private val promise: Promise[SessionTokenEntity])
      extends SessionTokenCreateCallback {

    override def onSuccess(entity: SessionTokenEntity): Unit =
      promise.success(entity)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
