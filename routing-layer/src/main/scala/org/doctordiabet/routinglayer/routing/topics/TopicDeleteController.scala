package org.doctordiabet.routinglayer.routing.topics

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{complete, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.entity.topics.TopicDeleteModel
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.topics.{
  TopicDeleteCallback,
  TopicDeleteUseCase
}
import org.doctordiabet.routinglayer.dto.categories.CategoryUpdateDTO
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.Try

class TopicDeleteController(private val topicDelete: TopicDeleteUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String], topicId: UUID): Route = {
    val promise: Promise[UUID] = Promise[UUID]()

    //User should be:
    //AUTHORIZED or SPECIALIST and and OWNER or CONTRIBUTOR to this artifact
    //or
    //User should be:
    //MODERATOR or ADMINISTRATOR
    //regardless of relationship with this artifact
    security.validate(
      promise,
      bearer.getOrElse(""),
      topicId,
      List(RoleEntity.AUTHORIZED, RoleEntity.SPECIALIST),
      List(RelationshipEntity.CREATOR, RelationshipEntity.CONTRIBUTOR),
      List(RoleEntity.MODERATOR, RoleEntity.ADMINISTRATOR),
      (identityId: UUID,
       _: java.util.List[RoleEntity],
       _: RelationshipEntity,
       promise: Promise[UUID]) => {
        topicDelete.execute(new TopicDeleteModel(topicId, identityId),
                            new TopicDeletePromiseBridge(promise))
      }
    )

    onSuccess(promise.future) { id: UUID =>
      complete(
        HttpResponse(status = StatusCodes.NoContent,
                     entity = HttpEntity(ContentTypes.`application/json`, "")))
    }
  }

  private class TopicDeletePromiseBridge(private val promise: Promise[UUID])
      extends TopicDeleteCallback {

    override def onSuccess(id: UUID): Unit = promise.complete(Try(id))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
