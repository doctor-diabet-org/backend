package org.doctordiabet.routinglayer.routing.identities

import java.sql.Timestamp
import java.util

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{complete, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.identities.IdentitiesFetchUseCase
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.pagination.CollectionDTO
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.identities.IdentitiesFetchCallback

import scala.collection.JavaConverters._
import scala.concurrent.Promise

class IdentitiesFetchController(
    private val identitiesFetch: IdentitiesFetchUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(offset: Option[Int],
               limit: Option[Int],
               modifiedSince: Timestamp,
               bearer: Option[String]): Route = {

    val promise: Promise[(Int, util.List[IdentityEntity])] =
      Promise[(Int, util.List[IdentityEntity])]()

    bearer match {
      case Some(value) =>
        security.validate(
          promise,
          value,
          (promise: Promise[(Int, util.List[IdentityEntity])]) => {
            identitiesFetch.execute(offset.getOrElse(0),
                                    limit.getOrElse(20),
                                    modifiedSince,
                                    false,
                                    new IdentitiesFetchPromiseBridge(promise))
          }
        )
      case None => {
        identitiesFetch.execute(offset.getOrElse(0),
                                limit.getOrElse(20),
                                modifiedSince,
                                false,
                                new IdentitiesFetchPromiseBridge(promise))
      }
    }

    onSuccess(promise.future) { (totalCount: Int, entities: util.List[IdentityEntity]) =>
      val collection = CollectionDTO(
        entities.size(),
        totalCount,
        s"#/identities/?offset=${offset.getOrElse(0)}&limit=${limit.getOrElse(
          20)}&last_modified_time=${modifiedSince.toString}",
        ODataTypes.COLLECTION,
        entities.asScala
          .map((entity: IdentityEntity) => entity.toDTO)
          .map(dto => EntityDTO(odataType = "", value = dto))
          .asJava
      )

      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity = HttpEntity(
                       ContentTypes.`application/json`,
                       new ObjectMapper().writeValueAsString(collection))))
    }
  }

  private class IdentitiesFetchPromiseBridge(
      val promise: Promise[(Int, util.List[IdentityEntity])])
      extends IdentitiesFetchCallback {

    override def onSuccess(totalCount: Int, entities: util.List[IdentityEntity]): Unit =
      promise.success((totalCount, entities))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
