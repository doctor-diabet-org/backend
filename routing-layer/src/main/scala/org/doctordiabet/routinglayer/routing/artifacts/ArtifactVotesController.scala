package org.doctordiabet.routinglayer.routing.artifacts

import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.usecase.artifacts.{
  DownvoteUseCase,
  UpvoteUseCase
}
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.{Failure, Success, Try}

class ArtifactVotesController(private val upvote: UpvoteUseCase,
                              private val downvote: DownvoteUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String],
               artifactId: UUID,
               voteType: Vote): Route = {
    val promise: Promise[Boolean] = Promise[Boolean]()

    security.validate(
      promise,
      bearer.getOrElse(""),
      artifactId,
      List(RoleEntity.AUTHORIZED,
           RoleEntity.SPECIALIST,
           RoleEntity.MODERATOR,
           RoleEntity.ADMINISTRATOR),
      List(RelationshipEntity.CREATOR,
           RelationshipEntity.CONTRIBUTOR,
           RelationshipEntity.NONE),
      List.empty,
      (identityId: UUID,
       _: java.util.List[RoleEntity],
       _: RelationshipEntity,
       promise: Promise[Boolean]) => {
        voteType match {
          case Upvote =>
            Try(upvote.executeBlocking(identityId, artifactId)) match {
              case Success(isVoteChanged) => promise.success(isVoteChanged)
              case Failure(t)             => promise.failure(t)
            }
          case Downvote =>
            Try(downvote.executeBlocking(identityId, artifactId)) match {
              case Success(isVoteChanged) => promise.success(isVoteChanged)
              case Failure(t)             => promise.failure(t)
            }
        }
      }
    )

    onSuccess(promise.future) { isVoteChanged: Boolean =>
      if (isVoteChanged) {
        complete(
          HttpResponse(
            status = StatusCodes.OK,
            entity = HttpEntity(ContentTypes.`application/json`, "")))
      } else {
        complete(
          HttpResponse(
            status = StatusCodes.NoContent,
            entity = HttpEntity(ContentTypes.`application/json`, "")))
      }
    }
  }
}

sealed trait Vote

case object Upvote extends Vote

case object Downvote extends Vote
