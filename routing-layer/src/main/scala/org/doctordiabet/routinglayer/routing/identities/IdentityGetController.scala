package org.doctordiabet.routinglayer.routing.identities

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.identities.IdentityGetUseCase
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.identities.IdentityDTO
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.identities.IdentityGetCallback

import scala.concurrent.Promise

class IdentityGetController(private val identityGet: IdentityGetUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(id: UUID, bearer: Option[String])(
      implicit security: SecurityProvider): Route = {

    val promise: Promise[IdentityEntity] = Promise[IdentityEntity]

    bearer match {
      case Some(value) =>
        security.validate(
          promise,
          value,
          (identityId: UUID, promise: Promise[IdentityEntity]) => {
            identityGet.execute(id,
                                identityId.equals(id),
                                new IdentityGetPromiseBridge(promise))
          })
      case None => {
        identityGet.execute(id, false, new IdentityGetPromiseBridge(promise))
      }
    }

    onSuccess(promise.future) { entity: IdentityEntity =>
      val dto: IdentityDTO = entity.toDTO
      val body = EntityDTO(s"#/identities/${id.toString}/", dto.odataType, dto)

      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity =
                       HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
    }
  }

  private class IdentityGetPromiseBridge(
      private val promise: Promise[IdentityEntity])
      extends IdentityGetCallback {

    override def onSuccess(entity: IdentityEntity): Unit =
      promise.success(entity)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
