package org.doctordiabet.routinglayer.routing.comments

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.Unmarshaller
import org.doctordiabet.di.components.comments.CommentsInterfaceAdapterComponent
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository.Referral
import org.doctordiabet.routinglayer.routing.utils.HttpMetadataUtils.{extractBearer, extractIfModifiedSince, extractIfUnmodifiedSince}
import org.doctordiabet.routinglayer.security.SecurityProvider

object CommentsDirective {
  implicit val uuidUnmarshaller: Unmarshaller[String, UUID] =
    Unmarshaller.strict(UUID.fromString)
}

class CommentsDirective()(
    private implicit val dependencyVertex: CommentsInterfaceAdapterComponent,
    private implicit val security: SecurityProvider) {

  import CommentsDirective.uuidUnmarshaller

  private val commentsFetchController = new CommentsFetchController(
    dependencyVertex.getCommentsFetch)
  private val commentsGetController = new CommentsGetController(
    dependencyVertex.getCommentsGet)
  private val commentDeleteController = new CommentDeleteController(
    dependencyVertex.getCommentDelete)
  private val commentGetController =
    new CommentGetController(dependencyVertex.getCommentGet)
  private val commentCreateController = new CommentCreateController(
    dependencyVertex.getCommentCreate)
  private val commentUpdateController = new CommentUpdateController(
    dependencyVertex.getCommentUpdate)

  def commentsRouting(): Route = {
    ignoreTrailingSlash {
      extractRequest { implicit request: HttpRequest =>
        // TOPICS' COMMENTS ROUTING
        pathPrefix("topics") {
          pathPrefix(JavaUUID) { topicId: UUID =>
            pathPrefix("comments") {
              get {
                parameters('offset.as[Int].?, 'limit.as[Int].?, 'tag.as[UUID].*) {
                  (offset: Option[Int],
                   limit: Option[Int],
                   tagIds: Iterable[UUID]) =>
                    (extractIfUnmodifiedSince().map(Timestamp.valueOf),
                     extractIfModifiedSince().map(Timestamp.valueOf)) match {
                      case (Some(unmodifiedSince), _) =>
                        commentsGetController.dispatch(offset,
                                                       limit,
                                                       unmodifiedSince,
                                                       tagIds,
                                                       extractBearer,
                                                       Some(topicId))
                      case (_, Some(modifiedSince)) =>
                        commentsFetchController.dispatch(offset,
                                                         limit,
                                                         modifiedSince,
                                                         tagIds,
                                                         extractBearer,
                                                         Some(topicId))
                      case (_, _) =>
                          commentsGetController.dispatch(offset,
                              limit,
                              Timestamp.valueOf(LocalDateTime.now()),
                              tagIds,
                              extractBearer,
                              Some(topicId))
                    }
                }
              } ~
                post {
                  //create entity
                  commentCreateController.dispatch(extractBearer, topicId)
                }
            }
          }
        } ~
          //CATEGORIES' COMMENTS ROUTING
          pathPrefix("categories") {
            pathPrefix(JavaUUID) { categoryId: UUID =>
              pathPrefix("comments") {
                get {
                  parameters('offset.as[Int].?,
                             'limit.as[Int].?,
                             'tag.as[UUID].*) {
                    (offset: Option[Int],
                     limit: Option[Int],
                     tagIds: Iterable[UUID]) =>
                      (extractIfUnmodifiedSince().map(Timestamp.valueOf),
                       extractIfModifiedSince().map(Timestamp.valueOf)) match {
                        case (Some(unmodifiedSince), _) =>
                          commentsGetController.dispatch(offset,
                                                         limit,
                                                         unmodifiedSince,
                                                         tagIds,
                                                         extractBearer,
                                                         Some(categoryId))
                        case (_, Some(modifiedSince)) =>
                          commentsFetchController.dispatch(offset,
                                                           limit,
                                                           modifiedSince,
                                                           tagIds,
                                                           extractBearer,
                                                           Some(categoryId))
                        case (_, _) =>
                            commentsGetController.dispatch(offset,
                                limit,
                                Timestamp.valueOf(LocalDateTime.now()),
                                tagIds,
                                extractBearer,
                                Some(categoryId))
                      }
                  }
                } ~
                  post {
                    //create entity
                    commentCreateController.dispatch(extractBearer, categoryId)
                  }
              }
            }
          } ~
          //IDENTITIES' COMMENTS ROUTING
          pathPrefix("identities") {
            pathPrefix(JavaUUID) { identityId: UUID =>
              pathPrefix("comments") {
                get {
                  parameters('offset.as[Int].?,
                             'limit.as[Int].?,
                             'tag.as[UUID].*) {
                    (offset: Option[Int],
                     limit: Option[Int],
                     tagIds: Iterable[UUID]) =>
                      (extractIfUnmodifiedSince().map(Timestamp.valueOf),
                       extractIfModifiedSince().map(Timestamp.valueOf)) match {
                        case (Some(unmodifiedSince), _) =>
                          commentsGetController.dispatch(offset,
                                                         limit,
                                                         unmodifiedSince,
                                                         tagIds,
                                                         extractBearer,
                                                         Some(identityId))
                        case (_, Some(modifiedSince)) =>
                          commentsFetchController.dispatch(offset,
                                                           limit,
                                                           modifiedSince,
                                                           tagIds,
                                                           extractBearer,
                                                           Some(identityId))
                        case (_, _) =>
                            commentsGetController.dispatch(offset,
                                limit,
                                Timestamp.valueOf(LocalDateTime.now()),
                                tagIds,
                                extractBearer,
                                Some(identityId))
                      }
                  }
                } ~
                  post {
                    //create entity
                    commentCreateController.dispatch(extractBearer, identityId)
                  }
              }
            }
          } ~
          //COMMENTS' COMMENTS ROUTING
          pathPrefix("comments") {
            pathPrefix(JavaUUID) { commentId: UUID =>
              pathPrefix("comments") {
                get {
                  parameters('offset.as[Int].?,
                             'limit.as[Int].?,
                             'tag.as[UUID].*) {
                    (offset: Option[Int],
                     limit: Option[Int],
                     tagIds: Iterable[UUID]) =>
                      (extractIfUnmodifiedSince().map(Timestamp.valueOf),
                       extractIfModifiedSince().map(Timestamp.valueOf)) match {
                        case (Some(unmodifiedSince), _) =>
                          commentsGetController.dispatch(offset,
                                                         limit,
                                                         unmodifiedSince,
                                                         tagIds,
                                                         extractBearer,
                                                         Some(commentId))
                        case (_, Some(modifiedSince)) =>
                          commentsFetchController.dispatch(offset,
                                                           limit,
                                                           modifiedSince,
                                                           tagIds,
                                                           extractBearer,
                                                           Some(commentId))
                        case (_, _) =>
                            commentsGetController.dispatch(offset,
                                limit,
                                Timestamp.valueOf(LocalDateTime.now()),
                                tagIds,
                                extractBearer,
                                Some(commentId))
                      }
                  }
                } ~
                  post {
                    //create entity
                    commentCreateController.dispatch(extractBearer, commentId)
                  }
              }
            }
          } ~
          pathPrefix("comments") {
            get {
              parameters('offset.as[Int].?, 'limit.as[Int].?, 'tag.as[UUID].*) {
                (offset: Option[Int],
                 limit: Option[Int],
                 tagIds: Iterable[UUID]) =>
                  (extractIfUnmodifiedSince().map(Timestamp.valueOf),
                   extractIfModifiedSince().map(Timestamp.valueOf)) match {
                    case (Some(unmodifiedSince), _) =>
                      commentsGetController.dispatch(offset,
                                                     limit,
                                                     unmodifiedSince,
                                                     tagIds,
                                                     extractBearer)
                    case (_, Some(modifiedSince)) =>
                      commentsFetchController.dispatch(offset,
                                                       limit,
                                                       modifiedSince,
                                                       tagIds,
                                                       extractBearer)
                    case (_, _) =>
                        commentsGetController.dispatch(offset,
                            limit,
                            Timestamp.valueOf(LocalDateTime.now()),
                            tagIds,
                            extractBearer)
                  }
              }
            } ~
              pathPrefix(JavaUUID) { (id: UUID) =>
                put {
                  //backup deleted record
                  complete("")
                } ~
                  get {
                    //get single entity
                    commentGetController.dispatch(extractBearer, id)
                  } ~
                  patch {
                    //update entity
                    commentUpdateController.dispatch(extractBearer, id)
                  } ~
                  delete {
                    //delete entity
                    commentDeleteController.dispatch(extractBearer, id)
                  }
              }
          }
      }
    }
  }
}
