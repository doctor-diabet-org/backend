package org.doctordiabet.routinglayer.routing.topics

import java.sql.Timestamp
import java.util
import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{complete, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.topics.{
  TopicsGetCallback,
  TopicsGetUseCase
}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.pagination.CollectionDTO
import org.doctordiabet.routinglayer.dto.topics.TopicDTO
import org.doctordiabet.routinglayer.mapping.topics.TopicsMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.collection.JavaConverters._
import scala.concurrent.Promise

class TopicsGetController(private val topicsGet: TopicsGetUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(offset: Option[Int],
               limit: Option[Int],
               unmodifiedSince: Timestamp,
               tagIds: Iterable[UUID],
               bearer: Option[String],
               categoryId: Option[UUID] = None): Route = {

    val promise: Promise[(Int, util.List[TopicEntity])] =
      Promise[(Int, util.List[TopicEntity])]

    bearer match {
      case Some(value) =>
        security.validate(
          promise,
          value,
          (identityId: UUID, promise: Promise[(Int, util.List[TopicEntity])]) => {
            categoryId match {
              case Some(category) =>
                topicsGet.execute(
                  category,
                  offset.getOrElse(0),
                  limit.getOrElse(20),
                  unmodifiedSince,
                  tagIds.toList.asJava,
                  new TopicsGetPromiseBridge(promise)
                )
              case None =>
                topicsGet.execute(
                  offset.getOrElse(0),
                  limit.getOrElse(20),
                  unmodifiedSince,
                  tagIds.toList.asJava,
                  new TopicsGetPromiseBridge(promise)
                )
            }
          }
        )
      case None => {
        categoryId match {
          case Some(category) =>
            topicsGet.execute(
              category,
              offset.getOrElse(0),
              limit.getOrElse(20),
              unmodifiedSince,
              tagIds.toList.asJava,
              new TopicsGetPromiseBridge(promise)
            )
          case None =>
            topicsGet.execute(
              offset.getOrElse(0),
              limit.getOrElse(20),
              unmodifiedSince,
              tagIds.toList.asJava,
              new TopicsGetPromiseBridge(promise)
            )
        }
      }
    }

    onSuccess(promise.future) { (totalCount: Int, entities: util.List[TopicEntity]) =>
      val collection = CollectionDTO(
        entities.size(),
        totalCount,
        s"#/topics/?offset=${offset.getOrElse(0)}&limit=${limit.getOrElse(20)}",
        ODataTypes.COLLECTION,
        entities.asScala
          .map((entity: TopicEntity) => entity.toDTO)
          .map((dto: TopicDTO) =>
            EntityDTO[TopicDTO](odataType = ODataTypes.TOPIC, value = dto))
          .asJava
      )

      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity = HttpEntity(
                       ContentTypes.`application/json`,
                       new ObjectMapper().writeValueAsString(collection))))
    }
  }

  private class TopicsGetPromiseBridge(
      val promise: Promise[(Int, util.List[TopicEntity])])
      extends TopicsGetCallback {

    override def onSuccess(totalCount: Int, entities: util.List[TopicEntity]): Unit =
      promise.success((totalCount, entities))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
