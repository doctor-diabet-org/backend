package org.doctordiabet.routinglayer.routing.categories

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.categories.{
  CategoryDeleteModel,
  CategoryEntity
}
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.categories.CategoryDeleteUseCase
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.categories.CategoryDeleteCallback
import org.doctordiabet.routinglayer.dto.categories.CategoryUpdateDTO

import scala.concurrent.Promise
import scala.util.Try

private class CategoryDeleteController(
    private val categoryDelete: CategoryDeleteUseCase)(
    private implicit val security: SecurityProvider) {

  def dispatch(id: UUID, bearer: Option[String]): Route = {

    val promise: Promise[UUID] = Promise[UUID]()

    //User should be:
    //AUTHORIZED or SPECIALIST and and OWNER or CONTRIBUTOR to this artifact
    //or
    //User should be:
    //MODERATOR or ADMINISTRATOR
    //regardless of relationship with this artifact
    security.validate(
      promise,
      bearer.getOrElse(""),
      id,
      List(RoleEntity.AUTHORIZED, RoleEntity.SPECIALIST),
      List(RelationshipEntity.CREATOR, RelationshipEntity.CONTRIBUTOR),
      List(RoleEntity.MODERATOR, RoleEntity.ADMINISTRATOR),
      (identityId: UUID,
       _: java.util.List[RoleEntity],
       _: RelationshipEntity,
       promise: Promise[UUID]) => {
        categoryDelete.execute(new CategoryDeleteModel(id, identityId),
                               new CategoryDeletePromiseBridge(promise))
      }
    )

    onSuccess(promise.future) { id: UUID =>
      complete(
        HttpResponse(status = StatusCodes.NoContent,
                     entity = HttpEntity(ContentTypes.`application/json`, "")))
    }
  }

  private class CategoryDeletePromiseBridge(private val promise: Promise[UUID])
      extends CategoryDeleteCallback {

    override def onSuccess(id: UUID): Unit = promise.complete(Try(id))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
