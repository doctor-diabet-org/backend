package org.doctordiabet.routinglayer.routing.identities

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.identities.IdentityUpdateUseCase
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.identities.IdentityUpdateDTO
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.identities.IdentityUpdateCallback

import scala.concurrent.Promise

class IdentityUpdateController(
    private val identityUpdate: IdentityUpdateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(id: UUID, bearer: Option[String]): Route = entity(as[String]) {
    (dto: String) =>
      {

        val promise: Promise[IdentityEntity] = Promise[IdentityEntity]

        //TODO("Implement ownership validation!")
        
        security.validate(
          promise,
          bearer.getOrElse(""),
          (identityId: UUID, promise: Promise[IdentityEntity]) => {
            identityUpdate.execute(mappingContext
                                     .readValue[IdentityUpdateDTO](dto)
                                     .toEntity(identityId),
                                   new IdentityUpdatePromiseBridge(promise))
          }
        )

        onSuccess(promise.future) { entity: IdentityEntity =>
          val dto = entity.toDTO
          val body = EntityDTO(s"#/identities/${id.toString}/", "", dto)
          complete(
            HttpResponse(
              status = StatusCodes.OK,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
  }

  private class IdentityUpdatePromiseBridge(
      private val promise: Promise[IdentityEntity])
      extends IdentityUpdateCallback {

    override def onSuccess(entity: IdentityEntity): Unit =
      promise.success(entity)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
