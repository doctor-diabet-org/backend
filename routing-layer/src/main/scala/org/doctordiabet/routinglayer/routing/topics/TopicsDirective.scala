package org.doctordiabet.routinglayer.routing.topics

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.Unmarshaller
import org.doctordiabet.di.components.topics.TopicsInterfaceAdapterComponent
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.routinglayer.routing.utils.HttpMetadataUtils._

object TopicsDirective {
  implicit val uuidUnmarshaller: Unmarshaller[String, UUID] =
    Unmarshaller.strict(UUID.fromString)
}

class TopicsDirective()(
    private implicit val dependencyVertex: TopicsInterfaceAdapterComponent,
    private implicit val security: SecurityProvider) {

  import TopicsDirective.uuidUnmarshaller

  private val topicsFetchController = new TopicsFetchController(
    dependencyVertex.getTopicsFetch)
  private val topicsGetController = new TopicsGetController(
    dependencyVertex.getTopicsGet)
  private val topicDeleteController = new TopicDeleteController(
    dependencyVertex.getTopicDelete)
  private val topicGetController = new TopicGetController(
    dependencyVertex.getTopicGet)
  private val topicCreateController = new TopicCreateController(
    dependencyVertex.getTopicCreate)
  private val topicUpdateController = new TopicUpdateController(
    dependencyVertex.getTopicUpdate)

  def topicsRouting(): Route = {
    ignoreTrailingSlash {
      extractRequest { implicit request: HttpRequest =>
        pathPrefix("topics") {
          pathPrefix(JavaUUID) { (id: UUID) =>
            put {
              //backup deleted record
              complete("")
            } ~
              get {
                //get single entity
                topicGetController.dispatch(extractBearer, id)
              } ~
              patch {
                //update entity
                topicUpdateController.dispatch(extractBearer, id)
              } ~
              delete {
                //delete entity
                topicDeleteController.dispatch(extractBearer, id)
              }
          } ~
            parameters('offset.as[Int].?, 'limit.as[Int].?, 'tag.as[UUID].*) {
              (offset: Option[Int],
               limit: Option[Int],
               tagIds: Iterable[UUID]) =>
                (extractIfUnmodifiedSince().map(Timestamp.valueOf),
                 extractIfModifiedSince().map(Timestamp.valueOf)) match {
                  case (Some(unmodifiedSince), _) =>
                    topicsGetController.dispatch(offset,
                                                 limit,
                                                 unmodifiedSince,
                                                 tagIds,
                                                 extractBearer)
                  case (_, Some(modifiedSince)) =>
                    topicsFetchController.dispatch(offset,
                                                   limit,
                                                   modifiedSince,
                                                   tagIds,
                                                   extractBearer)
                  case (_, _) =>
                      topicsGetController.dispatch(offset,
                          limit,
                          Timestamp.valueOf(LocalDateTime.now()),
                          tagIds,
                          extractBearer)
                }
            }
        } ~
          pathPrefix("categories") {
            pathPrefix(JavaUUID) { categoryId: UUID =>
              pathPrefix("topics") {
                get {
                  parameters('offset.as[Int].?,
                             'limit.as[Int].?,
                             'tag.as[UUID].*) {
                    (offset: Option[Int],
                     limit: Option[Int],
                     tagIds: Iterable[UUID]) =>
                      (extractIfUnmodifiedSince().map(Timestamp.valueOf),
                       extractIfModifiedSince().map(Timestamp.valueOf)) match {
                        case (Some(unmodifiedSince), _) =>
                          topicsGetController.dispatch(offset,
                                                       limit,
                                                       unmodifiedSince,
                                                       tagIds,
                                                       extractBearer,
                                                       Some(categoryId))
                        case (_, Some(modifiedSince)) =>
                          topicsFetchController.dispatch(offset,
                                                         limit,
                                                         modifiedSince,
                                                         tagIds,
                                                         extractBearer,
                                                         Some(categoryId))
                        case (_, _) =>
                            topicsGetController.dispatch(offset,
                                limit,
                                Timestamp.valueOf(LocalDateTime.now()),
                                tagIds,
                                extractBearer,
                                Some(categoryId))
                      }
                  }
                } ~
                  post {
                    topicCreateController.dispatch(extractBearer, categoryId)
                  }
              }
            }
          }
      }
    }
  }
}
