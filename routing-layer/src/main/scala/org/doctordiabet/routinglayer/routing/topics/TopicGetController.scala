package org.doctordiabet.routinglayer.routing.topics

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{complete, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.topics.{
  TopicGetCallback,
  TopicGetUseCase
}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.mapping.topics.TopicsMappingContext._
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.Try

class TopicGetController(private val topicGet: TopicGetUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String], topicId: UUID): Route = {

    val promise: Promise[TopicEntity] = Promise[TopicEntity]()

    bearer match {
      case Some(value) => security.validate(
        promise,
        value,
        (_: UUID, promise: Promise[TopicEntity]) => {
          topicGet.execute(topicId, new TopicGetPromiseBridge(promise))
        })
      case _ =>
        topicGet.execute(topicId, new TopicGetPromiseBridge(promise))
    }

    onSuccess(promise.future) { entity: TopicEntity =>
      val body = EntityDTO(s"#/topics/${topicId.toString}/",
                           ODataTypes.TOPIC,
                           entity.toDTO)
      complete(
        HttpResponse(status = StatusCodes.OK,
                     entity =
                       HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
    }
  }

  private class TopicGetPromiseBridge(private val promise: Promise[TopicEntity])
      extends TopicGetCallback {

    override def onSuccess(entity: TopicEntity): Unit =
      promise.complete(Try(entity))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
