package org.doctordiabet.routinglayer.routing.topics

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives.{as, complete, entity, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.entity.topics.{
  TopicDeleteModel,
  TopicEntity
}
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.topics.{
  TopicUpdateCallback,
  TopicUpdateUseCase
}
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.dto.topics.TopicUpdateDTO
import org.doctordiabet.routinglayer.mapping.topics.TopicsMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.Try

class TopicUpdateController(private val topicUpdate: TopicUpdateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String], topicId: UUID): Route =
    entity(as[String]) { (dto: String) =>
      {

        val promise: Promise[TopicEntity] = Promise[TopicEntity]()

        //User should be:
        //AUTHORIZED or SPECIALIST and and OWNER or CONTRIBUTOR to this artifact
        //or
        //User should be:
        //MODERATOR or ADMINISTRATOR
        //regardless of relationship with this artifact
        security.validate(
          promise,
          bearer.getOrElse(""),
          topicId,
          List(RoleEntity.AUTHORIZED, RoleEntity.SPECIALIST),
          List(RelationshipEntity.CREATOR, RelationshipEntity.CONTRIBUTOR),
          List(RoleEntity.MODERATOR, RoleEntity.ADMINISTRATOR),
          (identityId: UUID,
           _: java.util.List[RoleEntity],
           _: RelationshipEntity,
           promise: Promise[TopicEntity]) => {
            topicUpdate.execute(mappingContext
                                  .readValue(dto, classOf[TopicUpdateDTO])
                                  .toEntity(topicId, identityId),
                                new TopicUpdatePromiseBridge(promise))
          }
        )

        onSuccess(promise.future) { entity: TopicEntity =>
          val body = EntityDTO(s"#/topics/${topicId.toString}/",
                               ODataTypes.TOPIC,
                               entity.toDTO)
          complete(
            HttpResponse(
              status = StatusCodes.OK,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
    }

  private class TopicUpdatePromiseBridge(
      private val promise: Promise[TopicEntity])
      extends TopicUpdateCallback {

    override def onSuccess(entity: TopicEntity): Unit =
      promise.complete(Try(entity))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
