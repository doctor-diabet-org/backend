package org.doctordiabet.routinglayer.routing.authorization

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import org.doctordiabet.di.components.aggregates.authorization.AuthorizationInterfaceAdapterComponent
import org.doctordiabet.di.components.identities.IdentitiesInterfaceAdapterComponent
import org.doctordiabet.di.components.sessions.SessionsInterfaceAdapterComponent
import org.doctordiabet.di.components.verification_codes.VerificationCodesInterfaceAdapterComponent
import org.doctordiabet.routinglayer.security.SecurityProvider

class AuthorizationDirective()(
    private implicit val sessionsVertex: SessionsInterfaceAdapterComponent,
    private implicit val codesVertex: VerificationCodesInterfaceAdapterComponent,
    private implicit val identitiesVertex: IdentitiesInterfaceAdapterComponent,
    private implicit val authorizationVertex: AuthorizationInterfaceAdapterComponent,
    private implicit val security: SecurityProvider) {

  private val codeRequestController = new AuthorizationCodeRequestController(
    codesVertex.getVerificationCodeSend,
    identitiesVertex.getIdentityGet,
    security)
  private val tokenRequestController = new AuthorizationTokenRequestController(
    authorizationVertex.getAuthorizeCode,
    authorizationVertex.getAuthorizeCredentials)
  private val sessionRequestController =
    new AuthorizationSessionRequestController(
      sessionsVertex.getSessionTokenCreate)

  def authorizationRouting(): Route = {
    ignoreTrailingSlash {
      pathPrefix("authorize") {
        path("code") {
          post {
            codeRequestController.dispatch()
          } ~
            put {
              tokenRequestController.dispatch(AuthorizationCode)
            }
        } ~
          path("credentials") {
            put {
              tokenRequestController.dispatch(AuthorizationCredentials)
            }
          } ~
          path("token") {
            put {
              sessionRequestController.dispatch()
            }
          }
      }
    }
  }
}
