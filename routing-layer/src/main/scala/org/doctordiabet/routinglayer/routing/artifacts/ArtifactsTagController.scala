package org.doctordiabet.routinglayer.routing.artifacts

import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{entity, _}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.usecase.artifacts.{
  TagsAttachUseCase,
  TagsDetachUseCase
}
import org.doctordiabet.routinglayer.dto.artifacts.TagIdsDTO
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise
import scala.util.{Failure, Success, Try}

class ArtifactsTagController(private val tagsAttach: TagsAttachUseCase,
                             private val tagsDetach: TagsDetachUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String],
               artifactId: UUID,
               actionType: TagActionType): Route = {
    val promise: Promise[Unit] = Promise[Unit]()

    //User should be:
    //AUTHORIZED or SPECIALIST and and OWNER or CONTRIBUTOR to this artifact
    //or
    //User should be:
    //MODERATOR or ADMINISTRATOR
    //regardless of relationship with this artifact

    entity(as[String]) { json: String =>
      val dto: TagIdsDTO = mappingContext.readValue[TagIdsDTO](json)

      security.validate(
        promise,
        bearer.getOrElse(""),
        artifactId,
        List(RoleEntity.AUTHORIZED, RoleEntity.SPECIALIST),
        List(RelationshipEntity.CREATOR, RelationshipEntity.CONTRIBUTOR),
        List(RoleEntity.MODERATOR, RoleEntity.ADMINISTRATOR),
        (_: UUID,
         _: java.util.List[RoleEntity],
         _: RelationshipEntity,
         promise: Promise[Unit]) => {
          actionType match {
            case TagAttachAction =>
              Try(tagsAttach.executeBlocking(artifactId, dto.getTagIds)) match {
                case Success(_)            => promise.success(Unit)
                case Failure(t: Throwable) => promise.failure(t)
              }
            case TagDetachAction =>
              Try(tagsDetach.executeBlocking(artifactId, dto.getTagIds)) match {
                case Success(_)            => promise.success(Unit)
                case Failure(t: Throwable) => promise.failure(t)
              }
          }
        }
      )

      onSuccess(promise.future) {
        complete(
          HttpResponse(
            status = StatusCodes.NoContent,
            entity = HttpEntity(ContentTypes.`application/json`, "")))
      }
    }
  }
}

sealed trait TagActionType

case object TagAttachAction extends TagActionType

case object TagDetachAction extends TagActionType
