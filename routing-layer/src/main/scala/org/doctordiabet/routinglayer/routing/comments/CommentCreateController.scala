package org.doctordiabet.routinglayer.routing.comments

import java.util.UUID

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives.{as, complete, entity, onSuccess}
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.comments.{CommentCreateCallback, CommentCreateUseCase}
import org.doctordiabet.routinglayer.dto.comments.CommentCreateDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.mapping.comments.CommentsMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.Promise

class CommentCreateController(private val commentCreate: CommentCreateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(bearer: Option[String], artifactId: UUID): Route =
    entity(as[String]) { (dto: String) =>
      {

        val promise: Promise[CommentEntity] = Promise[CommentEntity]()

        security.validate(
          promise,
          bearer.getOrElse(""),
          (identityId: UUID,
           roles: List[RoleEntity],
           promise: Promise[CommentEntity]) => {
            if (security.validateRole(
                  promise,
                  roles,
                  "Forbidden. Session owner have not enough privileges to perform this action",
                  RoleEntity.AUTHORIZED,
                  RoleEntity.SPECIALIST,
                  RoleEntity.MODERATOR,
                  RoleEntity.ADMINISTRATOR
                )) {
              commentCreate.execute(mappingContext
                                      .readValue(dto, classOf[CommentCreateDTO])
                                      .toEntity(artifactId, identityId),
                                    new CommentCreatePromiseBridge(promise))
            }
          }
        )

        onSuccess(promise.future) { entity: CommentEntity =>
          val body = EntityDTO(s"#/topics/${artifactId.toString}/comments/",
                               ODataTypes.COMMENT,
                               entity.toDTO)
          complete(
            HttpResponse(
              status = StatusCodes.Created,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
    }

  private class CommentCreatePromiseBridge(
      private val promise: Promise[CommentEntity])
      extends CommentCreateCallback {

    override def onSuccess(entity: CommentEntity): Unit =
      promise.success(entity)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }
}
