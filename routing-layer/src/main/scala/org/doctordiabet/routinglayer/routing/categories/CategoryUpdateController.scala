package org.doctordiabet.routinglayer.routing.categories

import java.util.UUID

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  StatusCodes
}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.usecase.categories.CategoryUpdateUseCase
import org.doctordiabet.routinglayer.dto.categories.CategoryUpdateDTO
import org.doctordiabet.routinglayer.dto.entity.EntityDTO
import org.doctordiabet.routinglayer.mapping.categories.CategoriesMappingContext._
import org.doctordiabet.routinglayer.metadata.ODataTypes
import org.doctordiabet.routinglayer.security.SecurityProvider
import org.doctordiabet.domainlayer.usecase.categories.CategoryUpdateCallback
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity._
import org.doctordiabet.domainlayer.entity.identity.RoleEntity

import scala.concurrent.Promise
import scala.util.Try

private class CategoryUpdateController(
    private val categoryUpdate: CategoryUpdateUseCase)(
    private implicit val security: SecurityProvider) {

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)
  mappingContext.setSerializationInclusion(Include.NON_NULL)

  def dispatch(id: UUID, bearer: Option[String]): Route = entity(as[String]) {
    (dto: String) =>
      {
        val promise: Promise[CategoryEntity] = Promise[CategoryEntity]()

        //User should be:
        //AUTHORIZED or SPECIALIST and and OWNER or CONTRIBUTOR to this artifact
        //or
        //User should be:
        //MODERATOR or ADMINISTRATOR
        //regardless of relationship with this artifact
        security.validate(
          promise,
          bearer.getOrElse(""),
          id,
          List(RoleEntity.AUTHORIZED, RoleEntity.SPECIALIST),
          List(RelationshipEntity.CREATOR, RelationshipEntity.CONTRIBUTOR),
          List(RoleEntity.MODERATOR, RoleEntity.ADMINISTRATOR),
          (identityId: UUID,
           _: java.util.List[RoleEntity],
           _: RelationshipEntity,
           promise: Promise[CategoryEntity]) => {
            categoryUpdate.execute(mappingContext
                                     .readValue[CategoryUpdateDTO](dto)
                                     .toEntity(id, identityId),
                                   new CategoryUpdatePromiseBridge(promise))
          }
        )

        onSuccess(promise.future) { entity: CategoryEntity =>
          val body = EntityDTO(s"#/categories/${id.toString}/",
                               ODataTypes.CATEGORY,
                               entity.toDTO)
          complete(
            HttpResponse(
              status = StatusCodes.OK,
              entity = HttpEntity(ContentTypes.`application/json`,
                                  new ObjectMapper().writeValueAsString(body))))
        }
      }
  }

  private class CategoryUpdatePromiseBridge(
      private val promise: Promise[CategoryEntity])
      extends CategoryUpdateCallback {

    override def onSuccess(entity: CategoryEntity): Unit =
      promise.complete(Try(entity))

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

}
