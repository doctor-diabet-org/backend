package org.doctordiabet.routinglayer.confiuration

import java.io.{File, FileInputStream}
import java.util
import java.util.Properties

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential

object NotificationsConfig {
    
    private val properties: Properties = new Properties() {
        load(new FileInputStream(new File("./data-layer/firebase-service.json")))
    }
    
    def token: String = properties.getProperty("firebase.token")
    
    def key: String = properties.getProperty("firebase.key")
    
    def sender: String = properties.getProperty("firebase.sender")
    
    def accessToken: String = {
        val googleCredential: GoogleCredential = GoogleCredential
            .fromStream(new FileInputStream("./data-layer/firebase-service.json"))
            .createScoped(util.Arrays.asList("https://www.googleapis.com/auth/firebase.messaging"))
        
        googleCredential.refreshToken()
        
        googleCredential.getAccessToken
    }
}
