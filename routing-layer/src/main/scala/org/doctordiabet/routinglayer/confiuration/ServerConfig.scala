package org.doctordiabet.routinglayer.confiuration

import java.io.{File, FileInputStream}
import java.util.Properties

import scala.util.{Success, Try}

object ServerConfig {
    
    private val properties: Properties = new Properties() {
        load(new FileInputStream(new File("./data-layer/server.properties")))
    }
    
    def interface: String = Try(properties.getProperty("server.interface")) match {
        case Success(interface) => interface
        case _ => "0.0.0.0"
    }
    
    def port: Int = Try(properties.getProperty("server.port").toInt) match {
        case Success(port) => port
        case _ => 8080
    }
}
