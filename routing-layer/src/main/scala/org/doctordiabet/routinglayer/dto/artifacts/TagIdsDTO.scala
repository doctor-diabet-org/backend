package org.doctordiabet.routinglayer.dto.artifacts

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class TagIdsDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("tag_ids")
    tagIds: java.util.List[UUID]
)
