package org.doctordiabet.routinglayer.dto.artifacts

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class TagsCreateDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("values")
    tagIds: java.util.List[String]
)
