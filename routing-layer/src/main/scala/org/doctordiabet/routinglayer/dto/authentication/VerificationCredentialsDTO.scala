package org.doctordiabet.routinglayer.dto.authentication

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class VerificationCredentialsDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("email") email: String,
    @BeanProperty @(JsonProperty @field @getter @param)("password") password: String
)
