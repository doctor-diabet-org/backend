package org.doctordiabet.routinglayer.dto.identities

import com.fasterxml.jackson.annotation.JsonProperty
import org.doctordiabet.routinglayer.dto.metadata.MetadataDTO
import org.doctordiabet.routinglayer.metadata.ODataTypes

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

sealed trait IdentityDTO {

  val firstName: String
  val lastName: String
  val metadata: MetadataDTO

  def odataType: String = this match {
    case IdentityPublicDTO(_, _, _, _)        => ODataTypes.IDENTITY_PUBLIC
    case IdentityPrivateDTO(_, _, _, _, _, _) => ODataTypes.IDENTITY_PRIVATE
  }
}

case class IdentityPublicDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("first_name") firstName: String,
    @BeanProperty @(JsonProperty @field @getter @param)("last_name") lastName: String,
    @BeanProperty @(JsonProperty @field @getter @param)("image") image: ImageDTO,
    @BeanProperty @(JsonProperty @field @getter @param)("metadata") metadata: MetadataDTO
) extends IdentityDTO

case class IdentityPrivateDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("first_name") firstName: String,
    @BeanProperty @(JsonProperty @field @getter @param)("last_name") lastName: String,
    @BeanProperty @(JsonProperty @field @getter @param)("image") image: ImageDTO,
    @BeanProperty @(JsonProperty @field @getter @param)("phone_number") phoneNumber: String,
    @BeanProperty @(JsonProperty @field @getter @param)("email") email: String,
    @BeanProperty @(JsonProperty @field @getter @param)("metadata") metadata: MetadataDTO
) extends IdentityDTO

case class ImageDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("small") smallUrl: String,
    @BeanProperty @(JsonProperty @field @getter @param)("medium") mediumUrl: String,
    @BeanProperty @(JsonProperty @field @getter @param)("large") largeUrl: String,
    @BeanProperty @(JsonProperty @field @getter @param)("origin") originUrl: String
)
