package org.doctordiabet.routinglayer.dto.identities

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class IdentityUpdateDTO(
    @BeanProperty @(JsonProperty @param @field @getter)("first_name") firstName: String,
    @BeanProperty @(JsonProperty @param @field @getter)("last_name") lastName: String,
    @BeanProperty @(JsonProperty @param @field @getter)("phone_number") phoneNumber: String,
    @BeanProperty @(JsonProperty @param @field @getter)("email") email: String,
    @BeanProperty @(JsonProperty @param @field @getter)("password") password: String,
    @BeanProperty @(JsonProperty @param @field @getter)("id") id: UUID
)
