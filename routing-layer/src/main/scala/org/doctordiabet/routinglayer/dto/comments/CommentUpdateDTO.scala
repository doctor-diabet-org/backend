package org.doctordiabet.routinglayer.dto.comments

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class CommentUpdateDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("content_text") contentText: String
)
