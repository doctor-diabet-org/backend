package org.doctordiabet.routinglayer.dto.session

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class SessionRequestDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("identity_id") identityId: UUID,
    @BeanProperty @(JsonProperty @field @getter @param)("refresh_token") refreshToken: String
)
