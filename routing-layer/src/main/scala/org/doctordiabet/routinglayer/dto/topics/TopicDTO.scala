package org.doctordiabet.routinglayer.dto.topics

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty
import org.doctordiabet.routinglayer.dto.metadata.MetadataDTO

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class TopicDTO(
    @BeanProperty @(JsonProperty @field)("title") title: String,
    @BeanProperty @(JsonProperty @field)("description") description: String,
    @BeanProperty @(JsonProperty @field)("category_id") categoryId: UUID,
    @BeanProperty @(JsonProperty @field)("comments_count") commentsCount: Long,
    @BeanProperty @(JsonProperty @field)("metadata") metadata: MetadataDTO
)
