package org.doctordiabet.routinglayer.dto.metadata

import java.sql.Timestamp
import java.util
import java.util.UUID

import com.fasterxml.jackson.annotation.{JsonFormat, JsonProperty}
import org.doctordiabet.routinglayer.dto.identities.IdentityLinkDTO

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class MetadataDTO(
    @BeanProperty @(JsonProperty @field)("id") id: UUID,
    @BeanProperty @(JsonFormat @field)(
      shape = JsonFormat.Shape.STRING,
      pattern = "dd-MM-yyyy'T'HH:mm:ss.SSS'Z'",
      timezone = "GMT"
    )
    @(JsonProperty @field)("created_time") createdTime: Timestamp,
    @BeanProperty @(JsonProperty @field)("created_by_identity") createdByIdentity: IdentityLinkDTO,
    @BeanProperty @(JsonFormat @field)(
      shape = JsonFormat.Shape.STRING,
      pattern = "dd-MM-yyyy'T'HH:mm:ss.SSS'Z'",
      timezone = "GMT"
    ) @(JsonProperty @field)("last_modified_time") lastModifiedTime: Timestamp,
    @BeanProperty @(JsonProperty @field)("last_modified_by_identity") lastModifiedByIdentity: IdentityLinkDTO,
    @BeanProperty @(JsonProperty @field)("votes") votes: Int,
    @BeanProperty @(JsonProperty @field)("tags") tags: util.Set[TagDTO]
)
