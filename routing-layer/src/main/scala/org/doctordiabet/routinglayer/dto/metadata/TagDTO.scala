package org.doctordiabet.routinglayer.dto.metadata

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class TagDTO(
    @BeanProperty @(JsonProperty @field)("id") id: UUID,
    @BeanProperty @(JsonProperty @field)("value") value: String
)
