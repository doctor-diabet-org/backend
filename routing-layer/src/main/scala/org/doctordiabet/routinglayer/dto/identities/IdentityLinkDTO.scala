package org.doctordiabet.routinglayer.dto.identities

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class IdentityLinkDTO(
    @BeanProperty @(JsonProperty @param @field @getter)("id") id: UUID,
    @BeanProperty @(JsonProperty @param @field @getter)("first_name") firstName: String,
    @BeanProperty @(JsonProperty @param @field @getter)("last_name") lastName: String
)
