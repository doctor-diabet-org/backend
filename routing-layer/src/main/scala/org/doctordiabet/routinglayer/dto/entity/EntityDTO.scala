package org.doctordiabet.routinglayer.dto.entity

import com.fasterxml.jackson.annotation.{JsonInclude, JsonProperty}

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class EntityDTO[T](
    @BeanProperty @(JsonInclude @field)(JsonInclude.Include.NON_DEFAULT) @(JsonProperty @field)(
      "@odata.context") odataContext: String = null,
    @BeanProperty @(JsonProperty @field)("@odata.type") odataType: String,
    @BeanProperty @(JsonProperty @field)("value") value: T
)
