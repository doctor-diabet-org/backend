package org.doctordiabet.routinglayer.dto.categories

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class CategoryCreateDTO(
    @BeanProperty @(JsonProperty @param @field @getter)("title") title: String,
    @BeanProperty @(JsonProperty @param @field @getter)("description") description: String
)
