package org.doctordiabet.routinglayer.dto.categories

import com.fasterxml.jackson.annotation.JsonProperty
import org.doctordiabet.routinglayer.dto.metadata.MetadataDTO

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class CategoryDTO(
    @BeanProperty @(JsonProperty @field)("title") title: String,
    @BeanProperty @(JsonProperty @field)("description") description: String,
    @BeanProperty @(JsonProperty @field)("topics_count") topicsCount: Long,
    @BeanProperty @(JsonProperty @field)("metadata") metadata: MetadataDTO
)
