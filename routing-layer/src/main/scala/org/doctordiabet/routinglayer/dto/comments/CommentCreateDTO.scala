package org.doctordiabet.routinglayer.dto.comments

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class CommentCreateDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("content_text") contentText: String
)
