package org.doctordiabet.routinglayer.dto.comments

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty
import org.doctordiabet.routinglayer.dto.metadata.MetadataDTO

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class CommentDTO(
    @BeanProperty @(JsonProperty @field)("referral_id") artifactReferralId: UUID,
    @BeanProperty @(JsonProperty @field)("content_text") contentText: String,
    @BeanProperty @(JsonProperty @field)("metadata") metadata: MetadataDTO
)
