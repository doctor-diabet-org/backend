package org.doctordiabet.routinglayer.dto.pagination

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class CollectionDTO[T](
    @BeanProperty @(JsonProperty @field)("@odata.count") odataCount: Int,
    @BeanProperty @(JsonProperty @field)("@odata.total") odataTotal: Int,
    @BeanProperty @(JsonProperty @field)("@odata.context") odataContext: String,
    @BeanProperty @(JsonProperty @field)("@odata.type") odataType: String,
    @BeanProperty @(JsonProperty @field)("value") value: java.util.List[T]
)
