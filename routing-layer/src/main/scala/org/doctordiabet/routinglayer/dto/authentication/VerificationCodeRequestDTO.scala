package org.doctordiabet.routinglayer.dto.authentication

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class VerificationCodeRequestDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("phone_number") phoneNumber: String
)
