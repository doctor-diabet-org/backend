package org.doctordiabet.routinglayer.dto.topics

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class TopicCreateDTO(
    @BeanProperty @(JsonProperty @param @field @getter)("title") title: String,
    @BeanProperty @(JsonProperty @param @field @getter)("description") description: String,
    @BeanProperty @(JsonProperty @param @field @getter)("tag_ids") tags: java.util.ArrayList[UUID]
)
