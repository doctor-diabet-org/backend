package org.doctordiabet.routinglayer.dto.session

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class SessionDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("identity_id") identityId: UUID,
    @BeanProperty @(JsonProperty @field @getter @param)("refresh_token") refreshToken: String,
    @BeanProperty @(JsonProperty @field @getter @param)("session_token") sessionToken: String,
    @BeanProperty @(JsonProperty @field @getter @param)("created_time") createdTime: Long,
    @BeanProperty @(JsonProperty @field @getter @param)("expiration") expiration: Long,
    @BeanProperty @(JsonProperty @field @getter @param)("expiration_time") expirationTime: Long,
)