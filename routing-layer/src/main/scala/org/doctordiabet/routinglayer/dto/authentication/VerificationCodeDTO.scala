package org.doctordiabet.routinglayer.dto.authentication

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class VerificationCodeDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("phone_number") phone: String,
    @BeanProperty @(JsonProperty @field @getter @param)("code") code: String
)
