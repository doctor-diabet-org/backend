package org.doctordiabet.routinglayer.dto.identities

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class IdentityDeleteDTO(
    @BeanProperty @(JsonProperty @field @getter @param)("id") id: UUID
)
