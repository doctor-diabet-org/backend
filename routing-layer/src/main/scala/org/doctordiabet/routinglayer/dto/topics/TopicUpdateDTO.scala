package org.doctordiabet.routinglayer.dto.topics

import java.util.UUID

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.{field, getter, param}
import scala.beans.BeanProperty

case class TopicUpdateDTO(
    @BeanProperty @(JsonProperty @param @field @getter)("title") title: String,
    @BeanProperty @(JsonProperty @param @field @getter)("description") description: String,
    @BeanProperty @(JsonProperty @param @field @getter)("category_id") categoryId: UUID
)
