package org.doctordiabet.routinglayer.dto.error

import com.fasterxml.jackson.annotation.JsonProperty

import scala.annotation.meta.field
import scala.beans.BeanProperty

case class ErrorDTO(
    @BeanProperty @(JsonProperty @field)("type") errorType: String,
    @BeanProperty @(JsonProperty @field)("code") code: Int,
    @BeanProperty @(JsonProperty @field)("message") message: String
)
