package org.doctordiabet.routinglayer.application

import java.util.UUID

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.coding.{Deflate, Gzip, NoCoding}
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, RequestContext, Route}
import akka.stream.ActorMaterializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import org.doctordiabet.di.components.aggregates.authorization.AuthorizationInterfaceAdapterComponent
import org.doctordiabet.di.components.categories.{CategoriesInterfaceAdapterComponent, CategoriesRepositoryComponent}
import org.doctordiabet.di.components.comments.{CommentsInterfaceAdapterComponent, CommentsRepositoryComponent}
import org.doctordiabet.di.components.identities.{IdentitiesInterfaceAdapterComponent, IdentitiesRepositoryComponent}
import org.doctordiabet.di.components.roles.RolesInterfaceAdapterComponent
import org.doctordiabet.di.components.sessions.{SessionsInterfaceAdapterComponent, SessionsRepositoryComponent}
import org.doctordiabet.di.components.topics.{TopicsInterfaceAdapterComponent, TopicsRepositoryComponent}
import org.doctordiabet.di.components.verification_codes.{VerificationCodesInterfaceAdapterComponent, VerificationCodesRepositoryComponent}
import org.doctordiabet.di.components.aggregates.authorization.AuthorizationInterfaceAdapterComponentKt.resolveAuthenticationInterfaceAdapterComponent
import org.doctordiabet.di.components.artifacts.{ArtifactsInterfaceAdapterComponent, ArtifactsRepositoryComponent}
import org.doctordiabet.di.graph.DependencyGraph
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.exceptions._
import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository
import org.doctordiabet.domainlayer.repository.categories.CategoriesRepository
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.repository.topics.TopicsRepository
import org.doctordiabet.routinglayer.confiuration.ServerConfig
import org.doctordiabet.routinglayer.dto.error.ErrorDTO
import org.doctordiabet.routinglayer.notifications._
import org.doctordiabet.routinglayer.routing.artifacts.ArtifactsDirective
import org.doctordiabet.routinglayer.routing.authorization.AuthorizationDirective
import org.doctordiabet.routinglayer.routing.categories.CategoriesDirective
import org.doctordiabet.routinglayer.routing.comments.CommentsDirective
import org.doctordiabet.routinglayer.routing.identities.IdentitiesDirective
import org.doctordiabet.routinglayer.routing.topics.TopicsDirective
import org.doctordiabet.routinglayer.security.SecurityProvider

import scala.concurrent.{ExecutionContextExecutor, Future, Promise}
import scala.util.Try

object Application {

  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem("routing-system")
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher

    val dependencyGraph = new DependencyGraph()

    implicit val categoriesRepositoryContext: CategoriesRepositoryComponent =
      dependencyGraph
        .resolveCategoriesDataComponent()
        .resolveCategoriesRepositoryComponent()

    implicit val categoriesModelContext: CategoriesRepository =
      categoriesRepositoryContext.getRepository

    implicit val categoriesContext: CategoriesInterfaceAdapterComponent =
      categoriesRepositoryContext
        .resolveCategoriesInterfaceAdapterComponent()

    implicit val topicsRepositoryContext: TopicsRepositoryComponent =
      dependencyGraph
        .resolveTopicsDataComponent()
        .resolveTopicsRepositoryComponent()

    implicit val topicsModelContext: TopicsRepository =
      topicsRepositoryContext.getRepository

    implicit val topicsContext: TopicsInterfaceAdapterComponent =
      topicsRepositoryContext
        .resolveTopicsInterfaceAdapterComponent()

    implicit val commentsRepositoryContext: CommentsRepositoryComponent =
      dependencyGraph
        .resolveCommentsDataComponent()
        .resolveCommentsRepositoryComponent()

    implicit val commentsModelContext: CommentsRepository =
      commentsRepositoryContext.getRepository

    implicit val commentsContext: CommentsInterfaceAdapterComponent =
      commentsRepositoryContext
        .resolveCommentsInterfaceAdapterComponent()

    implicit val identitiesRepositoryContext: IdentitiesRepositoryComponent =
      dependencyGraph
        .resolveIdentitiesDataComponent()
        .resolveIdentitiesRepositoryComponent()

    implicit val identitiesModelContext: IdentitiesRepository =
      identitiesRepositoryContext.getRepository

    implicit val identitiesContext: IdentitiesInterfaceAdapterComponent =
      identitiesRepositoryContext
        .resolveIdentitiesInterfaceAdapterComponent()

    implicit val sessionsRepositoryContext: SessionsRepositoryComponent =
      dependencyGraph
        .resolveSessionsDataComponent()
        .resolveSessionsRepositoryComponent()

    implicit val sessionsContext: SessionsInterfaceAdapterComponent =
      sessionsRepositoryContext
        .resolveSessionsInterfaceAdapterComponent()

    implicit val verificationRepositoryContext
      : VerificationCodesRepositoryComponent =
      dependencyGraph
        .resolveVerificationCodesDataComponent()
        .resolveVerificationCodesRepositoryComponent(
          dependencyGraph.resolveSmsDataComponent())

    implicit val verificationContext
      : VerificationCodesInterfaceAdapterComponent =
      verificationRepositoryContext
        .resolveVerificationCodesInterfaceAdapterComponent()

    implicit val rolesContext: RolesInterfaceAdapterComponent = dependencyGraph
      .resolveRolesDataComponent()
      .resolveRolesRepositoryComponent()
      .resolveRolesInterfaceAdapterComponent()

    implicit val authorizationContext: AuthorizationInterfaceAdapterComponent =
      resolveAuthenticationInterfaceAdapterComponent(
        identitiesRepositoryContext,
        sessionsRepositoryContext,
        verificationRepositoryContext
      )

    implicit val artifactsRepositoryContext: ArtifactsRepositoryComponent =
      dependencyGraph
        .resolveArtifactsDataComponent()
        .resolveArtifactsRepositoryComponent()

    implicit val artifactsModelContext: ArtifactsRepository =
      artifactsRepositoryContext.getRepository

    implicit val artifactsContext: ArtifactsInterfaceAdapterComponent =
      artifactsRepositoryContext
        .resolveArtifactsInterfaceAdapterComponent()

    val notifications: ActorRef = ForumNotificationsActor(
      artifactsContext.getSubscribersGet)

    implicit val security: SecurityProvider = new SecurityProvider()

    //Subscribes to artifact model change events streams
    //and stream all notifications to notifications actor
    //that sends them to topics and identities
    sub(notifications)

    //Important note:
    //If some routes change their relative position in code to others
    //Everything might stop working due to route priorities
    val route: Route = handleExceptions(debugExceptionHandler) {
      respondWithHeaders(RawHeader("Access-Control-Allow-Origin", "*"),
        RawHeader("Access-Control-Expose-Headers", "*"),
        RawHeader("Access-Control-Allow-Headers", "*"),
        RawHeader("Access-Control-Allow-Methods", "*")) {
      decodeRequestWith(Deflate, Gzip, NoCoding) {
        encodeResponseWith(NoCoding, Deflate, Gzip) {
          extractRequestContext { ctx =>
            println(ctx.request)
            options {
              complete("")
            } ~
            pathPrefix("api") {
              /**
                * Endpoint directive description related to:
                * "/comments/"
                * "/comments/<comment-id>/"
                **/
              new CommentsDirective().commentsRouting() ~
                  /**
                    * Endpoint directive description related to:
                    * "/categories/<category-id>/topics/"
                    * "/topics/"
                    * "/topics/<topic-id>/"
                    **/
                  new TopicsDirective().topicsRouting() ~
                  /**
                    * Endpoint directive description related to:
                    * "/categories/"
                    * "/categories/<category-id>/"
                    **/
                  new CategoriesDirective().categoriesRouting() ~
                  /**
                    * Endpoint directive description related to:
                    * "/identities/"
                    * "/identities/<identity-id>/"
                    **/
                  new IdentitiesDirective().identitiesRouting() ~
                  /**
                    * Endpoint directive description related to:
                    * "/authorize/code/"
                    * "/authorize/token/"
                    **/
                  new AuthorizationDirective().authorizationRouting() ~
                  /**
                    * Endpoin direcrive description related to:
                    * "/tags/"
                    * "/artifacts/<artifact-id>/tags/attach"
                    * "/artifacts/<artifact-id>/tags/detach"
                    * "/artifacts/<artifact-id>/tags/upvote"
                    * "/artifacts/<artifact-id>/tags/downvote"
                    **/
                  new ArtifactsDirective().artifactsRouting() ~
                  /**
                    * Endpoint directive description related to:
                    * /firebase
                    **/
                  (path("firebase") & parameter('token.as[String])) { (token: String) =>
                    post {
                      extractRequestContext { implicit request: RequestContext =>
                        implicit val req = request.request
              
                        import org.doctordiabet.routinglayer.routing.utils.HttpMetadataUtils.extractBearer
              
                        val promise: Promise[Unit] = Promise[Unit]()
              
                        security.validate[Unit](
                          promise,
                          extractBearer.getOrElse(""),
                          (identityId: UUID,
                           roles: List[RoleEntity],
                           promise: Promise[Unit]) => {
                            promise.success(Unit)
                            notifications ! FirebaseToken(identityId, token)
                          }
                        )
              
                        onSuccess(promise.future) {
                          complete("")
                        }
                      }
                    }
                  }
            }
          }
          }
        }
      }
    }

    implicit val bindingFuture: Future[Http.ServerBinding] =
      Http().bindAndHandle(route, ServerConfig.interface, ServerConfig.port)

    println(
      s"Server started and available at http://${ServerConfig.interface}:${ServerConfig.port}")

    Runtime.getRuntime.addShutdownHook(new Thread(new ServerShutdownHook()))
  }

  private def sub(notifications: ActorRef)(
      implicit commentsRepository: CommentsRepository,
      topicsRepository: TopicsRepository,
      categoriesRepository: CategoriesRepository,
      identitiesRepository: IdentitiesRepository,
      artifactsRepository: ArtifactsRepository,
      system: ActorSystem): Unit = {

    Observable
      .merge(
        categoriesRepository.sub(),
        topicsRepository.sub(),
        commentsRepository.sub(),
        artifactsRepository.sub()
      )
      .subscribe(new DisposableObserver[Any] {
        override def onError(e: Throwable): Unit = {
          e.printStackTrace()
        }
        override def onComplete(): Unit = {
          println("Application::sub completed")
        }
        override def onNext(t: Any): Unit = t match {

          case event: CategoriesRepository.Event.Create =>
            notifications ! CategoryCreate(event.getIdentityId,
                                           event.getEntityId)

          case event: CategoriesRepository.Event.Update =>
            notifications ! CategoryUpdate(event.getIdentityId,
                                           event.getEntityId)

          case event: CategoriesRepository.Event.Delete =>
            notifications ! CategoryDelete(event.getIdentityId,
                                           event.getEntityId)

          case event: ArtifactsRepository.Event.CategoryUpvote =>
            notifications ! CategoryUpvote(event.getIdentityId,
                                           event.getEntityId)

          case event: ArtifactsRepository.Event.CategoryDownvote =>
            notifications ! CategoryDownvote(event.getIdentityId,
                                             event.getEntityId)

          case event: TopicsRepository.Event.Create =>
            notifications ! TopicCreate(event.getIdentityId, event.getEntityId)

          case event: TopicsRepository.Event.Update =>
            notifications ! TopicUpdate(event.getIdentityId, event.getEntityId)

          case event: TopicsRepository.Event.Delete =>
            notifications ! TopicDelete(event.getIdentityId, event.getEntityId)

          case event: ArtifactsRepository.Event.TopicUpvote =>
            notifications ! TopicUpvote(event.getIdentityId, event.getEntityId)

          case event: ArtifactsRepository.Event.TopicDownvote =>
            notifications ! TopicDownvote(event.getIdentityId,
                                          event.getEntityId)

          case event: CommentsRepository.Event.Create =>
            notifications ! CommentCreate(event.getReferral,
                                          event.getIdentityId,
                                          event.getEntityId)

          case event: CommentsRepository.Event.Update =>
            notifications ! CommentUpdate(event.getReferral,
                                          event.getIdentityId,
                                          event.getEntityId)

          case event: CommentsRepository.Event.Delete =>
            notifications ! CommentDelete(event.getReferral,
                                          event.getIdentityId,
                                          event.getEntityId)

          case event: ArtifactsRepository.Event.CommentUpvote =>
            notifications ! CommentUpvote(event.getIdentityId,
                                          event.getEntityId)

          case event: ArtifactsRepository.Event.CommentDownvote =>
            notifications ! CommentDownvote(event.getIdentityId,
                                            event.getEntityId)

          case event: ArtifactsRepository.Event.Subscribe =>
            notifications ! Subscribe(event.getIdentityId,
                                      event.getEntityId,
                                      event.getOwnerId)

          case event: ArtifactsRepository.Event.Unsubscribe =>
            notifications ! Unsubscribe(event.getIdentityId,
                                        event.getEntityId,
                                        event.getOwnerId)

          case _ => {
            println(s"Unknown event received: $t")
          }
        }
      })
  }

  private def mappingContext = new ObjectMapper with ScalaObjectMapper

  mappingContext.registerModule(DefaultScalaModule)

  private val debugExceptionHandler = ExceptionHandler {
    case error: ExceptionBundle => {
      respondWithHeaders(RawHeader("Access-Control-Allow-Origin", "*"),
        RawHeader("Access-Control-Expose-Headers", "*"),
        RawHeader("Access-Control-Allow-Headers", "*"),
        RawHeader("Access-Control-Allow-Methods", "*")) {
        decodeRequestWith(Deflate, Gzip, NoCoding) {
          encodeResponseWith(NoCoding, Deflate, Gzip) {
            error.getSource match {
              case ExceptionSource.BUSINESS =>
                val accessor =
                  error.getAccessor.asInstanceOf[BusinessExceptionAccessor]
                val dto = ErrorDTO(ExceptionSource.BUSINESS.name(),
                  accessor.getCode,
                  accessor.getMessage)
  
                accessor.getCode match {
                  case BusinessExceptionAccessor.CODE_ARTIFACT_DELETED =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.TemporaryRedirect,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.NotFound,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_CATEGORY_TITLE_BAD_LENGTH =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.BadRequest,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_IDENTITY_BAD_EMAIL =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.BadRequest,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_IDENTITY_BAD_FIRST_NAME =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.BadRequest,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_IDENTITY_BAD_LAST_NAME =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.BadRequest,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_IDENTITY_BAD_PHONE =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.BadRequest,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_VERIFICATION_BAD_PHONE =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.BadRequest,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_NO_PERSISTENT_TOKEN =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.Unauthorized,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_NO_SESSION_TOKEN =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.Unauthorized,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_NO_SESSION_IDENTITY =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.Unauthorized,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case BusinessExceptionAccessor.CODE_ACTION_FORBIDDEN =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.Forbidden,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                  case default =>
                    complete(
                      HttpResponse(
                        status = StatusCodes.InternalServerError,
                        entity = HttpEntity(ContentTypes.`application/json`,
                          mappingContext.writeValueAsString(dto))
                      )
                    )
                }
              case ExceptionSource.DATABASE =>
                val accessor =
                  error.getAccessor.asInstanceOf[DatabaseExceptionAccessor]
                val dto = ErrorDTO(ExceptionSource.DATABASE.name(),
                  accessor.getCode,
                  accessor.getMessage)
                accessor.getCause.printStackTrace()
  
                complete(
                  HttpResponse(
                    status = StatusCodes.InternalServerError,
                    entity = HttpEntity(ContentTypes.`application/json`,
                      mappingContext.writeValueAsString(dto))
                  )
                )
              case ExceptionSource.INTERNAL =>
                val accessor =
                  error.getAccessor.asInstanceOf[InternalExceptionAccessor]
                val dto =
                  ErrorDTO(ExceptionSource.INTERNAL.name(), -1, accessor.getMessage)
                accessor.getCause.printStackTrace()
  
                complete(
                  HttpResponse(
                    status = StatusCodes.InternalServerError,
                    entity = HttpEntity(ContentTypes.`application/json`,
                      mappingContext.writeValueAsString(dto))
                  )
                )
              case ExceptionSource.NETWORK =>
                val accessor =
                  error.getAccessor.asInstanceOf[NetworkExceptionAccessor]
                val dto = ErrorDTO(ExceptionSource.NETWORK.name(),
                  accessor.getCode,
                  accessor.getMessage)
  
                complete(
                  HttpResponse(
                    status = StatusCodes.InternalServerError,
                    entity = HttpEntity(ContentTypes.`application/json`,
                      mappingContext.writeValueAsString(dto))
                  )
                )
              case ExceptionSource.SMS =>
                val accessor = error.getAccessor.asInstanceOf[SmsExceptionAccessor]
                val dto = ErrorDTO(ExceptionSource.SMS.name(),
                  accessor.getCode,
                  s"Current balance: ${accessor.getBalance}")
  
                complete(
                  HttpResponse(
                    status = StatusCodes.InternalServerError,
                    entity = HttpEntity(ContentTypes.`application/json`,
                      mappingContext.writeValueAsString(dto))
                  )
                )
            }
          }
        }
      }
    }
    case t: Throwable => {
      t.printStackTrace()
      respondWithHeaders(RawHeader("Access-Control-Allow-Origin", "*"),
        RawHeader("Access-Control-Expose-Headers", "*"),
        RawHeader("Access-Control-Allow-Headers", "*"),
        RawHeader("Access-Control-Allow-Methods", "*")) {
        decodeRequestWith(Deflate, Gzip, NoCoding) {
          encodeResponseWith(NoCoding, Deflate, Gzip) {
            complete(
              HttpResponse(
                status = StatusCodes.InternalServerError,
                entity = HttpEntity(ContentTypes.`application/json`,
                  "{\"message\":\"unknown error occured\", \"type\":\"INTERNAL\", \"code\":\"-1\"}")
              )
            )
          }
        }
      }
    }
  }

  private class ServerShutdownHook()(
      implicit private val binding: Future[Http.ServerBinding],
      private implicit val system: ActorSystem,
      private implicit val executionContext: ExecutionContextExecutor)
      extends Runnable {

    override def run(): Unit =
      binding
        .flatMap((_: Http.ServerBinding).unbind())
        .onComplete((_: Try[Unit]) => system.terminate())
  }
}
