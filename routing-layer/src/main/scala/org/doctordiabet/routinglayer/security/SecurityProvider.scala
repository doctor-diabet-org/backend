package org.doctordiabet.routinglayer.security

import java.util
import java.util.UUID

import org.doctordiabet.di.components.roles.RolesInterfaceAdapterComponent
import org.doctordiabet.di.components.sessions.SessionsInterfaceAdapterComponent
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.entity.session.SessionTokenEntity
import org.doctordiabet.domainlayer.exceptions.{
  BusinessExceptionAccessor,
  ExceptionBundle,
  ExceptionSource
}
import org.doctordiabet.domainlayer.usecase.rbac.{
  RelationshipGetUseCase,
  RolesGetUseCase
}
import org.doctordiabet.domainlayer.usecase.session.SessionTokenGetUseCase
import org.doctordiabet.domainlayer.usecase.roles.{
  RelationshipGetCallback,
  RolesGetCallback
}
import org.doctordiabet.domainlayer.usecase.session.SessionTokenGetCallback

import scala.collection.JavaConverters._
import scala.concurrent.Promise
import scala.util.{Failure, Success, Try}

class SecurityProvider()(
    implicit sessionsVertex: SessionsInterfaceAdapterComponent,
    rolesVertex: RolesInterfaceAdapterComponent) {

  private val sessionTokenGet: SessionTokenGetUseCase =
    sessionsVertex.getSessionTokenGet
  private implicit val rolesGet: RolesGetUseCase = rolesVertex.getRolesGet
  private implicit val relationshipsGet: RelationshipGetUseCase =
    rolesVertex.getRelationshipGet

  def validate[R](promise: Promise[R],
                  bearer: String,
                  delegate: (Promise[R]) => Unit): Unit = {
    sessionTokenGet.execute(
      bearer,
      new SessionTokenGetStrictPromiseBridge[R](promise, delegate))
  }

  def validate[R](
      promise: Promise[R],
      bearer: String,
      delegate: (Promise[R]) => Unit,
      fallbackDelegate: (Promise[R], ExceptionBundle) => Unit): Unit = {
    sessionTokenGet.execute(
      bearer,
      new SessionTokenGetFallbackPromiseBridge[R](promise,
                                                  delegate,
                                                  fallbackDelegate))
  }

  def validate[R](promise: Promise[R],
                  bearer: String,
                  delegate: (UUID, Promise[R]) => Unit): Unit = {
    sessionTokenGet.execute(
      bearer,
      new SessionTokenGetStrictExtendedPromiseBridge[R](promise, delegate))
  }

  def validate[R](
      promise: Promise[R],
      bearer: String,
      delegate: (UUID, Promise[R]) => Unit,
      fallbackDelegate: (Promise[R], ExceptionBundle) => Unit): Unit = {
    sessionTokenGet.execute(
      bearer,
      new SessionTokenGetFallbackExtendedPromiseBridge[R](promise,
                                                          delegate,
                                                          fallbackDelegate))
  }

  def validate[R](
      promise: Promise[R],
      bearer: String,
      delegate: (UUID, List[RoleEntity], Promise[R]) => Unit): Unit = {
    sessionTokenGet.execute(
      bearer,
      new SessionTokenGetStrictExtendedPromiseBridge[R](
        promise,
        (uuid: UUID, promise: Promise[R]) => {
          rolesGet.execute(
            uuid,
            new RolesGetStrictPromiseBridge[R](promise, uuid, delegate))
        })
    )
  }

  def validate[R](
      promise: Promise[R],
      bearer: String,
      delegate: (UUID, List[RoleEntity], Promise[R]) => Unit,
      fallbackDelegate: (Promise[R], ExceptionBundle) => Unit): Unit = {
    sessionTokenGet.execute(
      bearer,
      new SessionTokenGetFallbackExtendedPromiseBridge[R](
        promise,
        (uuid: UUID, promise: Promise[R]) => {
          rolesGet.execute(
            uuid,
            new RolesGetFallbackPromiseBridge[R](promise,
                                                 uuid,
                                                 delegate,
                                                 fallbackDelegate))
        },
        fallbackDelegate
      )
    )
  }

  def validate[R](
      promise: Promise[R],
      bearer: String,
      artifactId: UUID,
      delegate: (UUID,
                 util.List[RoleEntity],
                 RelationshipEntity,
                 Promise[R]) => Unit
  ): Boolean = {
    Try(sessionTokenGet.executeBlocking(bearer))
      .map((token: SessionTokenEntity) => token.getIdentityId)
      .flatMap((identityId: UUID) =>
        Try(rolesGet.executeBlocking(identityId))
          .map((roles: util.List[RoleEntity]) => (identityId, roles)))
      .flatMap((identityIdAndRoles: (UUID, util.List[RoleEntity])) =>
        Try(relationshipsGet.executeBlocking(identityIdAndRoles._1, artifactId))
          .map((relationship: RelationshipEntity) =>
            (identityIdAndRoles._1, identityIdAndRoles._2, relationship))) match {
      case Success((identityId, roles, relationship)) =>
        delegate(identityId, roles, relationship, promise)
        true;
      case Failure(e) =>
        promise.failure(e)
        false;
    }
  }

  def validate[R](
      promise: Promise[R],
      bearer: String,
      artifactId: UUID,
      expectedRoles: List[RoleEntity],
      expectedRelationships: List[RelationshipEntity],
      alwaysAllowForRoles: List[RoleEntity] = List.empty,
      delegate: (UUID,
                 util.List[RoleEntity],
                 RelationshipEntity,
                 Promise[R]) => Unit
  ): Boolean = {
    Try(sessionTokenGet.executeBlocking(bearer))
      .map((token: SessionTokenEntity) => token.getIdentityId)
      .flatMap((identityId: UUID) =>
        Try(rolesGet.executeBlocking(identityId))
          .map((roles: util.List[RoleEntity]) => (identityId, roles)))
      .flatMap((identityIdAndRoles: (UUID, util.List[RoleEntity])) =>
        Try(relationshipsGet.executeBlocking(identityIdAndRoles._1, artifactId))
          .map((relationship: RelationshipEntity) =>
            (identityIdAndRoles._1, identityIdAndRoles._2, relationship))) match {
      case Success(
          (identityId: UUID,
           roles: java.util.List[RoleEntity],
           relationship: RelationshipEntity)) =>
        if (alwaysAllowForRoles.exists((alwaysAllowForRole: RoleEntity) =>
              roles.contains(alwaysAllowForRole))) {
          delegate(identityId, roles, relationship, promise)
          return true
        }

        if (validateRoleWithRelationship(
              promise,
              "You don't have enough rights to access this feature",
              relationship,
              expectedRelationships,
              roles.asScala.toList,
              expectedRoles)) {
          delegate(identityId, roles, relationship, promise)
          return true
        }

        false
      case Failure(e) =>
        promise.failure(e)
        false;
    }
  }

  def validateRole[R](promise: Promise[R],
                      roles: List[RoleEntity],
                      failureMessage: String,
                      expected: RoleEntity*): Boolean = {
    for (role <- expected) if (roles.contains(role)) return true

    val bundle: ExceptionBundle = new ExceptionBundle(ExceptionSource.BUSINESS)
    val accessor: BusinessExceptionAccessor =
      bundle.getAccessor.asInstanceOf[BusinessExceptionAccessor]
    accessor.setCode(BusinessExceptionAccessor.CODE_ACTION_FORBIDDEN)
    accessor.setMessage(failureMessage)
    promise.failure(bundle)
    false
  }

  def validateRelationship[R](promise: Promise[R],
                              relationship: RelationshipEntity,
                              failureMessage: String,
                              expected: RelationshipEntity*): Boolean = {
    if (expected.contains(relationship)) return true

    val bundle: ExceptionBundle = new ExceptionBundle(ExceptionSource.BUSINESS)
    val accessor: BusinessExceptionAccessor =
      bundle.getAccessor.asInstanceOf[BusinessExceptionAccessor]
    accessor.setCode(BusinessExceptionAccessor.CODE_ACTION_FORBIDDEN)
    accessor.setMessage(failureMessage)
    promise.failure(bundle)
    false
  }

  def validateRoleWithRelationship[R](
      promise: Promise[R],
      failureMessage: String,
      relationship: RelationshipEntity,
      expectedRelationships: List[RelationshipEntity],
      roles: List[RoleEntity],
      expectedRoles: List[RoleEntity],
      successOnAny: Boolean = false): Boolean = {
    val relationshipMatches: Boolean =
      expectedRelationships.contains(relationship)
    val rolesMatch: Boolean = expectedRoles.exists((expectedRole: RoleEntity) =>
      roles.contains(expectedRole))

    val check: Boolean = if (successOnAny) {
      relationshipMatches || rolesMatch
    } else {
      relationshipMatches && rolesMatch
    }

    if (check) {
      return true
    }

    val bundle: ExceptionBundle = new ExceptionBundle(ExceptionSource.BUSINESS)
    val accessor: BusinessExceptionAccessor =
      bundle.getAccessor.asInstanceOf[BusinessExceptionAccessor]
    accessor.setCode(BusinessExceptionAccessor.CODE_ACTION_FORBIDDEN)
    accessor.setMessage(failureMessage)
    promise.failure(bundle)
    false
  }

  def respondForbidden[R](promise: Promise[R],
                          code: Int,
                          message: Option[String] = None): Unit = {
    val bundle: ExceptionBundle = new ExceptionBundle(ExceptionSource.BUSINESS)
    val accessor: BusinessExceptionAccessor =
      bundle.getAccessor.asInstanceOf[BusinessExceptionAccessor]
    accessor.setCode(code)
    accessor.setMessage(message.getOrElse(""))
    promise.failure(bundle)
  }

  private class SessionTokenGetStrictPromiseBridge[R](
      private val promise: Promise[R],
      private val delegate: (Promise[R]) => Unit)
      extends SessionTokenGetCallback {

    override def onSuccess(entity: SessionTokenEntity): Unit = delegate(promise)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

  private class SessionTokenGetFallbackPromiseBridge[R](
      private val promise: Promise[R],
      private val delegate: (Promise[R]) => Unit,
      private val fallbackDelegate: (Promise[R], ExceptionBundle) => Unit)
      extends SessionTokenGetCallback {

    override def onSuccess(entity: SessionTokenEntity): Unit = delegate(promise)

    override def onFailure(error: ExceptionBundle): Unit =
      fallbackDelegate(promise, error)
  }

  private class SessionTokenGetStrictExtendedPromiseBridge[R](
      private val promise: Promise[R],
      private val delegate: (UUID, Promise[R]) => Unit)
      extends SessionTokenGetCallback {

    override def onSuccess(entity: SessionTokenEntity): Unit =
      delegate(entity.getIdentityId, promise)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

  private class SessionTokenGetFallbackExtendedPromiseBridge[R](
      private val promise: Promise[R],
      private val delegate: (UUID, Promise[R]) => Unit,
      private val fallbackDelegate: (Promise[R], ExceptionBundle) => Unit)
      extends SessionTokenGetCallback {

    override def onSuccess(entity: SessionTokenEntity): Unit =
      delegate(entity.getIdentityId, promise)

    override def onFailure(error: ExceptionBundle): Unit =
      fallbackDelegate(promise, error)
  }

  private class RolesGetStrictPromiseBridge[R](
      private val promise: Promise[R],
      private val identityId: UUID,
      private val delegate: (UUID, List[RoleEntity], Promise[R]) => Unit)
      extends RolesGetCallback {

    override def onSuccess(entities: util.List[RoleEntity]): Unit =
      delegate(identityId, entities.asScala.toList, promise)

    override def onFailure(error: ExceptionBundle): Unit =
      promise.failure(error)
  }

  private class RolesGetFallbackPromiseBridge[R](
      private val promise: Promise[R],
      private val identityId: UUID,
      private val delegate: (UUID, List[RoleEntity], Promise[R]) => Unit,
      private val fallbackDelegate: (Promise[R], ExceptionBundle) => Unit)
      extends RolesGetCallback {

    override def onSuccess(entities: util.List[RoleEntity]): Unit =
      delegate(identityId, entities.asScala.toList, promise)

    override def onFailure(error: ExceptionBundle): Unit =
      fallbackDelegate(promise, error)
  }

  private class RelationshipPromiseBridge[R](
      private val promise: Promise[R],
      private val identityId: UUID,
      private val artifactId: UUID,
      private val delegate: (RelationshipEntity, Promise[R]) => Unit
  )

  private class RolesWithRelationshipPromiseBridge[R](
      private val promise: Promise[R],
      private val identityId: UUID,
      private val artifactId: UUID,
      private val delegate: (UUID,
                             List[RoleEntity],
                             RelationshipEntity,
                             Promise[R]) => Unit
  )(private implicit val rolesGet: RolesGetUseCase,
    private implicit val relationshipGet: RelationshipGetUseCase) {

    private var failure: Option[Throwable] = None
    private var result: (Option[List[RoleEntity]], Option[RelationshipEntity]) =
      (None, None)

    def bind(): Unit = {
      rolesGet.execute(
        identityId,
        new RolesGetCallback {
          override def onFailure(error: ExceptionBundle): Unit = {
            failure = Some(error)
            promise.failure(error)
          }

          override def onSuccess(entities: util.List[RoleEntity]): Unit = {
            if (failure.isEmpty) {
              result = (Some(entities.asScala.toList), result._2)
              result match {
                case (Some(roles), Some(relationship)) =>
                  delegate(identityId, roles, relationship, promise)
                case _ =>
              }
            }
          }
        }
      )

      relationshipsGet.execute(
        identityId,
        artifactId,
        new RelationshipGetCallback {
          override def onFailure(error: ExceptionBundle): Unit = {
            failure = Some(error)
            promise.failure(error)
          }

          override def onSuccess(entity: RelationshipEntity): Unit = {
            if (failure.isEmpty) {
              result = (result._1, Some(entity))
              result match {
                case (Some(roles), Some(relationship)) =>
                  delegate(identityId, roles, relationship, promise)
                case _ =>
              }
            }
          }
        }
      )
    }
  }
}
