package org.doctordiabet.routinglayer.mapping.comments

import java.util.UUID

import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import org.doctordiabet.domainlayer.entity.comments._
import org.doctordiabet.routinglayer.dto.metadata._
import org.doctordiabet.routinglayer.dto.comments._
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._

import scala.collection.JavaConverters._

object CommentsMappingContext {

  implicit class Entity(entity: CommentEntity) {

    def toDTO: CommentDTO = CommentDTO(
      entity.getArtifactReferralId,
      entity.getContentText,
      MetadataDTO(
        entity.getMetadata.getId,
        entity.getMetadata.getCreatedTime,
        entity.getMetadata.getCreatedByIdentity.toDTO,
        entity.getMetadata.getLastModifiedTime,
        entity.getMetadata.getLastModifiedByIdentity.toDTO,
        entity.getMetadata.getVotes,
        entity.getMetadata.getTags.asScala
          .map(
            (element: TagEntity) =>
              TagDTO(
                element.getId,
                element.getValue
            ))
          .asJava
      )
    )
  }

  implicit class Create(dto: CommentCreateDTO) {

    def toEntity(artifactReferralId: UUID,
                 modifyingByIdentity: UUID): CommentCreateModel =
      new CommentCreateModel(
        artifactReferralId,
        dto.contentText,
        modifyingByIdentity
      )
  }

  implicit class Update(dto: CommentUpdateDTO) {

    def toEntity(id: UUID, modifyingByIdentity: UUID): CommentUpdateModel =
      new CommentUpdateModel(
        id,
        dto.contentText,
        modifyingByIdentity
      )
  }
}
