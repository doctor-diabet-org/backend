package org.doctordiabet.routinglayer.mapping.artifacts

import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import org.doctordiabet.routinglayer.dto.metadata.TagDTO

object TagsMappingContext {

  implicit class Entity(entity: TagEntity) {

    def toDTO: TagDTO = TagDTO(
      entity.getId,
      entity.getValue
    )
  }

}
