package org.doctordiabet.routinglayer.mapping.topics

import java.util.UUID

import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import org.doctordiabet.domainlayer.entity.topics._
import org.doctordiabet.routinglayer.dto.metadata._
import org.doctordiabet.routinglayer.dto.topics._
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._

import scala.collection.JavaConverters._

object TopicsMappingContext {

  implicit class Entity(entity: TopicEntity) {

    def toDTO: TopicDTO = TopicDTO(
      entity.getTitle,
      entity.getDescription,
      entity.getCategoryId,
      entity.getCommentsCount,
      MetadataDTO(
        entity.getMetadata.getId,
        entity.getMetadata.getCreatedTime,
        entity.getMetadata.getCreatedByIdentity.toDTO,
        entity.getMetadata.getLastModifiedTime,
        entity.getMetadata.getLastModifiedByIdentity.toDTO,
        entity.getMetadata.getVotes,
        entity.getMetadata.getTags.asScala
          .map(
            (element: TagEntity) =>
              TagDTO(
                element.getId,
                element.getValue
            ))
          .asJava
      )
    )
  }

  implicit class Create(dto: TopicCreateDTO) {

    def toEntity(categoryId: UUID,
                 modifyingByIdentity: UUID): TopicCreateModel =
      new TopicCreateModel(
        dto.getTitle,
        dto.getDescription,
        dto.getTags,
        categoryId,
        modifyingByIdentity
      )
  }

  implicit class Update(dto: TopicUpdateDTO) {

    def toEntity(id: UUID, modifyingByIdentity: UUID): TopicUpdateModel =
      new TopicUpdateModel(
        id,
        dto.getTitle,
        dto.getDescription,
        dto.getCategoryId,
        modifyingByIdentity
      )
  }
}
