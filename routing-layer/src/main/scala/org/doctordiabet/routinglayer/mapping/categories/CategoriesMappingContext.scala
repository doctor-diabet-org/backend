package org.doctordiabet.routinglayer.mapping.categories

import java.util.UUID

import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import org.doctordiabet.domainlayer.entity.categories.{
  CategoryCreateModel,
  CategoryDeleteModel,
  CategoryEntity,
  CategoryUpdateModel
}
import org.doctordiabet.routinglayer.dto.categories.{
  CategoryCreateDTO,
  CategoryDTO,
  CategoryUpdateDTO
}
import org.doctordiabet.routinglayer.dto.metadata.{MetadataDTO, TagDTO}
import org.doctordiabet.routinglayer.mapping.identities.IdentitiesMappingContext._

import scala.collection.JavaConverters._

object CategoriesMappingContext {

  implicit class Entity(entity: CategoryEntity) {

    def toDTO: CategoryDTO = CategoryDTO(
      entity.getTitle,
      entity.getDescription,
      entity.getTopicsCount,
      MetadataDTO(
        entity.getMetadata.getId,
        entity.getMetadata.getCreatedTime,
        entity.getMetadata.getCreatedByIdentity.toDTO,
        entity.getMetadata.getLastModifiedTime,
        entity.getMetadata.getLastModifiedByIdentity.toDTO,
        entity.getMetadata.getVotes,
        entity.getMetadata.getTags.asScala
          .map(
            (element: TagEntity) =>
              TagDTO(
                element.getId,
                element.getValue
            ))
          .asJava
      )
    )
  }

  implicit class Create(dto: CategoryCreateDTO) {

    def toEntity(identityId: UUID): CategoryCreateModel =
      new CategoryCreateModel(
        identityId,
        dto.title,
        dto.description
      )
  }

  implicit class Update(dto: CategoryUpdateDTO) {

    def toEntity(id: UUID, identityId: UUID): CategoryUpdateModel =
      new CategoryUpdateModel(
        id,
        dto.title,
        dto.description,
        identityId
      )
  }
}
