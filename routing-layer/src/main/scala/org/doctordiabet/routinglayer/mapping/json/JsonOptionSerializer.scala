package org.doctordiabet.routinglayer.mapping.json

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.{JsonSerializer, SerializerProvider}

class JsonOptionSerializer extends JsonSerializer[Option[AnyVal]] {
  override def serialize(value: Option[AnyVal],
                         gen: JsonGenerator,
                         serializers: SerializerProvider): Unit = {
    gen.writeRawValue(value.getOrElse("").toString)
  }
}
