package org.doctordiabet.routinglayer.mapping.identities

import java.util.UUID

import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import org.doctordiabet.domainlayer.entity.identity.{
  IdentityPrivateEntity,
  IdentityPublicEntity,
  _
}
import org.doctordiabet.routinglayer.dto.identities._
import org.doctordiabet.routinglayer.dto.metadata.{MetadataDTO, TagDTO}

import scala.collection.JavaConverters._

object IdentitiesMappingContext {
  
  implicit class Link(entity: IdentityLink) {
    
    def toDTO: IdentityLinkDTO = IdentityLinkDTO(
      entity.getId,
      entity.getFirstName,
      entity.getLastName
    )
  }

  implicit class Entity(entity: IdentityEntity) {

    def toDTO: IdentityDTO = entity match {
      case privateEntity: IdentityPrivateEntity => {
        IdentityPrivateDTO(
          privateEntity.getFirstName,
          privateEntity.getLastName,
          ImageDTO(
            s"/identities/image/${privateEntity.getMetadata.getId.toString}/?size=small",
            s"/identities/image/${privateEntity.getMetadata.getId.toString}/?size=medium",
            s"/identities/image/${privateEntity.getMetadata.getId.toString}/?size=large",
            s"/identities/image/${privateEntity.getMetadata.getId.toString}/?size=origin"
          ),
          privateEntity.asInstanceOf[IdentityPrivateEntity].getPhoneNumber,
          privateEntity.getEmail,
          MetadataDTO(
            privateEntity.getMetadata.getId,
            privateEntity.getMetadata.getCreatedTime,
            privateEntity.getMetadata.getCreatedByIdentity.toDTO,
            privateEntity.getMetadata.getLastModifiedTime,
            privateEntity.getMetadata.getLastModifiedByIdentity.toDTO,
            privateEntity.getMetadata.getVotes,
            privateEntity.getMetadata.getTags.asScala
              .map(
                (element: TagEntity) =>
                  TagDTO(
                    element.getId,
                    element.getValue
                ))
              .asJava
          )
        )
      }

      case publicEntity: IdentityPublicEntity => {
        IdentityPublicDTO(
          publicEntity.getFirstName,
          publicEntity.getLastName,
          ImageDTO(
            s"/identities/image/${publicEntity.getMetadata.getId.toString}/?size=small",
            s"/identities/image/${publicEntity.getMetadata.getId.toString}/?size=medium",
            s"/identities/image/${publicEntity.getMetadata.getId.toString}/?size=large",
            s"/identities/image/${publicEntity.getMetadata.getId.toString}/?size=origin"
          ),
          MetadataDTO(
            publicEntity.getMetadata.getId,
            publicEntity.getMetadata.getCreatedTime,
            publicEntity.getMetadata.getCreatedByIdentity.toDTO,
            publicEntity.getMetadata.getLastModifiedTime,
            publicEntity.getMetadata.getLastModifiedByIdentity.toDTO,
            publicEntity.getMetadata.getVotes,
            publicEntity.getMetadata.getTags.asScala
              .map(
                (element: TagEntity) =>
                  TagDTO(
                    element.getId,
                    element.getValue
                ))
              .asJava
          )
        )
      }
    }
  }

  implicit class Create(dto: IdentityCreateDTO) {

    def toEntity(): IdentityCreateModel =
      new IdentityCreateModel(
        dto.firstName,
        dto.lastName,
        dto.phoneNumber,
        dto.email,
        dto.password,
        null
      )
  }

  implicit class Update(dto: IdentityUpdateDTO) {

    def toEntity(identityId: UUID): IdentityUpdateModel =
      new IdentityUpdateModel(
        dto.id,
        identityId,
        dto.firstName,
        dto.lastName,
        dto.phoneNumber,
        dto.email,
        dto.password
      )
  }

  implicit class Delete(dto: IdentityDeleteDTO) {

    def toEntity(identityId: UUID): IdentityDeleteModel =
      new IdentityDeleteModel(
        dto.id,
        identityId
      )
  }

}
