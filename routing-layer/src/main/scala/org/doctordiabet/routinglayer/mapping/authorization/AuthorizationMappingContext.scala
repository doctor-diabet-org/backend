package org.doctordiabet.routinglayer.mapping.authorization

import org.doctordiabet.domainlayer.entity.session.{
  PersistentTokenCreateModel,
  PersistentTokenEntity,
  SessionTokenCreateModel,
  SessionTokenEntity
}
import org.doctordiabet.domainlayer.entity.verification_codes.{
  VerificationCodeCreateModel,
  VerificationCodeVerifyModel
}
import org.doctordiabet.routinglayer.dto.session.{
  RefreshTokenDTO,
  SessionDTO,
  SessionRequestDTO
}
import org.doctordiabet.routinglayer.dto.authentication.{
  VerificationCodeDTO,
  VerificationCodeRequestDTO
}

object AuthorizationMappingContext {

  implicit class TokenResponse(entity: PersistentTokenEntity) {

    def toDTO: RefreshTokenDTO = RefreshTokenDTO(
      entity.getIdentityId,
      entity.getRefreshToken
    )
  }

  implicit class SessionRequest(dto: SessionRequestDTO) {

    def toEntity: SessionTokenCreateModel = new SessionTokenCreateModel(
      dto.identityId,
      dto.refreshToken
    )
  }

  implicit class SessionResponse(entity: SessionTokenEntity) {

    def toDTO: SessionDTO = SessionDTO(
      entity.getIdentityId,
      entity.getRefreshToken,
      entity.getSessionToken,
      entity.getCreatedTime,
      entity.getExpiration,
      entity.getExpirationTime
    )
  }

}
