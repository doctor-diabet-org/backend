CREATE FUNCTION release.comment_current_versions_hierarchy(UUID) RETURNS TABLE (
  id UUID,
  artifact_referral_id UUID,
  content_text TEXT,
  comment_version INTEGER,
  created_time TIMESTAMP,
  created_by_identity UUID,
  last_modified_time TIMESTAMP,
  last_modified_by_identity UUID,
  created BOOLEAN,
  updated BOOLEAN,
  deleted BOOLEAN,
  votes BIGINT,
  tags JSON,
  depth INTEGER
) AS
$$
BEGIN
  RETURN QUERY WITH RECURSIVE comment_current_versions
  (id, artifact_referral_id, content_text, comment_version, created_time, created_by_identity, last_modified_time,
      last_modified_by_identity, created, updated, deleted, votes, tags, depth)
  AS (
    SELECT
      release.comments.id AS id,
      release.comments.artifact_referral_id AS artifact_referral_id,
      release.comments.content_text AS content_text,
      shadow.version AS comment_version,
      artifacts.created_time AS created_time,
      artifacts.created_by_identity AS created_by_identity,
      artifacts_shadow.last_modified_time AS last_modified_time,
      artifacts_shadow.last_modified_by_identity AS last_modified_by_identity,
      artifacts_shadow.created AS created,
      artifacts_shadow.updated AS updated,
      artifacts_shadow.deleted AS deleted,
      (SELECT COALESCE(SUM(release.identity_artifact_votes.value), 0)
       FROM release.identity_artifact_votes
       WHERE release.identity_artifact_votes.artifact = artifacts.id) AS votes,
      (SELECT JSON_AGG(JSON_BUILD_OBJECT('id', release.tags.id, 'value', release.tags.value))
       FROM release.artifact_tags_link
         INNER JOIN release.tags ON release.artifact_tags_link.tag_id = release.tags.id
       WHERE release.artifact_tags_link.artifact_id = artifacts.id)   AS tags,
      1 AS depth
    FROM release.comments
      INNER JOIN release.comments_shadow shadow ON release.comments.id = shadow.id AND shadow.version = (SELECT MAX(version)
                                                                                                         FROM release.comments_shadow
                                                                                                         WHERE
                                                                                                           release.comments_shadow.id =
                                                                                                           release.comments.id)
      INNER JOIN release.artifacts artifacts ON release.comments.id = artifacts.id
      INNER JOIN release.artifacts_shadow artifacts_shadow
        ON shadow.id = artifacts_shadow.id AND shadow.version = artifacts_shadow.version
    WHERE comments.artifact_referral_id = $1

    UNION ALL

    SELECT
      comments.id AS id,
      comments.artifact_referral_id AS artifact_referral_id,
      comments.content_text AS content_text,
      shadow.version AS comment_version,
      artifacts.created_time AS created_time,
      artifacts.created_by_identity AS created_by_identity,
      artifacts_shadow.last_modified_time AS last_modified_time,
      artifacts_shadow.last_modified_by_identity AS last_modified_by_identity,
      artifacts_shadow.created AS created,
      artifacts_shadow.updated AS updated,
      artifacts_shadow.deleted AS deleted,
      (SELECT COALESCE(SUM(release.identity_artifact_votes.value), 0)
       FROM release.identity_artifact_votes
       WHERE release.identity_artifact_votes.artifact = artifacts.id) AS votes,
      (SELECT JSON_AGG(JSON_BUILD_OBJECT('id', release.tags.id, 'value', release.tags.value))
       FROM release.artifact_tags_link
         INNER JOIN release.tags ON release.artifact_tags_link.tag_id = release.tags.id
       WHERE release.artifact_tags_link.artifact_id = artifacts.id)   AS tags,
      rec.depth + 1 AS depth
    FROM release.comments comments
      INNER JOIN comment_current_versions rec ON comments.artifact_referral_id = rec.id
      INNER JOIN release.comments_shadow shadow ON comments.id = shadow.id AND shadow.version = (SELECT MAX(version)
                                                                                                 FROM release.comments_shadow
                                                                                                 WHERE
                                                                                                   release.comments_shadow.id =
                                                                                                   comments.id)
      INNER JOIN release.artifacts artifacts ON comments.id = artifacts.id
      INNER JOIN release.artifacts_shadow artifacts_shadow
        ON shadow.id = artifacts_shadow.id AND shadow.version = artifacts_shadow.version
  ) SELECT * FROM comment_current_versions;
END;
$$language 'plpgsql';