CREATE TYPE release.privilege_type AS ENUM(
  'categories_create',
  'categories_read',
  'categories_update',
  'categories_delete',
  'categories_vote',

  'topics_create',
  'topics_read',
  'topics_update',
  'topics_delete',
  'topics_vote',

  'comments_create',
  'comments_read',
  'comments_update',
  'comments_delete',
  'comments_vote',

  'identities_create',
  'identities_read',
  'identities_update',
  'identities_delete',
  'identities_vote',

  'tags_create',
  'tags_update',
  'tags_delete'
);