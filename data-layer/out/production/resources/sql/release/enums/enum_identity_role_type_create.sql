CREATE TYPE release.identity_role_type AS ENUM(
  'guest',
  'authorized',
  'specialist',
  'moderator',
  'administrator'
);