CREATE TABLE release.artifact_operation_privileges_link(
  operation_type release.artifact_operation NOT NULL,
  privilege_type release.privilege_type NOT NULL
);