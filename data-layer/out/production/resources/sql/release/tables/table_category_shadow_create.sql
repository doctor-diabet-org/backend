CREATE TABLE release.categories_shadow(
  id UUID NOT NULL,
  version INTEGER NOT NULL,
  title TEXT NOT NULL,
  description TEXT NOT NULL DEFAULT(''),
  PRIMARY KEY (id, version)
);