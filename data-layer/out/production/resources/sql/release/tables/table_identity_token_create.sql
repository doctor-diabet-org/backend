CREATE TABLE release.identity_tokens(
  identity UUID PRIMARY KEY NOT NULL,
  token TEXT NOT NULL
);