CREATE TABLE release.identity_artifact_subscriptions(
  identity UUID NOT NULL,
  artifact UUID NOT NULL,
  last_acknowledgment_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (identity, artifact)
);