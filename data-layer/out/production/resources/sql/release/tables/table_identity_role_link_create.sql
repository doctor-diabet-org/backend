CREATE TABLE release.identity_roles_link(
  identity_id UUID NOT NULL,
  role_type release.identity_role_type NOT NULL
);