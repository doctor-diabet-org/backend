CREATE TABLE release.artifacts_shadow(
  id UUID NOT NULL,
  version INTEGER NOT NULL,
  type release.artifact_type NOT NULL,
  last_modified_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_modified_by_identity UUID NOT NULL,
  created BOOLEAN NOT NULL DEFAULT FALSE,
  updated BOOLEAN NOT NULL DEFAULT FALSE,
  deleted BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY(id, version)
);

