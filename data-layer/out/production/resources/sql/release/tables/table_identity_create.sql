CREATE TABLE release.identities(
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1mc(),
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  phone_number TEXT UNIQUE,
  email TEXT NOT NULL UNIQUE,
  password_hash TEXT NOT NULL
);