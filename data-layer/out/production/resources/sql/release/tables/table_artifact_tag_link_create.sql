CREATE TABLE release.artifact_tags_link(
  artifact_id UUID NOT NULL,
  tag_id UUID NOT NULL,
  PRIMARY KEY(artifact_id, tag_id)
);