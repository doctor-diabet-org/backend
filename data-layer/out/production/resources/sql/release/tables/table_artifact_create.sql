CREATE TABLE release.artifacts(
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1mc(),
  type release.artifact_type NOT NULL,
  created_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by_identity UUID NOT NULL DEFAULT uuid_nil()
);