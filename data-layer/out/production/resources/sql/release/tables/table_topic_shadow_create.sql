CREATE TABLE release.topics_shadow(
  id UUID NOT NULL,
  version INTEGER NOT NULL,
  category UUID NOT NULL,
  title TEXT NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (id, version)
);