CREATE TABLE release.identities_shadow(
  id UUID NOT NULL,
  version INTEGER NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  phone_number TEXT,
  email TEXT NOT NULL,
  password_hash TEXT NOT NULL,
  PRIMARY KEY (id, version)
);