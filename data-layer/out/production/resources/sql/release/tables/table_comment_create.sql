CREATE TABLE release.comments(
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1mc(),
  artifact_referral_id UUID NOT NULL,
  content_text TEXT NOT NULL
);