CREATE VIEW release.topic_current_versions AS
  SELECT
    release.topics.id,
    release.topics.title,
    release.topics.description,
    release.topics.category,
    a.version,
    release.artifacts.created_time,
    release.artifacts.created_by_identity,
    release.artifacts_shadow.last_modified_time,
    release.artifacts_shadow.last_modified_by_identity,
    release.artifacts_shadow.created,
    release.artifacts_shadow.updated,
    release.artifacts_shadow.deleted,
    (SELECT COALESCE(SUM(release.identity_artifact_votes.value), 0) FROM release.identity_artifact_votes WHERE release.identity_artifact_votes.artifact = release.artifacts.id) AS votes,
    (SELECT JSON_AGG(JSON_BUILD_OBJECT('id', release.tags.id, 'value', release.tags.value)) FROM release.artifact_tags_link INNER JOIN release.tags ON release.artifact_tags_link.tag_id = release.tags.id WHERE release.artifact_tags_link.artifact_id = release.artifacts.id) AS tags,
    (
      SELECT COALESCE(COUNT(comments.id), 0) FROM release.comment_current_versions_hierarchy(release.topics.id) comments WHERE comments.deleted = FALSE
    ) AS comments_count
  FROM release.topics
    INNER JOIN release.topics_shadow a ON release.topics.id = a.id AND a.version = (SELECT MAX(version) FROM release.topics_shadow WHERE release.topics_shadow.id = release.topics.id)
    INNER JOIN release.artifacts ON release.topics.id = release.artifacts.id
    INNER JOIN release.artifacts_shadow ON a.id = release.artifacts_shadow.id AND a.version = release.artifacts_shadow.version
  ORDER BY release.artifacts.created_time DESC;