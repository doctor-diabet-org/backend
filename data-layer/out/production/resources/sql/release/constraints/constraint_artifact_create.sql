ALTER TABLE release.artifacts
  ADD CONSTRAINT artifact_type_fkey FOREIGN KEY (type)
    REFERENCES release.artifact_types (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT created_by_identity_fkey FOREIGN KEY (created_by_identity)
    REFERENCES release.identities (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;