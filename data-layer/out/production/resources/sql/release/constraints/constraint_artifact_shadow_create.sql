ALTER TABLE release.artifacts_shadow
  ADD CONSTRAINT artifact_type_fkey FOREIGN KEY (type)
    REFERENCES release.artifact_types (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT last_modified_by_identity_fkey FOREIGN KEY (last_modified_by_identity)
    REFERENCES release.identities (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;