ALTER TABLE release.identities
  ADD CONSTRAINT artifact_fkey FOREIGN KEY (id)
    REFERENCES release.artifacts (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;