ALTER TABLE release.comments
  ADD CONSTRAINT artifact_fkey FOREIGN KEY (id)
    REFERENCES release.artifacts (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT artifact_referral_fkey FOREIGN KEY (artifact_referral_id)
    REFERENCES release.artifacts (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;