ALTER TABLE release.identity_tokens
  ADD CONSTRAINT identity_fkey FOREIGN KEY (identity)
    REFERENCES release.identities (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;