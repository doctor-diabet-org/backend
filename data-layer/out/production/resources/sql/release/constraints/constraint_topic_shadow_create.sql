ALTER TABLE release.topics_shadow
  ADD CONSTRAINT topic_fkey FOREIGN KEY (id)
    REFERENCES release.topics (id)
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT category_fkey FOREIGN KEY (category)
    REFERENCES release.categories (id)
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT artifact_shadow_fkey FOREIGN KEY (id, version)
    REFERENCES release.artifacts_shadow (id, version) MATCH FULL
    ON UPDATE NO ACTION ON DELETE CASCADE;