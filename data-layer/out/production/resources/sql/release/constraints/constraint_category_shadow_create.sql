ALTER TABLE release.categories_shadow
  ADD CONSTRAINT category_fkey FOREIGN KEY (id)
    REFERENCES release.categories (id)
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT artifact_shadow_fkey FOREIGN KEY (id, version)
    REFERENCES release.artifacts_shadow (id, version) MATCH FULL
    ON UPDATE NO ACTION ON DELETE CASCADE;