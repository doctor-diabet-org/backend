ALTER TABLE release.identity_artifact_subscriptions
  ADD CONSTRAINT identity_fkey FOREIGN KEY (identity)
    REFERENCES release.identities (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT artifact_fkey FOREIGN KEY (artifact)
    REFERENCES release.artifacts (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;
