package org.doctordiabet.datalayer.services.base

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.identity.toIdentityLink
import org.doctordiabet.datalayer.dbo.metadata.Artifact
import org.doctordiabet.datalayer.dbo.metadata.ArtifactMetadata
import org.doctordiabet.datalayer.dbo.metadata.ArtifactTag
import org.doctordiabet.datalayer.dbo.metadata.toArtifactTag
import org.doctordiabet.domainlayer.exceptions.*
import org.jooq.Configuration
import org.jooq.impl.DSL
import org.postgresql.util.PSQLException
import java.util.*

abstract class AbstractPersistenceService(protected val adapter: PostgresAdapter) {

    protected enum class VersionType {
        CREATED ,
        MODIFIED,
        DELETED ;
    }

    private val table                = Tables.ARTIFACTS
    private val shadow               = Tables.ARTIFACTS_SHADOW
    private val view                 = Tables.ARTIFACT_CURRENT_VERSIONS
    private val tags                 = Tables.TAGS
    private val tagsBinding          = Tables.ARTIFACT_TAGS_LINK
    private val votesBinding         = Tables.IDENTITY_ARTIFACT_VOTES
    private val subscriptionsBinding = Tables.IDENTITY_ARTIFACT_SUBSCRIPTIONS
    private val identities           = Tables.IDENTITY_CURRENT_VERSIONS

    protected fun subscribeInternal(configuration: Configuration, identityId: UUID, artifactId: UUID): Boolean {
        val exists = DSL.using(configuration)
                .fetchExists(
                        DSL.select(subscriptionsBinding.IDENTITY, subscriptionsBinding.ARTIFACT)
                                .from(subscriptionsBinding)
                                .where(subscriptionsBinding.IDENTITY.eq(identityId).and(subscriptionsBinding.ARTIFACT.eq(artifactId)))
                )

        if(!exists) {
            DSL.using(configuration)
                    .insertInto(subscriptionsBinding)
                    .columns(subscriptionsBinding.IDENTITY, subscriptionsBinding.ARTIFACT)
                    .values(identityId, artifactId)
                    .execute()
        }

        return !exists
    }

    protected fun unsubscribeInternal(configuration: Configuration, identityId: UUID, artifactId: UUID): Boolean {
        val exists = DSL.using(configuration)
                .fetchExists(
                        DSL.select(subscriptionsBinding.IDENTITY, subscriptionsBinding.ARTIFACT)
                                .from(subscriptionsBinding)
                                .where(subscriptionsBinding.IDENTITY.eq(identityId).and(subscriptionsBinding.ARTIFACT.eq(artifactId)))
                )

        if(exists) {
            DSL.using(configuration)
                    .deleteFrom(subscriptionsBinding)
                    .where(subscriptionsBinding.IDENTITY.eq(identityId).and(subscriptionsBinding.ARTIFACT.eq(artifactId)))
                    .execute()
        }

        return exists
    }

    protected fun subscribersInternal(configuration: Configuration, artifactId: UUID): List<UUID>
            = DSL.using(configuration)
            .select(subscriptionsBinding.IDENTITY)
            .from(subscriptionsBinding)
            .where(subscriptionsBinding.ARTIFACT.eq(artifactId))
            .fetch(subscriptionsBinding.IDENTITY)

    protected fun createTagInternal(configuration: Configuration, value: String): ArtifactTag {
        val existing = DSL.using(configuration)
                .select()
                .from(tags)
                .where(tags.VALUE.eq(value))
                .fetchOptional()
                .map { ArtifactTag(it[tags.ID], it[tags.VALUE]) }

        return existing.orElseGet {
            DSL.using(configuration)
                    .insertInto(tags)
                    .columns(tags.VALUE)
                    .values(value)
                    .returning()
                    .fetchOne()
                    .map {
                        ArtifactTag(
                                id = it[tags.ID],
                                value = it[tags.VALUE]
                        )
                    }
        }
    }

    protected fun createTagsInternal(configuration: Configuration, values: List<String>): List<ArtifactTag> {
        val existing = DSL.using(configuration)
                .select(tags.VALUE)
                .from(tags)
                .where(tags.VALUE.`in`(values))
                .fetch()
                .map {
                    it[tags.VALUE]
                }

        var query = DSL.using(configuration)
                .insertInto(tags)
                .columns(tags.VALUE)

        for(value in values.filterNot { existing.contains(it) }) {
            query = query.values(value)
        }

        return query.returning()
                .fetch()
                .map {
                    ArtifactTag(
                            id = it[tags.ID],
                            value = it[tags.VALUE]
                    )
                }
    }

    protected fun readTagsInternal(configuration: Configuration): List<ArtifactTag> =
            DSL.using(configuration)
                    .select()
                    .from(tags)
                    .fetch()
                    .map {
                        ArtifactTag(it[tags.ID], it[tags.VALUE])
                    }

    protected fun attachTagsInternal(configuration: Configuration, artifactId: UUID, tagIds: List<UUID>): Unit {
        var tagBindings = DSL.using(configuration)
                .insertInto(tagsBinding)
                .columns(tagsBinding.ARTIFACT_ID, tagsBinding.TAG_ID)

        for(tagId in tagIds) {
            tagBindings = tagBindings.values(artifactId, tagId)
        }

        tagBindings.onDuplicateKeyIgnore().execute()
    }

    protected fun detachTagsInternal(configuration: Configuration, artifactId: UUID, tagIds: List<UUID>): Unit {
        DSL.using(configuration)
                .deleteFrom(tagsBinding)
                .where(tagsBinding.TAG_ID.`in`(tagIds).and(tagsBinding.ARTIFACT_ID.eq(artifactId)))
                .execute()
    }

    protected fun voteInternal(configuration: Configuration, identityId: UUID, artifactId: UUID, value: Int): Boolean {
        val current = DSL.using(configuration)
                .select(votesBinding.VALUE)
                .from(votesBinding)
                .where(votesBinding.IDENTITY.eq(identityId).and(votesBinding.ARTIFACT.eq(artifactId)))
                .fetchOptional()
                .map { it[votesBinding.VALUE] }

        current.filter { it == value }
                .orElseGet {
                    DSL.using(configuration)
                            .deleteFrom(votesBinding)
                            .where(votesBinding.IDENTITY.eq(identityId).and(votesBinding.ARTIFACT.eq(artifactId)))
                            .execute()

                    DSL.using(configuration)
                            .insertInto(votesBinding)
                            .columns(votesBinding.IDENTITY, votesBinding.ARTIFACT, votesBinding.VALUE)
                            .values(identityId, artifactId, value)
                            .returning()
                            .fetchOne()
                            .map { it[votesBinding.VALUE] }
                }

        //if the supplied value already presented – false, else – true
        return current.filter { it == value }.isPresent
    }

    protected fun createArtifact(configuration: Configuration, type: ArtifactType): Artifact = DSL.using(configuration)
            .insertInto(table)
            .columns(table.TYPE)
            .values(type)
            .returning()
            .fetchOne()
            .map {
                Artifact(
                        id = it[table.ID],
                        type = it[table.TYPE],
                        createdTime = it[table.CREATED_TIME],
                        createdByIdentity = it[table.CREATED_BY_IDENTITY]
                )
            }

    protected fun createArtifact(configuration: Configuration, type: ArtifactType, creatingByIdentity: UUID): Artifact = DSL.using(configuration)
            .insertInto(table)
            .columns(table.TYPE, table.CREATED_BY_IDENTITY)
            .values(type, creatingByIdentity)
            .returning()
            .fetchOne()
            .map {
                Artifact(
                        id = it[table.ID],
                        type = it[table.TYPE],
                        createdTime = it[table.CREATED_TIME],
                        createdByIdentity = it[table.CREATED_BY_IDENTITY]
                )
            }

    protected fun createShadow(configuration: Configuration, artifact: Artifact): ArtifactMetadata = DSL.using(configuration)
            .insertInto(shadow)
            .columns(shadow.ID, shadow.VERSION, shadow.TYPE, shadow.LAST_MODIFIED_TIME, shadow.LAST_MODIFIED_BY_IDENTITY, shadow.CREATED)
            .values(artifact.id, 0, artifact.type, artifact.createdTime, artifact.createdByIdentity, true)
            .returning()
            .fetchOne()
            .map {
                ArtifactMetadata(
                        id = it[shadow.ID],
                        version = it[shadow.VERSION],
                        type = it[shadow.TYPE],
                        createdTime = artifact.createdTime,
                        createdByIdentity = DSL.using(configuration).select(identities.ID, identities.FIRST_NAME, identities.LAST_NAME).from(identities).where(identities.ID.eq(artifact.createdByIdentity)).fetchOne().toIdentityLink(),
                        lastModifiedTime = it[shadow.LAST_MODIFIED_TIME],
                        lastModifiedByIdentity = DSL.using(configuration).select(identities.ID, identities.FIRST_NAME, identities.LAST_NAME).from(identities).where(identities.ID.eq(it[shadow.LAST_MODIFIED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                        created = it[shadow.CREATED],
                        modified = it[shadow.UPDATED],
                        deleted = it[shadow.DELETED],
                        votes = 0,
                        tags = emptySet()
                )
            }

    protected fun updateShadow(configuration: Configuration, id: UUID, lastModifiedByIdentity: UUID, versionType: VersionType): ArtifactMetadata? {
        return getShadow(configuration, id)?.let { artifactShadow ->
            when(versionType) {
                VersionType.CREATED -> DSL.using(configuration).insertInto(shadow)
                        .columns(shadow.ID, shadow.VERSION, shadow.TYPE, shadow.LAST_MODIFIED_BY_IDENTITY, shadow.CREATED)
                        .values(artifactShadow.id, artifactShadow.version + 1, artifactShadow.type, lastModifiedByIdentity, true)
                VersionType.MODIFIED -> DSL.using(configuration).insertInto(shadow)
                        .columns(shadow.ID, shadow.VERSION, shadow.TYPE, shadow.LAST_MODIFIED_BY_IDENTITY, shadow.UPDATED)
                        .values(artifactShadow.id, artifactShadow.version + 1, artifactShadow.type, lastModifiedByIdentity, true)
                VersionType.DELETED -> DSL.using(configuration).insertInto(shadow)
                        .columns(shadow.ID, shadow.VERSION, shadow.TYPE, shadow.LAST_MODIFIED_BY_IDENTITY, shadow.DELETED)
                        .values(artifactShadow.id, artifactShadow.version + 1, artifactShadow.type, lastModifiedByIdentity, true)
            }.execute()
        }
                ?.let { getShadow(configuration, id) }
    }

    protected fun getType(configuration: Configuration, id: UUID): ArtifactType? {
        return DSL.using(configuration)
                .select(view.TYPE)
                .from(view)
                .where(view.ID.eq(id))
                .fetchAny()
                ?.map { it[view.TYPE] }
    }

    protected fun getOwner(configuration: Configuration, id: UUID): UUID? {
        return DSL.using(configuration)
                .select(view.CREATED_BY_IDENTITY)
                .from(view)
                .where(view.ID.eq(id))
                .fetchAny()
                ?.map { it[view.CREATED_BY_IDENTITY] }
    }

    private fun getShadow(configuration: Configuration, id: UUID): ArtifactMetadata? = DSL.using(configuration)
            .select()
            .from(view)
            .where(view.ID.eq(id))
            .fetchAny()
            ?.map {
                ArtifactMetadata(
                        id = it[view.ID],
                        version = it[view.VERSION],
                        type = it[view.TYPE],
                        createdTime = it[view.CREATED_TIME],
                        createdByIdentity = DSL.using(configuration).select(identities.ID, identities.FIRST_NAME, identities.LAST_NAME).from(identities).where(identities.ID.eq(it[view.CREATED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                        lastModifiedTime = it[view.LAST_MODIFIED_TIME],
                        lastModifiedByIdentity = DSL.using(configuration).select(identities.ID, identities.FIRST_NAME, identities.LAST_NAME).from(identities).where(identities.ID.eq(it[view.LAST_MODIFIED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                        created = it[view.CREATED],
                        modified = it[view.UPDATED],
                        deleted = it[view.DELETED],
                        votes = it[view.VOTES].toInt(),
                        tags = it[view.TAGS].let {
                            if(it is ArrayNode && it.size() != 0) {
                                List(it.size(), { index ->
                                    (it[index] as ObjectNode).toArtifactTag()
                                }).toSet()
                            } else {
                                emptySet()
                            }
                        }
                )
            }

    protected fun resourceNotFound(id: UUID): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.BUSINESS)
        val accessor = bundle.accessor as BusinessExceptionAccessor
        accessor.code = BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST
        accessor.message = "Resource with id: $id does not exist"
        return bundle
    }

    protected fun resourceNotFound(message: String): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.BUSINESS)
        val accessor = bundle.accessor as BusinessExceptionAccessor
        accessor.code = BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST
        accessor.message = "Resource does not exist; ${message}"
        return bundle
    }

    protected fun resourceDeleted(id: UUID): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.BUSINESS)
        val accessor = bundle.accessor as BusinessExceptionAccessor
        accessor.code = BusinessExceptionAccessor.CODE_ARTIFACT_DELETED
        accessor.message = "Resource with id: $id was deleted"
        return bundle
    }

    protected fun PSQLException.parse(): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.DATABASE)
        val accessor = bundle.accessor as DatabaseExceptionAccessor
        accessor.code = errorCode
        accessor.message = serverErrorMessage.toString()
        accessor.cause = this@parse
        return bundle
    }

    protected fun Throwable.parse(): ExceptionBundle {
        printStackTrace()
        val bundle = ExceptionBundle(ExceptionSource.INTERNAL)
        val accessor = bundle.accessor as InternalExceptionAccessor
        accessor.message = message ?: localizedMessage ?: "N/A"
        accessor.cause = this@parse
        return bundle
    }
}