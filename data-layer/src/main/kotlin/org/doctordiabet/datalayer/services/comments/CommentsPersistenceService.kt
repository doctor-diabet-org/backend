package org.doctordiabet.datalayer.services.comments

import io.reactivex.Single
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.comments.*
import org.doctordiabet.datalayer.services.base.AbstractPersistenceService
import org.jooq.Condition
import org.jooq.impl.DSL
import org.postgresql.util.PSQLException
import java.sql.Timestamp
import java.util.*

class CommentsPersistenceService(adapter: PostgresAdapter): AbstractPersistenceService(adapter) {

    private val artifacts = Tables.ARTIFACTS_SHADOW
    private val table = Tables.COMMENTS
    private val shadow = Tables.COMMENTS_SHADOW

    private val view = Tables.COMMENT_CURRENT_VERSIONS

    fun create(model: CommentCreateModel): Single<Pair<ArtifactType, CommentDBO>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(artifacts)
                        .where(artifacts.ID.eq(model.artifactReferralId).and(artifacts.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(model.artifactReferralId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(artifacts)
                        .where(artifacts.ID.eq(model.artifactReferralId))
                )) {
                    emitter.onError(resourceNotFound(model.artifactReferralId))
                    return@withTransaction
                }

                //create the object metadata
                val artifact = when(model.modifyingByIdentity) {
                    null -> createArtifact(configuration, ArtifactType.comment)
                    else -> createArtifact(configuration, ArtifactType.comment, model.modifyingByIdentity)
                }
                //create the object metadata snapshot
                val artifactShadow = createShadow(configuration, artifact)
                //create the object itself
                DSL.using(configuration)
                        .insertInto(table)
                        .columns(table.ID, table.ARTIFACT_REFERRAL_ID, table.CONTENT_TEXT)
                        .values(artifact.id, model.artifactReferralId, model.contentText)
                        .execute()

                //create the object snapshot
                DSL.using(configuration)
                        .insertInto(shadow)
                        .columns(shadow.ID, shadow.VERSION, shadow.ARTIFACT_REFERRAL_ID, shadow.CONTENT_TEXT)
                        .values(artifact.id, artifactShadow.version, model.artifactReferralId, model.contentText)
                        .execute()

                val referralType = getType(configuration, model.artifactReferralId)

                if(referralType != null) {
                    DSL.using(configuration)
                            .select()
                            .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                            .from(view)
                            .where(view.ID.eq(artifact.id))
                            .fetchOne()
                            .map { it.toComment(configuration) }
                            .let { emitter.onSuccess(referralType to it) }
                } else {
                    emitter.onError(resourceNotFound(model.artifactReferralId))
                }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(id: UUID): Single<CommentDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(id).and(view.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(id))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .select()
                        .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                        .from(view)
                        .where(view.ID.eq(id).and(view.DELETED.eq(false)))
                        .fetchAny()
                        ?.map { it.toComment(configuration) }
                        ?.let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(offset: Int, limit: Int, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                        .from(view)
                        .where(view.DELETED.eq(false)
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toComment(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .select(DSL.countDistinct(view.ID))
                                    .from(view)
                                    .where(view.DELETED.eq(false)
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(offset: Int, limit: Int, modifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.gt(modifiedSince)
                                .and(view.DELETED.eq(false))
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toComment(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .select(DSL.countDistinct(view.ID))
                                    .from(view)
                                    .where(view.LAST_MODIFIED_TIME.gt(modifiedSince)
                                            .and(view.DELETED.eq(false))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readUnmodified(offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                        .from(view)
                        .where(view.DELETED.eq(false)
                                .and(view.LAST_MODIFIED_TIME.le(unmodifiedSince))
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toComment(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .select(DSL.countDistinct(view.ID))
                                    .from(view)
                                    .where(view.DELETED.eq(false)
                                            .and(view.LAST_MODIFIED_TIME.le(unmodifiedSince))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readModified(offset: Int, limit: Int, modifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                        .from(view)
                        .where(view.DELETED.eq(false)
                                .and(view.LAST_MODIFIED_TIME.gt(modifiedSince))
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toComment(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .select(DSL.countDistinct(view.ID))
                                    .from(view)
                                    .where(view.DELETED.eq(false)
                                            .and(view.LAST_MODIFIED_TIME.gt(modifiedSince))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readUnmodified(rootArtifactReferralId: UUID, offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(artifacts)
                        .where(artifacts.ID.eq(rootArtifactReferralId).and(artifacts.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(rootArtifactReferralId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(artifacts)
                        .where(artifacts.ID.eq(rootArtifactReferralId))
                )) {
                    emitter.onError(resourceNotFound(rootArtifactReferralId))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .select()
                        .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                        .from(view)
                        .where(view.ROOT_ARTIFACT_REFERRAL_ID.eq(rootArtifactReferralId)
                                .and(view.DELETED.eq(false))
                                .and(view.LAST_MODIFIED_TIME.le(unmodifiedSince))
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toComment(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .select(DSL.countDistinct(view.ID))
                                    .from(view)
                                    .where(view.ROOT_ARTIFACT_REFERRAL_ID.eq(rootArtifactReferralId)
                                            .and(view.DELETED.eq(false))
                                            .and(view.LAST_MODIFIED_TIME.le(unmodifiedSince))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readModified(rootArtifactReferralId: UUID, offset: Int, limit: Int, modifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(artifacts)
                        .where(artifacts.ID.eq(rootArtifactReferralId).and(artifacts.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(rootArtifactReferralId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(artifacts)
                        .where(artifacts.ID.eq(rootArtifactReferralId))
                )) {
                    emitter.onError(resourceNotFound(rootArtifactReferralId))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .select()
                        .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                        .from(view)
                        .where(view.ROOT_ARTIFACT_REFERRAL_ID.eq(rootArtifactReferralId)
                                .and(view.DELETED.eq(false))
                                .and(view.LAST_MODIFIED_TIME.gt(modifiedSince))
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toComment(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .select(DSL.countDistinct(view.ID))
                                    .from(view)
                                    .where(view.ROOT_ARTIFACT_REFERRAL_ID.eq(rootArtifactReferralId)
                                            .and(view.DELETED.eq(false))
                                            .and(view.LAST_MODIFIED_TIME.gt(modifiedSince))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun update(model: CommentUpdateModel): Single<Pair<ArtifactType, CommentDBO>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                //update the artifact shadow

                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(model.id).and(view.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.MODIFIED)?.let { updatedShadow ->
                    //update the comment
                    val comment = DSL.using(configuration)
                            .updateQuery(table)

                    model.contentText?.let { contentText ->
                        comment.addValue(table.CONTENT_TEXT, contentText)
                    }

                    comment.setReturning()
                    comment.execute()
                    //update the comment shadow
                    DSL.using(configuration)
                            .insertInto(shadow)
                            .columns(shadow.ID, shadow.VERSION, shadow.ARTIFACT_REFERRAL_ID, shadow.CONTENT_TEXT)
                            .values(updatedShadow.id, updatedShadow.version, comment.returnedRecord[table.ARTIFACT_REFERRAL_ID], comment.returnedRecord[table.CONTENT_TEXT])
                            .execute()

                    val referralType = getType(configuration, comment.returnedRecord[table.ARTIFACT_REFERRAL_ID])

                    if(referralType != null) {
                        DSL.using(configuration)
                                .select()
                                .hint("distinct on(\"release\".\"comment_current_versions\".\"id\")")
                                .from(view)
                                .where(view.ID.eq(model.id))
                                .fetchOne()
                                .map { it.toComment(configuration) }
                                .let { emitter.onSuccess(referralType to it) }
                    } else {
                        emitter.onError(resourceNotFound(comment.returnedRecord[table.ARTIFACT_REFERRAL_ID]))
                    }
                } ?: emitter.onError(resourceNotFound(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(model: CommentDeleteModel): Single<Pair<ArtifactType, UUID>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                //update the artifact shadow

                if(DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(model.id).and(view.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.DELETED)?.let { updatedShadow ->
                    val comment = DSL.using(configuration)
                            .select()
                            .from(table)
                            .where(table.ID.eq(model.id))
                            .fetchOne()

                    val referralType = getType(configuration, comment[table.ARTIFACT_REFERRAL_ID])

                    if(referralType != null) {
                        //update the comment shadow
                        DSL.using(configuration).insertInto(shadow)
                                .columns(shadow.ID, shadow.VERSION, shadow.ARTIFACT_REFERRAL_ID, shadow.CONTENT_TEXT)
                                .values(updatedShadow.id, updatedShadow.version, comment[table.ARTIFACT_REFERRAL_ID], comment[table.CONTENT_TEXT])
                                .returning(shadow.ID)
                                .fetchOne()
                                .let { emitter.onSuccess(referralType to it[shadow.ID]) }
                    } else {
                        emitter.onError(resourceNotFound(comment[table.ARTIFACT_REFERRAL_ID]))
                    }
                } ?: emitter.onError(resourceNotFound(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    private fun Condition.withTags(tagIds: List<UUID>): Condition = let {
        if (tagIds.isNotEmpty()) {
            val queryText = StringBuilder("exists(select * from json_array_elements(${view.TAGS.name}) where")

            for ((index, tagId) in tagIds.withIndex()) {
                queryText.append(" value::jsonb @> '{\"id\":\"$tagId\"}'")
                if (index + 1 != tagIds.size) {
                    queryText.append(" and")
                }
            }

            queryText.append(")")

            it.and(queryText.toString())
        } else {
            it
        }
    }
}