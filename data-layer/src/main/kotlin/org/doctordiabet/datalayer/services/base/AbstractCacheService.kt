package org.doctordiabet.datalayer.services.base

import org.doctordiabet.datalayer.database.core.RedisAdapter
import org.doctordiabet.domainlayer.exceptions.*
import redis.clients.jedis.exceptions.JedisException
import java.util.*
import kotlin.coroutines.experimental.buildSequence

open class AbstractCacheService(private val adapter: RedisAdapter) {

    protected fun set(bean: RedisBean, duplicateScoreProperties: Boolean = false, duplicateLexProperties: Boolean = false): Unit = adapter.withRedis {
        val key = "${bean.entityType}:${bean.hash}"

        for(property in bean.properties) {
            when(property) {
                is RedisBeanIndexPropertyPrototype -> {
                    val secondaryIndexKey = "${bean.entityType}:${property.field}"
                    //add the property index
                    zadd(secondaryIndexKey, property.binding.toScore(property.value), bean.hash)
                    if(duplicateScoreProperties) {
                        //add the property itself to the entity
                        hset(key, property.field, property.value.toString())
                    }
                }
                is RedisBeanLexIndexPropertyPrototype -> {
                    val secondaryIndexKey = "${bean.entityType}:${property.field}"
                    //add the property lex index
                    zadd(secondaryIndexKey, 0.0, "${property.value}:${bean.hash}")
                    if(duplicateLexProperties) {
                        //add the property itself to the entity
                        hset(key, property.field, property.value)
                    }
                }
                else -> {
                    //add the property itself to the entity
                    hset(key, property.field, property.value.toString())
                }
            }
        }
    }

    protected fun add(index: RedisBeanIndex): Unit = adapter.withRedis {
        zadd(index.entityType, index.score, index.hash)
    }

    protected fun add(bean: RedisSetBean): Unit = adapter.withRedis {
        sadd("${bean.entityType}:${bean.hash}", bean.value)
    }

    protected fun get(query: RedisSingleQuery): Map<String, String?>? = adapter.withRedis {
        val key = "${query.entityType}:${query.hash}"
        return@withRedis hgetAll(key)
    }

    protected fun get(query: RedisRangeQuery): Set<String> = adapter.withRedis {
        val key = "${query.entityType}:${query.field}"
        return@withRedis zrange(key, query.offset, query.limit)
    }

    protected fun get(query: RedisRangeScoreQuery): Set<String> = adapter.withRedis {
        val key = "${query.entityType}:${query.field}"
        val fromScore = when(query.fromScore) {
            null -> "-inf"
            else -> "(${query.fromScore.toString()}"
        }
        val toScore = when(query.toScore) {
            null -> "+inf"
            else -> "(${query.toScore.toString()}"
        }
        return@withRedis when(query.order) {
            OrderBy.ASC -> zrangeByScore(key, fromScore, toScore, query.offset, query.limit)
            OrderBy.DESC -> zrevrangeByScore(key, toScore, fromScore, query.offset, query.limit)
        }
    }

    protected fun get(query: RedisRangeLexScoreQuery): Set<String> = adapter.withRedis {
        val key = "${query.entityType}:${query.field}"
        val fromScore = when(query.fromScore) {
            null -> "-"
            else -> "[${query.fromScore}"
        }
        val toScore = when(query.toScore) {
            null -> "-"
            else -> "[${query.toScore}"
        }

        return@withRedis when(query.order) {
            //the form of lex-indexed set member is value:hash, where hash is bean identifier, mostly in the form of uuid.
            //so to return the ids we should take the second part of the set member and make set of them
            OrderBy.ASC -> zrangeByLex(key, fromScore, toScore, query.offset, query.limit).map { it.split(' ')[1] }.toSet()
            OrderBy.DESC -> zrevrangeByLex(key, toScore, fromScore, query.offset, query.limit).map { it.split(' ')[1] }.toSet()
        }
    }

    protected fun get(query: RedisRangeScoreQueryRaw): Set<String> = adapter.withRedis {
        val key = query.entityType
        val fromScore = when(query.fromScore) {
            null -> "-"
            else -> "[${query.fromScore}"
        }
        val toScore = when(query.toScore) {
            null -> "-"
            else -> "[${query.toScore}"
        }

        return@withRedis when(query.order) {
        //the form of score-indexed set member is value,
            OrderBy.ASC -> zrangeByScore(key, fromScore, toScore, query.offset, query.limit).map { it.split(' ')[1] }.toSet()
            OrderBy.DESC -> zrevrangeByScore(key, toScore, fromScore, query.offset, query.limit).map { it.split(' ')[1] }.toSet()
        }
    }

    protected fun get(prototype: RedisSetBeanPrototype): Set<String> = adapter.withRedis {
        smembers("${prototype.entityType}:${prototype.hash}")
    }

    protected fun exists(bean: RedisSetBean): Boolean = adapter.withRedis {
        sismember("${bean.entityType}:${bean.hash}", bean.value)
    }

    protected fun get(query: RedisHashSetQuery, appendEntityType: Boolean = true): List<Map<String, String?>?> = adapter.withRedis {
        return@withRedis buildSequence<Map<String, String?>?> {
            for(hash in query.hashes) {
                when(appendEntityType) {
                    true -> yield(hgetAll("${query.entityType}:${hash}"))
                    else -> yield(hgetAll(hash))
                }
            }
        }.toList()
    }

    protected fun remove(prototype: RedisSingleQuery): Unit = adapter.withRedis {
        del("${prototype.entityType}:${prototype.hash}")
    }

    protected fun remove(bean: RedisSetBean) = adapter.withRedis {
        srem("${bean.entityType}:${bean.hash}", bean.value)
    }

    protected fun resourceNotFound(contents: String): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.BUSINESS)
        val accessor = bundle.accessor as BusinessExceptionAccessor
        accessor.code = BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST
        accessor.message = "Resource with contents: $contents does not exist"
        return bundle
    }

    protected fun resourceDeleted(contents: String): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.BUSINESS)
        val accessor = bundle.accessor as BusinessExceptionAccessor
        accessor.code = BusinessExceptionAccessor.CODE_ARTIFACT_DELETED
        accessor.message = "Resource with contents: $contents was deleted"
        return bundle
    }

    protected fun resourceNotFound(id: UUID): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.BUSINESS)
        val accessor = bundle.accessor as BusinessExceptionAccessor
        accessor.code = BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST
        accessor.message = "Resource with id: $id does not exist"
        return bundle
    }

    protected fun resourceDeleted(id: UUID): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.DATABASE)
        val accessor = bundle.accessor as BusinessExceptionAccessor
        accessor.code = BusinessExceptionAccessor.CODE_ARTIFACT_DELETED
        accessor.message = "Resource with id: $id was deleted"
        return bundle
    }

    protected fun JedisException.parse() = ExceptionBundle(ExceptionSource.DATABASE).apply {
        (accessor as DatabaseExceptionAccessor).apply {
            code = DatabaseExceptionAccessor.CODE_REDIS
            message = this@parse.message ?: this@parse.localizedMessage
            cause = this@parse
        }
    }

    protected fun Throwable.parse(): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.INTERNAL)
        val accessor = bundle.accessor as InternalExceptionAccessor
        accessor.message = message ?: localizedMessage ?: "N/A"
        accessor.cause = this
        printStackTrace()
        return bundle
    }
}

interface RedisBean {

    val hash: String
    val entityType: String
    val properties: Set<RedisBeanPropertyPrototype<Any?>>

    data class Impl(
            override val hash: String,
            override val entityType: String,
            override val properties: Set<RedisBeanPropertyPrototype<Any?>>
    ): RedisBean
}

interface RedisSetBean {

    val hash: String
    val entityType: String
    val value: String

    data class Impl(
            override val hash: String,
            override val entityType: String,
            override val value: String
    ): RedisSetBean
}

interface RedisBeanIndex {

    val hash: String
    val entityType: String
    val score: Double

    data class Impl(
            override val hash: String,
            override val entityType: String,
            override val score: Double
    ): RedisBeanIndex
}

interface RedisSetBeanPrototype {

    val hash: String
    val entityType: String

    data class Impl(
            override val hash: String,
            override val entityType: String
    ): RedisSetBeanPrototype
}

interface RedisSingleQuery {

    val hash: String
    val entityType: String

    data class Impl(
            override val hash: String,
            override val entityType: String
    ): RedisSingleQuery
}

interface RedisRangeQuery {

    val entityType: String
    val field: String
    val offset: Long
    val limit: Long
    val order: OrderBy

    data class Impl(
            override val entityType: String,
            override val field: String     ,
            override val offset: Long        = 0,
            override val limit: Long         = -1,
            override val order: OrderBy     = OrderBy.DESC
    ): RedisRangeQuery
}

interface RedisRangeScoreQueryRaw {

    val entityType: String
    val fromScore: Double?
    val toScore: Double?
    val offset: Int
    val limit: Int
    val order: OrderBy

    data class Impl(
            override val entityType: String ,
            override val fromScore: Double? ,
            override val toScore: Double?   ,
            override val offset: Int        = 0,
            override val limit: Int         = -1,
            override val order: OrderBy     = OrderBy.DESC
    ): RedisRangeScoreQueryRaw
}

interface RedisRangeScoreQuery {

    val entityType: String
    val field: String
    val fromScore: Double?
    val toScore: Double?
    val offset: Int
    val limit: Int
    val order: OrderBy

    data class Impl(
            override val entityType: String ,
            override val field: String      ,
            override val fromScore: Double? ,
            override val toScore: Double?   ,
            override val offset: Int        = 0,
            override val limit: Int         = -1,
            override val order: OrderBy     = OrderBy.DESC
    ): RedisRangeScoreQuery
}

interface RedisRangeLexScoreQuery {

    val entityType: String
    val field: String
    val fromScore: String?
    val toScore: String?
    val offset: Int
    val limit: Int
    val order: OrderBy

    data class Impl(
            override val entityType: String ,
            override val field: String      ,
            override val fromScore: String? ,
            override val toScore: String?   ,
            override val offset: Int        = 0,
            override val limit: Int         = -1,
            override val order: OrderBy     = OrderBy.DESC
    ): RedisRangeLexScoreQuery
}

interface RedisHashSetQuery {

    val entityType: String
    val hashes: Set<String>

    data class Impl(
            override val entityType: String,
            override val hashes: Set<String>
    ): RedisHashSetQuery
}

interface RedisBeanPropertyPrototype<out T> {

    val field: String
    val value: T

    data class Impl<out T>(
            override val field: String,
            override val value: T
    ): RedisBeanPropertyPrototype<T>
}

interface RedisBeanIndexPropertyPrototype<T>: RedisBeanPropertyPrototype<T> {

    val binding: RedisBeanIndexBinding<T>

    data class Impl<T>(
            override val field: String,
            override val value: T,
            override val binding: RedisBeanIndexBinding<T>
    ): RedisBeanIndexPropertyPrototype<T>
}

interface RedisBeanLexIndexPropertyPrototype: RedisBeanPropertyPrototype<String> {

    override val field: String
    override val value: String

    data class Impl(
            override val field: String,
            override val value: String
    ): RedisBeanLexIndexPropertyPrototype
}

interface RedisBeanIndexBinding<in T> {

    fun toScore(property: T): Double
}

enum class OrderBy {
    ASC  ,
    DESC ;
}

