package org.doctordiabet.datalayer.services.identities

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.identity.*
import org.doctordiabet.datalayer.services.base.AbstractPersistenceService
import org.jooq.impl.DSL
import org.postgresql.util.PSQLException
import java.sql.Timestamp
import java.util.*

class IdentitiesPersistenceService(adapter: PostgresAdapter): AbstractPersistenceService(adapter) {

    private val table = Tables.IDENTITIES
    private val shadow = Tables.IDENTITIES_SHADOW

    private val view = Tables.IDENTITY_CURRENT_VERSIONS

    fun create(model: IdentityCreateModel): Single<IdentityDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                //create the object metadata
                val artifact = when(model.creatingByIdentity) {
                    null -> createArtifact(configuration, ArtifactType.identity)
                    else -> createArtifact(configuration, ArtifactType.identity, model.creatingByIdentity)
                }
                //create the object metadata snapshot
                val artifactShadow = createShadow(configuration, artifact)
                //create the object itself
                DSL.using(configuration)
                        .insertInto(table)
                        .columns(table.ID, table.FIRST_NAME, table.LAST_NAME, table.PHONE_NUMBER, table.EMAIL, table.PASSWORD_HASH)
                        .values(artifact.id, model.firstName, model.lastName, model.phoneNumber, model.email, model.passwordHash)
                        .execute()
                //create the object snapshot
                DSL.using(configuration)
                        .insertInto(shadow)
                        .columns(shadow.ID, shadow.VERSION, shadow.FIRST_NAME, shadow.LAST_NAME, shadow.PHONE_NUMBER, shadow.EMAIL, shadow.PASSWORD_HASH)
                        .values(artifact.id, artifactShadow.version, model.firstName, model.lastName, model.phoneNumber, model.email, model.passwordHash)
                        .execute()

                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.ID.eq(artifact.id))
                        .fetchOne()
                        .map { it.toIdentity(configuration) }
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(id: UUID): Single<IdentityDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.ID.eq(id))
                        .fetchAny()
                        ?.map { it.toIdentity(configuration) }
                        ?.let {
                            if(it.metadata.deleted) {
                                emitter.onError(resourceDeleted(id))
                            } else {
                                emitter.onSuccess(it)
                            }
                        }
                        ?: emitter.onError(resourceNotFound(id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readByPhone(phoneNumber: String): Single<IdentityDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.PHONE_NUMBER.eq(phoneNumber))
                        .fetchAny()
                        ?.map { it.toIdentity(configuration) }
                        ?.let {
                            if(it.metadata.deleted) {
                                emitter.onError(resourceDeleted(it.metadata.id))
                            } else {
                                emitter.onSuccess(it)
                            }
                        }
                        ?: emitter.onError(resourceNotFound(phoneNumber))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readByEmail(email: String): Single<IdentityDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.EMAIL.eq(email))
                        .fetchAny()
                        ?.map { it.toIdentity(configuration) }
                        ?.let {
                            if(it.metadata.deleted) {
                                emitter.onError(resourceDeleted(it.metadata.id))
                            } else {
                                emitter.onSuccess(it)
                            }
                        }
                        ?: emitter.onError(resourceNotFound(email))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(offset: Int, limit: Int): Single<Pair<Int, List<IdentityDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.DELETED.eq(false))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toIdentity(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.DELETED.eq(false))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readModified(offset: Int, limit: Int, modifiedTime: Timestamp): Single<Pair<Int, List<IdentityDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.gt(modifiedTime).and(view.DELETED.eq(false)))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toIdentity(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.LAST_MODIFIED_TIME.gt(modifiedTime).and(view.DELETED.eq(false)))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readUnmodified(offset: Int, limit: Int, unmodifiedTime: Timestamp): Single<Pair<Int, List<IdentityDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.le(unmodifiedTime).and(view.DELETED.eq(false)))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toIdentity(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.LAST_MODIFIED_TIME.le(unmodifiedTime).and(view.DELETED.eq(false)))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(offset: Int, limit: Int, nameSubstring: String): Single<Pair<Int, List<IdentityDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.FIRST_NAME.concat(" ").concat(view.LAST_NAME).lower().like("${nameSubstring.toLowerCase()}%").and(view.DELETED.eq(false)))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toIdentity(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.FIRST_NAME.concat(" ").concat(view.LAST_NAME).lower().like("${nameSubstring.toLowerCase()}%").and(view.DELETED.eq(false)))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readModified(offset: Int, limit: Int, nameSubstring: String, timestamp: Timestamp): Single<Pair<Int, List<IdentityDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.FIRST_NAME.concat(" ").concat(view.LAST_NAME).lower().like("${nameSubstring.toLowerCase()}%").and(view.LAST_MODIFIED_TIME.gt(timestamp)).and(view.DELETED.eq(false)))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toIdentity(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.FIRST_NAME.concat(" ").concat(view.LAST_NAME).lower().like("${nameSubstring.toLowerCase()}%").and(view.LAST_MODIFIED_TIME.gt(timestamp)).and(view.DELETED.eq(false)))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readUnmodified(offset: Int, limit: Int, nameSubstring: String, timestamp: Timestamp): Single<Pair<Int, List<IdentityDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.FIRST_NAME.concat(" ").concat(view.LAST_NAME).lower().like("${nameSubstring.toLowerCase()}%").and(view.LAST_MODIFIED_TIME.le(timestamp)).and(view.DELETED.eq(false)))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toIdentity(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.FIRST_NAME.concat(" ").concat(view.LAST_NAME).lower().like("${nameSubstring.toLowerCase()}%").and(view.LAST_MODIFIED_TIME.le(timestamp)).and(view.DELETED.eq(false)))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun update(model: IdentityUpdateModel): Single<IdentityDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->

                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(
                                view.ID.eq(model.id)
                                        .and(view.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                //update the artifact shadow
                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.MODIFIED)?.let { (id, version) ->
                    //update the identity
                    val identity = DSL.using(configuration)
                            .updateQuery(table)

                    model.firstName?.let { firstName ->
                        identity.addValue(table.FIRST_NAME, firstName)
                    }

                    model.lastName?.let { lastName ->
                        identity.addValue(table.LAST_NAME, lastName)
                    }

                    model.phoneNumber?.let { phoneNumber ->
                        identity.addValue(table.PHONE_NUMBER, phoneNumber)
                    }

                    model.email?.let { email ->
                        identity.addValue(table.EMAIL, email)
                    }

                    model.passwordHash?.let { passwordHash ->
                        identity.addValue(table.PASSWORD_HASH, passwordHash)
                    }

                    identity.setReturning()
                    identity.execute()
                    //update the identity shadow
                    DSL.using(configuration)
                            .insertInto(shadow)
                            .columns(shadow.ID, shadow.VERSION, shadow.FIRST_NAME, shadow.LAST_NAME, shadow.PHONE_NUMBER, shadow.EMAIL, shadow.PASSWORD_HASH)
                            .values(id, version, identity.returnedRecord[table.FIRST_NAME], identity.returnedRecord[table.LAST_NAME], identity.returnedRecord[table.PHONE_NUMBER], identity.returnedRecord[table.EMAIL], identity.returnedRecord[table.PASSWORD_HASH])
                            .execute()

                    DSL.using(configuration)
                            .select()
                            .from(view)
                            .where(view.ID.eq(model.id))
                            .fetchOne()
                            .map { it.toIdentity(configuration) }
                            .let { emitter.onSuccess(it) }
                } ?: emitter.onError(resourceNotFound(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(model: IdentityDeleteModel): Single<UUID> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(
                                view.ID.eq(model.id)
                                        .and(view.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                //update the artifact shadow
                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.DELETED)?.let { (id, version) ->
                    val identity = DSL.using(configuration)
                            .select()
                            .from(table)
                            .where(view.ID.eq(model.id))
                            .fetchOne()
                    //update the identity shadow
                    DSL.using(configuration).insertInto(shadow)
                            .columns(shadow.ID, shadow.VERSION, shadow.FIRST_NAME, shadow.LAST_NAME, shadow.PHONE_NUMBER, shadow.EMAIL, shadow.PASSWORD_HASH)
                            .values(id, version, identity[table.FIRST_NAME], identity[table.LAST_NAME], identity[table.PHONE_NUMBER], identity[table.EMAIL], identity[table.PASSWORD_HASH])
                            .returning(shadow.ID)
                            .fetchOne()
                            .let { emitter.onSuccess(
                                    it[shadow.ID]
                            ) }
                } ?: emitter.onError(resourceNotFound(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }
}