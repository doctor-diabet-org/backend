package org.doctordiabet.datalayer.services.topics

import io.reactivex.Single
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.topics.*
import org.doctordiabet.datalayer.services.base.AbstractPersistenceService
import org.jooq.Condition
import org.jooq.impl.DSL
import org.postgresql.util.PSQLException
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.Executors

class TopicsPersistenceService(adapter: PostgresAdapter): AbstractPersistenceService(adapter) {

    private val categories = Tables.CATEGORY_CURRENT_VERSIONS
    private val table = Tables.TOPICS
    private val shadow = Tables.TOPICS_SHADOW

    private val view = Tables.TOPIC_CURRENT_VERSIONS

    fun create(model: TopicCreateModel): Single<TopicDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                //verify the category exists
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(categories)
                        .where(categories.ID.eq(model.categoryId)
                                .and(categories.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(model.categoryId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(categories)
                        .where(categories.ID.eq(model.categoryId))
                )) {
                    emitter.onError(resourceNotFound(model.categoryId))
                    return@withTransaction
                }

                //create the object metadata
                val artifact = when(model.creatingByIdentity) {
                    null -> createArtifact(configuration, ArtifactType.topic)
                    else -> createArtifact(configuration, ArtifactType.topic, model.creatingByIdentity)
                }
                //create the object metadata snapshot
                val artifactShadow = createShadow(configuration, artifact)

                if(model.tagIds.isNotEmpty()) {
                    println("Attaching tags to ${artifact.id}: ${model.tagIds}")
                    attachTagsInternal(configuration, artifact.id, model.tagIds)
                    println("Attached tags to ${artifact.id}: ${model.tagIds}")
                }

                //create the object itself
                DSL.using(configuration)
                        .insertInto(table)
                        .columns(table.ID, table.TITLE, table.DESCRIPTION, table.CATEGORY)
                        .values(artifact.id, model.title, model.description, model.categoryId)
                        .execute()
                //create the object snapshot
                DSL.using(configuration)
                        .insertInto(shadow)
                        .columns(shadow.ID, shadow.VERSION, shadow.TITLE, shadow.DESCRIPTION, shadow.CATEGORY)
                        .values(artifact.id, artifactShadow.version, model.title, model.description, model.categoryId)
                        .execute()

                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.ID.eq(artifact.id))
                        .fetchOne()
                        .map { it.toTopic(configuration) }
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(id: UUID): Single<TopicDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(id)
                                .and(view.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(id))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.ID.eq(id)
                                .and(view.DELETED.eq(false))
                        )
                        .fetchAny()
                        ?.map { it.toTopic(configuration) }
                        ?.let { emitter.onSuccess(it) }
                        ?: emitter.onError(resourceNotFound(id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readUnmodified(offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<TopicDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.le(unmodifiedSince)
                                .and(view.DELETED.eq(false))
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toTopic(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.LAST_MODIFIED_TIME.le(unmodifiedSince)
                                            .and(view.DELETED.eq(false))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readModified(offset: Int, limit: Int, modifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<TopicDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.gt(modifiedSince)
                                .and(view.DELETED.eq(false))
                                .withTags(tagIds))
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toTopic(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.LAST_MODIFIED_TIME.gt(modifiedSince)
                                            .and(view.DELETED.eq(false))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readUnmodified(categoryId: UUID, offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<TopicDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(categories)
                        .where(categories.ID.eq(categoryId)
                                .and(categories.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(categoryId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(categories)
                        .where(categories.ID.eq(categoryId))
                )) {
                    emitter.onError(resourceNotFound(categoryId))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.CATEGORY.eq(categoryId)
                                .and(view.LAST_MODIFIED_TIME.le(unmodifiedSince))
                                .and(view.DELETED.eq(false))
                                .withTags(tagIds)
                        )
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toTopic(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.CATEGORY.eq(categoryId)
                                            .and(view.LAST_MODIFIED_TIME.le(unmodifiedSince))
                                            .and(view.DELETED.eq(false))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readModified(categoryId: UUID, offset: Int, limit: Int, modifiedSince: Timestamp, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<TopicDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(categories)
                        .where(categories.ID.eq(categoryId)
                                .and(categories.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(categoryId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(categories)
                        .where(categories.ID.eq(categoryId))
                )) {
                    emitter.onError(resourceNotFound(categoryId))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.gt(modifiedSince)
                                .and(view.CATEGORY.eq(categoryId))
                                .and(view.DELETED.eq(false))
                                .withTags(tagIds)
                        )
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toTopic(configuration) }
                        .let {
                            val count = DSL.using(configuration)
                                    .selectCount()
                                    .from(view)
                                    .where(view.LAST_MODIFIED_TIME.gt(modifiedSince)
                                            .and(view.CATEGORY.eq(categoryId))
                                            .and(view.DELETED.eq(false))
                                            .withTags(tagIds))
                                    .fetchOne(0, Int::class.java)

                            emitter.onSuccess(count to it)
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun update(model: TopicUpdateModel): Single<TopicDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                //update the artifact shadow

                model.categoryId?.let { categoryId ->
                    if (DSL.using(configuration).fetchExists(DSL.select()
                            .from(categories)
                            .where(categories.ID.eq(categoryId).and(categories.DELETED.eq(true)))
                    )) {
                        emitter.onError(resourceDeleted(categoryId))
                        return@withTransaction
                    }

                    if (!DSL.using(configuration).fetchExists(DSL.select()
                            .from(categories)
                            .where(categories.ID.eq(categoryId))
                    )) {
                        emitter.onError(resourceNotFound(categoryId))
                        return@withTransaction
                    }
                }

                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(model.id).and(view.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.MODIFIED)?.let { (id, version) ->
                    val topic = DSL.using(configuration)
                            .updateQuery(table)

                    model.title?.let { title ->
                        topic.addValue(table.TITLE, title)
                    }

                    model.description?.let { description ->
                        topic.addValue(table.DESCRIPTION, description)
                    }

                    model.categoryId?.let { categoryId ->
                        topic.addValue(table.CATEGORY, categoryId)
                    }

                    topic.setReturning()
                    topic.execute()
                    //update the topic shadow
                    DSL.using(configuration)
                            .insertInto(shadow)
                            .columns(shadow.ID, shadow.VERSION, shadow.TITLE, shadow.DESCRIPTION, shadow.CATEGORY)
                            .values(id, version, topic.returnedRecord[table.TITLE], topic.returnedRecord[table.DESCRIPTION], topic.returnedRecord[table.CATEGORY])
                            .execute()

                    DSL.using(configuration)
                            .select()
                            .from(view)
                            .where(view.ID.eq(model.id))
                            .fetchOne()
                            .map { it.toTopic(configuration) }
                            .let { emitter.onSuccess(it) }
                } ?: emitter.onError(resourceNotFound(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(model: TopicDeleteModel): Single<UUID> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->

                if(DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(model.id).and(view.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                //update the artifact shadow
                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.DELETED)?.let { (id, version) ->
                    val topic = DSL.using(configuration)
                            .select()
                            .from(table)
                            .where(view.ID.eq(model.id))
                            .fetchOne()
                    //update the identity shadow
                    DSL.using(configuration).insertInto(shadow)
                            .columns(shadow.ID, shadow.VERSION, shadow.TITLE, shadow.DESCRIPTION, shadow.CATEGORY)
                            .values(id, version, topic[table.TITLE], topic[table.DESCRIPTION], topic[table.CATEGORY])
                            .returning(shadow.ID)
                            .fetchOne()
                            .let { emitter.onSuccess(it[shadow.ID]) }
                } ?: emitter.onError(resourceNotFound(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    private fun Condition.withTags(tagIds: List<UUID>): Condition = let {
        if (tagIds.isNotEmpty()) {
            val queryText = StringBuilder("exists(select * from json_array_elements(${view.TAGS.name}) where")

            for ((index, tagId) in tagIds.withIndex()) {
                queryText.append(" value::jsonb @> '{\"id\":\"$tagId\"}'")
                if (index + 1 != tagIds.size) {
                    queryText.append(" and")
                }
            }

            queryText.append(")")

            it.and(queryText.toString())
        } else {
            it
        }
    }
}