package org.doctordiabet.datalayer.services.verification_codes

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.concurrency.cleanup.CleanupMessage
import org.doctordiabet.datalayer.concurrency.cleanup.CleanupService
import org.doctordiabet.datalayer.database.core.RedisAdapter
import org.doctordiabet.datalayer.dbo.codes.VerificationCodeDBO
import org.doctordiabet.datalayer.dbo.codes.toCacheObject
import org.doctordiabet.datalayer.dbo.codes.toVerificationCode
import org.doctordiabet.datalayer.dbo.codes.verificationCodeSingleQuery
import org.doctordiabet.datalayer.services.base.AbstractCacheService
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import redis.clients.jedis.exceptions.JedisException
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

class VerificationCodesCacheService(adapter: RedisAdapter): AbstractCacheService(adapter) {

    companion object {

        private const val VERIFICATION_CODE_EXPIRATION = 30_000L
    }

    private val cleanupQueue: BlockingQueue<CleanupMessage<VerificationCodeDBO>> = LinkedBlockingQueue()

    init {
        CleanupService.install(
                "Codes cleanup Thread Pool",
                1,
                cleanupQueue,
                { dbo -> delete(dbo) },
                expiration = VERIFICATION_CODE_EXPIRATION
        )
    }

    fun set(dbo: VerificationCodeDBO): Single<VerificationCodeDBO> = Single.create { emitter ->
        try {
            set(dbo.toCacheObject()).also { emitter.onSuccess(dbo) }
            cleanupQueue.offer(CleanupMessage(System.currentTimeMillis(), dbo))
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(identityId: UUID): Single<VerificationCodeDBO> = Single.create { emitter ->
        try {
            get(verificationCodeSingleQuery(identityId))
                    ?.toVerificationCode(identityId)
                    ?.let { emitter.onSuccess(it) }
                    ?: emitter.onError(resourceNotFound(identityId))
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    private fun delete(dbo: VerificationCodeDBO) {
        remove(verificationCodeSingleQuery(dbo.identityId))
    }
}