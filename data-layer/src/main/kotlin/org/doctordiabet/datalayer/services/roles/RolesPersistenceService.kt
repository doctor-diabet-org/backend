package org.doctordiabet.datalayer.services.roles

import io.reactivex.Single
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.enums.IdentityRoleType
import org.doctordiabet.datalayer.dbo.roles.RolesDBO
import org.doctordiabet.datalayer.services.base.AbstractPersistenceService
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.jooq.impl.DSL
import org.postgresql.util.PSQLException
import java.util.*

class RolesPersistenceService(
        adapter: PostgresAdapter
): AbstractPersistenceService(adapter) {

    private val identities = Tables.IDENTITY_CURRENT_VERSIONS
    private val links = Tables.IDENTITY_ROLES_LINK
    private val artifacts = Tables.ARTIFACTS
    private val artifactsShadow = Tables.ARTIFACTS_SHADOW

    fun grant(identityId: UUID, role: IdentityRoleType): Single<RolesDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->

                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(identities)
                        .where(
                                identities.ID.eq(identityId)
                                        .and(identities.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(identityId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(identities)
                        .where(identities.ID.eq(identityId))
                )) {
                    emitter.onError(resourceNotFound(identityId))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .insertInto(links)
                        .columns(links.IDENTITY_ID, links.ROLE_TYPE)
                        .values(identityId, role)
                        .execute()

                DSL.using(configuration)
                        .select(links.ROLE_TYPE)
                        .from(links)
                        .where(links.IDENTITY_ID.eq(identityId))
                        .fetch()
                        .map { it[links.ROLE_TYPE] }
                        .fold(mutableListOf<IdentityRoleType>(), { list, role ->
                            list.add(role)
                            list
                        })
                        .let { RolesDBO(identityId, it.toList()) }
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(identityId: UUID): Single<RolesDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->

                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(identities)
                        .where(
                                identities.ID.eq(identityId)
                                        .and(identities.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(identityId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(identities)
                        .where(identities.ID.eq(identityId))
                )) {
                    emitter.onError(resourceNotFound(identityId))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .select(links.ROLE_TYPE)
                        .from(links)
                        .where(links.IDENTITY_ID.eq(identityId))
                        .fetch()
                        .map { it[links.ROLE_TYPE] }
                        .fold(mutableListOf<IdentityRoleType>(), { list, role ->
                            list.add(role)
                            list
                        })
                        .let { RolesDBO(identityId, it.toList()) }
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun revoke(identityId: UUID, role: IdentityRoleType): Single<RolesDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(identities)
                        .where(
                                identities.ID.eq(identityId)
                                        .and(identities.DELETED.eq(true))
                        )
                )) {
                    emitter.onError(resourceDeleted(identityId))
                    return@withTransaction
                }

                if (!DSL.using(configuration).fetchExists(DSL.select()
                        .from(identities)
                        .where(identities.ID.eq(identityId))
                )) {
                    emitter.onError(resourceNotFound(identityId))
                    return@withTransaction
                }

                DSL.using(configuration)
                        .deleteFrom(links)
                        .where(links.IDENTITY_ID.eq(identityId).and(links.ROLE_TYPE.eq(role)))
                        .execute()

                DSL.using(configuration)
                        .select(links.ROLE_TYPE)
                        .from(links)
                        .where(links.IDENTITY_ID.eq(identityId))
                        .fetch()
                        .map { it[links.ROLE_TYPE] }
                        .fold(mutableListOf<IdentityRoleType>(), { list, role ->
                            list.add(role)
                            list
                        })
                        .let { RolesDBO(identityId, it.toList()) }
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readRelationships(identityId: UUID, artifactId: UUID): Single<RelationshipEntity> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                when {
                    DSL.using(configuration)
                            .fetchExists(
                                    DSL.select(artifacts.ID)
                                            .from(artifacts)
                                            .where(artifacts.ID.eq(artifactId).and(artifacts.CREATED_BY_IDENTITY.eq(identityId)))
                            ) -> emitter.onSuccess(RelationshipEntity.CREATOR)
                    DSL.using(configuration)
                            .fetchExists(
                                    DSL.select(artifactsShadow.ID)
                                            .from(artifacts)
                                            .where(artifactsShadow.ID.eq(artifactId).and(artifactsShadow.LAST_MODIFIED_BY_IDENTITY.eq(identityId)))
                            ) -> emitter.onSuccess(RelationshipEntity.CONTRIBUTOR)
                    else -> emitter.onSuccess(RelationshipEntity.NONE)
                }
            }
        } catch (e: PSQLException) {
            emitter.onError(e.parse())
        } catch (e: Exception) {
            emitter.onError(e.parse())
        }
    }
}