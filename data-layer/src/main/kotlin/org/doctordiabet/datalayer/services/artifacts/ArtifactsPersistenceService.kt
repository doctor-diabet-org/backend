package org.doctordiabet.datalayer.services.artifacts

import io.reactivex.Single
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.metadata.ArtifactTag
import org.doctordiabet.datalayer.services.base.AbstractPersistenceService
import org.postgresql.util.PSQLException
import java.util.*

class ArtifactsPersistenceService(adapter: PostgresAdapter): AbstractPersistenceService(adapter) {

    fun subscribe(identityId: UUID, artifactId: UUID): Single<Pair<UUID, Boolean>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                val owner = getOwner(configuration, artifactId)
                if(owner != null) {
                    emitter.onSuccess(owner to super.subscribeInternal(configuration, identityId, artifactId))
                } else {
                    emitter.onError(resourceNotFound(artifactId))
                }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun unsubscribe(identityId: UUID, artifactId: UUID): Single<Pair<UUID, Boolean>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                val owner = getOwner(configuration, artifactId)
                if(owner != null) {
                    emitter.onSuccess(owner to super.unsubscribeInternal(configuration, identityId, artifactId))
                } else {
                    emitter.onError(resourceNotFound(artifactId))
                }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun subscribers(artifactId: UUID): Single<List<UUID>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                emitter.onSuccess(super.subscribersInternal(configuration, artifactId))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun createTags(values: List<String>): Single<List<ArtifactTag>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                emitter.onSuccess(super.createTagsInternal(configuration, values))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readTags(): Single<List<ArtifactTag>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                emitter.onSuccess(super.readTagsInternal(configuration))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun attachTags(artifactId: UUID, tagIds: List<UUID>): Single<Unit> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                emitter.onSuccess(super.attachTagsInternal(configuration, artifactId, tagIds))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun detachTags(artifactId: UUID, tagIds: List<UUID>): Single<Unit> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                emitter.onSuccess(super.detachTagsInternal(configuration, artifactId, tagIds))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun upvote(identityId: UUID, artifactId: UUID): Single<Pair<ArtifactType, Boolean>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                val artifactType = getType(configuration, artifactId)
                if(artifactType != null) {
                    emitter.onSuccess(artifactType to super.voteInternal(configuration, identityId, artifactId, 1))
                } else {
                    emitter.onError(resourceNotFound(artifactId))
                }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun downvote(identityId: UUID, artifactId: UUID): Single<Pair<ArtifactType, Boolean>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                val artifactType = getType(configuration, artifactId)
                if(artifactType != null) {
                    emitter.onSuccess(artifactType to super.voteInternal(configuration, identityId, artifactId, -1))
                } else {
                    emitter.onError(resourceNotFound(artifactId))
                }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }
}