package org.doctordiabet.datalayer.services.categories

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.database.core.RedisAdapter
import org.doctordiabet.datalayer.dbo.categories.*
import org.doctordiabet.datalayer.services.base.AbstractCacheService
import org.doctordiabet.datalayer.services.base.OrderBy
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import redis.clients.jedis.exceptions.JedisException
import java.sql.Timestamp
import java.util.*
import kotlin.coroutines.experimental.buildSequence

class CategoriesCacheService(adapter: RedisAdapter): AbstractCacheService(adapter) {

    fun set(dbo: CategoryDBO): Single<CategoryDBO> = Single.create { emitter ->
        try {
            set(dbo.toCacheObject()).also { emitter.onSuccess(dbo) }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun set(dbos: Collection<CategoryDBO>): Single<List<CategoryDBO>> = Single.create { emitter ->
        try {
            buildSequence {
                for (dbo in dbos) {
                    set(dbo.toCacheObject())
                    yield(dbo)
                }
            }.toList().let { emitter.onSuccess(it) }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
            e.printStackTrace()
        } catch(e: Exception) {
            emitter.onError(e.parse())
            e.printStackTrace()
        }
    }

    fun get(id: UUID): Maybe<CategoryDBO> = Maybe.create { emitter ->
        try {
            get(categoryCacheSingleQuery(id))
                    ?.toCategoryDBO()
                    ?.let { emitter.onSuccess(it) }
                    ?: emitter.onError(EmptyMaybeException())
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(offset: Int = 0, limit: Int = -1, order: OrderBy = OrderBy.DESC): Single<List<CategoryDBO>> = Single.create { emitter ->
        try {
            get(categoryLastModifiedRangeCacheGet(offset, limit, order)).let {
                get(categoryHashRangeCacheGet(it)).map { it?.toCategoryDBO() }
                        .toList()
                        .filterNotNull()
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(lowerBoundExclusive: Timestamp? = null, upperBoundExclusive: Timestamp? = null, offset: Int = 0, limit: Int = -1, order: OrderBy = OrderBy.DESC): Single<List<CategoryDBO>> = Single.create { emitter ->
        try {
            get(categoryLastModifiedRangeScoreCacheGet(lowerBoundExclusive, upperBoundExclusive, offset, limit, order)).let {
                get(categoryHashRangeCacheGet(it)).map { it?.toCategoryDBO() }
                        .toList()
                        .filterNotNull()
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(id: UUID): Single<UUID> = Single.create { emitter ->
        try {
            remove(categoryCacheSingleQuery(id))
            emitter.onSuccess(id)
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }
}