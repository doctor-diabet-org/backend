package org.doctordiabet.datalayer.services.identities

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.database.core.RedisAdapter
import org.doctordiabet.datalayer.dbo.identity.*
import org.doctordiabet.datalayer.services.base.AbstractCacheService
import org.doctordiabet.datalayer.services.base.OrderBy
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import redis.clients.jedis.exceptions.JedisException
import java.sql.Timestamp
import java.util.*
import kotlin.coroutines.experimental.buildSequence

class IdentitiesCacheService(adapter: RedisAdapter): AbstractCacheService(adapter) {

    fun set(dbo: IdentityDBO): Single<IdentityDBO> = Single.create { emitter ->
        try {
            set(bean = dbo.toCacheObject(), duplicateLexProperties = false).also { emitter.onSuccess(dbo) }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun set(dbos: Collection<IdentityDBO>): Single<List<IdentityDBO>> = Single.create { emitter ->
        try {
            buildSequence {
                for (dbo in dbos) {
                    set(bean = dbo.toCacheObject(), duplicateLexProperties = false)
                    yield(dbo)
                }
            }.toList().let { emitter.onSuccess(it) }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
            e.printStackTrace()
        } catch(e: Exception) {
            emitter.onError(e.parse())
            e.printStackTrace()
        }
    }

    fun get(id: UUID): Maybe<IdentityDBO> = Maybe.create { emitter ->
        try {
            get(identityCacheSingleQuery(id))
                    ?.toIdentityDBO()
                    ?.let { emitter.onSuccess(it) }
                    ?: emitter.onError(EmptyMaybeException())
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(offset: Int = 0, limit: Int = -1, order: OrderBy = OrderBy.DESC): Single<List<IdentityDBO>> = Single.create { emitter ->
        try {
            get(identityLastModifiedRangeCacheGet(offset, limit, order)).let {
                get(identityHashRangeCacheGet(it)).map { it?.toIdentityDBO() }
                        .toList()
                        .filterNotNull()
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(lowerBoundExclusive: Timestamp? = null, upperBoundExclusive: Timestamp? = null, offset: Int = 0, limit: Int = -1, order: OrderBy = OrderBy.DESC): Single<List<IdentityDBO>> = Single.create { emitter ->
        try {
            get(identityLastModifiedRangeScoreCacheGet(lowerBoundExclusive, upperBoundExclusive, offset, limit, order)).let {
                get(identityHashRangeCacheGet(it)).map { it?.toIdentityDBO() }
                        .toList()
                        .filterNotNull()
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(nameSubstring: String, offset: Int = 0, limit: Int = 0, order: OrderBy = OrderBy.ASC): Single<List<IdentityDBO>> = Single.create { emitter ->
        try {
            get(identityNameSubstringRangeScoreCacheGet(nameSubstring, null, offset, limit, order)).let {
                get(identityHashRangeCacheGet(it)).map { it?.toIdentityDBO() }
                        .toList()
                        .filterNotNull()
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(id: UUID): Single<UUID> = Single.create { emitter ->
        try {
            remove(identityCacheSingleQuery(id))
            emitter.onSuccess(id)
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }
}