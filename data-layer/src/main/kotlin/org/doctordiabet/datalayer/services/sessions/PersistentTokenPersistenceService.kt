package org.doctordiabet.datalayer.services.sessions

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.dbo.session.PersistentTokenDBO
import org.doctordiabet.datalayer.dbo.session.toPersistentToken
import org.doctordiabet.datalayer.services.base.AbstractPersistenceService
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import org.jooq.impl.DSL
import org.postgresql.util.PSQLException
import java.util.*

class PersistentTokenPersistenceService(adapter: PostgresAdapter): AbstractPersistenceService(adapter) {

    private val table = Tables.IDENTITY_TOKENS

    fun create(dbo: PersistentTokenDBO): Single<PersistentTokenDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration).insertInto(table)
                        .columns(table.IDENTITY, table.TOKEN)
                        .values(dbo.identityId, dbo.refreshToken)
                        .returning(table.IDENTITY, table.TOKEN)
                        .fetchOne()
                        .map { it.toPersistentToken() }
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(identityId: UUID): Single<PersistentTokenDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration).select(table.IDENTITY, table.TOKEN)
                        .from(table)
                        .where(table.IDENTITY.eq(identityId))
                        .fetchAny()
                        ?.map { it.toPersistentToken() }
                        ?.let { emitter.onSuccess(it) }
                        ?: emitter.onError(EmptyMaybeException())
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(identityId: UUID): Maybe<PersistentTokenDBO> = Maybe.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration).delete(table)
                        .where(table.IDENTITY.eq(identityId))
                        .returning(table.IDENTITY, table.TOKEN)
                        .fetchOptional()
                        .map { it.toPersistentToken() }
                        .let {
                            when(it.isPresent) {
                                true -> emitter.onSuccess(it.get())
                                else -> emitter.onError(EmptyMaybeException())
                            }
                        }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }
}

