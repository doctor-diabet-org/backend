package org.doctordiabet.datalayer.services.sessions

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.concurrency.cleanup.CleanupMessage
import org.doctordiabet.datalayer.concurrency.cleanup.CleanupService
import org.doctordiabet.datalayer.database.core.RedisAdapter
import org.doctordiabet.datalayer.dbo.session.*
import org.doctordiabet.datalayer.repository.sessions.SessionsRepositoryImpl
import org.doctordiabet.datalayer.services.base.AbstractCacheService
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import redis.clients.jedis.exceptions.JedisException
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

class SessionTokenCacheService(adapter: RedisAdapter): AbstractCacheService(adapter) {

    private val cleanupQueue: BlockingQueue<CleanupMessage<SessionTokenDBO>> = LinkedBlockingQueue<CleanupMessage<SessionTokenDBO>>()

    init {
        CleanupService.install(
                name  = "Sessions cleanup Thread Pool",
                size = 1,
                queue = cleanupQueue,
                delegate = { dbo ->
                    delete(dbo)
                },
                expiration = SessionsRepositoryImpl.SESSION_TOKEN_EXPIRATION
        )
    }

    fun set(dbo: SessionTokenDBO): Single<SessionTokenDBO> = Single.create { emitter ->
        try {
            add(dbo.toCacheObject())
            set(dbo.toMirrorCacheObject())
            cleanupQueue.offer(CleanupMessage(dbo.createdTime, dbo))
            emitter.onSuccess(dbo)
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(sessionToken: String): Single<SessionTokenDBO> = Single.create { emitter ->
        try {
            get(sessionTokenMirrorSingleQuery(sessionToken))
                    ?.toSessionToken(sessionToken)
                    ?.let { emitter.onSuccess(it) }
                    ?: emitter.onError(resourceNotFound(sessionToken))
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(identityId: UUID): Single<Set<SessionTokenDBO>> = Single.create { emitter ->
        try {
            get(sessionTokenSingleQuery(identityId))
                    .mapNotNull { get(sessionTokenMirrorSingleQuery(it))?.toSessionToken(it) }
                    .let { emitter.onSuccess(it.toSet()) }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(identityId: UUID): Single<UUID> = Single.create { emitter ->
        try {
            get(sessionTokenSingleQuery(identityId))
                    .mapNotNull { get(sessionTokenMirrorSingleQuery(it))?.toSessionToken(it) }
                    .forEach { remove(sessionTokenMirrorSingleQuery(it.sessionToken)) }
                    .also {
                        val sessions = get(sessionTokenSingleQuery(identityId)).map { sessionToken ->
                            get(sessionTokenMirrorSingleQuery(sessionToken))
                                    ?.toSessionToken(sessionToken)
                        }.filterNotNull()
                        for(session in sessions) {
                            remove(session.toCacheObject())
                        }
                    }
                    .also { emitter.onSuccess(identityId) }
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    private fun delete(dbo: SessionTokenDBO) {
        remove(sessionTokenMirrorSingleQuery(dbo.sessionToken))
        remove(dbo.toCacheObject())
    }
}