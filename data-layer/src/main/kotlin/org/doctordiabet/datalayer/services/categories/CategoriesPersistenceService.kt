package org.doctordiabet.datalayer.services.categories

import io.reactivex.Single
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.categories.*
import org.doctordiabet.datalayer.services.base.AbstractPersistenceService
import org.doctordiabet.domainlayer.contract.order.ArtifactFilter
import org.jooq.*
import org.jooq.impl.DSL
import org.postgresql.util.PSQLException
import java.sql.Timestamp
import java.util.*

class CategoriesPersistenceService(adapter: PostgresAdapter): AbstractPersistenceService(adapter) {

    private val table = Tables.CATEGORIES
    private val shadow = Tables.CATEGORIES_SHADOW

    private val view = Tables.CATEGORY_CURRENT_VERSIONS

    fun create(model: CategoryCreateModel): Single<CategoryDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->

                //create the object metadata
                val artifact = when(model.creatingByIdentity) {
                    null -> createArtifact(configuration, ArtifactType.category)
                    else -> createArtifact(configuration, ArtifactType.category, model.creatingByIdentity)
                }

                //create the object metadata snapshot
                val artifactShadow = createShadow(configuration, artifact)

                //create the object itself
                DSL.using(configuration)
                        .insertInto(table)
                        .columns(table.ID, table.TITLE, table.DESCRIPTION)
                        .values(artifact.id, model.title, model.description)
                        .execute()

                //create the object snapshot
                DSL.using(configuration)
                        .insertInto(shadow)
                        .columns(shadow.ID, shadow.VERSION, shadow.TITLE, shadow.DESCRIPTION)
                        .values(artifact.id, artifactShadow.version, model.title, model.description)
                        .execute()

                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.ID.eq(artifact.id))
                        .fetchOne()
                        .map { it.toCategory(configuration) }
                        .let { emitter.onSuccess(it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(id: UUID): Single<CategoryDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.ID.eq(id))
                        .fetchAny()
                        ?.map { it.toCategory(configuration) }
                        ?.let {
                            if(it.metadata.deleted) {
                                emitter.onError(resourceDeleted(id))
                            } else {
                                emitter.onSuccess(it)
                            }
                        }
                        ?: emitter.onError(resourceNotFound(id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun read(offset: Int, limit: Int, tagIds: List<UUID> = emptyList(), criteria: List<ArtifactFilter> = emptyList()): Single<Pair<Int, List<CategoryDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.DELETED.eq(false)
                                .withTags(tagIds)
                                .withCriteria(criteria)
                        )
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toCategory(configuration) }
                        .let { emitter.onSuccess(DSL.using(configuration).fetchCount(view, view.DELETED.eq(false).withTags(tagIds).withCriteria(criteria)) to it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readModified(offset: Int, limit: Int, modifiedTime: Timestamp, tagIds: List<UUID> = emptyList(), criteria: List<ArtifactFilter> = emptyList()): Single<Pair<Int, List<CategoryDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.gt(modifiedTime)
                                .and(view.DELETED.eq(false))
                                .withTags(tagIds)
                                .withCriteria(criteria)
                        )
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toCategory(configuration) }
                        .let { emitter.onSuccess(DSL.using(configuration).fetchCount(view, view.LAST_MODIFIED_TIME.gt(modifiedTime).and(view.DELETED.eq(false).withTags(tagIds).withCriteria(criteria))) to it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun readUnmodified(offset: Int, limit: Int, unmodifiedTime: Timestamp, tagIds: List<UUID> = emptyList(), criteria: List<ArtifactFilter> = emptyList()): Single<Pair<Int, List<CategoryDBO>>> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                DSL.using(configuration)
                        .select()
                        .from(view)
                        .where(view.LAST_MODIFIED_TIME.le(unmodifiedTime)
                                .and(view.DELETED.eq(false))
                                .withTags(tagIds)
                                .withCriteria(criteria)
                        )
                        .offset(offset)
                        .limit(limit)
                        .fetch()
                        .map { it.toCategory(configuration) }
                        .let { emitter.onSuccess(DSL.using(configuration).fetchCount(view, view.LAST_MODIFIED_TIME.le(unmodifiedTime).and(view.DELETED.eq(false).withTags(tagIds).withCriteria(criteria))) to it) }
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun update(model: CategoryUpdateModel): Single<CategoryDBO> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->
                //update the artifact shadow

                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(model.id).and(view.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.MODIFIED)?.let { (id, version) ->
                    val category = DSL.using(configuration)
                            .updateQuery(table)

                    model.title?.let { title ->
                        category.addValue(table.TITLE, title)
                    }

                    model.description?.let { description ->
                        category.addValue(table.DESCRIPTION, description)
                    }

                    category.setReturning()
                    category.execute()

                    DSL.using(configuration)
                            .insertInto(shadow)
                            .columns(shadow.ID, shadow.VERSION, shadow.TITLE, shadow.DESCRIPTION)
                            .values(id, version, model.title, model.description)
                            .execute()

                    DSL.using(configuration)
                            .select()
                            .from(view)
                            .where(view.ID.eq(model.id))
                            .fetchOne()
                            .map { it.toCategory(configuration) }
                            .let { emitter.onSuccess(it) }
                } ?: emitter.onError(resourceDeleted(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(model: CategoryDeleteModel): Single<UUID> = Single.create { emitter ->
        try {
            adapter.withTransaction { configuration ->

                if (DSL.using(configuration).fetchExists(DSL.select()
                        .from(view)
                        .where(view.ID.eq(model.id).and(view.DELETED.eq(true)))
                )) {
                    emitter.onError(resourceDeleted(model.id))
                    return@withTransaction
                }

                //update the artifact shadow
                updateShadow(configuration, model.id, model.modifyingByIdentity, VersionType.DELETED)?.let { (id, version) ->
                    val category = DSL.using(configuration)
                            .select()
                            .from(table)
                            .where(table.ID.eq(model.id))
                            .fetchOne()
                    //update the category shadow
                    DSL.using(configuration).insertInto(shadow)
                            .columns(shadow.ID, shadow.VERSION, shadow.TITLE)
                            .values(id, version, category[table.TITLE])
                            .returning(shadow.ID)
                            .fetchOne()
                            .map { it[shadow.ID] }
                            .let(emitter::onSuccess)
                } ?: emitter.onError(resourceNotFound(model.id))
            }
        } catch(e: PSQLException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    private fun Condition.withTags(tagIds: List<UUID>): Condition = let {
        if (tagIds.isNotEmpty()) {
            val queryText = StringBuilder("exists(select * from json_array_elements(${view.TAGS.name}) where")

            for ((index, tagId) in tagIds.withIndex()) {
                queryText.append(" value::jsonb @> '{\"id\":\"$tagId\"}'")
                if (index + 1 != tagIds.size) {
                    queryText.append(" and")
                }
            }

            queryText.append(")")

            it.and(queryText.toString())
        } else {
            it
        }
    }

    private fun Condition.withCriteria(criteria: List<ArtifactFilter>): Condition = let {
        var condition = it
        if (criteria.isNotEmpty()) {
            for (criterion in criteria) {
                when (criterion) {
                    is ArtifactFilter.Votes.Gt -> {
                        condition = condition.and(view.VOTES.ge(criterion.value))
                    }
                    is ArtifactFilter.Votes.Lt -> {
                        condition = condition.and(view.VOTES.le(criterion.value))
                    }
                    is ArtifactFilter.Votes.Range -> {
                        condition = condition.and(view.VOTES.ge(criterion.lower).and(view.VOTES.le(criterion.upper)))
                    }
                    is ArtifactFilter.Votes.Simple -> {

                    }
                }
            }
        }

        condition
    }
}