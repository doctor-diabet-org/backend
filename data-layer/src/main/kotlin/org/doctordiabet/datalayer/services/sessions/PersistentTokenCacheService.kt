package org.doctordiabet.datalayer.services.sessions

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.database.core.RedisAdapter
import org.doctordiabet.datalayer.dbo.session.*
import org.doctordiabet.datalayer.services.base.AbstractCacheService
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import redis.clients.jedis.exceptions.JedisException
import java.util.*

class PersistentTokenCacheService(adapter: RedisAdapter): AbstractCacheService(adapter) {

    fun set(persistentTokenDBO: PersistentTokenDBO): Single<PersistentTokenDBO> = Single.create { emitter ->
        try {
            //expire previous token if needed
            get(persistentTokenSingleQuery(persistentTokenDBO.identityId))
                    ?.toPersistentToken(persistentTokenDBO.identityId)
                    ?.let { remove(persistentTokenMirrorSingleQuery(it.refreshToken)) }

            //set the current token
            set(persistentTokenDBO.toCacheObject())
            //and its vice-versa version to be able to get identityId by token
            set(persistentTokenDBO.toMirrorCacheObject())

            emitter.onSuccess(persistentTokenDBO)
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(identityId: UUID): Single<PersistentTokenDBO> = Single.create { emitter ->
        try {
            get(persistentTokenSingleQuery(identityId))
                    ?.toPersistentToken(identityId)
                    ?.let { emitter.onSuccess(it) }
                    ?: emitter.onError(resourceNotFound(identityId))
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun get(token: String): Single<PersistentTokenDBO> = Single.create { emitter ->
        try {
            get(persistentTokenMirrorSingleQuery(token))
                    ?.toPersistentToken(token)
                    ?.let { emitter.onSuccess(it) }
                    ?: emitter.onError(resourceNotFound(token))
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(identityId: UUID): Single<UUID> = Single.create { emitter ->
        try {
            remove(persistentTokenSingleQuery(identityId))
            emitter.onSuccess(identityId)
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    fun delete(token: String): Single<String> = Single.create { emitter ->
        try {
            remove(persistentTokenMirrorSingleQuery(token))
            emitter.onSuccess(token)
        } catch(e: JedisException) {
            emitter.onError(e.parse())
        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }
}