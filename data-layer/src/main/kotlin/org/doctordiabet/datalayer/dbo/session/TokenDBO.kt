package org.doctordiabet.datalayer.dbo.session

import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.services.base.*
import org.jooq.Record
import java.util.*

data class PersistentTokenDBO(
        val identityId: UUID,
        val refreshToken: String
)

fun Record.toPersistentToken() = PersistentTokenDBO(
        this[Tables.IDENTITY_TOKENS.IDENTITY],
        this[Tables.IDENTITY_TOKENS.TOKEN]
)

data class SessionTokenDBO(
        val identityId: UUID,
        val refreshToken: String,
        val sessionToken: String,
        val createdTime: Long,
        val expiration: Long,
        val expirationTime: Long
)

private object CacheMapping {

    object Persistent {

        const val ENTITY_TYPE        = "Refresh-token"
        const val ENTITY_TYPE_MIRROR = "Refresh-token-mirror"
        const val IDENTITY_ID        = "identity"
        const val TOKEN              = "token"
    }

    object Session {

        const val ENTITY_TYPE        = "Session-token"
        const val ENTITY_TYPE_MIRROR = "Session-token-mirror"
        const val IDENTITY_ID        = "identity"
        const val REFRESH_TOKEN      = "refresh"
        const val TOKEN              = "token"
        const val CREATED_TIME       = "created"
        const val EXPIRATION         = "expiration"
        const val EXPIRATION_TIME    = "expiration_time"
    }
}

fun persistentTokenSingleQuery(identityId: UUID) = RedisSingleQuery.Impl(
        entityType = CacheMapping.Persistent.ENTITY_TYPE,
        hash = identityId.toString()
)

fun persistentTokenMirrorSingleQuery(token: String) = RedisSingleQuery.Impl(
        entityType = CacheMapping.Persistent.ENTITY_TYPE_MIRROR,
        hash = token
)

fun PersistentTokenDBO.toCacheObject() = with(CacheMapping.Persistent) {
    RedisBean.Impl(
            hash = identityId.toString(),
            entityType = ENTITY_TYPE,
            properties = setOf(
                    RedisBeanPropertyPrototype.Impl(TOKEN, refreshToken)
            )
    )
}

fun PersistentTokenDBO.toMirrorCacheObject() = with(CacheMapping.Persistent) {
    RedisBean.Impl(
            hash = refreshToken,
            entityType = ENTITY_TYPE_MIRROR,
            properties = setOf(
                    RedisBeanPropertyPrototype.Impl(IDENTITY_ID, identityId.toString())
            )
    )
}

fun Map<String, String?>.toPersistentToken(identityId: UUID): PersistentTokenDBO? = with(CacheMapping.Persistent) {
    val refreshToken = get(TOKEN)
    return@with when {
        refreshToken == null -> null
        else -> PersistentTokenDBO(
                identityId,
                refreshToken
        )
    }
}

fun Map<String, String?>.toPersistentToken(refreshToken: String): PersistentTokenDBO? = with(CacheMapping.Persistent) {
    val identityId = get(IDENTITY_ID)?.let { UUID.fromString(it) }
    when {
        identityId == null -> null
        else -> PersistentTokenDBO(
                identityId,
                refreshToken
        )
    }
}

fun sessionTokenSingleQuery(identityId: UUID) = RedisSetBeanPrototype.Impl(
        entityType = CacheMapping.Session.ENTITY_TYPE,
        hash = identityId.toString()
)

fun sessionTokenMirrorSingleQuery(token: String) = RedisSingleQuery.Impl(
        entityType = CacheMapping.Session.ENTITY_TYPE_MIRROR,
        hash = token
)

fun SessionTokenDBO.toCacheObject() = with(CacheMapping.Session) {
    RedisSetBean.Impl(
            hash = identityId.toString(),
            entityType = ENTITY_TYPE,
            value = sessionToken
    )
}

fun SessionTokenDBO.toMirrorCacheObject() = with(CacheMapping.Session) {
    RedisBean.Impl(
            hash = sessionToken,
            entityType = ENTITY_TYPE_MIRROR,
            properties = setOf(
                    RedisBeanPropertyPrototype.Impl(IDENTITY_ID, identityId.toString()),
                    RedisBeanPropertyPrototype.Impl(REFRESH_TOKEN, refreshToken),
                    RedisBeanPropertyPrototype.Impl(CREATED_TIME, createdTime),
                    RedisBeanPropertyPrototype.Impl(EXPIRATION, expiration),
                    RedisBeanPropertyPrototype.Impl(EXPIRATION_TIME, expirationTime)
            )
    )
}

fun SessionTokenDBO.toSetPrototype() = RedisSetBeanPrototype.Impl(
        entityType = CacheMapping.Session.ENTITY_TYPE,
        hash = sessionToken
)

fun Map<String, String?>.toSessionToken(sessionToken: String): SessionTokenDBO? = with(CacheMapping.Session) {
    val identityId = get(IDENTITY_ID)?.let { UUID.fromString(it) }
    val refreshToken = get(REFRESH_TOKEN)
    val createdTime = get(CREATED_TIME)?.toLongOrNull()
    val expiration = get(EXPIRATION)?.toLongOrNull()
    val expirationTime = get(EXPIRATION_TIME)?.toLongOrNull()

    when {
        identityId == null -> null
        refreshToken == null -> null
        createdTime == null -> null
        expiration == null -> null
        expirationTime == null -> null
        else -> SessionTokenDBO(
                identityId = identityId,
                refreshToken = refreshToken,
                sessionToken = sessionToken,
                createdTime = createdTime,
                expiration = expiration,
                expirationTime = expirationTime
        )
    }
}