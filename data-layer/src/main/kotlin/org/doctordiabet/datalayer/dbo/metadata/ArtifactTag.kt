package org.doctordiabet.datalayer.dbo.metadata

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import java.util.*

data class ArtifactTag(
        val id: UUID,
        val value: String
)

fun ObjectNode.toArtifactTag() = ArtifactTag(
        UUID.fromString(this["id"].textValue()),
        this["value"].textValue()
)

fun ArtifactTag.toJsonObject(): ObjectNode = JsonNodeFactory.instance.objectNode().apply {
    put("id", id.toString())
    put("value", value)
}

fun ArtifactTag.toTagEntity() = TagEntity(
        id = this.id,
        value = this.value
)