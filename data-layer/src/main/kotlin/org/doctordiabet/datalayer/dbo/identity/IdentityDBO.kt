package org.doctordiabet.datalayer.dbo.identity

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.metadata.*
import org.doctordiabet.datalayer.mapping.utils.redis.RedisTimestampBinding
import org.doctordiabet.datalayer.services.base.*
import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity
import org.doctordiabet.domainlayer.entity.identity.IdentityLink
import org.doctordiabet.domainlayer.entity.identity.IdentityPrivateEntity
import org.doctordiabet.domainlayer.entity.identity.IdentityPublicEntity
import org.jooq.Configuration
import org.jooq.Record
import org.jooq.impl.DSL
import java.sql.Timestamp
import java.util.*

data class IdentityDBO(
        val firstName: String,
        val lastName: String,
        val phoneNumber: String?,
        val email: String,
        val passwordHash: String,
        val metadata: ArtifactMetadata,
        val tags: Set<ArtifactTag> = emptySet()
)

data class IdentityLinkDBO(
        val firstName: String,
        val lastName: String,
        val id: UUID
)

fun Record.toIdentity(configuration: Configuration) = IdentityDBO(
        firstName = this[Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME],
        lastName = this[Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME],
        phoneNumber = this[Tables.IDENTITY_CURRENT_VERSIONS.PHONE_NUMBER],
        email = this[Tables.IDENTITY_CURRENT_VERSIONS.EMAIL],
        passwordHash = this[Tables.IDENTITY_CURRENT_VERSIONS.PASSWORD_HASH],
        metadata = ArtifactMetadata(
                id = this[Tables.IDENTITY_CURRENT_VERSIONS.ID],
                version = this[Tables.IDENTITY_CURRENT_VERSIONS.VERSION],
                type = ArtifactType.identity,
                createdTime = this[Tables.IDENTITY_CURRENT_VERSIONS.CREATED_TIME],
                createdByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.IDENTITY_CURRENT_VERSIONS.CREATED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                lastModifiedTime = this[Tables.IDENTITY_CURRENT_VERSIONS.LAST_MODIFIED_TIME],
                lastModifiedByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.IDENTITY_CURRENT_VERSIONS.LAST_MODIFIED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                created = this[Tables.IDENTITY_CURRENT_VERSIONS.CREATED],
                modified = this[Tables.IDENTITY_CURRENT_VERSIONS.UPDATED],
                deleted = this[Tables.IDENTITY_CURRENT_VERSIONS.DELETED],
                votes = this[Tables.IDENTITY_CURRENT_VERSIONS.VOTES].toInt(),
                tags = this[Tables.IDENTITY_CURRENT_VERSIONS.TAGS].let {
                    if(it is ArrayNode && it.size() != 0) {
                        List(it.size(), { index ->
                            (it[index] as ObjectNode).toArtifactTag()
                        }).toSet()
                    } else {
                        emptySet()
                    }
                }
        )
)

fun Record.toIdentityLink() = IdentityLinkDBO(
        firstName = this[Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME],
        lastName = this[Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME],
        id = this[Tables.IDENTITY_CURRENT_VERSIONS.ID]
)

fun IdentityDBO.toPublicEntity() = IdentityPublicEntity(
        firstName = firstName,
        lastName = lastName,
        metadata = ArtifactEntity.Metadata(
                id = this.metadata.id,
                createdTime = this.metadata.createdTime,
                createdByIdentity = this.metadata.createdByIdentity.toEntity(),
                lastModifiedTime = this.metadata.lastModifiedTime,
                lastModifiedByIdentity = this.metadata.lastModifiedByIdentity.toEntity(),
                tags = this.metadata.tags.map { it.toTagEntity() }.toSet(),
                type = when {
                    this.metadata.deleted -> ArtifactEntity.Type.DELETED
                    else -> ArtifactEntity.Type.EXISTING
                },
                votes = this.metadata.votes
        )
)

fun IdentityDBO.toPrivateEntity() = IdentityPrivateEntity(
        firstName = firstName,
        lastName = lastName,
        phoneNumber = phoneNumber,
        email = email,
        pwdHash = passwordHash,
        metadata = ArtifactEntity.Metadata(
                id = this.metadata.id,
                createdTime = this.metadata.createdTime,
                createdByIdentity = this.metadata.createdByIdentity.toEntity(),
                lastModifiedTime = this.metadata.lastModifiedTime,
                lastModifiedByIdentity = this.metadata.lastModifiedByIdentity.toEntity(),
                tags = this.metadata.tags.map { it.toTagEntity() }.toSet(),
                type = when {
                    this.metadata.deleted -> ArtifactEntity.Type.DELETED
                    else -> ArtifactEntity.Type.EXISTING
                },
                votes = this.metadata.votes
        )
)

fun IdentityLinkDBO.toEntity() = IdentityLink(
        firstName = firstName,
        lastName = lastName,
        id = id
)

private object CacheMapping {

    const val ENTITY_TYPE = "identity"
    const val FIRST_NAME = "first_name"
    const val LAST_NAME = "last_name"
    const val FULL_NAME = "full_name"
    const val PHONE_NUMBER = "phone_number"
    const val EMAIL = "email"
    const val PASSWORD_HASH = "password_hash"
    const val ID = "eid"
    const val VERSION = "v"
    const val CREATED_TIME = "ct"
    const val CREATED_BY_IDENTITY = "cbi"
    const val LAST_MODIFIED_TIME = "lmt"
    const val LAST_MODIFIED_BY_IDENTITY = "lmbi"
    const val TAGS = "tags"
    const val CREATED = "cr"
    const val MODIFIED = "mod"
    const val DELETED = "del"
    const val VOTES = "votes"
}

fun identityCacheSingleQuery(id: UUID) = RedisSingleQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        hash = id.toString()
)

fun identityLastModifiedRangeCacheGet(offset: Int = 0,
                                      limit: Int = -1,
                                      order: OrderBy = OrderBy.DESC) = RedisRangeQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        field = CacheMapping.LAST_MODIFIED_TIME,
        offset = offset.toLong(),
        limit = limit.toLong(),
        order = order
)

fun identityNameSubstringRangeScoreCacheGet(fromNameSubstring: String,
                                            toNameSubstring: String? = null,
                                            offset: Int = 0,
                                            limit: Int = -1,
                                            order: OrderBy = OrderBy.DESC) = RedisRangeLexScoreQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        field = CacheMapping.LAST_MODIFIED_TIME,
        fromScore = fromNameSubstring,
        toScore = toNameSubstring,
        offset = offset,
        limit = limit,
        order = order
)

fun identityLastModifiedRangeScoreCacheGet(lowerBoundInclusive: Timestamp? = null,
                                           upperBoundInclusive: Timestamp? = null,
                                           offset: Int = 0,
                                           limit: Int = -1,
                                           order: OrderBy = OrderBy.DESC) = RedisRangeScoreQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        field = CacheMapping.LAST_MODIFIED_TIME,
        fromScore = when(lowerBoundInclusive) {
            null -> null
            else -> RedisTimestampBinding.instance.toScore(lowerBoundInclusive)
        },
        toScore = when(upperBoundInclusive) {
            null -> null
            else -> RedisTimestampBinding.instance.toScore(upperBoundInclusive)
        },
        offset = offset,
        limit = limit,
        order = order
)

fun identityHashRangeCacheGet(hashes: Set<String>) = RedisHashSetQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        hashes = hashes
)

fun IdentityDBO.toCacheObject() = with(CacheMapping) {
    RedisBean.Impl(
            hash = metadata.id.toString(),
            entityType = ENTITY_TYPE,
            properties = setOf(
                    RedisBeanPropertyPrototype.Impl(FIRST_NAME, firstName),
                    RedisBeanPropertyPrototype.Impl(LAST_NAME, lastName),
                    RedisBeanPropertyPrototype.Impl(FULL_NAME, firstName + lastName),
                    RedisBeanPropertyPrototype.Impl(PHONE_NUMBER, phoneNumber),
                    RedisBeanPropertyPrototype.Impl(EMAIL, email),
                    RedisBeanPropertyPrototype.Impl(PASSWORD_HASH, passwordHash),
                    RedisBeanPropertyPrototype.Impl(ID, metadata.id),
                    RedisBeanPropertyPrototype.Impl(VERSION, metadata.version),
                    RedisBeanIndexPropertyPrototype.Impl(CREATED_TIME, metadata.createdTime, RedisTimestampBinding.instance),
                    RedisBeanPropertyPrototype.Impl(CREATED_BY_IDENTITY, metadata.createdByIdentity),
                    RedisBeanIndexPropertyPrototype.Impl(LAST_MODIFIED_TIME, metadata.lastModifiedTime, RedisTimestampBinding.instance),
                    RedisBeanPropertyPrototype.Impl(LAST_MODIFIED_BY_IDENTITY, metadata.lastModifiedByIdentity),
                    RedisBeanPropertyPrototype.Impl(TAGS, JsonNodeFactory.instance.arrayNode().apply {
                        addAll(metadata.tags.map { it.toJsonObject() })
                    }),
                    RedisBeanPropertyPrototype.Impl(CREATED, metadata.created),
                    RedisBeanPropertyPrototype.Impl(MODIFIED, metadata.modified),
                    RedisBeanPropertyPrototype.Impl(DELETED, metadata.deleted),
                    RedisBeanPropertyPrototype.Impl(VOTES, metadata.votes)
            )
    )
}

fun Map<String, String?>?.toIdentityDBO() = when(this) {
    null -> null
    else -> {
        val firstName = this[CacheMapping.FIRST_NAME]
        val lastName = this[CacheMapping.LAST_NAME]
        val phoneNumber = this[CacheMapping.PHONE_NUMBER]
        val email = this[CacheMapping.EMAIL]
        val passwordHash = this[CacheMapping.PASSWORD_HASH]

        val id = this[CacheMapping.ID]?.let { UUID.fromString(it) }
        val version = this[CacheMapping.VERSION]?.toIntOrNull()
        val createdTime = this[CacheMapping.CREATED_TIME]?.let { Timestamp.valueOf(it) }
        val createdByIdentity = this[CacheMapping.CREATED_BY_IDENTITY]?.let { UUID.fromString(it) }
        val lastModifiedTime = this[CacheMapping.LAST_MODIFIED_TIME]?.let { Timestamp.valueOf(it) }
        val lastModifiedByIdentity = this[CacheMapping.LAST_MODIFIED_BY_IDENTITY]?.let { UUID.fromString(it) }
        val tags = this[CacheMapping.TAGS]?.let {
            ObjectMapper()
                    .configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true)
                    .readTree(it) }?.let {
            List(it.size(), { index ->
                (it[index] as ObjectNode).toArtifactTag()
            }).toSet()
        }
        val created = this[CacheMapping.CREATED]?.toBoolean()
        val modified = this[CacheMapping.MODIFIED]?.toBoolean()
        val deleted = this[CacheMapping.DELETED]?.toBoolean()
        val votes = this[CacheMapping.VOTES]?.toIntOrNull()


        when {
            firstName == null -> {
                null
            }
            lastName == null -> {
                null
            }
            email == null -> {
                null
            }
            passwordHash == null -> {
                null
            }
            id == null -> {
                null
            }
            version == null -> {
                null
            }
            createdTime == null -> {
                null
            }
            createdByIdentity == null -> {
                null
            }
            lastModifiedTime == null -> {
                null
            }
            lastModifiedByIdentity == null -> {
                null
            }
            tags == null -> {
                null
            }
            created == null -> {
                null
            }
            modified == null -> {
                null
            }
            deleted == null -> {
                null
            }
            votes == null -> {
                null
            }
            else -> null
            /*else -> IdentityDBO(
                    firstName = firstName,
                    lastName = lastName,
                    phoneNumber = phoneNumber,
                    email = email,
                    passwordHash = passwordHash,
                    metadata = ArtifactMetadata(
                            id = id,
                            version = version,
                            type = ArtifactType.category,
                            createdTime = createdTime,
                            createdByIdentity = createdByIdentity,
                            lastModifiedTime = lastModifiedTime,
                            lastModifiedByIdentity = lastModifiedByIdentity,
                            tags = tags,
                            created = created,
                            modified = modified,
                            deleted = deleted,
                            votes = votes
                    )
            )*/
        }
    }
}
