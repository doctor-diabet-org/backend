package org.doctordiabet.datalayer.dbo.roles

import org.doctordiabet.datalayer.database.enums.IdentityRoleType
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import java.util.*

data class RolesDBO(
        val identityId: UUID,
        val roles: List<IdentityRoleType>
)

fun RoleEntity.toDBO() = when(this) {
    RoleEntity.GUEST -> IdentityRoleType.guest
    RoleEntity.AUTHORIZED -> IdentityRoleType.authorized
    RoleEntity.SPECIALIST -> IdentityRoleType.specialist
    RoleEntity.MODERATOR -> IdentityRoleType.moderator
    RoleEntity.ADMINISTRATOR -> IdentityRoleType.administrator
}

fun IdentityRoleType.toEntity() = when(this) {
    IdentityRoleType.guest -> RoleEntity.GUEST
    IdentityRoleType.authorized -> RoleEntity.AUTHORIZED
    IdentityRoleType.specialist -> RoleEntity.SPECIALIST
    IdentityRoleType.moderator -> RoleEntity.MODERATOR
    IdentityRoleType.administrator -> RoleEntity.ADMINISTRATOR
}