package org.doctordiabet.datalayer.dbo.topics

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.comments.toComment
import org.doctordiabet.datalayer.dbo.identity.toEntity
import org.doctordiabet.datalayer.dbo.identity.toIdentityLink
import org.doctordiabet.datalayer.dbo.metadata.ArtifactMetadata
import org.doctordiabet.datalayer.dbo.metadata.toArtifactTag
import org.doctordiabet.datalayer.dbo.metadata.toTagEntity
import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.jooq.Configuration
import org.jooq.Record
import org.jooq.impl.DSL
import java.util.*

data class TopicDBO(
        val title: String,
        val description: String,
        val categoryId: UUID,
        val commentsCount: Long,
        val metadata: ArtifactMetadata
)

fun Record.toTopic(configuration: Configuration) = TopicDBO(
        title = this[Tables.TOPIC_CURRENT_VERSIONS.TITLE],
        description = this[Tables.TOPIC_CURRENT_VERSIONS.DESCRIPTION],
        categoryId = this[Tables.TOPIC_CURRENT_VERSIONS.CATEGORY],
        commentsCount = this[Tables.TOPIC_CURRENT_VERSIONS.COMMENTS_COUNT],
        metadata = ArtifactMetadata(
                id = this[Tables.TOPIC_CURRENT_VERSIONS.ID],
                version = this[Tables.TOPIC_CURRENT_VERSIONS.VERSION],
                type = ArtifactType.topic,
                createdTime = this[Tables.TOPIC_CURRENT_VERSIONS.CREATED_TIME],
                createdByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.TOPIC_CURRENT_VERSIONS.CREATED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                lastModifiedTime = this[Tables.TOPIC_CURRENT_VERSIONS.LAST_MODIFIED_TIME],
                lastModifiedByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.TOPIC_CURRENT_VERSIONS.LAST_MODIFIED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                created = this[Tables.TOPIC_CURRENT_VERSIONS.CREATED],
                modified = this[Tables.TOPIC_CURRENT_VERSIONS.UPDATED],
                deleted = this[Tables.TOPIC_CURRENT_VERSIONS.DELETED],
                votes = this[Tables.TOPIC_CURRENT_VERSIONS.VOTES].toInt(),
                tags = this[Tables.TOPIC_CURRENT_VERSIONS.TAGS].let {
                    if(it is ArrayNode && it.size() != 0) {
                        List(it.size(), { index ->
                            (it[index] as ObjectNode).toArtifactTag()
                        }).toSet()
                    } else {
                        emptySet()
                    }
                }
        )
)

fun TopicDBO.toEntity() = TopicEntity(
        title = this.title,
        description = this.description,
        categoryId = this.categoryId,
        commentsCount = this.commentsCount,
        metadata = ArtifactEntity.Metadata(
                id = this.metadata.id,
                createdTime = this.metadata.createdTime,
                createdByIdentity = this.metadata.createdByIdentity.toEntity(),
                lastModifiedTime = this.metadata.lastModifiedTime,
                lastModifiedByIdentity = this.metadata.lastModifiedByIdentity.toEntity(),
                tags = this.metadata.tags.map { it.toTagEntity() }.toSet(),
                type = when {
                    this.metadata.deleted -> ArtifactEntity.Type.DELETED
                    else -> ArtifactEntity.Type.EXISTING
                }
        )
)