package org.doctordiabet.datalayer.dbo.codes

import org.doctordiabet.datalayer.services.base.RedisBean
import org.doctordiabet.datalayer.services.base.RedisBeanPropertyPrototype
import org.doctordiabet.datalayer.services.base.RedisSingleQuery
import java.util.*

data class VerificationCodeDBO(
        val identityId: UUID,
        val code: String
)

private object CacheMapping {

    const val ENTITY_TYPE = "Verification-code"
    const val IDENTITY_ID = "identity"
    const val CODE = "code"
}

fun verificationCodeSingleQuery(identityId: UUID) = RedisSingleQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        hash = identityId.toString()
)

fun VerificationCodeDBO.toCacheObject() = with(CacheMapping) {
    RedisBean.Impl(
            hash = identityId.toString(),
            entityType = ENTITY_TYPE,
            properties = setOf(
                    RedisBeanPropertyPrototype.Impl(CODE, code)
            )
    )
}

fun Map<String, String?>.toVerificationCode(identityId: UUID) = with(CacheMapping) {
    val code = get(CODE)
    when {
        code == null -> null
        else -> VerificationCodeDBO(
                identityId = identityId,
                code = code
        )
    }
}