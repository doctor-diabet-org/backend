package org.doctordiabet.datalayer.dbo.identity

import org.doctordiabet.domainlayer.encryption.HashProcessor
import java.util.UUID;

data class IdentityCreateModel(
        val firstName: String,
        val lastName: String,
        val phoneNumber: String? = null,
        val email: String,
        val passwordHash: String,
        val creatingByIdentity: UUID? = null
)

fun org.doctordiabet.domainlayer.entity.identity.IdentityCreateModel.toDatabaseModel(processor: HashProcessor) = IdentityCreateModel(
        firstName = firstName,
        lastName = lastName,
        phoneNumber = phoneNumber,
        email = email,
        passwordHash = password.let(processor::apply),
        creatingByIdentity = creatingByIdentity
)