package org.doctordiabet.datalayer.dbo.comments

import java.util.*

data class CommentDeleteModel(
        val id: UUID,
        val modifyingByIdentity: UUID
)

fun org.doctordiabet.domainlayer.entity.comments.CommentDeleteModel.toDatabaseModel() = CommentDeleteModel(
        id,
        modifyingByIdentity
)