package org.doctordiabet.datalayer.dbo.categories

import java.util.*

data class CategoryDeleteModel(
        val id: UUID,
        val modifyingByIdentity: UUID
)

fun org.doctordiabet.domainlayer.entity.categories.CategoryDeleteModel.toDatabaseModel() = CategoryDeleteModel(
        id = id,
        modifyingByIdentity = modifyingByIdentity
)