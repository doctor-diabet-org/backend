package org.doctordiabet.datalayer.dbo.topics

import java.util.*

data class TopicUpdateModel(
        val id: UUID,
        val title: String? = null,
        val description: String? = null,
        val categoryId: UUID? = null,
        val modifyingByIdentity: UUID
)

fun org.doctordiabet.domainlayer.entity.topics.TopicUpdateModel.toDatabaseModel() = TopicUpdateModel(
        id = id,
        title = title,
        description = description,
        categoryId = categoryId,
        modifyingByIdentity = modifyingByIdentity
)