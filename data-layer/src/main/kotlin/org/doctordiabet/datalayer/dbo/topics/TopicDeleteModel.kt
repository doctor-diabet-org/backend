package org.doctordiabet.datalayer.dbo.topics

import java.util.*

data class TopicDeleteModel(
        val id: UUID,
        val modifyingByIdentity: UUID
)

fun org.doctordiabet.domainlayer.entity.topics.TopicDeleteModel.toDatabaseModel() = TopicDeleteModel(
        id = id,
        modifyingByIdentity = modifyingByIdentity
)