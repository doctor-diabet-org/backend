package org.doctordiabet.datalayer.dbo.categories

import java.util.*

data class CategoryCreateModel(
        val creatingByIdentity: UUID? = null,
        val title: String,
        val description: String
)

fun org.doctordiabet.domainlayer.entity.categories.CategoryCreateModel.toDatabaseModel() = CategoryCreateModel(
        creatingByIdentity = this.creatingByIdentity,
        title = this.title,
        description = this.description
)