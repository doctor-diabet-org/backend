package org.doctordiabet.datalayer.dbo.identity

import java.util.*

data class IdentityDeleteModel(
        val id: UUID,
        val modifyingByIdentity: UUID
)

fun org.doctordiabet.domainlayer.entity.identity.IdentityDeleteModel.toDatabaseModel() = IdentityDeleteModel(
        id = id,
        modifyingByIdentity = modifyingByIdentity
)