package org.doctordiabet.datalayer.dbo.comments

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.identity.toEntity
import org.doctordiabet.datalayer.dbo.identity.toIdentityLink
import org.doctordiabet.datalayer.dbo.metadata.ArtifactMetadata
import org.doctordiabet.datalayer.dbo.metadata.toArtifactTag
import org.doctordiabet.datalayer.dbo.metadata.toTagEntity
import org.doctordiabet.datalayer.dbo.topics.toEntity
import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.jooq.Configuration
import org.jooq.Record
import org.jooq.impl.DSL
import java.util.*

data class CommentDBO(
        val rootArtifactReferralId: UUID,
        val artifactReferralId: UUID,
        val contentText: String,
        val depth: Int,
        val metadata: ArtifactMetadata
)

fun Record.toComment(configuration: Configuration) = CommentDBO(
        rootArtifactReferralId = this[Tables.COMMENT_CURRENT_VERSIONS.ROOT_ARTIFACT_REFERRAL_ID],
        artifactReferralId = this[Tables.COMMENT_CURRENT_VERSIONS.ARTIFACT_REFERRAL_ID],
        contentText = this[Tables.COMMENT_CURRENT_VERSIONS.CONTENT_TEXT],
        depth = this[Tables.COMMENT_CURRENT_VERSIONS.DEPTH],
        metadata = ArtifactMetadata(
                id = this[Tables.COMMENT_CURRENT_VERSIONS.ID],
                version = this[Tables.COMMENT_CURRENT_VERSIONS.COMMENT_VERSION],
                type = ArtifactType.comment,
                createdTime = this[Tables.COMMENT_CURRENT_VERSIONS.CREATED_TIME],
                createdByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.COMMENT_CURRENT_VERSIONS.CREATED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                lastModifiedTime = this[Tables.COMMENT_CURRENT_VERSIONS.LAST_MODIFIED_TIME],
                lastModifiedByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.COMMENT_CURRENT_VERSIONS.LAST_MODIFIED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                created = this[Tables.COMMENT_CURRENT_VERSIONS.CREATED],
                modified = this[Tables.COMMENT_CURRENT_VERSIONS.UPDATED],
                deleted = this[Tables.COMMENT_CURRENT_VERSIONS.DELETED],
                votes = this[Tables.COMMENT_CURRENT_VERSIONS.VOTES].toInt(),
                tags = this[Tables.COMMENT_CURRENT_VERSIONS.TAGS].let {
                    if(it is ArrayNode && it.size() != 0) {
                        List(it.size(), { index ->
                            (it[index] as ObjectNode).toArtifactTag()
                        }).toSet()
                    } else {
                        emptySet()
                    }
                }
        )
)

fun CommentDBO.toEntity() = CommentEntity(
        artifactReferralId = this.artifactReferralId,
        contentText = this.contentText,
        metadata = ArtifactEntity.Metadata(
                id = this.metadata.id,
                createdTime = this.metadata.createdTime,
                createdByIdentity = this.metadata.createdByIdentity.toEntity(),
                lastModifiedTime = this.metadata.lastModifiedTime,
                lastModifiedByIdentity = this.metadata.lastModifiedByIdentity.toEntity(),
                tags = this.metadata.tags.map { it.toTagEntity() }.toSet(),
                type = when {
                    this.metadata.deleted -> ArtifactEntity.Type.DELETED
                    else -> ArtifactEntity.Type.EXISTING
                }
        )
)