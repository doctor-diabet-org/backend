package org.doctordiabet.datalayer.dbo.identity

import org.doctordiabet.domainlayer.encryption.HashProcessor
import java.util.*

data class IdentityUpdateModel(
        val id: UUID,
        val modifyingByIdentity: UUID,
        val firstName: String? = null,
        val lastName: String? = null,
        val phoneNumber: String? = null,
        val email: String? = null,
        val passwordHash: String? = null
)

fun org.doctordiabet.domainlayer.entity.identity.IdentityUpdateModel.toDatabaseModel(processor: HashProcessor) = IdentityUpdateModel(
        id = id,
        modifyingByIdentity = modifyingByIdentity,
        firstName = firstName,
        lastName = lastName,
        phoneNumber = phoneNumber,
        email = email,
        passwordHash = password?.let(processor::apply)
)