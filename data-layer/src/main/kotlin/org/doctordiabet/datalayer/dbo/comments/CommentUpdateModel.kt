package org.doctordiabet.datalayer.dbo.comments

import java.util.*

data class CommentUpdateModel(
        val id: UUID,
        val contentText: String? = null,
        val modifyingByIdentity: UUID
)

fun org.doctordiabet.domainlayer.entity.comments.CommentUpdateModel.toDatabaseModel() = CommentUpdateModel(
        id,
        contentText,
        modifyingByIdentity
)