package org.doctordiabet.datalayer.dbo.comments

import java.util.*

data class CommentCreateModel(
        val artifactReferralId: UUID,
        val contentText: String,
        val modifyingByIdentity: UUID? = null
)

fun org.doctordiabet.domainlayer.entity.comments.CommentCreateModel.toDatabaseModel() = CommentCreateModel(
        artifactReferralId,
        contentText,
        modifyingByIdentity
)