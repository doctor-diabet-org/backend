package org.doctordiabet.datalayer.dbo.categories

import java.util.*

data class CategoryUpdateModel(
        val id: UUID,
        val modifyingByIdentity: UUID,
        val title: String? = null,
        val description: String? = null
)

fun org.doctordiabet.domainlayer.entity.categories.CategoryUpdateModel.toDatabaseModel() = CategoryUpdateModel(
        id = this.id,
        modifyingByIdentity = this.modifyingByIdentity,
        title = this.title,
        description = this.description
)