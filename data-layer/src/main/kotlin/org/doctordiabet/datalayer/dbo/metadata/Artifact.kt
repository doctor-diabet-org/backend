package org.doctordiabet.datalayer.dbo.metadata

import org.doctordiabet.datalayer.database.enums.ArtifactType
import java.sql.Timestamp
import java.util.*

data class Artifact(
        val id: UUID,
        val type: ArtifactType,
        val createdTime: Timestamp,
        val createdByIdentity: UUID
)