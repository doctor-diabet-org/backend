package org.doctordiabet.datalayer.dbo.metadata

import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.identity.IdentityLinkDBO

import java.sql.Timestamp
import java.util.*

data class ArtifactMetadata(
        val id: UUID,
        val version: Int = 0,
        val type: ArtifactType,
        val createdTime: Timestamp,
        val createdByIdentity: IdentityLinkDBO,
        val lastModifiedTime: Timestamp,
        val lastModifiedByIdentity: IdentityLinkDBO,
        val created: Boolean = false,
        val modified: Boolean = false,
        val deleted: Boolean = false,
        val votes: Int = 0,
        val tags: Set<ArtifactTag>
)