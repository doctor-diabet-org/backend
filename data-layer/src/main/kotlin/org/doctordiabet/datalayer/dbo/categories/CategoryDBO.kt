package org.doctordiabet.datalayer.dbo.categories

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import org.doctordiabet.datalayer.database.Tables
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.identity.toEntity
import org.doctordiabet.datalayer.dbo.identity.toIdentityLink
import org.doctordiabet.datalayer.dbo.metadata.ArtifactMetadata
import org.doctordiabet.datalayer.dbo.metadata.toArtifactTag
import org.doctordiabet.datalayer.dbo.metadata.toJsonObject
import org.doctordiabet.datalayer.dbo.metadata.toTagEntity
import org.doctordiabet.datalayer.mapping.utils.redis.RedisTimestampBinding
import org.doctordiabet.datalayer.services.base.*
import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.jooq.Configuration
import org.jooq.Record
import org.jooq.impl.DSL
import java.sql.Timestamp
import java.util.*

private val mappingContext = ObjectMapper()

data class CategoryDBO(
        val title: String,
        val description: String,
        val topicsCount: Long,
        val metadata: ArtifactMetadata
)

fun Record.toCategory(configuration: Configuration) = CategoryDBO(
        title = this[Tables.CATEGORY_CURRENT_VERSIONS.TITLE],
        description = this[Tables.CATEGORY_CURRENT_VERSIONS.DESCRIPTION],
        topicsCount = this[Tables.CATEGORY_CURRENT_VERSIONS.TOPICS_COUNT],
        metadata = ArtifactMetadata(
                id = this[Tables.CATEGORY_CURRENT_VERSIONS.ID],
                version = this[Tables.CATEGORY_CURRENT_VERSIONS.VERSION],
                type = ArtifactType.category,
                createdTime = this[Tables.CATEGORY_CURRENT_VERSIONS.CREATED_TIME],
                createdByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.CATEGORY_CURRENT_VERSIONS.CREATED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                lastModifiedTime = this[Tables.CATEGORY_CURRENT_VERSIONS.LAST_MODIFIED_TIME],
                lastModifiedByIdentity = DSL.using(configuration).select(Tables.IDENTITY_CURRENT_VERSIONS.ID, Tables.IDENTITY_CURRENT_VERSIONS.FIRST_NAME, Tables.IDENTITY_CURRENT_VERSIONS.LAST_NAME).from(Tables.IDENTITY_CURRENT_VERSIONS).where(Tables.IDENTITY_CURRENT_VERSIONS.ID.eq(this[Tables.CATEGORY_CURRENT_VERSIONS.LAST_MODIFIED_BY_IDENTITY])).fetchOne().toIdentityLink(),
                created = this[Tables.CATEGORY_CURRENT_VERSIONS.CREATED],
                modified = this[Tables.CATEGORY_CURRENT_VERSIONS.UPDATED],
                deleted = this[Tables.CATEGORY_CURRENT_VERSIONS.DELETED],
                votes = this[Tables.CATEGORY_CURRENT_VERSIONS.VOTES].toInt(),
                tags = this[Tables.CATEGORY_CURRENT_VERSIONS.TAGS].let {
                    if(it is ArrayNode && it.size() != 0) {
                        List(it.size(), { index ->
                            (it[index] as ObjectNode).toArtifactTag()
                        }).toSet()
                    } else {
                        emptySet()
                    }
                }
        )
)

fun CategoryDBO.toEntity() = CategoryEntity(
        title = this.title,
        description = this.description,
        topicsCount = this.topicsCount,
        metadata = ArtifactEntity.Metadata(
                id = this.metadata.id,
                createdTime = this.metadata.createdTime,
                createdByIdentity = this.metadata.createdByIdentity.toEntity(),
                lastModifiedTime = this.metadata.lastModifiedTime,
                lastModifiedByIdentity = this.metadata.lastModifiedByIdentity.toEntity(),
                tags = this.metadata.tags.map { it.toTagEntity() }.toSet(),
                type = when {
                    this.metadata.deleted -> ArtifactEntity.Type.DELETED
                    else -> ArtifactEntity.Type.EXISTING
                },
                votes = this.metadata.votes
        )
)

private object CacheMapping {

    const val ENTITY_TYPE = "category"
    const val ENTITY_TYPE_CT = "category-ct-index"
    const val ENTITY_TYPE_LMT = "category-lmt-index"

    const val TITLE = "title"
    const val TOPICS_COUNT = "topics_count"
    const val ID = "eid"
    const val VERSION = "v"
    const val CREATED_TIME = "ct"
    const val CREATED_BY_IDENTITY = "cbi"
    const val LAST_MODIFIED_TIME = "lmt"
    const val LAST_MODIFIED_BY_IDENTITY = "lmbi"
    const val TAGS = "tags"
    const val CREATED = "cr"
    const val MODIFIED = "mod"
    const val DELETED = "del"
    const val VOTES = "votes"
}

fun categoryCacheSingleQuery(id: UUID) = RedisSingleQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        hash = id.toString()
)

fun categoryLastModifiedRangeCacheGet(offset: Int = 0,
                                      limit: Int = -1,
                                      order: OrderBy = OrderBy.DESC) = RedisRangeQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        field = CacheMapping.LAST_MODIFIED_TIME,
        offset = offset.toLong(),
        limit = limit.toLong(),
        order = order
)

fun categoryLastModifiedRangeScoreCacheGet(lowerBoundInclusive: Timestamp? = null,
                                           upperBoundInclusive: Timestamp? = null,
                                           offset: Int = 0,
                                           limit: Int = -1,
                                           order: OrderBy = OrderBy.DESC) = RedisRangeScoreQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        field = CacheMapping.LAST_MODIFIED_TIME,
        fromScore = when(lowerBoundInclusive) {
            null -> null
            else -> RedisTimestampBinding.instance.toScore(lowerBoundInclusive)
        },
        toScore = when(upperBoundInclusive) {
            null -> null
            else -> RedisTimestampBinding.instance.toScore(upperBoundInclusive)
        },
        offset = offset,
        limit = limit,
        order = order
)

fun categoryHashRangeCacheGet(hashes: Set<String>) = RedisHashSetQuery.Impl(
        entityType = CacheMapping.ENTITY_TYPE,
        hashes = hashes
)

fun CategoryDBO.toCacheObject() = with(CacheMapping) {
    RedisBean.Impl(
            hash = metadata.id.toString(),
            entityType = ENTITY_TYPE,
            properties = setOf(
                    RedisBeanPropertyPrototype.Impl(TITLE, title),
                    RedisBeanPropertyPrototype.Impl(TOPICS_COUNT, topicsCount),
                    RedisBeanPropertyPrototype.Impl(ID, metadata.id),
                    RedisBeanPropertyPrototype.Impl(VERSION, metadata.version),
                    RedisBeanIndexPropertyPrototype.Impl(CREATED_TIME, metadata.createdTime, RedisTimestampBinding.instance),
                    RedisBeanPropertyPrototype.Impl(CREATED_BY_IDENTITY, metadata.createdByIdentity),
                    RedisBeanIndexPropertyPrototype.Impl(LAST_MODIFIED_TIME, metadata.lastModifiedTime, RedisTimestampBinding.instance),
                    RedisBeanPropertyPrototype.Impl(LAST_MODIFIED_BY_IDENTITY, metadata.lastModifiedByIdentity),
                    RedisBeanPropertyPrototype.Impl(TAGS, JsonNodeFactory.instance.arrayNode().apply {
                        addAll(metadata.tags.map { it.toJsonObject() })
                    }),
                    RedisBeanPropertyPrototype.Impl(CREATED, metadata.created),
                    RedisBeanPropertyPrototype.Impl(MODIFIED, metadata.modified),
                    RedisBeanPropertyPrototype.Impl(DELETED, metadata.deleted),
                    RedisBeanPropertyPrototype.Impl(VOTES, metadata.votes)
            )
    )
}

fun CategoryDBO.toCtCacheIndex() = with(CacheMapping) {
    RedisBeanIndex.Impl(
            metadata.id.toString(),
            ENTITY_TYPE_CT,
            metadata.createdTime.time.toDouble()
    )
}

fun categoryCtRangeQuery(lowerBoundary: Timestamp, offset: Int, limit: Int, order: OrderBy = OrderBy.DESC) = with(CacheMapping) {
    RedisRangeScoreQueryRaw.Impl(
            ENTITY_TYPE_CT,
            lowerBoundary.time.toDouble(),
            null,
            offset,
            limit,
            order
    )
}

fun CategoryDBO.toLmtCacheIndex() = with(CacheMapping) {
    RedisBeanIndex.Impl(
            metadata.id.toString(),
            ENTITY_TYPE_LMT,
            metadata.lastModifiedTime.time.toDouble()
    )
}

fun categoryLmtRangeQuery(lowerBoundary: Timestamp, offset: Int, limit: Int, order: OrderBy = OrderBy.DESC) = with(CacheMapping) {
    RedisRangeScoreQueryRaw.Impl(
            ENTITY_TYPE_LMT,
            lowerBoundary.time.toDouble(),
            null,
            offset,
            limit,
            order
    )
}

fun Map<String, String?>?.toCategoryDBO() = when(this) {
    null -> null
    else -> {
        val title = this[CacheMapping.TITLE]
        val topicsCount = this[CacheMapping.TOPICS_COUNT]?.toLongOrNull()
        val id = this[CacheMapping.ID]?.let { UUID.fromString(it) }
        val version = this[CacheMapping.VERSION]?.toIntOrNull()
        val createdTime = this[CacheMapping.CREATED_TIME]?.let { Timestamp.valueOf(it) }
        val createdByIdentity = this[CacheMapping.CREATED_BY_IDENTITY]?.let { UUID.fromString(it) }
        val lastModifiedTime = this[CacheMapping.LAST_MODIFIED_TIME]?.let { Timestamp.valueOf(it) }
        val lastModifiedByIdentity = this[CacheMapping.LAST_MODIFIED_BY_IDENTITY]?.let { UUID.fromString(it) }
        val tags = this[CacheMapping.TAGS]?.let {
            ObjectMapper()
                .configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true)
                .readTree(it) }?.let {
            List(it.size(), { index ->
                (it[index] as ObjectNode).toArtifactTag()
            }).toSet()
        }
        val created = this[CacheMapping.CREATED]?.toBoolean()
        val modified = this[CacheMapping.MODIFIED]?.toBoolean()
        val deleted = this[CacheMapping.DELETED]?.toBoolean()
        val votes = this[CacheMapping.VOTES]?.toIntOrNull()


        when {
            title == null -> {
                null
            }
            topicsCount == null -> {
                null
            }
            id == null -> {
                null
            }
            version == null -> {
                null
            }
            createdTime == null -> {
                null
            }
            createdByIdentity == null -> {
                null
            }
            lastModifiedTime == null -> {
                null
            }
            lastModifiedByIdentity == null -> {
                null
            }
            tags == null -> {
                null
            }
            created == null -> {
                null
            }
            modified == null -> {
                null
            }
            deleted == null -> {
                null
            }
            votes == null -> {
                null
            }
            else -> null
            /*else -> CategoryDBO(
                    title = title,
                    topicsCount = topicsCount,
                    metadata = ArtifactMetadata(
                            id = id,
                            version = version,
                            type = ArtifactType.category,
                            createdTime = createdTime,
                            createdByIdentity = createdByIdentity,
                            lastModifiedTime = lastModifiedTime,
                            lastModifiedByIdentity = lastModifiedByIdentity,
                            tags = tags,
                            created = created,
                            modified = modified,
                            deleted = deleted,
                            votes = votes
                    )
            )*/
        }
    }
}