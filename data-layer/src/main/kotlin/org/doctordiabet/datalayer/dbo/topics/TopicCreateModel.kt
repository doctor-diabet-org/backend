package org.doctordiabet.datalayer.dbo.topics

import java.util.*

data class TopicCreateModel(
        val title: String,
        val description: String,
        val tagIds: List<UUID>,
        val categoryId: UUID,
        val creatingByIdentity: UUID? = null
)

fun org.doctordiabet.domainlayer.entity.topics.TopicCreateModel.toDatabaseModel() = TopicCreateModel(
        title = title,
        description = description,
        tagIds = tagIds,
        categoryId = categoryId,
        creatingByIdentity = creatingByIdentity
)