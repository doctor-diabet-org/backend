package org.doctordiabet.datalayer.encryption.hashes

import org.doctordiabet.datalayer.encryption.utils.toHexString
import org.doctordiabet.domainlayer.encryption.HashProcessor
import java.security.MessageDigest

class SHA256Processor : HashProcessor {

    override fun apply(value: String): String = MessageDigest.getInstance("SHA-256").let {
        it.update(value.toByteArray(charset("UTF-8")))
        it.digest().toHexString()
    }
}