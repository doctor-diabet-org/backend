package org.doctordiabet.datalayer.encryption.utils

import java.util.*

private val HEX_CHARS = "0123456789ABCDEF".toCharArray()

fun ByteArray.toHexString(): String {
    val sb = StringBuilder(size * 2)
    for (b in this) {
        sb.append(HEX_CHARS[b.toInt() and 0xF0 shr 4])
        sb.append(HEX_CHARS[b.toInt() and 0x0F])
    }

    return sb.toString()
}

fun randomBytes(generator: Random, size: Int): ByteArray = kotlin.ByteArray(size).apply {
    generator.nextBytes(this)
}