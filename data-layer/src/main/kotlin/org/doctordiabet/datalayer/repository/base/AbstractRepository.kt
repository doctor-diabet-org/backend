package org.doctordiabet.datalayer.repository.base

import io.reactivex.*
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.exceptions.InternalExceptionAccessor

abstract class AbstractRepository {

    protected fun <T> hookInternalMaybeExceptions(): MaybeTransformer<T, T> {
        return MaybeTransformer { upstream ->
            upstream.onErrorResumeNext { throwable: Throwable ->
                when (throwable) {
                    is EmptyMaybeException -> Maybe.error(throwable)
                    is ExceptionBundle -> Maybe.error(throwable)
                    else -> Maybe.error(throwable.parse())
                }
            }
        }
    }

    protected fun <T> hookInternalSingleExceptions(): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream.onErrorResumeNext { throwable: Throwable ->
                when (throwable) {
                    is ExceptionBundle -> Single.error(throwable)
                    else -> Single.error(throwable.parse())
                }
            }
        }
    }

    protected fun <T> hookInternalObservableExceptions(): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream.onErrorResumeNext { throwable: Throwable ->
                when (throwable) {
                    is ExceptionBundle -> Observable.error(throwable)
                    else -> Observable.error(throwable.parse())
                }
            }
        }
    }

    protected fun <T> hookInternalFlowableExceptions(): FlowableTransformer<T, T> {
        return FlowableTransformer { upstream ->
            upstream.onErrorResumeNext { throwable: Throwable ->
                when (throwable) {
                    is ExceptionBundle -> Flowable.error(throwable)
                    else -> Flowable.error(throwable.parse())
                }
            }
        }
    }

    private fun Throwable.parse(): ExceptionBundle {
        val bundle = ExceptionBundle(ExceptionSource.INTERNAL)
        val accessor = bundle.accessor as InternalExceptionAccessor
        accessor.message = message ?: localizedMessage ?: "N/A"
        accessor.cause = this
        return bundle
    }
}