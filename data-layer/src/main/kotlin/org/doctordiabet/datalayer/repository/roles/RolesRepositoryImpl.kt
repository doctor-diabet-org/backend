package org.doctordiabet.datalayer.repository.roles

import io.reactivex.Single
import org.doctordiabet.datalayer.dbo.roles.toDBO
import org.doctordiabet.datalayer.dbo.roles.toEntity
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.roles.RolesPersistenceService
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.repository.roles.RolesRepository
import java.util.*

class RolesRepositoryImpl(
        private val persistence: RolesPersistenceService
): RolesRepository, AbstractRepository() {

    override fun grant(identityId: UUID, role: RoleEntity): Single<List<RoleEntity>> = persistence.grant(identityId, role.toDBO())
            .map { it.roles }
            .map { it.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun read(identityId: UUID): Single<List<RoleEntity>> = persistence.read(identityId)
            .map { it.roles }
            .map { it.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun revoke(identityId: UUID, role: RoleEntity): Single<List<RoleEntity>> = persistence.revoke(identityId, role.toDBO())
            .map { it.roles }
            .map { it.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun relationshipOf(identityId: UUID, artifactId: UUID): Single<RelationshipEntity> = persistence.readRelationships(identityId, artifactId)
            .compose(hookInternalSingleExceptions())
}