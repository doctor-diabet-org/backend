package org.doctordiabet.datalayer.repository.categories

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import org.doctordiabet.datalayer.dbo.categories.toDatabaseModel
import org.doctordiabet.datalayer.dbo.categories.toEntity
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.categories.CategoriesCacheService
import org.doctordiabet.datalayer.services.categories.CategoriesPersistenceService
import org.doctordiabet.domainlayer.contract.order.ArtifactFilter
import org.doctordiabet.domainlayer.entity.categories.CategoryCreateModel
import org.doctordiabet.domainlayer.entity.categories.CategoryDeleteModel
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.entity.categories.CategoryUpdateModel
import org.doctordiabet.domainlayer.repository.categories.CategoriesRepository
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class CategoriesRepositoryImpl(
        private val persistence: CategoriesPersistenceService,
        private val cache: CategoriesCacheService
): AbstractRepository(), CategoriesRepository {

    private val observers: MutableList<Observer> = mutableListOf()

    override fun create(model: CategoryCreateModel): Single<CategoryEntity> = persistence.create(model.toDatabaseModel())
            .map { it.toEntity() }
            .doOnSuccess { entity -> notifyObserversCreate(model.creatingByIdentity, entity.metadata.id) }
            .compose(hookInternalSingleExceptions())

    override fun read(id: UUID): Single<CategoryEntity> = persistence.read(id)
            .map { it.toEntity() }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, tagIds: List<UUID>, criteria: List<ArtifactFilter>): Single<Pair<Int, List<CategoryEntity>>> = persistence.read(offset, limit, tagIds)
            .map { it.first to it.second.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID>, criteria: List<ArtifactFilter>): Single<Pair<Int, List<CategoryEntity>>> = when(modified) {
        true -> persistence.readModified(offset, limit, timestamp, tagIds)
        else -> persistence.readUnmodified(offset, limit, timestamp, tagIds)
    }
            .map { it.first to it.second.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun update(model: CategoryUpdateModel): Single<CategoryEntity> = persistence.update(model.toDatabaseModel())
            .map { it.toEntity() }
            .doOnSuccess { entity -> notifyObserversUpdate(model.modifyingByIdentity, entity.metadata.id) }
            .compose(hookInternalSingleExceptions())

    override fun delete(model: CategoryDeleteModel): Single<UUID> = persistence.delete(model.toDatabaseModel())
            .doOnSuccess { entityId -> notifyObserversDelete(model.modifyingByIdentity, entityId) }
            .compose(hookInternalSingleExceptions())

    override fun sub(): Observable<CategoriesRepository.Event> = Observable.create { emitter ->
        val observer = object: Observer {
            override fun onCreate(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(CategoriesRepository.Event.Create(identityId, entityId))
                }
            }

            override fun onUpdate(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(CategoriesRepository.Event.Update(identityId, entityId))
                }
            }

            override fun onDelete(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(CategoriesRepository.Event.Delete(identityId, entityId))
                }
            }
        }

        emitter.setDisposable(object: Disposable {
            private val disposed: AtomicBoolean = AtomicBoolean(false)

            override fun isDisposed(): Boolean = disposed.get()

            override fun dispose() {
                if(disposed.compareAndSet(false, true)) {
                    observers.remove(observer)
                }
            }
        })

        observers.add(observer)
    }

    private fun notifyObserversCreate(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onCreate(identityId, entityId) }
    }

    private fun notifyObserversUpdate(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onUpdate(identityId, entityId) }
    }

    private fun notifyObserversDelete(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onDelete(identityId, entityId) }
    }

    private interface Observer {

        fun onCreate(identityId: UUID, entityId: UUID)

        fun onUpdate(identityId: UUID, entityId: UUID)

        fun onDelete(identityId: UUID, entityId: UUID)
    }
}