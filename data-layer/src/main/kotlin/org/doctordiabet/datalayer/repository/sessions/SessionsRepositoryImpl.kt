package org.doctordiabet.datalayer.repository.sessions

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.datalayer.dbo.session.PersistentTokenDBO
import org.doctordiabet.datalayer.dbo.session.SessionTokenDBO
import org.doctordiabet.datalayer.encryption.hashes.SHA512Processor
import org.doctordiabet.datalayer.encryption.utils.randomBytes
import org.doctordiabet.datalayer.encryption.utils.toHexString
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.sessions.PersistentTokenCacheService
import org.doctordiabet.datalayer.services.sessions.PersistentTokenPersistenceService
import org.doctordiabet.datalayer.services.sessions.SessionTokenCacheService
import org.doctordiabet.domainlayer.encryption.HashProcessor
import org.doctordiabet.domainlayer.entity.session.PersistentTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity
import org.doctordiabet.domainlayer.entity.session.SessionTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.SessionTokenEntity
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import org.doctordiabet.domainlayer.repository.session.SessionsRepository
import java.security.SecureRandom
import java.util.*

class SessionsRepositoryImpl(
        private val persistentTokenPersistence: PersistentTokenPersistenceService,
        private val persistentTokenCache: PersistentTokenCacheService,
        private val sessionTokenCache: SessionTokenCacheService
): AbstractRepository(), SessionsRepository {

    companion object {

        const val SESSION_TOKEN_EXPIRATION = 3_600_000L
    }

    private val random: Random = SecureRandom()

    private val processor: HashProcessor = SHA512Processor()

    override fun createPersistent(model: PersistentTokenCreateModel): Single<PersistentTokenEntity> = persistentTokenPersistence
            .read(model.identityId)
            .onErrorResumeNext { throwable: Throwable ->
                when(throwable) {
                    is EmptyMaybeException -> persistentTokenPersistence
                            .create(PersistentTokenDBO(model.identityId, newPersistent(model.identityId)))
                            .doOnSuccess { persistentTokenCache.set(it).subscribe() }
                    else -> Single.error(throwable)
                }
            }
            .map { PersistentTokenEntity(it.identityId, it.refreshToken) }
            .compose(hookInternalSingleExceptions())

    override fun createSession(model: SessionTokenCreateModel): Single<SessionTokenEntity> = sessionTokenCache.set(
            System.currentTimeMillis().let { createdTime ->
                SessionTokenDBO(
                        identityId = model.identityId,
                        refreshToken = model.refreshToken,
                        sessionToken = newSession(model.identityId, model.refreshToken),
                        createdTime = createdTime,
                        expiration = SESSION_TOKEN_EXPIRATION,
                        expirationTime = createdTime + SESSION_TOKEN_EXPIRATION
                )
            }
    )
            .map { SessionTokenEntity(
                    identityId = it.identityId,
                    refreshToken = model.refreshToken,
                    sessionToken = it.sessionToken,
                    createdTime = it.createdTime,
                    expiration = it.expiration,
                    expirationTime = it.expirationTime
            ) }
            .compose(hookInternalSingleExceptions())

    override fun readPersistent(identityId: UUID): Single<PersistentTokenEntity> = persistentTokenPersistence.read(identityId)
            .map { PersistentTokenEntity(
                    identityId = it.identityId,
                    refreshToken = it.refreshToken
            ) }
            .compose(hookInternalSingleExceptions())

    override fun readSession(identityId: UUID): Single<List<SessionTokenEntity>> = sessionTokenCache.get(identityId)
            .map { it.map { SessionTokenEntity(
                    identityId = it.identityId,
                    refreshToken = it.refreshToken,
                    sessionToken = it.sessionToken,
                    createdTime = it.createdTime,
                    expiration = it.expiration,
                    expirationTime = it.expirationTime
            ) } }
            .compose(hookInternalSingleExceptions())

    override fun readSession(token: String): Single<UUID> = sessionTokenCache.get(token)
            .map { it.identityId }
            .compose(hookInternalSingleExceptions())

    private fun newSession(identityId: UUID, refreshToken: String) =
            processor.apply("${identityId}::${refreshToken}::${randomBytes(random, 32).toHexString()}")

    private fun newPersistent(identityId: UUID) =
            (("${identityId}::${System.currentTimeMillis()}::${randomBytes(random, 32).toHexString()}".let(processor::apply)
                    + randomBytes(random, 32).toHexString())
                    .let(processor::apply)) + randomBytes(random, 32).toHexString()
}