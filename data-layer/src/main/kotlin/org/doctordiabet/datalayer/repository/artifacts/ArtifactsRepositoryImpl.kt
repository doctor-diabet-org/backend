package org.doctordiabet.datalayer.repository.artifacts

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.artifacts.ArtifactsPersistenceService
import org.doctordiabet.domainlayer.entity.artifact.TagCreateModel
import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class ArtifactsRepositoryImpl(private val persistence: ArtifactsPersistenceService): ArtifactsRepository, AbstractRepository() {

    private val observers: MutableList<Observer> = mutableListOf()

    override fun subscribe(identityId: UUID, artifactId: UUID): Single<Boolean> =
            persistence.subscribe(identityId, artifactId)
                    .doOnSuccess { (first, _) -> notifyObserversSubscribe(identityId, artifactId, first) }
                    .map { it.second }
                    .compose(hookInternalSingleExceptions())

    override fun unsubscribe(identityId: UUID, artifactId: UUID): Single<Boolean> =
            persistence.unsubscribe(identityId, artifactId)
                    .doOnSuccess { (first, _) -> notifyObserversUnsubscribe(identityId, artifactId, first) }
                    .map { it.second }
                    .compose(hookInternalSingleExceptions())

    override fun subscribers(artifactId: UUID): Single<List<UUID>> =
            persistence.subscribers(artifactId)
                    .compose(hookInternalSingleExceptions())

    override fun createTags(models: List<TagCreateModel>): Single<List<TagEntity>> =
            persistence.createTags(models.map { it.value })
                .map { it.map { TagEntity(it.id, it.value) } }
                .compose(hookInternalSingleExceptions())

    override fun readTags(): Single<List<TagEntity>> =
            persistence.readTags()
                    .map { it.map { TagEntity(it.id, it.value) } }
                    .compose(hookInternalSingleExceptions())

    override fun attachTags(artifactId: UUID, tagIds: List<UUID>): Single<Unit> =
            persistence.attachTags(artifactId, tagIds)
                    .compose(hookInternalSingleExceptions())

    override fun detachTags(artifactId: UUID, tagIds: List<UUID>): Single<Unit> =
            persistence.detachTags(artifactId, tagIds)
                    .compose(hookInternalSingleExceptions())

    override fun upvote(identityId: UUID, artifactId: UUID): Single<Boolean> =
            persistence.upvote(identityId, artifactId)
                    .doOnSuccess { (type, modified) ->
                        if(modified) {
                            when(type) {
                                ArtifactType.identity -> notifyObserversIdentityUpvote(identityId, artifactId)
                                ArtifactType.category -> notifyObserversCategoryUpvote(identityId, artifactId)
                                ArtifactType.topic -> notifyObserversTopicUpvote(identityId, artifactId)
                                ArtifactType.comment -> notifyObserversCommentUpvote(identityId, artifactId)
                            }
                        }
                    }
                    .map { it.second }
                    .compose(hookInternalSingleExceptions())

    override fun downvote(identityId: UUID, artifactId: UUID): Single<Boolean> =
            persistence.downvote(identityId, artifactId)
                    .doOnSuccess { (type, modified) ->
                        if(modified) {
                            when(type) {
                                ArtifactType.identity -> notifyObserversIdentityDownvote(identityId, artifactId)
                                ArtifactType.category -> notifyObserversCategoryDownvote(identityId, artifactId)
                                ArtifactType.topic -> notifyObserversTopicDownvote(identityId, artifactId)
                                ArtifactType.comment -> notifyObserversCommentDownvote(identityId, artifactId)
                            }
                        }
                    }
                    .map { it.second }
                    .compose(hookInternalSingleExceptions())

    override fun sub(): Observable<ArtifactsRepository.Event> = Observable.create { emitter ->
        val observer = object: Observer {

            override fun onSubscribe(identityId: UUID, entityId: UUID, ownerId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.Subscribe(identityId, entityId, ownerId))
                }
            }

            override fun onUnsubscribe(identityId: UUID, entityId: UUID, ownerId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.Unsubscribe(identityId, entityId, ownerId))
                }
            }

            override fun onIdentityUpvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.IdentityUpvote(identityId, entityId))
                }
            }

            override fun onIdentityDownvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.IdentityDownvote(identityId, entityId))
                }
            }

            override fun onCategoryUpvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.CategoryUpvote(identityId, entityId))
                }
            }

            override fun onCategoryDownvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.CategoryDownvote(identityId, entityId))
                }
            }

            override fun onTopicUpvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.TopicUpvote(identityId, entityId))
                }
            }

            override fun onTopicDownvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.TopicDownvote(identityId, entityId))
                }
            }

            override fun onCommentUpvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.CommentUpvote(identityId, entityId))
                }
            }

            override fun onCommentDownvote(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(ArtifactsRepository.Event.CommentDownvote(identityId, entityId))
                }
            }
        }

        emitter.setDisposable(object: Disposable {
            private val disposed: AtomicBoolean = AtomicBoolean(false)

            override fun isDisposed(): Boolean = disposed.get()

            override fun dispose() {
                if(disposed.compareAndSet(false, true)) {
                    observers.remove(observer)
                }
            }
        })

        observers.add(observer)
    }

    private fun notifyObserversSubscribe(identityId: UUID, entityId: UUID, ownerId: UUID) {
        observers.forEach { it.onSubscribe(identityId, entityId, ownerId) }
    }

    private fun notifyObserversUnsubscribe(identityId: UUID, entityId: UUID, ownerId: UUID) {
        observers.forEach { it.onUnsubscribe(identityId, entityId, ownerId) }
    }

    private fun notifyObserversIdentityUpvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onIdentityUpvote(identityId, entityId) }
    }

    private fun notifyObserversIdentityDownvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onIdentityDownvote(identityId, entityId) }
    }

    private fun notifyObserversCategoryUpvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onCategoryUpvote(identityId, entityId) }
    }

    private fun notifyObserversCategoryDownvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onCategoryDownvote(identityId, entityId) }
    }

    private fun notifyObserversTopicUpvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onTopicUpvote(identityId, entityId) }
    }

    private fun notifyObserversTopicDownvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onTopicDownvote(identityId, entityId) }
    }

    private fun notifyObserversCommentUpvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onCommentUpvote(identityId, entityId) }
    }

    private fun notifyObserversCommentDownvote(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onCommentDownvote(identityId, entityId) }
    }

    private interface Observer {

        fun onSubscribe(identityId: UUID, entityId: UUID, ownerId: UUID)

        fun onUnsubscribe(identityId: UUID, entityId: UUID, ownerId: UUID)

        fun onIdentityUpvote(identityId: UUID, entityId: UUID)

        fun onIdentityDownvote(identityId: UUID, entityId: UUID)

        fun onCategoryUpvote(identityId: UUID, entityId: UUID)

        fun onCategoryDownvote(identityId: UUID, entityId: UUID)

        fun onTopicUpvote(identityId: UUID, entityId: UUID)

        fun onTopicDownvote(identityId: UUID, entityId: UUID)

        fun onCommentUpvote(identityId: UUID, entityId: UUID)

        fun onCommentDownvote(identityId: UUID, entityId: UUID)
    }
}