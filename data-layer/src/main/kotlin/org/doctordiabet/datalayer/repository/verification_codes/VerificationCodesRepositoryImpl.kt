package org.doctordiabet.datalayer.repository.verification_codes

import io.reactivex.Single
import org.doctordiabet.datalayer.dbo.codes.VerificationCodeDBO
import org.doctordiabet.datalayer.dto.sms.SmsReqDTO
import org.doctordiabet.datalayer.encryption.hashes.SHA256Processor
import org.doctordiabet.datalayer.encryption.utils.randomBytes
import org.doctordiabet.datalayer.encryption.utils.toHexString
import org.doctordiabet.datalayer.processing.sms.SmsProcessor
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.verification_codes.VerificationCodesCacheService
import org.doctordiabet.domainlayer.encryption.HashProcessor
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeCreateModel
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeVerifyModel
import org.doctordiabet.domainlayer.repository.verification_codes.VerificationCodesRepository
import java.security.SecureRandom
import java.util.*

class VerificationCodesRepositoryImpl(
        private val cache: VerificationCodesCacheService,
        private val smsProcessor: SmsProcessor
): AbstractRepository(), VerificationCodesRepository {

    private val random: Random = SecureRandom()
    private val hashProcessor: HashProcessor = SHA256Processor()

    override fun create(model: VerificationCodeCreateModel): Single<Unit> = smsProcessor.process(SmsReqDTO(model.phoneNumber, newCode()))
            .doOnSuccess { cache.set(VerificationCodeDBO(model.identityId, it.message)).subscribe() }
            .map { Unit }
            .compose(hookInternalSingleExceptions())

    override fun verify(model: VerificationCodeVerifyModel): Single<Boolean> = cache.get(model.identityId)
            .map { model.code.equals(it.code, ignoreCase = true) }
            .compose(hookInternalSingleExceptions())

    private fun newCode() = randomBytes(random, 16).toHexString().let(hashProcessor::apply).substring(0, 6)
}