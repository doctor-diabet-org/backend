package org.doctordiabet.datalayer.repository.topics

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import org.doctordiabet.datalayer.dbo.topics.toDatabaseModel
import org.doctordiabet.datalayer.dbo.topics.toEntity
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.topics.TopicsPersistenceService
import org.doctordiabet.domainlayer.entity.topics.TopicCreateModel
import org.doctordiabet.domainlayer.entity.topics.TopicDeleteModel
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.entity.topics.TopicUpdateModel
import org.doctordiabet.domainlayer.repository.topics.TopicsRepository
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class TopicsRepositoryImpl(
        private val persistence: TopicsPersistenceService
): AbstractRepository(), TopicsRepository {

    private val observers: MutableList<Observer> = mutableListOf()

    override fun create(model: TopicCreateModel): Single<TopicEntity> = persistence.create(model.toDatabaseModel())
            .map { it.toEntity() }
            .doOnSuccess { entity -> notifyObserversCreate(model.creatingByIdentity, entity.metadata.id) }
            .compose(hookInternalSingleExceptions())

    override fun read(id: UUID): Single<TopicEntity> = persistence.read(id)
            .map { it.toEntity() }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID>): Single<Pair<Int, List<TopicEntity>>> = when(modified) {
        true -> persistence.readModified(offset, limit, timestamp, tagIds)
        false -> persistence.readUnmodified(offset, limit, timestamp, tagIds)
    }
            .map { it.first to it.second.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun read(categoryId: UUID, offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID>): Single<Pair<Int, List<TopicEntity>>> = when(modified) {
        true -> persistence.readModified(categoryId, offset, limit, timestamp, tagIds)
        false -> persistence.readUnmodified(categoryId, offset, limit, timestamp, tagIds)
    }
            .map { it.first to it.second.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun update(model: TopicUpdateModel): Single<TopicEntity> = persistence.update(model.toDatabaseModel())
            .map { it.toEntity() }
            .doOnSuccess { entity -> notifyObserversUpdate(model.modifyingByIdentity, entity.metadata.id) }
            .compose(hookInternalSingleExceptions())

    override fun delete(model: TopicDeleteModel): Single<UUID> = persistence.delete(model.toDatabaseModel())
            .doOnSuccess { entityId -> notifyObserversDelete(model.modifyingByIdentity, entityId) }
            .compose(hookInternalSingleExceptions())

    private fun notifyObserversCreate(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onCreate(identityId, entityId) }
    }

    private fun notifyObserversUpdate(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onUpdate(identityId, entityId) }
    }

    private fun notifyObserversDelete(identityId: UUID, entityId: UUID) {
        observers.forEach { it.onDelete(identityId, entityId) }
    }

    override fun sub(): Observable<TopicsRepository.Event> = Observable.create { emitter ->
        val observer = object: Observer {
            override fun onCreate(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(TopicsRepository.Event.Create(identityId, entityId))
                }
            }

            override fun onUpdate(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(TopicsRepository.Event.Update(identityId, entityId))
                }
            }

            override fun onDelete(identityId: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(TopicsRepository.Event.Delete(identityId, entityId))
                }
            }
        }

        emitter.setDisposable(object: Disposable {
            private val disposed = AtomicBoolean(false)

            override fun isDisposed(): Boolean = disposed.get()

            override fun dispose() {
                if(disposed.compareAndSet(false, true)) {
                    observers.remove(observer)
                }
            }
        })

        observers.add(observer)
    }

    private interface Observer {

        fun onCreate(identityId: UUID, entityId: UUID)

        fun onUpdate(identityId: UUID, entityId: UUID)

        fun onDelete(identityId: UUID, entityId: UUID)
    }
}