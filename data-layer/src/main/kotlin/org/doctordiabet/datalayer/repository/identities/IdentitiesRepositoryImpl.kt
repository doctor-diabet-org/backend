package org.doctordiabet.datalayer.repository.identities

import io.reactivex.Single
import org.doctordiabet.datalayer.dbo.identity.IdentityDBO
import org.doctordiabet.datalayer.dbo.identity.toDatabaseModel
import org.doctordiabet.datalayer.dbo.identity.toPrivateEntity
import org.doctordiabet.datalayer.dbo.identity.toPublicEntity
import org.doctordiabet.datalayer.encryption.hashes.SHA256Processor
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.identities.IdentitiesCacheService
import org.doctordiabet.datalayer.services.identities.IdentitiesPersistenceService
import org.doctordiabet.domainlayer.encryption.HashProcessor
import org.doctordiabet.domainlayer.entity.identity.IdentityCreateModel
import org.doctordiabet.domainlayer.entity.identity.IdentityDeleteModel
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.entity.identity.IdentityUpdateModel
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import java.sql.Timestamp
import java.util.*

class IdentitiesRepositoryImpl(
        private val persistence: IdentitiesPersistenceService,
        private val cache: IdentitiesCacheService
): AbstractRepository(), IdentitiesRepository {

    private val processor: HashProcessor = SHA256Processor()

    override fun create(model: IdentityCreateModel, full: Boolean): Single<IdentityEntity> = persistence.create(model.toDatabaseModel(processor))
            .map { it.toEntity(full) }
            .compose(hookInternalSingleExceptions())

    override fun read(id: UUID, full: Boolean): Single<IdentityEntity> = persistence.read(id)
            .map { it.toEntity(full) }
            .compose(hookInternalSingleExceptions())

    override fun readByPhone(phoneNumber: String, full: Boolean): Single<IdentityEntity> = persistence.readByPhone(phoneNumber)
            .map { it.toEntity(full) }
            .compose(hookInternalSingleExceptions())

    override fun readByEmail(email: String, full: Boolean): Single<IdentityEntity> = persistence.readByEmail(email)
            .map { it.toEntity(full) }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, full: Boolean): Single<Pair<Int, List<IdentityEntity>>> = persistence.read(offset, limit)
            .map { it.first to it.second.map { it.toEntity(full) } }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, full: Boolean): Single<Pair<Int, List<IdentityEntity>>> = when(modified) {
        true -> persistence.readModified(offset, limit, timestamp)
        else -> persistence.readUnmodified(offset, limit, timestamp)
    }
            .map { it.first to it.second.map { it.toEntity(full) } }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, nameSubstring: String, full: Boolean): Single<Pair<Int, List<IdentityEntity>>> = persistence.read(offset, limit, nameSubstring)
            .map { it.first to it.second.map { it.toEntity(full) } }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, nameSubstring: String, timestamp: Timestamp, modified: Boolean, full: Boolean): Single<Pair<Int, List<IdentityEntity>>> = when(modified) {
        true -> persistence.readModified(offset, limit, nameSubstring, timestamp)
        else -> persistence.readUnmodified(offset, limit, nameSubstring, timestamp)
    }
            .map { it.first to it.second.map { it.toEntity(full) } }
            .compose(hookInternalSingleExceptions())

    override fun update(model: IdentityUpdateModel, full: Boolean): Single<IdentityEntity> = persistence.update(model.toDatabaseModel(processor))
            .map { it.toEntity(full) }
            .compose(hookInternalSingleExceptions())

    override fun delete(model: IdentityDeleteModel): Single<UUID> = persistence.delete(model.toDatabaseModel())
            .compose(hookInternalSingleExceptions())

    override fun calculatePwdHash(pwd: String): String =
            processor.apply(pwd)

    private fun IdentityDBO.toEntity(full: Boolean) = when(full) {
        true -> toPrivateEntity()
        else -> toPublicEntity()
    }
}