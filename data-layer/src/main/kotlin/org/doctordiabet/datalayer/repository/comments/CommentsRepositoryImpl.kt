package org.doctordiabet.datalayer.repository.comments

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.comments.toDatabaseModel
import org.doctordiabet.datalayer.dbo.comments.toEntity
import org.doctordiabet.datalayer.repository.base.AbstractRepository
import org.doctordiabet.datalayer.services.comments.CommentsPersistenceService
import org.doctordiabet.domainlayer.entity.comments.CommentCreateModel
import org.doctordiabet.domainlayer.entity.comments.CommentDeleteModel
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.entity.comments.CommentUpdateModel
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class CommentsRepositoryImpl(private val persistence: CommentsPersistenceService): AbstractRepository(), CommentsRepository {

    private val observers: MutableList<Observer> = mutableListOf()

    override fun create(model: CommentCreateModel): Single<CommentEntity> = persistence.create(model.toDatabaseModel())
            .map { it.first to it.second.toEntity() }
            .doOnSuccess { (type, entity) -> notifyObserversCreate(type.toReferral(), model.modifyingByIdentity, entity.metadata.id) }
            .map { it.second }
            .compose(hookInternalSingleExceptions())

    override fun read(id: UUID): Single<CommentEntity> = persistence.read(id)
            .map { it.toEntity() }
            .compose(hookInternalSingleExceptions())

    override fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID>): Single<Pair<Int, List<CommentEntity>>> = when(modified) {
        true -> persistence.readModified(offset, limit, timestamp, tagIds)
        else -> persistence.readUnmodified(offset, limit, timestamp, tagIds)
    }
            .map { it.first to it.second.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun read(artifactReferralId: UUID, offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID>): Single<Pair<Int, List<CommentEntity>>> = when(modified) {
        true -> persistence.readModified(artifactReferralId, offset, limit, timestamp, tagIds)
        else -> persistence.readUnmodified(artifactReferralId, offset, limit, timestamp, tagIds)
    }
            .map { it.first to it.second.map { it.toEntity() } }
            .compose(hookInternalSingleExceptions())

    override fun update(model: CommentUpdateModel): Single<CommentEntity> = persistence.update(model.toDatabaseModel())
            .map { it.first to it.second.toEntity() }
            .doOnSuccess { (type, entity) -> notifyObserversUpdate(type.toReferral(), model.modifyingByIdentity, entity.metadata.id) }
            .map { it.second }
            .compose(hookInternalSingleExceptions())

    override fun delete(model: CommentDeleteModel): Single<UUID> = persistence.delete(model.toDatabaseModel())
            .doOnSuccess { (type, entityId) -> notifyObserversDelete(type.toReferral(), model.modifyingByIdentity, entityId) }
            .map { it.second }
            .compose(hookInternalSingleExceptions())

    private fun notifyObserversCreate(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID) {
        observers.forEach { it.onCreate(referral, identity, entityId) }
    }

    private fun notifyObserversUpdate(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID) {
        observers.forEach { it.onUpdate(referral, identity, entityId) }
    }

    private fun notifyObserversDelete(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID) {
        observers.forEach { it.onDelete(referral, identity, entityId) }
    }

    override fun sub(): Observable<CommentsRepository.Event> = Observable.create { emitter ->
        val observer = object: Observer {
            override fun onCreate(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(CommentsRepository.Event.Create(referral, identity, entityId))
                }
            }

            override fun onUpdate(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(CommentsRepository.Event.Update(referral, identity, entityId))
                }
            }

            override fun onDelete(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID) {
                if(!emitter.isDisposed) {
                    emitter.onNext(CommentsRepository.Event.Delete(referral, identity, entityId))
                }
            }
        }

        emitter.setDisposable(object: Disposable {
            private val disposed = AtomicBoolean(false)

            override fun isDisposed(): Boolean = disposed.get()

            override fun dispose() {
                if(disposed.compareAndSet(false, true)) {
                    observers.remove(observer)
                }
            }
        })

        observers.add(observer)
    }

    private fun ArtifactType.toReferral(): CommentsRepository.Referral = when(this) {
        ArtifactType.category -> CommentsRepository.Referral.Category
        ArtifactType.topic -> CommentsRepository.Referral.Topic
        ArtifactType.comment -> CommentsRepository.Referral.Comment
        ArtifactType.identity -> CommentsRepository.Referral.Identity
    }

    private interface Observer {

        fun onCreate(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID)

        fun onUpdate(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID)

        fun onDelete(referral: CommentsRepository.Referral, identity: UUID, entityId: UUID)
    }
}