package org.doctordiabet.datalayer.dto.organisation

import com.fasterxml.jackson.annotation.JsonProperty


/**
 * Created by i_komarov on 06.09.17.
 */

data class OrganisationDTO(
        @JsonProperty("value")
        val value: String,
        @JsonProperty("unrestricted_value")
        val unrestrictedValue: String,
        @JsonProperty("data")
        val data: OrganisationDataDTO
)