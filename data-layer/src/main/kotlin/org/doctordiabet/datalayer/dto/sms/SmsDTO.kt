package org.doctordiabet.datalayer.dto.sms

import com.fasterxml.jackson.annotation.JsonProperty

data class SmsDTO(
        @JsonProperty("sms_id")
        val id: String,
        @JsonProperty("status")
        val status: String,
        @JsonProperty("status_code")
        val code: Int
)