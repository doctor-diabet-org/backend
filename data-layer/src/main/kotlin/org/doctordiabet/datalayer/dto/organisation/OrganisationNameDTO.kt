package org.doctordiabet.datalayer.dto.organisation

import com.fasterxml.jackson.annotation.JsonProperty


/**
 * Created by i_komarov on 06.09.17.
 */

data class OrganisationNameDTO(
        @JsonProperty("full_width_opf")
        val fullOpf: String,
        @JsonProperty("short_width_opf")
        val shorOpf: String,
        @JsonProperty("latin")
        val latin: String?,
        @JsonProperty("full")
        val full: String,
        @JsonProperty("short")
        val short: String
)