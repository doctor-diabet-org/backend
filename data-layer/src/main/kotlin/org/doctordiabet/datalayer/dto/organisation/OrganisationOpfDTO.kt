package org.doctordiabet.datalayer.dto.organisation

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by i_komarov on 06.09.17.
 */

data class OrganisationOpfDTO(
        @JsonProperty("code")
        val code: String,
        @JsonProperty("full")
        val full: String,
        @JsonProperty("short")
        val short: String
)