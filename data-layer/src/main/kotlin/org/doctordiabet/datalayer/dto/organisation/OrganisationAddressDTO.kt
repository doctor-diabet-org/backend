package org.doctordiabet.datalayer.dto.organisation

import com.fasterxml.jackson.annotation.JsonProperty


/**
 * Created by i_komarov on 06.09.17.
 */

data class OrganisationAddressDTO(
        @JsonProperty("value")
        val value: String? = null,
        @JsonProperty("unrestricted_value")
        val unrestrictedValue: String? = null
)