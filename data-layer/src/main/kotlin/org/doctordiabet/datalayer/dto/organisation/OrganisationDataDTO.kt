package org.doctordiabet.datalayer.dto.organisation

import com.fasterxml.jackson.annotation.JsonProperty


/**
 * Created by i_komarov on 06.09.17.
 */

data class OrganisationDataDTO(
        @JsonProperty("kpp")
        val kpp: String,
        @JsonProperty("management")
        val management: OrganisationManagementDTO,
        @JsonProperty("branch_type")
        val branchType: String,
        @JsonProperty("branch_count")
        val branchCount: Int,
        @JsonProperty("type")
        val type: String,
        @JsonProperty("opf")
        val opf: OrganisationOpfDTO,
        @JsonProperty("name")
        val name: OrganisationNameDTO,
        @JsonProperty("inn")
        val inn: String,
        @JsonProperty("ogrn")
        val ogrn: String,
        @JsonProperty("okpo")
        val okpo: String,
        @JsonProperty("okved")
        val okved: String,
        @JsonProperty("state")
        val state: OrganisationStateDTO,
        @JsonProperty("address")
        val address: OrganisationAddressDTO
)