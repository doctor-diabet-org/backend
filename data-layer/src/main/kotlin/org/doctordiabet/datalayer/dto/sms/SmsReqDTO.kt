package org.doctordiabet.datalayer.dto.sms

data class SmsReqDTO(
        val phone: String,
        val message: String
)