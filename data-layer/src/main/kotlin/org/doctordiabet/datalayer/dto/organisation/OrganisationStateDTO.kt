package org.doctordiabet.datalayer.dto.organisation

import com.fasterxml.jackson.annotation.JsonProperty


/**
 * Created by i_komarov on 06.09.17.
 */

data class OrganisationStateDTO(
        @JsonProperty("status")
        val status: String,
        @JsonProperty("actuality_date")
        val actualityDate: Int,
        @JsonProperty("registration_date")
        val registrationDate: Int,
        @JsonProperty("liquidation_date")
        val liquidationDate: Int?
)