package org.doctordiabet.datalayer.dto.sms

import com.fasterxml.jackson.annotation.JsonProperty

data class SmsAckDTO(
        @JsonProperty("status")
        val status: String,
        @JsonProperty("status_code")
        val code: Int,
        @JsonProperty("balance")
        val balance: Double,
        @JsonProperty("sms")
        val smsMap: Map<String, SmsDTO>
)