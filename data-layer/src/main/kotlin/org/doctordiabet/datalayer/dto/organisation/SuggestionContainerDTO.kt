package org.doctordiabet.datalayer.dto.organisation

/**
 * Created by i_komarov on 06.09.17.
 */

data class SuggestionContainerDTO<T>(
        val suggestions: List<T>
)