package org.doctordiabet.datalayer.dto.organisation

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by i_komarov on 06.09.17.
 */

data class OrganisationManagementDTO(
        @JsonProperty("name")
        val name: String,
        @JsonProperty("post")
        val post: String
)