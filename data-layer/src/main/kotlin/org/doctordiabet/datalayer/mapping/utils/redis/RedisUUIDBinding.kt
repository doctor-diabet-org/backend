package org.doctordiabet.datalayer.mapping.utils.redis

import org.doctordiabet.datalayer.services.base.RedisBeanIndexBinding
import java.util.*

class RedisUUIDBinding: RedisBeanIndexBinding<UUID> {

    companion object {

        @JvmStatic val instance by lazy {
            RedisUUIDBinding()
        }
    }

    override fun toScore(property: UUID): Double = property.hashCode().toDouble()
}