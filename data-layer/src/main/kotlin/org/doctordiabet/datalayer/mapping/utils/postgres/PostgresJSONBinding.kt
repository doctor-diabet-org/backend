package org.doctordiabet.datalayer.mapping.utils.postgres

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.NullNode
import org.jooq.*
import org.jooq.impl.DSL
import java.sql.Types
import java.util.*
import java.sql.SQLFeatureNotSupportedException

class PostgresJSONBinding: Binding<Any, JsonNode> {

    /*private val mappingContext = ObjectMapper()

    override fun converter(): Converter<Any, JsonNode> = object: Converter<Any, JsonNode> {

        override fun from(databaseObject: Any?): JsonNode = when(databaseObject) {
            null -> NullNode.instance
            else -> mappingContext.convertValue(databaseObject, JsonNode::class.java)
        }

        override fun to(userObject: JsonNode?): Any = mappingContext.writeValueAsString(userObject)

        override fun fromType(): Class<Any> = Any::class.java

        override fun toType(): Class<JsonNode> = JsonNode::class.java
    }

    override fun sql(ctx: BindingSQLContext<JsonNode>): Unit {
        ctx.render().visit(DSL.`val`(ctx.convert(converter()).value())).sql("::json")
    }

    override fun register(ctx: BindingRegisterContext<JsonNode>) {
        ctx.statement().registerOutParameter(ctx.index(), Types.VARCHAR)
    }

    override fun set(ctx: BindingSetStatementContext<JsonNode>) {
        ctx.statement().setString(ctx.index(), Objects.toString(ctx.convert(converter()).value(), null))
    }

    override fun set(ctx: BindingSetSQLOutputContext<JsonNode>) {
        throw SQLFeatureNotSupportedException()
    }

    override fun get(ctx: BindingGetResultSetContext<JsonNode>) {
        ctx.convert(converter()).value(ctx.resultSet().getString(ctx.index()))
    }

    override fun get(ctx: BindingGetStatementContext<JsonNode>) {
        ctx.convert(converter()).value(ctx.statement().getString(ctx.index()))
    }

    override fun get(ctx: BindingGetSQLInputContext<JsonNode>?) {
        throw SQLFeatureNotSupportedException()
    }*/

    private val mappingContext = ObjectMapper()

    override fun converter(): Converter<Any, JsonNode> {
        return object: Converter<Any, JsonNode> {

            override fun from(databaseObject: Any?): JsonNode = when(databaseObject) {
                null -> NullNode.instance
                else -> mappingContext.readTree(databaseObject?.toString())
            }

            override fun to(userObject: JsonNode?): Any = mappingContext.writeValueAsString(userObject)

            override fun fromType(): Class<Any> = Any::class.java

            override fun toType(): Class<JsonNode> = JsonNode::class.java
        };
    }

    override fun sql(ctx: BindingSQLContext<JsonNode>) {
        ctx.render().visit(DSL.`val`(ctx.convert(converter()).value())).sql("::json")
    }

    override fun register(ctx: BindingRegisterContext<JsonNode>): Unit {
        ctx.statement()
           .registerOutParameter(ctx.index(),
                                 Types.VARCHAR);
    }

    override fun set(ctx: BindingSetStatementContext<JsonNode>): Unit {
        ctx.statement()
           .setString(ctx.index(),
                      Objects.toString(ctx.convert(converter()).value(),
                                       null));
    }

    override fun set(ctx: BindingSetSQLOutputContext<JsonNode>): Unit {
        throw SQLFeatureNotSupportedException()
    }

    override fun get(ctx: BindingGetResultSetContext<JsonNode>): Unit {
        ctx.convert(converter()).value(ctx.resultSet().getString(ctx.index()));
    }

    override fun get(ctx: BindingGetStatementContext<JsonNode>): Unit {
        ctx.convert(converter()).value(ctx.statement().getString(ctx.index()));
    }

    override fun get(ctx: BindingGetSQLInputContext<JsonNode>): Unit {
        throw SQLFeatureNotSupportedException()
    }
}