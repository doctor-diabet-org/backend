package org.doctordiabet.datalayer.mapping.utils.redis

import org.doctordiabet.datalayer.services.base.RedisBeanIndexBinding
import java.sql.Timestamp

class RedisTimestampBinding: RedisBeanIndexBinding<Timestamp> {

    companion object {

        @JvmStatic val instance by lazy {
            RedisTimestampBinding()
        }
    }

    override fun toScore(property: Timestamp): Double = property.time.toDouble()
}