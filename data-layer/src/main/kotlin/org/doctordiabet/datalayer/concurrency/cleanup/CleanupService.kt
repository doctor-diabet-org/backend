package org.doctordiabet.datalayer.concurrency.cleanup

import org.doctordiabet.datalayer.concurrency.pools.ThreadPoolManager
import java.util.concurrent.BlockingQueue

object CleanupService {

    fun <T> install(name: String,
                size: Int,
                queue: BlockingQueue<CleanupMessage<T>>,
                delegate: (T) -> Unit,
                expiration: Long) =
            ThreadPoolManager.create(name, size).execute(
                    CleanupRunnable(
                            queue = queue,
                            delegate = delegate,
                            expiration = expiration
                    )
            )
}