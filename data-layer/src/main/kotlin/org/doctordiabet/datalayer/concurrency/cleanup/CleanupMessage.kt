package org.doctordiabet.datalayer.concurrency.cleanup

data class CleanupMessage<out T>(
        val timestamp: Long,
        val data: T
)