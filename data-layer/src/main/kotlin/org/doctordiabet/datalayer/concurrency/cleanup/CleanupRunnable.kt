package org.doctordiabet.datalayer.concurrency.cleanup

import java.util.concurrent.BlockingQueue

class CleanupRunnable<T>(private val queue: BlockingQueue<CleanupMessage<T>>,
                         private val delegate: (T) -> Unit,
                         private val expiration: Long): Runnable {

    override fun run() = next()

    private fun next() {
        val message = queue.take()
        val delta = (message.timestamp + expiration) - System.currentTimeMillis()
        if(delta > 0) {
            Thread.sleep(delta)
        }

        message.data.let(delegate)
        next()
    }
}