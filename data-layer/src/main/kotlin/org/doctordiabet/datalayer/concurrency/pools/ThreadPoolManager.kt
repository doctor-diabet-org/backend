package org.doctordiabet.datalayer.concurrency.pools

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicInteger

object ThreadPoolManager {

    fun create(name: String): ExecutorService = Executors.newCachedThreadPool(NamedThreadFactory(name))

    fun create(name: String, size: Int): ExecutorService = Executors.newFixedThreadPool(size, NamedThreadFactory(name))

    private class NamedThreadFactory(name: String): ThreadFactory {

        private val group   = ThreadGroup(name)
        private val counter = AtomicInteger(0)

        override fun newThread(runnable: Runnable): Thread = Thread(group, runnable, "Thread: ${counter.incrementAndGet()}")
    }
}