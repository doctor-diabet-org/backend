package org.doctordiabet.datalayer.database.core

import org.doctordiabet.datalayer.config.DatabaseConfig
import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.exception.DataAccessException
import org.jooq.impl.DSL
import org.postgresql.ds.PGPoolingDataSource
import javax.sql.DataSource

class PostgresAdapter {

    private val dataSource: DataSource

    private val context: DSLContext

    constructor(): this(
            dataSourceName = DatabaseConfig.dataSourceName,
            serverName = DatabaseConfig.serverName,
            portNumber = DatabaseConfig.portNumber,
            databaseName = DatabaseConfig.databaseName,
            user = DatabaseConfig.user,
            password = DatabaseConfig.password,
            initialConnections = DatabaseConfig.initialConnections,
            maxConnections = DatabaseConfig.maxConnections
    )

    constructor(dataSourceName: String,
                serverName: String,
                portNumber: Int,
                databaseName: String,
                user: String,
                password: String,
                initialConnections: Int,
                maxConnections: Int) {
        dataSource = PGPoolingDataSource().apply {
            this@apply.dataSourceName = dataSourceName
            this@apply.serverName = serverName
            this@apply.portNumber = portNumber
            this@apply.databaseName = databaseName
            this@apply.user = user
            this@apply.password = password
            this@apply.initialConnections = initialConnections
            this@apply.maxConnections = maxConnections
        }

        context = DSL.using(
                dataSource,
                SQLDialect.POSTGRES_9_5
        )
    }

    fun withTransaction(block: (Configuration) -> Unit): Unit {
        context.transaction(block)
    }
}