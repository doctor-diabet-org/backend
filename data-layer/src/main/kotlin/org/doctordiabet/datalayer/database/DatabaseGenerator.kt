package org.doctordiabet.datalayer.database

import org.doctordiabet.datalayer.config.DatabaseConfig
import org.doctordiabet.datalayer.database.core.PostgresAdapter
import org.doctordiabet.datalayer.services.categories.CategoriesPersistenceService
import org.doctordiabet.datalayer.services.comments.CommentsPersistenceService
import org.doctordiabet.datalayer.services.topics.TopicsPersistenceService
import org.postgresql.util.PSQLException
import java.io.File
import java.io.InputStreamReader
import java.sql.Connection
import java.sql.DriverManager
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

fun main(args: Array<String>) = DatabaseGenerator.main(args)

object DatabaseGenerator {

    fun main(args: Array<String>) {
        val loader = javaClass.classLoader

        try {
            if(true) {
                DriverManager.getConnection(
                        DatabaseConfig.url,
                        DatabaseConfig.user,
                        DatabaseConfig.password
                ).use { connection ->
                    InputStreamReader(loader.getResourceAsStream("sql/statements_order.txt")).use { stream ->
                        stream.forEachLine { line ->
                            line.takeIf { it.isNotEmpty() && !it.startsWith("##") && !it.endsWith("##") }
                                    ?.let { statementFilePath ->
                                        val statementFilePath = "sql" + statementFilePath
                                        println(statementFilePath)
                                        executeStatement(connection, findStatement(loader, statementFilePath))
                                    }
                        }
                    }
                }
            }

            if(false) {
                with(PostgresAdapter()) {
                    val categoriesService = CategoriesPersistenceService(this)
                    val topicsService = TopicsPersistenceService(this)
                    val commentsService = CommentsPersistenceService(this)

                    val topicsCount = AtomicInteger(1)
                    val commentsCount = AtomicInteger(1)
                    val nestedCommentsCount = AtomicInteger(1)

                    commentsService.readUnmodified(UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), 0, 200, Timestamp(System.currentTimeMillis()))
                            .subscribe(
                                    { println(it.second.sortedByDescending { it.metadata.createdTime }.joinToString(separator = "\n")) },
                                    { it.printStackTrace() }
                            )

                    /*Observable.merge(
                            categoriesService.create(CategoryCreateModel(UUID.fromString("00000000-0000-0000-0000-000000000000"), "Category 1")).toObservable(),
                            categoriesService.create(CategoryCreateModel(UUID.fromString("00000000-0000-0000-0000-000000000000"), "Category 2")).toObservable(),
                            categoriesService.create(CategoryCreateModel(UUID.fromString("00000000-0000-0000-0000-000000000000"), "Category 3")).toObservable()
                    ).subscribe(
                        { println(it) },
                        { it.printStackTrace() },
                        { println("Completed") }
                    )*/

                    /*Single.merge(
                            listOf(
                            topicsService.create(TopicCreateModel("Topic ${topicsCount.getAndIncrement()}", "A topic inside the category: Category 1", UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), UUID.fromString("00000000-0000-0000-0000-000000000000"))).subscribeOn(Schedulers.io()),
                            topicsService.create(TopicCreateModel("Topic ${topicsCount.getAndIncrement()}", "A topic inside the category: Category 1", UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), UUID.fromString("00000000-0000-0000-0000-000000000000"))).subscribeOn(Schedulers.io()),
                            topicsService.create(TopicCreateModel("Topic ${topicsCount.getAndIncrement()}", "A topic inside the category: Category 1", UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), UUID.fromString("00000000-0000-0000-0000-000000000000"))).subscribeOn(Schedulers.io()),
                            topicsService.create(TopicCreateModel("Topic ${topicsCount.getAndIncrement()}", "A topic inside the category: Category 1", UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), UUID.fromString("00000000-0000-0000-0000-000000000000"))).subscribeOn(Schedulers.io()),
                            topicsService.create(TopicCreateModel("Topic ${topicsCount.getAndIncrement()}", "A topic inside the category: Category 2", UUID.fromString("edcb5a3e-fd49-11e7-ac57-d3da89e1c71d"), UUID.fromString("00000000-0000-0000-0000-000000000000"))).subscribeOn(Schedulers.io()),
                            topicsService.create(TopicCreateModel("Topic ${topicsCount.getAndIncrement()}", "A topic inside the category: Category 2", UUID.fromString("edcb5a3e-fd49-11e7-ac57-d3da89e1c71d"), UUID.fromString("00000000-0000-0000-0000-000000000000"))).subscribeOn(Schedulers.io())
                            )
                    ).subscribe(
                        { println(it) },
                        { it.printStackTrace() },
                        { println("Completed") }
                    )*/

                    /*Thread.sleep(5_000L)

                    Single.merge(
                            listOf(
                                    commentsService.create(CommentCreateModel(UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), "${commentsCount.incrementAndGet()} to Category 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), "${commentsCount.incrementAndGet()} to Category 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), "${commentsCount.incrementAndGet()} to Category 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), "${commentsCount.incrementAndGet()} to Category 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), "${commentsCount.incrementAndGet()} to Category 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("edcde61e-fd49-11e7-ac57-67dd4d92c5fb"), "${commentsCount.incrementAndGet()} to Category 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),

                                    commentsService.create(CommentCreateModel(UUID.fromString("c8665584-fd51-11e7-b362-ab68c59f6141"), "${commentsCount.incrementAndGet()} to Topic 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("c8665584-fd51-11e7-b362-ab68c59f6141"), "${commentsCount.incrementAndGet()} to Topic 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("c8665584-fd51-11e7-b362-ab68c59f6141"), "${commentsCount.incrementAndGet()} to Topic 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),

                                    commentsService.create(CommentCreateModel(UUID.fromString("c86628b6-fd51-11e7-94df-ffa3b18c8eed"), "${commentsCount.incrementAndGet()} to Topic 2", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("c86628b6-fd51-11e7-94df-ffa3b18c8eed"), "${commentsCount.incrementAndGet()} to Topic 2", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("c86628b6-fd51-11e7-94df-ffa3b18c8eed"), "${commentsCount.incrementAndGet()} to Topic 2", UUID.fromString("00000000-0000-0000-0000-000000000000"))),

                                    commentsService.create(CommentCreateModel(UUID.fromString("eedbf906-fd58-11e7-98c9-7b0dad42ed18"), "${commentsCount.incrementAndGet()} to Comment 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("eedbf906-fd58-11e7-98c9-7b0dad42ed18"), "${commentsCount.incrementAndGet()} to Comment 1", UUID.fromString("00000000-0000-0000-0000-000000000000"))),
                                    commentsService.create(CommentCreateModel(UUID.fromString("eedbf906-fd58-11e7-98c9-7b0dad42ed18"), "${commentsCount.incrementAndGet()} to Comment 1", UUID.fromString("00000000-0000-0000-0000-000000000000")))
                            )
                    ).subscribe(
                            { println(it) },
                            { it.printStackTrace() },
                            { println("Completed") }
                    )*/

                    /*Observable.merge(
                            categoriesService.create(CategoryCreateModel(UUID.fromString("00000000-0000-0000-0000-000000000000"), "Category 1")).toObservable(),
                            categoriesService.create(CategoryCreateModel(UUID.fromString("00000000-0000-0000-0000-000000000000"), "Category 2")).toObservable(),
                            categoriesService.create(CategoryCreateModel(UUID.fromString("00000000-0000-0000-0000-000000000000"), "Category 3")).toObservable()
                    ).flatMap { dbo -> topicsService.create(TopicCreateModel("Topic ${topicsCount.getAndIncrement()}", "A topic inside the category: ${dbo.title}", dbo.metadata.id, UUID.fromString("00000000-0000-0000-0000-000000000000"))).toObservable() }
                            .flatMap { dbo -> commentsService.create(CommentCreateModel(dbo.metadata.id, "A comment text ${commentsCount.getAndIncrement()}", UUID.fromString("00000000-0000-0000-0000-000000000000"))).toObservable() }
                            .flatMap { dbo -> commentsService.create(CommentCreateModel(dbo.metadata.id, "A nested comment text ${nestedCommentsCount.getAndIncrement()}", UUID.fromString("00000000-0000-0000-0000-000000000000"))).toObservable() }
                            .onErrorResumeNext { thr: Throwable -> Observable.error(thr) }
                            .subscribe(
                                    System.out::println,
                                    { throwable ->
                                        (throwable as? ExceptionBundle)?.let {
                                            when(it.source) {
                                                ExceptionSource.INTERNAL -> {
                                                    val accessor = it.accessor as InternalExceptionAccessor
                                                    println("Error with message: ${accessor.message}")
                                                    accessor.cause.printStackTrace()
                                                }
                                                ExceptionSource.DATABASE -> {
                                                    val accessor = it.accessor as DatabaseExceptionAccessor
                                                    println("Error with code: ${accessor.code}, message: ${accessor.message}")
                                                    accessor.cause.printStackTrace()
                                                }
                                            }
                                        }
                                    },
                                    { println("Completed") }
                            )*/
                }
            }
        } catch(e: PSQLException) {
            println("Error executing psql statement:\nserverErrorMessage: ${e.serverErrorMessage}\nsqlState: ${e.sqlState}\nerrorCode: ${e.errorCode}")
        }
    }

    private fun findStatement(loader: ClassLoader, statementFilePath: String): String = InputStreamReader(
            File(loader.getResource(statementFilePath).file).inputStream()
    ).use { stream ->
        stream.readText()
    }

    private fun executeStatement(connection: Connection, statement: String) {
        connection.prepareCall(statement).execute()
    }
}