package org.doctordiabet.datalayer.database.core

import org.doctordiabet.datalayer.concurrency.pools.ThreadPoolManager
import org.doctordiabet.datalayer.config.RedisConfig
import redis.clients.jedis.HostAndPort
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisCluster
import redis.clients.jedis.exceptions.JedisClusterException
import redis.clients.jedis.exceptions.JedisConnectionException
import redis.clients.jedis.exceptions.JedisNoReachableClusterNodeException
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import java.util.logging.Level
import java.util.logging.Logger

class RedisAdapter {

    companion object {

        private const val THREAD_POOL_SIZE = 1
        private const val THREAD_POOL_NAME = "Redis Thread Pool"
    }

    private val init: () -> Pair<Jedis, JedisCluster>

    private lateinit var jedis: Jedis

    private lateinit var cluster: JedisCluster

    private val lock: Lock

    private val executor: ExecutorService

    constructor(): this(RedisConfig.host, RedisConfig.port, RedisConfig.clusterHostAndPorts)

    constructor(host: String, port: Int, clusterHostAndPorts: Set<HostAndPort>) {
        init = { Pair(Jedis(RedisConfig.host, RedisConfig.port), JedisCluster(RedisConfig.clusterHostAndPorts)) }
        establishConnections()
        lock = ReentrantLock()
        executor = ThreadPoolManager.create(THREAD_POOL_NAME, THREAD_POOL_SIZE)
    }

    fun <T> withRedis(init: JedisCluster.() -> T): T = withRedisFuture(init)
            .get()

    private fun <T> withRedisFuture(init: JedisCluster.() -> T): Future<T> = executor.submit<T> {
        execute(init = init)
    }

    private fun <T> execute(init: JedisCluster.() -> T, attempt: Int = 0, attemptMax: Int = 3): T {
        val onConnectionFailure = fun (t: Throwable): T {
            establishConnections()
            if(attempt + 1 < attemptMax) {
                return execute(init, attempt + 1, attemptMax)
            } else {
                throw t
            }
        }

        return try {
            cluster.let(init)
        } catch (e: JedisNoReachableClusterNodeException) {
            Logger.getLogger("RedisAdapter").log(Level.WARNING, "could not connect to Redis ${attempt + 1}/$attemptMax times")
            Thread.sleep(5_000L)
            onConnectionFailure(e)
        } catch (e: JedisConnectionException) {
            Logger.getLogger("RedisAdapter").log(Level.WARNING, "could not connect to Redis ${attempt + 1}/$attemptMax times")
            Thread.sleep(5_000L)
            onConnectionFailure(e)
        } catch (e: JedisClusterException) {
            Logger.getLogger("RedisAdapter").log(Level.WARNING, "could not connect to Redis ${attempt + 1}/$attemptMax times")
            Thread.sleep(5_000L)
            onConnectionFailure(e)
        }
    }

    private fun establishConnections() {
        val connections = init()
        jedis = connections.first
        cluster = connections.second
    }
}