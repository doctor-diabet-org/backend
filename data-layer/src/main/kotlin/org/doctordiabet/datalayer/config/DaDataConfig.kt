package org.doctordiabet.datalayer.config

import java.io.File
import java.util.*

/**
 * Created by i_komarov on 06.09.17.
 */

object DaDataConfig {

    @JvmStatic private val properties by lazy {
        Properties().apply {
            load(File("dadata.properties").inputStream())
        }
    }

    @JvmStatic val apiKey: String by lazy {
        properties["api_key"]?.toString()
                ?: throw IllegalStateException("DaData api key was not found in dadata.properties configuration file")
    }

    @JvmStatic val apiSecret: String by lazy {
        properties["api_secret"]?.toString()
                ?: throw IllegalStateException("DaData secret key was not found in dadata.properties configuration file")
    }
}