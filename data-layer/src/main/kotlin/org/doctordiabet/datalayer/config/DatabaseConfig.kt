package org.doctordiabet.datalayer.config

import java.io.File
import java.io.PrintStream
import java.util.*

object DatabaseConfig {

    //TODO("UNCOMMENT THIS AND COMMENT BELOW WHEN AT SECURE WI-FI")
    /*@JvmStatic private val properties by lazy {
        Properties().apply {
            load(File("./data-layer/database.properties").inputStream())
        }
    }*/

    @JvmStatic private val properties by lazy {
        Properties().apply {
            load(File("./data-layer/database-local.properties").inputStream())
        }
    }

    @JvmStatic val driver: String by lazy {
        properties["db.driver"]?.toString()
                ?: throw IllegalStateException("Database driver was not found in database.properties configuration file")
    }

    @JvmStatic val url: String by lazy {
        properties["db.url"]?.toString()
                ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
    }

    @JvmStatic val dataSourceName: String by lazy {
        properties["db.datasource_name"]?.toString()
                ?: throw IllegalStateException("Database data source name was not found in database.properties configuration file")
    }

    @JvmStatic val serverName: String by lazy {
        properties["db.host"]?.toString()
                ?: throw IllegalStateException("Database host was not found in database.properties configuration file")
    }

    @JvmStatic val portNumber: Int by lazy {
        properties["db.port"]?.toString()?.toIntOrNull()
                ?: throw IllegalStateException("Database port was not found in database.properties configuration file")
    }

    @JvmStatic val databaseName: String by lazy {
        properties["db.database"]?.toString()
                ?: throw IllegalStateException("Database name was not found in database.properties configuration file")
    }

    @JvmStatic val user: String by lazy {
        properties["db.username"]?.toString()
                ?: throw IllegalStateException("Database username was not found in database.properties configuration file")
    }

    @JvmStatic val password: String by lazy {
        properties["db.password"]?.toString()
                ?: throw IllegalStateException("Database password was not found in database.properties configuration file")
    }

    @JvmStatic val initialConnections: Int by lazy {
        properties["db.initial_connections"]?.toString()?.toIntOrNull()
                ?: throw IllegalStateException("Database initial_connections was not found in database.properties configuration file")
    }

    @JvmStatic val maxConnections: Int by lazy {
        properties["db.max_connections"]?.toString()?.toIntOrNull()
                ?: throw IllegalStateException("Database max_connections was not found in database.properties configuration file")
    }
}