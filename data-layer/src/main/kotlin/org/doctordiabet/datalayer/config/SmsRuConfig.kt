package org.doctordiabet.datalayer.config

import java.io.File
import java.util.*

object SmsRuConfig {

    @JvmStatic private val properties by lazy {
        Properties().apply {
            load(File("./data-layer/smsru.properties").inputStream())
        }
    }

    @JvmStatic val apiKey: String by lazy {
        properties["api_key"]?.toString()
                ?: throw IllegalStateException("DaData api key was not found in dadata.properties configuration file")
    }
}