package org.doctordiabet.datalayer.config

import redis.clients.jedis.HostAndPort
import java.io.File
import java.util.*

object RedisConfig {

    @JvmStatic private val properties by lazy {
        Properties().apply {
            load(File("./data-layer/redis.properties").inputStream())
        }
    }

    @JvmStatic val host: String by lazy {
        properties["redis.host"]?.toString()
                ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
    }

    @JvmStatic val port: Int by lazy {
        properties["redis.port"]?.toString()?.toIntOrNull()
                ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
    }

    @JvmStatic val clusterHostAndPorts: Set<HostAndPort> = setOf(
            HostAndPort(
                    properties["redis.cluster0.host"]?.toString()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file"),
                    properties["redis.cluster0.port"]?.toString()?.toIntOrNull()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
            ),
            HostAndPort(
                    properties["redis.cluster1.host"]?.toString()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file"),
                    properties["redis.cluster1.port"]?.toString()?.toIntOrNull()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
            ),
            HostAndPort(
                    properties["redis.cluster2.host"]?.toString()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file"),
                    properties["redis.cluster2.port"]?.toString()?.toIntOrNull()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
            ),
            HostAndPort(
                    properties["redis.cluster3.host"]?.toString()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file"),
                    properties["redis.cluster3.port"]?.toString()?.toIntOrNull()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
            ),
            HostAndPort(
                    properties["redis.cluster4.host"]?.toString()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file"),
                    properties["redis.cluster4.port"]?.toString()?.toIntOrNull()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
            ),
            HostAndPort(
                    properties["redis.cluster5.host"]?.toString()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file"),
                    properties["redis.cluster5.port"]?.toString()?.toIntOrNull()
                            ?: throw IllegalStateException("Database url was not found in database.properties configuration file")
            )
    )
}