package org.doctordiabet.datalayer.processing.sms

import com.fasterxml.jackson.databind.ObjectMapper
import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.JsonNode
import com.mashape.unirest.http.Unirest
import io.reactivex.Single
import org.doctordiabet.datalayer.config.SmsRuConfig
import org.doctordiabet.datalayer.dto.sms.SmsAckDTO
import org.doctordiabet.datalayer.dto.sms.SmsDTO
import org.doctordiabet.datalayer.dto.sms.SmsReqDTO
import org.doctordiabet.domainlayer.exceptions.*
import java.net.HttpURLConnection

class SmsProcessor {

    fun process(query: SmsReqDTO): Single<SmsReqDTO> = Single.create { emitter ->
        try {
            val response: HttpResponse<JsonNode> = Unirest.post("https://sms.ru/sms/send")
                    .header("Accept", "application/json")
                    .queryString("api_id", SmsRuConfig.apiKey)
                    .queryString("to", "[${query.phone}]")
                    .queryString("msg", query.message)
                    .queryString("json", "1")
                    .asJson()

            if(response.status != HttpURLConnection.HTTP_OK) {
                emitter.onError(response.toExceptionBundle())
                return@create
            }

            val body = ObjectMapper().readValue(response.body.toString(), SmsAckDTO::class.java)

            if(body.status != "OK") {
                emitter.onError(body.toExceptionBundle())
                return@create
            }

            val (_, dto) = body.smsMap.entries.first()

            if(dto.status != "OK") {
                emitter.onError(dto.toExceptionBundle(body))
                return@create
            }

            emitter.onSuccess(query)

        } catch(e: Exception) {
            emitter.onError(e.parse())
        }
    }

    private fun HttpResponse<JsonNode>.toExceptionBundle() = ExceptionBundle(ExceptionSource.NETWORK).apply {
        (accessor as NetworkExceptionAccessor).apply {
            this@apply.code = this@toExceptionBundle.status
            this@apply.message = this@toExceptionBundle.statusText
        }
    }

    private fun SmsAckDTO.toExceptionBundle() = ExceptionBundle(ExceptionSource.SMS).apply {
        (accessor as SmsExceptionAccessor).apply {
            this@apply.balance = this@toExceptionBundle.balance
            this@apply.code = this@toExceptionBundle.code
        }
    }

    private fun SmsDTO.toExceptionBundle(metadata: SmsAckDTO) = ExceptionBundle(ExceptionSource.SMS).apply {
        (accessor as SmsExceptionAccessor).apply {
            this@apply.balance = metadata.balance
            this@apply.code = this@toExceptionBundle.code
        }
    }

    private fun Exception.parse(): ExceptionBundle = ExceptionBundle(ExceptionSource.INTERNAL).apply {
        (accessor as InternalExceptionAccessor).apply {
            this.cause = this@parse
            this.message = "Exception raised while performing SmsProcessor::process"
        }
    }
}