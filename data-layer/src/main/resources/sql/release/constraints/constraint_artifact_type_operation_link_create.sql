ALTER TABLE release.artifact_type_operations_link
  ADD CONSTRAINT artifact_type_fkey FOREIGN KEY (artifact_type)
    REFERENCES release.artifact_types (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT operation_type_fkey FOREIGN KEY (operation_type)
    REFERENCES release.artifact_operations (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;