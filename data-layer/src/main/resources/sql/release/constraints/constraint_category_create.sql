ALTER TABLE release.categories
  ADD CONSTRAINT artifact_fkey FOREIGN KEY (id)
    REFERENCES release.artifacts (id)
    ON UPDATE NO ACTION ON DELETE CASCADE;