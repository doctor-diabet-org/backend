ALTER TABLE release.artifact_operation_privileges_link
  ADD CONSTRAINT operation_type_fkey FOREIGN KEY (operation_type)
    REFERENCES release.artifact_operations (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT privilege_type_fkey FOREIGN KEY (privilege_type)
    REFERENCES release.privileges (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;