ALTER TABLE release.topics
  ADD CONSTRAINT artifact_fkey FOREIGN KEY (id)
    REFERENCES release.artifacts (id)
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT category_fkey FOREIGN KEY (category)
    REFERENCES release.categories (id)
    ON UPDATE NO ACTION ON DELETE CASCADE;