ALTER TABLE release.identity_role_privileges_link
  ADD CONSTRAINT identity_role_fkey FOREIGN KEY (identity_role)
    REFERENCES release.identity_roles (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT privilege_fkey FOREIGN KEY (privilege_type)
    REFERENCES release.privileges (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;