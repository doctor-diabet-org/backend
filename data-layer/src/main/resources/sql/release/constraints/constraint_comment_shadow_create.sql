ALTER TABLE release.comments_shadow
  ADD CONSTRAINT comment_fkey FOREIGN KEY (id)
    REFERENCES release.comments (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT artifact_referral_fkey FOREIGN KEY (artifact_referral_id)
    REFERENCES release.artifacts (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT artifact_shadow_fkey FOREIGN KEY (id, version)
    REFERENCES release.artifacts_shadow (id, version) MATCH FULL
    ON UPDATE NO ACTION ON DELETE CASCADE;