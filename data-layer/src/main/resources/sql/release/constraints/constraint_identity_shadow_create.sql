ALTER TABLE release.identities_shadow
  ADD CONSTRAINT identity_fkey FOREIGN KEY (id)
    REFERENCES release.identities (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT artifact_shadow_fkey FOREIGN KEY (id, version)
    REFERENCES release.artifacts_shadow (id, version) MATCH FULL
    ON UPDATE NO ACTION ON DELETE CASCADE;