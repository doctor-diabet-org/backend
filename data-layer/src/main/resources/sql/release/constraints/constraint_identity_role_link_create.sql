ALTER TABLE release.identity_roles_link
  ADD CONSTRAINT identity_fkey FOREIGN KEY (identity_id)
    REFERENCES release.identities (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT role_type_fkey FOREIGN KEY (role_type)
    REFERENCES release.identity_roles (type) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;