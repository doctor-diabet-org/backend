ALTER TABLE release.artifact_tags_link
  ADD CONSTRAINT artifact_fkey FOREIGN KEY (artifact_id)
    REFERENCES release.artifacts(id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE,
  ADD CONSTRAINT tag_fkey FOREIGN KEY (tag_id)
    REFERENCES release.tags(id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE CASCADE;