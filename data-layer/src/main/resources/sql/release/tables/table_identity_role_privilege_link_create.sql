CREATE TABLE release.identity_role_privileges_link(
  identity_role release.identity_role_type NOT NULL,
  privilege_type release.privilege_type NOT NULL
);