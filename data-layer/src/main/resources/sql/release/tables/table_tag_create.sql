CREATE TABLE release.tags(
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1mc(),
  value TEXT NULL
);