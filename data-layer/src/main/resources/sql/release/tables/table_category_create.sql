CREATE TABLE release.categories(
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1mc(),
  title TEXT NOT NULL,
  description TEXT NOT NULL DEFAULT('')
);