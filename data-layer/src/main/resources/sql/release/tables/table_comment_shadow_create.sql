CREATE TABLE release.comments_shadow(
  id UUID NOT NULL,
  version INTEGER NOT NULL,
  artifact_referral_id UUID NOT NULL,
  content_text TEXT NOT NULL,
  PRIMARY KEY (id, version)
);