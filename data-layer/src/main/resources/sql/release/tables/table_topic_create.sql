CREATE TABLE release.topics(
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1mc(),
  category UUID NOT NULL,
  title TEXT NOT NULL,
  description TEXT NOT NULL
);