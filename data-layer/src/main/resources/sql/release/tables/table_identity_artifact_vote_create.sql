CREATE TABLE release.identity_artifact_votes(
  identity UUID NOT NULL,
  artifact UUID NOT NULL,
  value INTEGER NOT NULL DEFAULT 1,
  PRIMARY KEY(identity, artifact)
);