CREATE VIEW release.artifact_tags AS
  SELECT
    release.artifacts.id AS id,
    release.tags.id AS tag_id,
    release.tags.value
  FROM release.artifacts
  INNER JOIN release.artifact_tags_link ON release.artifacts.id = release.artifact_tags_link.artifact_id
  INNER JOIN release.tags ON release.artifact_tags_link.tag_id = release.tags.id;