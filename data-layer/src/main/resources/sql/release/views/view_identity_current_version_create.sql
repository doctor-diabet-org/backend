CREATE VIEW release.identity_current_versions AS
  SELECT
    release.identities.id,
    release.identities.first_name,
    release.identities.last_name,
    release.identities.phone_number,
    release.identities.email,
    release.identities.password_hash,
    a.version,
    release.artifacts.created_time,
    release.artifacts.created_by_identity,
    release.artifacts_shadow.last_modified_time,
    release.artifacts_shadow.last_modified_by_identity,
    release.artifacts_shadow.created,
    release.artifacts_shadow.updated,
    release.artifacts_shadow.deleted,
    (SELECT COALESCE(SUM(release.identity_artifact_votes.value), 0) FROM release.identity_artifact_votes WHERE release.identity_artifact_votes.artifact = release.artifacts.id) AS votes,
    (SELECT JSON_AGG(JSON_BUILD_OBJECT('id', release.tags.id, 'value', release.tags.value)) FROM release.artifact_tags_link INNER JOIN release.tags ON release.artifact_tags_link.tag_id = release.tags.id WHERE release.artifact_tags_link.artifact_id = release.artifacts.id) AS tags
  FROM release.identities
    INNER JOIN release.identities_shadow a ON release.identities.id = a.id AND a.version = (SELECT MAX(version) FROM release.identities_shadow WHERE release.identities_shadow.id = release.identities.id)
    INNER JOIN release.artifacts ON release.identities.id = release.artifacts.id
    INNER JOIN release.artifacts_shadow ON a.id = release.artifacts_shadow.id AND a.version = release.artifacts_shadow.version
  ORDER BY release.artifacts.created_time DESC;