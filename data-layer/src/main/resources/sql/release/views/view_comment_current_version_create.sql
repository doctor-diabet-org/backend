CREATE VIEW release.comment_current_versions
  AS
    SELECT
      artifacts.id AS root_artifact_referral_id,
      comments.id,
      comments.artifact_referral_id,
      comments.content_text,
      comments.comment_version,
      comments.created_time,
      comments.created_by_identity,
      comments.last_modified_time,
      comments.last_modified_by_identity,
      comments.created,
      comments.updated,
      comments.deleted,
      comments.votes,
      comments.tags,
      comments.depth
    FROM release.artifacts artifacts, release.comment_current_versions_hierarchy(artifacts.id) AS comments;


