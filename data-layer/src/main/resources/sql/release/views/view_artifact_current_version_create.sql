CREATE VIEW release.artifact_current_versions AS
  SELECT
    release.artifacts.id,
    release.artifacts.type,
    a.version,
    release.artifacts.created_time,
    release.artifacts.created_by_identity,
    a.last_modified_time,
    a.last_modified_by_identity,
    a.created,
    a.updated,
    a.deleted,
    (SELECT COALESCE(SUM(release.identity_artifact_votes.value), 0) FROM release.identity_artifact_votes WHERE release.identity_artifact_votes.artifact = release.artifacts.id) AS votes,
    (SELECT JSON_AGG(JSON_BUILD_OBJECT('id', release.tags.id, 'value', release.tags.value)) FROM release.artifact_tags_link INNER JOIN release.tags ON release.artifact_tags_link.tag_id = release.tags.id WHERE release.artifact_tags_link.artifact_id = release.artifacts.id) AS tags
  FROM release.artifacts
    INNER JOIN release.artifacts_shadow a ON release.artifacts.id = a.id AND a.version = (SELECT MAX(version) FROM release.artifacts_shadow WHERE release.artifacts_shadow.id = release.artifacts.id)
  ORDER BY release.artifacts.created_time DESC;
