CREATE VIEW release.category_current_versions AS
  SELECT
    release.categories.id,
    release.categories.title,
    release.categories.description,
    a.version,
    release.artifacts.created_time,
    release.artifacts.created_by_identity,
    release.artifacts_shadow.last_modified_time,
    release.artifacts_shadow.last_modified_by_identity,
    release.artifacts_shadow.created,
    release.artifacts_shadow.updated,
    release.artifacts_shadow.deleted,
    (SELECT COALESCE(SUM(release.identity_artifact_votes.value), 0) FROM release.identity_artifact_votes WHERE release.identity_artifact_votes.artifact = release.artifacts.id) AS votes,
    (SELECT JSON_AGG(JSON_BUILD_OBJECT('id', release.tags.id, 'value', release.tags.value)) FROM release.artifact_tags_link INNER JOIN release.tags ON release.artifact_tags_link.tag_id = release.tags.id WHERE release.artifact_tags_link.artifact_id = release.artifacts.id) AS tags,
    (
      SELECT COALESCE(COUNT(topics.id), 0) FROM release.topics topics
      INNER JOIN release.artifacts_shadow topics_shadow ON topics_shadow.id = topics.id AND topics_shadow.deleted = FALSE AND topics_shadow.version = (SELECT MAX(version) FROM release.topics_shadow WHERE release.topics_shadow.id = topics_shadow.id)
      WHERE topics.category = release.categories.id
    ) AS topics_count
  FROM release.categories
    INNER JOIN release.categories_shadow a ON release.categories.id = a.id AND a.version = (SELECT MAX(version) FROM release.categories_shadow WHERE release.categories_shadow.id = release.categories.id)
    INNER JOIN release.artifacts ON release.categories.id = release.artifacts.id
    INNER JOIN release.artifacts_shadow ON release.categories.id = release.artifacts_shadow.id AND a.version = release.artifacts_shadow.version
  ORDER BY release.artifacts.created_time DESC;
