CREATE TYPE release.artifact_operation AS ENUM(
  'category_create',
  'category_read',
  'category_update',
  'category_delete',
  'topic_create',
  'topic_read',
  'topic_update',
  'topic_delete',
  'comment_create',
  'comment_read',
  'comment_update',
  'comment_delete',
  'comment_vote'
)