CREATE TYPE release.artifact_type AS ENUM(
  'category',
  'topic',
  'comment',
  'identity'
);