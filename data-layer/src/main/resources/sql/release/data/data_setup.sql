INSERT INTO release.artifact_types (type)
  VALUES
    ('category'), ('topic'), ('comment'), ('identity');

INSERT INTO release.privileges (type)
  VALUES
    ('categories_create'),('categories_read'),('categories_update'),('categories_delete'),('categories_vote'),

    ('topics_create'),('topics_read'),('topics_update'),('topics_delete'),('topics_vote'),

    ('comments_create'),('comments_read'),('comments_update'),('comments_delete'),('comments_vote'),

    ('identities_create'),('identities_read'),('identities_update'),('identities_delete'),('identities_vote'),

    ('tags_create'),('tags_update'),('tags_delete');

INSERT INTO release.identity_roles (type)
  VALUES
    ('guest'),('authorized'),('specialist'),('moderator'),('administrator');

INSERT INTO release.artifacts (id, type, created_by_identity)
  VALUES
    (uuid_nil(), 'identity', uuid_nil());

INSERT INTO release.artifacts_shadow (id, version, type, last_modified_time, last_modified_by_identity, created)
    VALUES
      (uuid_nil(), 1, 'identity', (SELECT release.artifacts.created_time FROM release.artifacts WHERE release.artifacts.id = uuid_nil()), uuid_nil(), TRUE);

INSERT INTO release.identities (id, first_name, last_name, phone_number, email, password_hash)
  VALUES
    (uuid_nil(), 'Global', 'Administrator', '+7(916)168-76-11', 'admin@doctordiabet.org', '9e81e9c9844bcd14e4b0cd28af6906895db42a0acb50821dbeb4471e348558e7');

INSERT INTO release.identities_shadow (id, version, first_name, last_name, phone_number, email, password_hash)
  VALUES
    (uuid_nil(), 1, 'Global', 'Administrator', '+7(916)168-76-11', 'admin@doctordiabet.org', '9e81e9c9844bcd14e4b0cd28af6906895db42a0acb50821dbeb4471e348558e7');

INSERT INTO release.identity_roles_link (identity_id, role_type)
  VALUES
    (uuid_nil(), 'administrator');
