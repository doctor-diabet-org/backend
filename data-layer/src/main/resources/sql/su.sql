GRANT CONNECT ON DATABASE postgres TO diabetics_std;
GRANT CONNECT ON DATABASE postgres TO diabetics_dev;

GRANT CONNECT ON DATABASE diabetics TO diabetics_std;
GRANT CONNECT ON DATABASE diabetics TO diabetics_dev;

GRANT ALL PRIVILEGES ON DATABASE diabetics TO diabetics_std;
GRANT ALL PRIVILEGES ON DATABASE diabetics TO diabetics_dev;

CREATE EXTENSION "uuid-ossp";