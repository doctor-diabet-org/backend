package org.doctordiabet.datalayer.repository.base

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.subscribers.TestSubscriber
import org.junit.Test

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.exceptions.InternalExceptionAccessor

class AbstractRepositoryTest {

    @Test
    fun hookInternalMaybeExceptions() {
        val testObserver = TestObserver<Unit>()

        val maybeTestRepository = object: AbstractRepository() {
            val testStream = Maybe.error<Unit>(IllegalStateException())
                    .compose(hookInternalMaybeExceptions())
        }

        maybeTestRepository.testStream.subscribe(testObserver)
        testObserver.assertNotComplete()
        testObserver.assertError({ t: Throwable ->
            t is ExceptionBundle &&
                    t.source == ExceptionSource.INTERNAL &&
                    (t.accessor as InternalExceptionAccessor).let {
                        it.cause is IllegalStateException && it.message == "N/A"
                    }
        })
    }

    @Test
    fun hookInternalSingleExceptions() {
        val testObserver = TestObserver<Unit>()

        val singleTestRepository = object: AbstractRepository() {
            val testStream = Single.error<Unit>(IllegalStateException())
                    .compose(hookInternalSingleExceptions())
        }

        singleTestRepository.testStream.subscribe(testObserver)
        testObserver.assertNotComplete()
        testObserver.assertError({ t: Throwable ->
            t is ExceptionBundle &&
                    t.source == ExceptionSource.INTERNAL &&
                    (t.accessor as InternalExceptionAccessor).let {
                        it.cause is IllegalStateException && it.message == "N/A"
                    }
        })
    }

    @Test
    fun hookInternalObservableExceptions() {
        val testObserver = TestObserver<Unit>()

        val observableTestRepository = object: AbstractRepository() {
            val testStream = Observable.error<Unit>(IllegalStateException())
                    .compose(hookInternalObservableExceptions())
        }

        observableTestRepository.testStream.subscribe(testObserver)
        testObserver.assertNotComplete()
        testObserver.assertError({ t: Throwable ->
            t is ExceptionBundle &&
                    t.source == ExceptionSource.INTERNAL &&
                    (t.accessor as InternalExceptionAccessor).let {
                        it.cause is IllegalStateException && it.message == "N/A"
                    }
        })
    }

    @Test
    fun hookInternalFlowableExceptions() {
        val testObserver = TestSubscriber<Unit>()

        val flowableTestRepository = object: AbstractRepository() {
            val testStream = Flowable.error<Unit>(IllegalStateException())
                    .compose(hookInternalFlowableExceptions())
        }

        flowableTestRepository.testStream.subscribe(testObserver)
        testObserver.assertNotComplete()
        testObserver.assertError({ t: Throwable ->
            t is ExceptionBundle &&
                    t.source == ExceptionSource.INTERNAL &&
                    (t.accessor as InternalExceptionAccessor).let {
                        it.cause is IllegalStateException && it.message == "N/A"
                    }
        })
    }

}