package org.doctordiabet.datalayer.repository.identities

import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.assertj.core.api.Assertions
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.identity.IdentityDBO
import org.doctordiabet.datalayer.dbo.identity.toDatabaseModel
import org.doctordiabet.datalayer.dbo.identity.toPrivateEntity
import org.doctordiabet.datalayer.dbo.identity.toPublicEntity
import org.doctordiabet.datalayer.dbo.metadata.ArtifactMetadata
import org.doctordiabet.datalayer.dbo.metadata.ArtifactTag
import org.doctordiabet.datalayer.encryption.hashes.SHA256Processor
import org.doctordiabet.datalayer.services.base.OrderBy
import org.doctordiabet.datalayer.services.identities.IdentitiesCacheService
import org.doctordiabet.datalayer.services.identities.IdentitiesPersistenceService
import org.doctordiabet.domainlayer.encryption.HashProcessor
import org.doctordiabet.domainlayer.entity.identity.IdentityCreateModel
import org.doctordiabet.domainlayer.entity.identity.IdentityDeleteModel
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.entity.identity.IdentityUpdateModel
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.postgresql.util.PSQLException
import org.postgresql.util.PSQLState
import redis.clients.jedis.exceptions.JedisException
import java.sql.Timestamp
import java.util.*

class IdentitiesRepositoryImplTest {

    /*private lateinit var persistence: IdentitiesPersistenceService
    private lateinit var cache: IdentitiesCacheService
    private lateinit var repository: IdentitiesRepositoryImpl

    @Before
    fun setUp() {
        persistence = mock(IdentitiesPersistenceService::class.java) //он эмулирован >> IdentitiesPersistenceService::class.java
        cache = mock(IdentitiesCacheService::class.java)
        repository = IdentitiesRepositoryImpl(persistence, cache)
    }

    @Test
    fun createSuccess() {
        val observer = TestObserver<IdentityEntity>()
        val id = UUID.randomUUID()
        val identityModel = IdentityCreateModel("Firstname","lastname","88005553535","email","qwer12345")
        val processor: HashProcessor = SHA256Processor()
        val databaseModel = identityModel.toDatabaseModel(processor)

        val dbo = generate(id)
        val full=true
        val entity = dbo.toPrivateEntity()

        `when`(persistence.create(databaseModel)).thenReturn(Single.just(dbo))
        `when`(cache.set(dbo)).thenReturn(Single.just(dbo))

        repository.create(identityModel,full).subscribe(observer)

        verify(persistence).create(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun createPersistenceFailure() {
        val observer = TestObserver<IdentityEntity>()

        val id = UUID.randomUUID()
        val identityModel = IdentityCreateModel("Firstname","lastname","88005553535","email","qwer12345")
        val processor: HashProcessor = SHA256Processor()
        val databaseModel = identityModel.toDatabaseModel(processor)

        `when`(persistence.create(databaseModel)).thenReturn(Single.error(PSQLException("An expected exception occured during junit test", PSQLState.UNKNOWN_STATE)))

        repository.create(identityModel,true).subscribe(observer)

        verify(persistence).create(databaseModel)
        verifyNoMoreInteractions(persistence)

        verifyZeroInteractions(cache)

        observer.assertNotComplete()
    }

    @Test
    fun createCacheFailure() {
        val observer = TestObserver<IdentityEntity>()

        val id = UUID.randomUUID()
        val identityModel = IdentityCreateModel("Firstname","lastname","88005553535","email","qwer12345")
        val processor: HashProcessor = SHA256Processor()
        val databaseModel = identityModel.toDatabaseModel(processor)
        val dbo = generate(id)

        `when`(persistence.create(databaseModel)).thenReturn(Single.just(dbo))
        `when`(cache.set(dbo)).thenReturn(Single.error(JedisException("An expected exception occured during junit test")))

        repository.create(identityModel,true).subscribe(observer)

        verify(persistence).create(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertNotComplete()
    }

    @Test
    fun readSingleHotCache() {
        val observer = TestObserver<IdentityEntity>()

        val id = UUID.randomUUID()
        val dbo = generate(id)
        val entity = dbo.toPrivateEntity()

        `when`(cache.get(id)).thenReturn(Maybe.just(dbo))
        // A dirty solution to overcome the Maybe::switchIfEmpty behavior
        // whether it'd be called, this test would fail, so no way
        // this solution will be the reason for test to behave strange
        `when`(persistence.read(id)).thenReturn(Maybe.empty())

        repository.read(id).subscribe(observer)

        verify(persistence).read(id)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(id)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun readSingleColdCache() {
        val observer = TestObserver<IdentityEntity>()

        val id = UUID.randomUUID()
        val dbo = generate(id)
        val entity = dbo.toPublicEntity()

        `when`(cache.get(id)).thenReturn(Maybe.error(EmptyMaybeException()))
        `when`(cache.set(dbo)).thenReturn(Single.just(dbo))
        `when`(persistence.read(id)).thenReturn(Maybe.just(dbo))

        repository.read(id).subscribe(observer)

        verify(persistence).read(id)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(id)
        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun readRangeHotCache() {
        val observer = TestObserver<List<IdentityEntity>>()

        val dbos = List(10, { generate(UUID.randomUUID()) })
        val entities = List(10, { dbos[it].toPrivateEntity() })

        `when`(cache.get(offset = 0, limit = 10)).thenReturn(Single.just(dbos))

        repository.read(offset = 0, limit = 10,full = true).subscribe(observer)

        verifyZeroInteractions(persistence)

        verify(cache).get(offset = 0, limit = 10)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).hasSameElementsAs(entities)
    }

    @Test
    fun readRangeWarmCache() {
        val observer = TestObserver<List<IdentityEntity>>()

        val dbos = List(10, { generate(UUID.randomUUID()) })
        val entities = List(10, { dbos[it].toPrivateEntity() })

        `when`(cache.get(offset = 0, limit = 10, order = OrderBy.DESC)).thenReturn(Single.just(dbos.take(5)))
        `when`(cache.set(dbos.drop(5))).thenReturn(Single.just(dbos.drop(5)))
        `when`(persistence.read(offset = 5, limit = 5)).thenReturn(Single.just(dbos.drop(5)))

        repository.read(offset = 0, limit = 10,full=true).subscribe(observer)

        verify(persistence).read(offset = 5, limit = 5)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(offset = 0, limit = 10, order = OrderBy.DESC)
        verify(cache).set(dbos.drop(5))
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).hasSameElementsAs(entities)
    }

    @Test
    fun readRangeColdCache() {
        val observer = TestObserver<List<IdentityEntity>>()

        val dbos = List(10, { generate(UUID.randomUUID()) })
        val entities = List(10, { dbos[it].toPrivateEntity() })

        `when`(cache.get(offset = 0, limit = 10, order = OrderBy.DESC)).thenReturn(Single.just(ArrayList()))
        `when`(cache.set(dbos)).thenReturn(Single.just(dbos))
        `when`(persistence.read(offset = 0, limit = 10)).thenReturn(Single.just(dbos))

        repository.read(offset = 0, limit = 10,full = true).subscribe(observer)

        verify(persistence).read(offset = 0, limit = 10)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(offset = 0, limit = 10, order = OrderBy.DESC)
        verify(cache).set(dbos)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).hasSameElementsAs(entities)
    }

    @Test
    fun updateSuccess() {
        val observer = TestObserver<IdentityEntity>()

        val id = UUID.randomUUID()
        val identityModel = IdentityUpdateModel(id,UUID.randomUUID(),"Firstname","lastname","88005553535","email","qwer12345")
        val processor: HashProcessor = SHA256Processor()
        val databaseModel = identityModel.toDatabaseModel(processor)
        val dbo = generate(id)
        val entity = dbo.toPrivateEntity()

        `when`(persistence.update(databaseModel)).thenReturn(Maybe.just(dbo))
        `when`(cache.set(dbo)).thenReturn(Single.just(dbo))

        repository.update(identityModel).subscribe(observer)

        verify(persistence).update(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun updatePersistenceNotFound() {
        val observer = TestObserver<IdentityEntity>()

        val id = UUID.randomUUID()
        val identityModel = IdentityUpdateModel(id, UUID.randomUUID(), "Firstname", "lastname", "88005553535", "email", "qwer12345")
        val processor: HashProcessor = SHA256Processor()
        val databaseModel = identityModel.toDatabaseModel(processor)

        `when`(persistence.update(databaseModel)).thenReturn(Maybe.empty())

        repository.update(identityModel).subscribe(observer)

        verify(persistence).update(databaseModel)
        verifyNoMoreInteractions(persistence)

        verifyZeroInteractions(cache)

        observer.assertComplete()
        observer.assertValueCount(0)
    }

    @Test fun updatePersistenceFailure() {
            val observer = TestObserver<IdentityEntity>()

            val id = UUID.randomUUID()
            val identityModel = IdentityUpdateModel(id, UUID.randomUUID(), "Firstname", "lastname", "88005553535", "email", "qwer12345")
            val processor: HashProcessor = SHA256Processor()
            val databaseModel = identityModel.toDatabaseModel(processor)

            `when`(persistence.update(databaseModel)).thenReturn(Maybe.error(IllegalStateException()))

            repository.update(identityModel).subscribe(observer)

            verify(persistence).update(databaseModel)
            verifyNoMoreInteractions(persistence)

            verifyZeroInteractions(cache)

            observer.assertNotComplete()
            observer.assertValueCount(0)
        }

    @Test
    fun updateCacheFailure() {
            val observer = TestObserver<IdentityEntity>()

            val id = UUID.randomUUID()
            val identityModel = IdentityUpdateModel(id,UUID.randomUUID(),"Firstname","lastname","88005553535","email","qwer12345")
            val processor: HashProcessor = SHA256Processor()
            val databaseModel = identityModel.toDatabaseModel(processor)
            val dbo = generate(id)

            `when`(persistence.update(databaseModel)).thenReturn(Maybe.just(dbo))
            `when`(cache.set(dbo)).thenReturn(Single.error(IllegalStateException()))

            repository.update(identityModel).subscribe(observer)

            verify(persistence).update(databaseModel)
            verifyNoMoreInteractions(persistence)

            verify(cache).set(dbo)
            verifyNoMoreInteractions(cache)

            observer.assertNotComplete()
            observer.assertValueCount(0)
        }

    @Test
    fun delete() {
        val observer = TestObserver<UUID>()

        val id = UUID.randomUUID()
        val entityModel = IdentityDeleteModel(id, UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.delete(databaseModel)).thenReturn(Maybe.just(id))
        `when`(cache.delete(id)).thenReturn(Single.just(id))

        repository.delete(entityModel).subscribe(observer)

        verify(persistence).delete(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).delete(id)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        Assertions.assertThat(observer.values()[0]).isEqualTo(id)
    }

    @Test
    fun deletePersistenceFailure() {
        val observer = TestObserver<UUID>()

        val id = UUID.randomUUID()
        val entityModel = IdentityDeleteModel(id, UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.delete(databaseModel)).thenReturn(Maybe.error(IllegalStateException()))

        repository.delete(entityModel).subscribe(observer)

        verify(persistence).delete(databaseModel)
        verifyNoMoreInteractions(persistence)

        verifyZeroInteractions(cache)

        observer.assertNotComplete()
        observer.assertValueCount(0)
    }

    @Test
    fun deleteCacheFailure() {
        val observer = TestObserver<UUID>()

        val id = UUID.randomUUID()
        val entityModel = IdentityDeleteModel(id, UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.delete(databaseModel)).thenReturn(Maybe.just(id))
        `when`(cache.delete(id)).thenReturn(Single.error(IllegalStateException()))

        repository.delete(entityModel).subscribe(observer)

        verify(persistence).delete(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).delete(id)
        verifyNoMoreInteractions(cache)

        observer.assertNotComplete()
        observer.assertValueCount(0)
    }


    private fun generate(id: UUID): IdentityDBO = IdentityDBO(
            firstName = "Firstname",
            lastName="lastname",
            phoneNumber="88005553535",
            email="email",
            pwdHash="ps",
            metadata = ArtifactMetadata(
                    id = id,
                    version = 1,
                    type = ArtifactType.category,
                    createdTime = Timestamp(System.currentTimeMillis()),
                    createdByIdentity = UUID.fromString("00000000-0000-0000-0000-000000000000"),
                    lastModifiedTime = Timestamp(System.currentTimeMillis()),
                    lastModifiedByIdentity = UUID.fromString("00000000-0000-0000-0000-000000000000"),
                    created = false,
                    modified = true,
                    deleted = false,
                    votes = 10,
                    tags = setOf(ArtifactTag(id = UUID.fromString("ff3fac34-b25d-11e7-abc4-cec278b6b50a"), value = "Tag 1"))
            ),
            tags = emptySet()
    )*/
}