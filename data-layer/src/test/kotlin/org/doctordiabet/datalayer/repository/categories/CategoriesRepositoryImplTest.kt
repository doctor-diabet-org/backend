package org.doctordiabet.datalayer.repository.categories

import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.doctordiabet.datalayer.database.enums.ArtifactType
import org.doctordiabet.datalayer.dbo.categories.CategoryDBO
import org.doctordiabet.datalayer.dbo.categories.toDatabaseModel
import org.doctordiabet.datalayer.dbo.categories.toEntity
import org.doctordiabet.datalayer.dbo.metadata.ArtifactMetadata
import org.doctordiabet.datalayer.dbo.metadata.ArtifactTag
import org.doctordiabet.datalayer.services.categories.CategoriesCacheService
import org.doctordiabet.datalayer.services.categories.CategoriesPersistenceService
import org.doctordiabet.domainlayer.entity.categories.CategoryCreateModel
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.junit.Before
import org.junit.Test

import org.mockito.Mockito.*
import java.sql.Timestamp
import java.util.*

import org.assertj.core.api.Assertions.*
import org.doctordiabet.datalayer.services.base.OrderBy
import org.doctordiabet.domainlayer.entity.categories.CategoryDeleteModel
import org.doctordiabet.domainlayer.entity.categories.CategoryUpdateModel
import org.doctordiabet.domainlayer.exceptions.EmptyMaybeException
import org.postgresql.util.PSQLException
import org.postgresql.util.PSQLState
import redis.clients.jedis.exceptions.JedisException

class CategoriesRepositoryImplTest {

    /*private lateinit var persistence: CategoriesPersistenceService
    private lateinit var cache: CategoriesCacheService
    private lateinit var repository: CategoriesRepositoryImpl

    @Before
    fun setUp() {
        persistence = mock(CategoriesPersistenceService::class.java)
        cache = mock(CategoriesCacheService::class.java)
        repository = CategoriesRepositoryImpl(persistence, cache)
    }

    @Test
    fun createSuccess() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val entityModel = CategoryCreateModel(id, "Category title")
        val databaseModel = entityModel.toDatabaseModel()

        val dbo = generate(id)
        val entity = dbo.toEntity()

        `when`(persistence.create(databaseModel)).thenReturn(Single.just(dbo))
        `when`(cache.set(dbo)).thenReturn(Single.just(dbo))

        repository.create(entityModel).subscribe(observer)

        verify(persistence).create(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun createPersistenceFailure() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val entityModel = CategoryCreateModel(id, "Category title")
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.create(databaseModel)).thenReturn(Single.error(PSQLException("An expected exception occured during junit test", PSQLState.UNKNOWN_STATE)))

        repository.create(entityModel).subscribe(observer)

        verify(persistence).create(databaseModel)
        verifyNoMoreInteractions(persistence)

        verifyZeroInteractions(cache)

        observer.assertNotComplete()
    }

    @Test
    fun createCacheFailure() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val entityModel = CategoryCreateModel(id, "Category title")
        val databaseModel = entityModel.toDatabaseModel()
        val dbo = generate(id)

        `when`(persistence.create(databaseModel)).thenReturn(Single.just(dbo))
        `when`(cache.set(dbo)).thenReturn(Single.error(JedisException("An expected exception occured during junit test")))

        repository.create(entityModel).subscribe(observer)

        verify(persistence).create(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertNotComplete()
    }

    @Test
    fun readSingleHotCache() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val dbo = generate(id)
        val entity = dbo.toEntity()

        `when`(cache.get(id)).thenReturn(Maybe.just(dbo))
        // A dirty solution to overcome the Maybe::switchIfEmpty behavior
        // whether it'd be called, this test would fail, so no way
        // this solution will be the reason for test to behave strange
        `when`(persistence.readModified(id)).thenReturn(Maybe.empty())

        repository.readModified(id).subscribe(observer)

        verify(persistence).readModified(id)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(id)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun readSingleColdCache() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val dbo = generate(id)
        val entity = dbo.toEntity()

        `when`(cache.get(id)).thenReturn(Maybe.error(EmptyMaybeException()))
        `when`(cache.set(dbo)).thenReturn(Single.just(dbo))
        `when`(persistence.readModified(id)).thenReturn(Maybe.just(dbo))

        repository.readModified(id).subscribe(observer)

        verify(persistence).readModified(id)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(id)
        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun readRangeHotCache() {
        val observer = TestObserver<List<CategoryEntity>>()

        val dbos = List(10, { generate(UUID.randomUUID()) })
        val entities = List(10, { dbos[it].toEntity() })

        `when`(cache.get(offset = 0, limit = 10)).thenReturn(Single.just(dbos))

        repository.readModified(offset = 0, limit = 10).subscribe(observer)

        verifyZeroInteractions(persistence)

        verify(cache).get(offset = 0, limit = 10)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).hasSameElementsAs(entities)
    }

    @Test
    fun readRangeWarmCache() {
        val observer = TestObserver<List<CategoryEntity>>()

        val dbos = List(10, { generate(UUID.randomUUID()) })
        val entities = List(10, { dbos[it].toEntity() })

        `when`(cache.get(offset = 0, limit = 10, order = OrderBy.DESC)).thenReturn(Single.just(dbos.take(5)))
        `when`(cache.set(dbos.drop(5))).thenReturn(Single.just(dbos.drop(5)))
        `when`(persistence.readModified(offset = 5, limit = 5)).thenReturn(Single.just(dbos.drop(5)))

        repository.readModified(offset = 0, limit = 10).subscribe(observer)

        verify(persistence).readModified(offset = 5, limit = 5)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(offset = 0, limit = 10, order = OrderBy.DESC)
        verify(cache).set(dbos.drop(5))
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).hasSameElementsAs(entities)
    }

    @Test
    fun readRangeColdCache() {
        val observer = TestObserver<List<CategoryEntity>>()

        val dbos = List(10, { generate(UUID.randomUUID()) })
        val entities = List(10, { dbos[it].toEntity() })

        `when`(cache.get(offset = 0, limit = 10, order = OrderBy.DESC)).thenReturn(Single.just(ArrayList()))
        `when`(cache.set(dbos)).thenReturn(Single.just(dbos))
        `when`(persistence.readModified(offset = 0, limit = 10)).thenReturn(Single.just(dbos))

        repository.readModified(offset = 0, limit = 10).subscribe(observer)

        verify(persistence).readModified(offset = 0, limit = 10)
        verifyNoMoreInteractions(persistence)

        verify(cache).get(offset = 0, limit = 10, order = OrderBy.DESC)
        verify(cache).set(dbos)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).hasSameElementsAs(entities)
    }

    @Test
    fun updateSuccess() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val entityModel = CategoryUpdateModel(id, "Category title", UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()
        val dbo = generate(id)
        val entity = dbo.toEntity()

        `when`(persistence.update(databaseModel)).thenReturn(Maybe.just(dbo))
        `when`(cache.set(dbo)).thenReturn(Single.just(dbo))

        repository.update(entityModel).subscribe(observer)

        verify(persistence).update(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).isEqualToComparingFieldByFieldRecursively(entity)
    }

    @Test
    fun updatePersistenceNotFound() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val entityModel = CategoryUpdateModel(id, "Category title", UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.update(databaseModel)).thenReturn(Maybe.empty())

        repository.update(entityModel).subscribe(observer)

        verify(persistence).update(databaseModel)
        verifyNoMoreInteractions(persistence)

        verifyZeroInteractions(cache)

        observer.assertComplete()
        observer.assertValueCount(0)
    }

    @Test
    fun updatePersistenceFailure() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val entityModel = CategoryUpdateModel(id, "Category title", UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.update(databaseModel)).thenReturn(Maybe.error(IllegalStateException()))

        repository.update(entityModel).subscribe(observer)

        verify(persistence).update(databaseModel)
        verifyNoMoreInteractions(persistence)

        verifyZeroInteractions(cache)

        observer.assertNotComplete()
        observer.assertValueCount(0)
    }

    @Test
    fun updateCacheFailure() {
        val observer = TestObserver<CategoryEntity>()

        val id = UUID.randomUUID()
        val entityModel = CategoryUpdateModel(id, "Category title", UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()
        val dbo = generate(id)

        `when`(persistence.update(databaseModel)).thenReturn(Maybe.just(dbo))
        `when`(cache.set(dbo)).thenReturn(Single.error(IllegalStateException()))

        repository.update(entityModel).subscribe(observer)

        verify(persistence).update(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).set(dbo)
        verifyNoMoreInteractions(cache)

        observer.assertNotComplete()
        observer.assertValueCount(0)
    }

    @Test
    fun delete() {
        val observer = TestObserver<UUID>()

        val id = UUID.randomUUID()
        val entityModel = CategoryDeleteModel(id, UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.delete(databaseModel)).thenReturn(Maybe.just(id))
        `when`(cache.delete(id)).thenReturn(Single.just(id))

        repository.delete(entityModel).subscribe(observer)

        verify(persistence).delete(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).delete(id)
        verifyNoMoreInteractions(cache)

        observer.assertComplete()
        observer.assertNoErrors()
        observer.assertValueCount(1)
        assertThat(observer.values()[0]).isEqualTo(id)
    }

    @Test
    fun deletePersistenceFailure() {
        val observer = TestObserver<UUID>()

        val id = UUID.randomUUID()
        val entityModel = CategoryDeleteModel(id, UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.delete(databaseModel)).thenReturn(Maybe.error(IllegalStateException()))

        repository.delete(entityModel).subscribe(observer)

        verify(persistence).delete(databaseModel)
        verifyNoMoreInteractions(persistence)

        verifyZeroInteractions(cache)

        observer.assertNotComplete()
        observer.assertValueCount(0)
    }

    @Test
    fun deleteCacheFailure() {
        val observer = TestObserver<UUID>()

        val id = UUID.randomUUID()
        val entityModel = CategoryDeleteModel(id, UUID.randomUUID())
        val databaseModel = entityModel.toDatabaseModel()

        `when`(persistence.delete(databaseModel)).thenReturn(Maybe.just(id))
        `when`(cache.delete(id)).thenReturn(Single.error(IllegalStateException()))

        repository.delete(entityModel).subscribe(observer)

        verify(persistence).delete(databaseModel)
        verifyNoMoreInteractions(persistence)

        verify(cache).delete(id)
        verifyNoMoreInteractions(cache)

        observer.assertNotComplete()
        observer.assertValueCount(0)
    }

    private fun generate(id: UUID): CategoryDBO = CategoryDBO(
            title = "Category title",
            topicsCount = 5,
            metadata = ArtifactMetadata(
                    id = id,
                    version = 1,
                    type = ArtifactType.category,
                    createdTime = Timestamp(System.currentTimeMillis()),
                    createdByIdentity = UUID.fromString("00000000-0000-0000-0000-000000000000"),
                    lastModifiedTime = Timestamp(System.currentTimeMillis()),
                    lastModifiedByIdentity = UUID.fromString("00000000-0000-0000-0000-000000000000"),
                    created = false,
                    modified = true,
                    deleted = false,
                    votes = 10,
                    tags = setOf(ArtifactTag(id = UUID.fromString("ff3fac34-b25d-11e7-abc4-cec278b6b50a"), value = "Tag 1"))
            )
    )*/
}