# Функциональные требования

### Общая информация

Система предполлагает наличие трех частей: 
1. Сервер БД (PostgreSQL, Redis Cluster)
2. Сервер приложения (Веб-сервис, Memcached, Nginx)
3. Тонкий (веб) клиент

Для проектирования и разработки Системы используются следующие языки программирования: 
1. Kotlin
2. Scala
3. Java
4. Groovy
5. SQL (PostgreSQL dialect)
6. JavaScript (vue.js framework)
7. HTML/CSS

## Требования к серверному приложению

Серверное приложение должно осуществлять обмен информацией с клиентским приложением
посредством использования протокола HTTP, технологии Push (с использованием сервиса Firebase Cloud Messaging) и WebSockets. 
Данное требование обусловлено широкой распространенностью данных протоколов, их сравнительной простотой 
и удобством использования. 

API сервиса должно соответствовать REST архитектуре, с частичной поддержкой спецификации OData 4.0.

Оно должно предоставлять функции для осуществления всех ключевых действий с сущностями проекта, и включать в себя 
следующие подсистемы: 
1. Подсистема "Авторизация и аутентификация"
2. Подсистема "Метаданные и версионирование артефактов"
3. Подсистема "Управление пользователями"
4. Подсистема "Форум"
5. Подсистема "Мессенджер"
6. Подсистема "Логгирование"
7. Подсистема "Роли"
8. Подсистема "Подписки"

### Принятые стандарты

Все идентификаторы имеют тип UUID.

Для типа datetime всегда должен использоваться формат yyyy-mm-dd hh:MM:ss.SSS с допущением в виде вариации yyyy-mm-dd hh:MM:ss.

Серверное API всегда возвращает datetime в формате yyyy-mm-dd'T'hh:MM:ss.SSS'Z'

Для каждого запроса, результатом которого является коллекция элементов, необходимо указывать один из двух Header-ов: 
1. If-Modified-Since – для получения сущностей, измененных после указанного в заголовке datetime
2. If-Unmodified-Since – для получения сущностей, не изменявшихся после указанного в заголовке datetime

Для каждого запроса, результатом которого является коллекция элементов, можно указать дополнительные query-параметры: 
1. offset – количество элементов выборки, которые, начиная с первого удовлетворяющего условиям поиска, не войдут в результирующую коллекцию
2. limit – максимальный размер результирующей коллекции

Для каждого запроса, результатом которого является коллекция элементов, можно указать список тегов, по которым будет вестись поиск, в виде последовательности query-параметров следующего вида: 
...&tag=<tag_1_id>&tag=<tag_2_id>&tag=<tag_3_id>&...

### Авторизаця и аутентификация

Данная подсистема должна обеспечивать регистрацию, вход и аутентификацию пользователей в Системе, гарантировать 
конфиденциальность личных данных. 

Авторизация состоит из двух этапов.

Первый этап осуществляется посредством одного из двух способов: 
1. Авторизация по СМС
2. Авторизация по логину-паролю

#### Описание авторизации по СМС: 

\[POST\] /authorize/code/

Headers: none

Query-parameters: none

Body: 

```json
{
  "phone_number":"phone number string"
}
```

Response example: (No Content) 

```json
{}
```

\[PUT\] /authorize/code/

Headers: none

Query-parameters: none

Body: 

```json
{
  "phone_number":"phone number string",
  "code":"six symbol code"
}
```

Response example: 

```json
{
    "@odata.context": "#/authorize/code/",
    "@odata.type": "Entity.RefreshToken",
    "value": {
        "identity_id": "00000000-0000-0000-0000-000000000000",
        "refresh_token": "6B70CF2C618A0187BDEAAA410338CDF635874A8E0C81ED32513AB34562CD087D0EFB9C7CF9618C2F39679C27BBA16C08F3FF48FCC34BD2326E1BB1EDEA6556199ACB203287994CE95F58A4E0845DDBC4BCAB19FE2BC336A95D5DA55BB9BD7897"
    }
}
```

#### Описании авторизации с использованием логина и пароля: 

\[PUT\] /authorize/credentials/

Headers: none

Query-parameters: none

Body: 

```json
{
  "email":"admin@bg.ru",
  "password":"some_very_secure_password"
}
```

Response example: 

```json
{
    "@odata.context": "#/authorize/code/",
    "@odata.type": "Entity.RefreshToken",
    "value": {
        "identity_id": "00000000-0000-0000-0000-000000000000",
        "refresh_token": "6B70CF2C618A0187BDEAAA410338CDF635874A8E0C81ED32513AB34562CD087D0EFB9C7CF9618C2F39679C27BBA16C08F3FF48FCC34BD2326E1BB1EDEA6556199ACB203287994CE95F58A4E0845DDBC4BCAB19FE2BC336A95D5DA55BB9BD7897"
    }
}
```

#### Описание процесса получения сессионного токена: 

\[PUT\] /authorize/token/

Headers: none

Query-parameters: none

Body: 

```json
{
	"identity_id":"00000000-0000-0000-0000-000000000000",
	"refresh_token":"6B70CF2C618A0187BDEAAA410338CDF635874A8E0C81ED32513AB34562CD087D0EFB9C7CF9618C2F39679C27BBA16C08F3FF48FCC34BD2326E1BB1EDEA6556199ACB203287994CE95F58A4E0845DDBC4BCAB19FE2BC336A95D5DA55BB9BD7897"
}
```

Response example: 

```json
{
    "@odata.context": "#/authorize/token/",
    "@odata.type": "Entity.Session",
    "value": {
        "identity_id": "00000000-0000-0000-0000-000000000000",
        "refresh_token": "6B70CF2C618A0187BDEAAA410338CDF635874A8E0C81ED32513AB34562CD087D0EFB9C7CF9618C2F39679C27BBA16C08F3FF48FCC34BD2326E1BB1EDEA6556199ACB203287994CE95F58A4E0845DDBC4BCAB19FE2BC336A95D5DA55BB9BD7897",
        "session_token": "F3C98905751455981A48C4609481FA280937E7ECC19E4029FD3904E6A550AE29340B2EEEEF18D4C90ED2E433ABA88412417E9EF180F12C2FBDE37BC9848A1BCE",
        "created_time": 1516604030032,
        "expiration": 3600000,
        "expiration_time": 1516607630032
    }
}
```

### Метаданные и версионирование артефактов

Данная подсистема должна обеспечивать генерацию, хранениие и обновление метаданных, 
а также хранение всей истории изменений каждого артефакта Системы.

Для данных целей используется т. н. "Tuple binding" – паттерн проектирования БД, обеспечивающий удобное 
для работы версионирование. 

Метаданные включают в себя:
1. ID (здесь и далее – уникальный идентификатор артефакта в Системе) в формате UUID
2. Время создания, последнего изменения артефакта
3. ID создателя, и пользователя, внесшего последнее изменение в артефакт
4. Версию артефакта
5. Тип артефакта

### Управление пользователями

Данная подсистема включает в себя функциональность изменения, удаления, получения информации о пользователях Системы, 
а также – набор дополнительной функциональности для привелегированных пользователей (пользователей с ролями Administrator, Moderator).

### Форум

Данная подсистема включает в себя функциональность изменениия, удаления, получения информации о каждом из артефактов следующих типов: 
1. Categories – обобщенные тематические разделы форума
2. Topics – вопросы, заданные пользователями
3. Comments – комментарии к артефактам типов Categories, Topics, Comments

#### Categories API

\[POST\] /categories/

Headers: Authorization

Query-parameters: none

Body: 

```json
{
  "title":"some very cool title"
}
```

Response example: 

```json
{
    "@odata.context": "#/categories/",
    "@odata.type": "Entity.Category",
    "value": {
        "title": "some very cool title",
        "topics_count": 0,
        "metadata": {
            "id": "9b5a7cf6-02dc-11e8-ae9c-072375c5e376",
            "created_time": "26-01-2018T21:05:15.669Z",
            "created_by_identity": "00000000-0000-0000-0000-000000000000",
            "last_modified_time": "26-01-2018T21:05:15.669Z",
            "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
            "votes": 0,
            "tags": []
        }
    }
}
```

\[GET\] /categories/

Headers: none / Authorization

Query-parameters: none

Body: none

Reponse example: 

```json
{
    "@odata.count": 1,
    "@odata.context": "#/categories/?offset=0&limit=20",
    "@odata.type": "Type.Collection",
    "value": [
        {
            "@odata.type": "Entity.Category",
            "value": {
                "title": "some very cool title",
                "topics_count": 0,
                "metadata": {
                    "id": "94bb2ba0-ff41-11e7-8590-6fb5edb04538",
                    "created_time": "22-01-2018T06:57:59.107Z",
                    "created_by_identity": "00000000-0000-0000-0000-000000000000",
                    "last_modified_time": "22-01-2018T06:57:59.107Z",
                    "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
                    "votes": 0,
                    "tags": [
                      {
                        "id": "fe182590-fdf1-11e7-9646-eb9d9e1038ae",
                        "value": "value"
                      }
                    ]
                }
            }
        }
    ]
}
```

\[GET\] /categories/{id}/

Headers: none / Authorization

Query-parameters: none

Body: none

Reponse example: 

```json
{
    "@odata.context": "#/categories/94bb2ba0-ff41-11e7-8590-6fb5edb04538/",
    "@odata.type": "Entity.Category",
    "value": {
        "title": "Title",
        "topics_count": 0,
        "metadata": {
            "id": "94bb2ba0-ff41-11e7-8590-6fb5edb04538",
            "created_time": "22-01-2018T06:57:59.107Z",
            "created_by_identity": "00000000-0000-0000-0000-000000000000",
            "last_modified_time": "22-01-2018T06:57:59.107Z",
            "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
            "votes": 0,
            "tags": []
        }
    }
}
```

\[PATCH\] /categories/{id}/

Headers: Authorization

Query-parameters: none

Body: 

```json
{
  "title":"some very new cool title"
}
```

Reponse example: 

```json
{
    "@odata.context": "#/categories/94bb2ba0-ff41-11e7-8590-6fb5edb04538/",
    "@odata.type": "Entity.Category",
    "value": {
        "title": "some very new cool title",
        "topics_count": 0,
        "metadata": {
            "id": "94bb2ba0-ff41-11e7-8590-6fb5edb04538",
            "created_time": "22-01-2018T06:57:59.107Z",
            "created_by_identity": "00000000-0000-0000-0000-000000000000",
            "last_modified_time": "26-01-2018T21:01:16.752Z",
            "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
            "votes": 0,
            "tags": []
        }
    }
}
```

\[DELETE\] /categories/{id}/

Headers: Authorization

Query-parameters: none

Body: none

Reponse example: (No Content)

```json
{}
```

#### Topics API

\[POST\] /categories/{id}/topics

Headers: Authorization

Query-parameters: none

Body: 

```json
{
  "title":"some very cool title",
  "description":"some very cool description"
}
```

Response example: 

```json
{
    "@odata.context": "#/categories/9b5a7cf6-02dc-11e8-ae9c-072375c5e376/topics/",
    "@odata.type": "Entity.Topic",
    "value": {
        "title": "some very cool title",
        "description": "some very cool description",
        "category_id": "9b5a7cf6-02dc-11e8-ae9c-072375c5e376",
        "comments_count": 0,
        "metadata": {
            "id": "4e476112-02dd-11e8-ae9c-a75b8d3e6665",
            "created_time": "26-01-2018T21:10:15.908Z",
            "created_by_identity": "00000000-0000-0000-0000-000000000000",
            "last_modified_time": "26-01-2018T21:10:15.908Z",
            "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
            "votes": 0,
            "tags": []
        }
    }
}
```

\[GET\] /categories/{id}/topics/

Headers: none/Authorization

Query-parameters: none

Body: none

Response example: 

```json
{
    "@odata.count": 1,
    "@odata.context": "#/topics/?offset=0&limit=20",
    "@odata.type": "Type.Collection",
    "value": [
        {
            "@odata.type": "Entity.Topic",
            "value": {
                "title": "some very cool title",
                "description": "some very cool description",
                "category_id": "9b5a7cf6-02dc-11e8-ae9c-072375c5e376",
                "comments_count": 0,
                "metadata": {
                    "id": "4e476112-02dd-11e8-ae9c-a75b8d3e6665",
                    "created_time": "26-01-2018T21:10:15.908Z",
                    "created_by_identity": "00000000-0000-0000-0000-000000000000",
                    "last_modified_time": "26-01-2018T21:10:15.908Z",
                    "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
                    "votes": 0,
                    "tags": []
                }
            }
        }
    ]
}
```

\[GET\] /topics/{id}/

Headers: none/Authorization

Query-parameters: none

Body: none

Response example: 

```json
{
    "@odata.context": "#/topics/4e476112-02dd-11e8-ae9c-a75b8d3e6665/",
    "@odata.type": "Entity.Topic",
    "value": {
        "title": "some very cool title",
        "description": "some very cool description",
        "category_id": "9b5a7cf6-02dc-11e8-ae9c-072375c5e376",
        "comments_count": 0,
        "metadata": {
            "id": "4e476112-02dd-11e8-ae9c-a75b8d3e6665",
            "created_time": "26-01-2018T21:10:15.908Z",
            "created_by_identity": "00000000-0000-0000-0000-000000000000",
            "last_modified_time": "26-01-2018T21:10:15.908Z",
            "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
            "votes": 0,
            "tags": []
        }
    }
}
```

\[PATCH\] /topics/{id}/

Headers: Authorization

Query-parameters: none

Body: 

```json
{
  "title":"optional field",
  "description":"optional field"
}
```

Response example: 

```json
{
    "@odata.context": "#/topics/4e476112-02dd-11e8-ae9c-a75b8d3e6665/",
    "@odata.type": "Entity.Topic",
    "value": {
        "title": "some very cool title",
        "description": "some very cool description",
        "category_id": "9b5a7cf6-02dc-11e8-ae9c-072375c5e376",
        "comments_count": 0,
        "metadata": {
            "id": "4e476112-02dd-11e8-ae9c-a75b8d3e6665",
            "created_time": "26-01-2018T21:10:15.908Z",
            "created_by_identity": "00000000-0000-0000-0000-000000000000",
            "last_modified_time": "26-01-2018T21:10:15.908Z",
            "last_modified_by_identity": "00000000-0000-0000-0000-000000000000",
            "votes": 0,
            "tags": []
        }
    }
}
```

\[DELETE\] /topics/{id}/

Headers: Authorization

Query-parameters: none

Body: none

Response example: (No Content)

```json
{}
```

#### Comments API

//TODO

\[POST\] /categories/{id}/comments/

\[POST\] /topics/{id}/comments/

\[POST\] /comments/{id}/comments/

\[GET\] /categories/{id}/comments/

\[GET\] /topics/{id}/comments/

\[GET\] /comments/{id}/comments/

\[GET\] /comments/{id}/

\[PATCH\] /comments/{id}/

\[DELETE\] /comments/{id}/

#### Artifact metadata API

//TODO

\[POST\] /artifacts/{id}/tags/attach

\[POST\] /artifacts/{id}/tags/detach

\[POST\] /artifacts/{id}/upvote

\[POST\] /artifacts/{id}/downvote

\[POST\] /tags/

\[GET\] /tags/

#### Identities API

//TODO

\[POST\] /identities/

\[GET\] /identities/

\[GET\] /identities/{id}/

\[PATCH\] /identities/{id}/

\[DELETE\] /identities/{id}/

\[GET\] /identities/{id}/image/

Query-parameters: 
1. size – размер изображения, enum = \[small, medium, large, origin\]

\[POST\] /identities/image/

### Мессенджер

Данная подсистема включает в себя функциональность изменения, удаления, получения информации о каждом из артефактов следующих типов: 
1. Conferences – конференции, в которых может участвовать более двух пользователей
2. Conversations – конференции, в которых может участвовать только два пользователя
3. Messages – сообщения в конференциях
4. Rich messages – сообщения с не-текстовым контентом (фотографии, видеоматериалы, ссылки)

### Логгирование

Данная подсистема включает в себя функциональность записи сообщений о ключевых действиях и возникающих ошибках Системы. 
Подсистема должна создавать файл-журнал для хранения информации о поведении серверного приложения каждый день.

### Роли

Данная подсистема должна предоставлять другим подсистемам внутренний API для проверки ролей пользователей, 
а также – набора дополнительных аттрибутов, связанных с пользователями. 

Помимо этого, у данной подсистемы должно быть внешнее API, доступное исключительно пользователям с ролями Administrator, Moderator.

Возможные роли в Системе:
1. Administrator
2. Moderator
3. Specialist
4. Authorized
5. Guest

### Подписки

Данная подсистема включает в себя функциональность отправки пуш-уведовлений клиентским приложениям 
при изменении артефактов, имеющихся у пользователей клиентских приложений в подписках. Помимо этого, 
она должна предоставлять внешнее API для создания/удаления подписки на изменения какого-либо артефакта, 
или одного из связанных с ним артефактов. 

## Требования к клиентскому приложению (веб)

Веб-приложение должно быть разработано с использованием js-фреймворка vue.js. В качестве нотаций для дизайна предоставляется выбор между 
Google Material design и Flat design, как простых, но полных и не лишенных стиля гайдлайнов. 

Веб-приложение обязано предоставлять удобный и понятный интерфейс для взаимодействия пользователей с API Системы. API системы должно быть полностью использовано в клиентской части.

### Реализация ролей

В зависимости от роли веб-приложение должно предоставлять пользователям различный интерфейс и функционал.

### Кэширование

Веб-приложение должно реализовывать предоставлять возможность кеширования ранее показанного контента. Все сущности по возможности должны комбинироваться с полученными от сервера и обновлять кэш. В разделах, где информация из кэша в течение длительного времени не была использована, кэш должен удаляться (старые сообщения; ранее просмотренные темы, к которым пользователь долго не возвращался и т.д.).
Все возможные функции на стороне клиента средствами vue.js максимально обращаются к кэшу и минимально используют ресурсы рендера.