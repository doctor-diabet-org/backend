export default {
  indexOf (collection, filter) {
    for (var i = 0; i < collection.length; i++) {
      if (filter(collection[i], i, collection)) return i
    }
    return -1
  },
  formatDate (date) {
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()
    var hour = date.getHours()
    var minute = date.getMinutes()
    var second = date.getSeconds()
    var hourFormatted = hour
    var minuteFormatted = minute < 10 ? '0' + minute : minute
    var secondFormatted = second < 10 ? '0' + second : second

    return year + '-' + month + '-' + day + ' ' + hourFormatted + ':' +
        minuteFormatted + ':' + secondFormatted + '.000'
  },
  formatUsername (identityLink) {
    return identityLink.first_name + ' ' + identityLink.last_name
  },
  formatDateTime (dateTime) {
    var dateTimeTokens = dateTime.split('T')
    var date = dateTimeTokens[0]
    var time = dateTimeTokens[1]

    var dateTokens = date.split('-')
    var year = dateTokens[0]
    var month = dateTokens[1]
    var day = dateTokens[2]

    var timeTokens = time.substring(0, time.length).split(':')
    var hours = timeTokens[0]
    var minutes = timeTokens[1]
    return year + '/' + month + '/' + day + ', ' + hours + ':' + minutes
  }
}
