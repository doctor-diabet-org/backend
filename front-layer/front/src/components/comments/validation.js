export default {
  commentTextRules: [
    (v) => !!v || 'Ответ не может быть пустым'
  ]
}
