export default {
  titleRules: [
    (v) => !!v || 'Название не может быть пустым'
  ],
  descriptionRules: [
    (v) => !!v || 'Пояснение не может быть пустым'
  ]
}
