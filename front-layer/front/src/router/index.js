import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'

import AuthorizationCredentials from '@/components/authorization/AuthorizationCredentials.vue'
import Authorize1 from '@/components/authorization/AuthorizationCredentials.1.vue'

import Registration from '@/components/registration/Registration.vue'
import register1 from '@/components/registration/Registration.1.vue'

import Index from '@/components/index/Index.vue'
import CategoriesList from '@/components/categories/CategoriesList.vue'
import TopicsList from '@/components/topics/TopicsList.vue'
import TopicsList1 from '@/components/topics/TopicsList.1.vue'
import TopicCreate from '@/components/topics/TopicCreate.vue'
import TopicCreate1 from '@/components/topics/TopicCreate.1.vue'
import realtopiccreate from '@/components/topics/TopicCreate.2.vue'
import CommentsList from '@/components/comments/CommentsList.vue'
import Profile from '@/components/users/Profile.vue'
import Profile1 from '@/components/users/Profile.1.vue'
import Sample from '@/components/sample/Sample.vue'

// import store from '@/store'

Vue.use(Router)
Vue.use(VueResource)

// Vue.http.headers.common['Authorization'] = $('meta[name="token"]').attr('value')

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/authorize',
      name: 'AuthorizationCredentials',
      component: AuthorizationCredentials
    },
    {
      path: '/categories',
      name: 'CategoriesList',
      component: CategoriesList
    },
    {
      path: '/categories/:categoryId/topics',
      name: 'TopicsList',
      component: TopicsList,
      props: true
    },
    {
      path: '/categories/:categoryId/topics/create',
      name: 'TopicCreate',
      component: TopicCreate,
      props: true
    },
    {
      path: '/topics/:topicId/comments',
      name: 'CommentsList',
      component: CommentsList,
      props: true
    },
    {
      path: '/register',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/users/:identity_id',
      name: 'Profile',
      component: Profile,
      props: true
    },
    {
      path: '/sample',
      name: 'Sample',
      component: Sample
    },
    {
      path: '/topicslist1',
      name: 'TopicsList1',
      component: TopicsList1
    },
    {
      path: '/topiccreate1',
      name: 'TopicCreate1',
      component: TopicCreate1
    },
    ,
    {
      path: '/realtopiccreate',
      name: 'realtopiccreate',
      component: realtopiccreate
    },
    {
      path: '/profile1',
      name: 'Profile1',
      component: Profile1
    }
    ,
    {
      path: '/authorize1',
      name: 'Authorize1',
      component: Authorize1
    },
    {
      path: '/register1',
      name: 'register1',
      component: register1
    }
  ]
})

export default router
