import Vue from 'vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'

import createPersistedState from 'vuex-persistedstate'
// import * as Cookies from 'js-cookie'

import router from '../router'
import utils from '../utils'

Vue.use(Vuex)
Vue.use(VueResource)

const HttpStatuses = {
// standard response code for successful querying
Ok: 200,
// standard response code for successful creation
Created: 201,
// standard response code for successful deletion
NoContent: 204,
// the resource is deleted
TemporaryRedirect: 307,
// the request contained malformed params, or validation failed
BadRequest: 400,
// the request was performed with expired or invalid session token
Unauthorized: 401,
// the operation or resource access was forbidden due to lack of privileges
Forbidden: 403,
// the resource was not found (NB: this not includes cases when the resource was deleted)
NotFound: 404,
// there was an internal error on server-side
InternalServerError: 500
}

const store = new Vuex.Store({
  state: {
    refreshToken: null,
    sessionToken: null,
    identity: null,

    registererror: null,
    loginState: 'email',
    phoneNumber: null,

    // данные пользователя после авторизации
    userFirstName: null,
    userLastName: null,
    userEmail: null,
    userPhoneNumber: null,
    userTopics: [],
    userTopicsTotal: null,

    selectedCategory: null,
    categoriesPageSize: 50,
    categoriesPage: -1,
    categoriesPageTotal: 0,
    categories: [

    ],

    popularTags: [

    ],

    selectedTopic: null,
    topicsPageSize: 50,
    topicsPage: -1,
    topicsPageTotal: 0,
    topics: [

    ],

    selectedComment: null,
    commentsPageSize: 5,
    commentsPage: -1,
    commentsPageTotal: 0,
    comments: [

    ],

    tags: [

    ]
  },
  mutations: {
    //
    setUserFirstName (state, newfirstname) {
      state.userFirstName = newfirstname;
    },
    setUserLastName (state, newlastname) {
      state.userLastName = newlastname;
    },
    setUserEmail (state, newemail) {
      state.userEmail = newemail;
    },
    setUserPhoneNumber (state, newphonenumber) {
      state.userPhoneNumber = newphonenumber
    },
    setUserTopics (state, newusertopics) {
      state.userTopics = newusertopics
    },
    setRefreshToken (state, refreshToken) {
      state.refreshToken = refreshToken
    },
    setSessionToken (state, sessionToken) {
      state.sessionToken = sessionToken
    },
    setIdentity (state, identity) {
      state.identity = identity
    },
    invalidateSessionToken (state) {
      state.sessionToken = null
    },
    invalidateIdentity (state) {
      state.refreshToken = null
      state.sessionToken = null
      state.identity = null
    },
    setLoginState (state, newstate) {
      state.loginState = newstate
    },
    setPhoneNumber (state, newnumber) {
      state.phoneNumber = newnumber
    },
    setCategoriesPage (state, page) {
      state.categoriesPage = page
    },
    setCategoriesPageTotal (state, total) {
      state.categoriesPageTotal = total
    },
    setUserTopicsTotal(state, total) {
      state.userTopicsTotal
    },
    setCategories (state, categories) {
      state.categories.length = 0
      for (var category of categories) {
        state.categories.push(category)
      }
    },
    setSelectedCategory (state, category) {
      state.selectedCategory = category
    },
    invalidateCategoriesState (state) {
      state.selectedCategory = null
      state.categoriesPageSize = 50
      state.categoriesPage = -1
      state.categories.length = 0
    },

    addCreatedTopic (state, topic) {
      state.topics.unshift(topic)
    },
    setTopicsPage (state, page) {
      state.topicsPage = page
    },
    setTopicsPageTotal (state, total) {
      state.topicsPageTotal = total
    },
    setTopics (state, topics) {
      state.topics.length = 0
      for (var topic of topics) {
        state.topics.push(topic)
      }
    },
    setSelectedTopic (state, topic) {
      state.selectedTopic = topic
    },
    invalidateTopicsState (state) {
      state.selectedTopic = null
      state.topicsPageSize = 50
      state.topicsPage = -1
      state.topics.length = 0
    },

    addCreatedComment (state, comment) {
      state.comments.unshift(comment)
    },
    setCommentsPage (state, page) {
      state.commentsPage = page
    },
    setCommentsPageTotal (state, total) {
      state.commentsPageTotal = total
    },
    setComments (state, comments) {
      state.comments.length = 0
      for (var comment of comments) {
        state.comments.push(comment)
      }
    },
    setSelectedComment (state, comment) {
      state.selectedComment = comment
    },
    invalidateCommentsState (state) {
      state.selectedComment = null
      state.commentsPageSize = 5
      state.commentsPage = -1
      state.comments.length = 0
    },

    setTags (state, newTags) {
      state.tags.length = 0
      for (var newTag of newTags) {
        state.tags.push(newTag)
      }
    },
    invalidateTags (state) {
      state.tags.length = 0
    },

    setPopularTags (state, newPopularTags) {
      state.popularTags.length = 0
      for (var tag of newPopularTags) {
        state.popularTags.push(tag)
      }
    },
    invalidatePopularTags (state) {
      state.popularTags.length = 0
    },
    setRegisterError (state, error){
      state.registererror = error
    }
  },
  actions: {
    authorizeCredentials1 ({ state, commit }, credentials) {
      return new Promise((resolve, reject) => {
        var bodyValue = { email: credentials.username, password: credentials.password }
        var headersValue = { 'Content-Type': 'application/json' }

        Vue.http.put(`authorize/credentials`, bodyValue, { headers: headersValue }).then(
          response => {
            console.log(response)

            var body = response.body
            var value = body['value']
            var identity = value['identity_id']
            var refreshToken = value['refresh_token']
            resolve({ identity_id: identity, refresh_token: refreshToken })
          },
          error => {
            reject(error)
          }
        )
      })
    },

    authorizeCredentials ({ state, commit, dispatch }, credentials) {
      return dispatch('authorizeCredentials1', credentials).then(
        (data) => {
          var bodyValue = data
          var headersValue = { 'Content-Type': 'application/json' }
          Vue.http.put(`authorize/token`, bodyValue, { headers: headersValue }).then(

            response => {
              console.log(response)

              var body = response.body
              var value = body['value']
              var refreshToken = value['refresh_token']
              var sessionToken = value['session_token']
              var identity = value['identity_id']
              commit('setRefreshToken', refreshToken)
              commit('setSessionToken', sessionToken)
              commit('setIdentity', identity)
              router.push('/categories')
            },
            error => {
              console.log(error)
            }
          )
        },
        (error) => {
          console.log('first step error: ' + error)
        }
      )
    },

    authorizePhone ({ state, commit }, phone) {
      var headersValue = { 'Content-Type': 'application/json' }
      var bodyValue = { 'phone_number': phone }

      Vue.http.post(`authorize/code`, bodyValue, headersValue).then(
        response => {
          console.log(response)

          var body = response.body
          console.log(body)
          commit('setLoginState', 'code')
          commit('setPhoneNumber', phone)
        },
        error => {
          console.error(error)
        }
      )
    },

    authorizeCode1 ({ state, commit }, data) {
      var headersValue = { 'Content-Type': 'application/json' }
      var bodyValue = { 'phone_number': data.phone, 'code': data.code }

      return new Promise((resolve, reject) => {
        Vue.http.put(`authorize/code`, bodyValue, headersValue).then(
          response => {
            console.log(response)

            var body = response.body
            var value = body['value']
            var identity = value['identity_id']
            var refreshToken = value['refresh_token']

            resolve({ identity_id: identity, refresh_token: refreshToken })
          },
          error => {
            console.error(error)
            reject(error)
          }
        )
      })
    },

    authorizeCode ({ state, commit, dispatch }, data) {
      return dispatch('authorizeCode1', data).then(
        (data) => {
          var bodyValue = data
          var headersValue = { 'Content-Type': 'application/json' }

          Vue.http.put(`authorize/token`, bodyValue, { headers: headersValue }).then(
            response => {
              console.log(response)

              var body = response.body
              var value = body['value']
              var refreshToken = value['refresh_token']
              var sessionToken = value['session_token']
              var identity = value['identity_id']
              commit('setRefreshToken', refreshToken)
              commit('setSessionToken', sessionToken)
              commit('setIdentity', identity)

              router.push('/categories')
            },
            error => {
              console.log(error)
            }
          )
        },
        (error) => {
          console.error(error)
        }
      )
    },

    register ({ state, commit }, data) {
      var headersValue = { 'Content-Type': 'application/json' }

      Vue.http.post(`identities`, data, headersValue).then(
        response => {
          console.log(response)

          var body = response.body
          console.log(body)

          router.push('/authorize')

        },
        error => {
          console.error(error)
          var body = error.body

          // bad email
          if (body.code===401){
            commit('setRegisterError','Введена недействительная почта!')
          }
          // bad phone
          else if (body.code===402){
            commit('setRegisterError','Введен недействительный номер телефона!')
          }
          // bad fn
          else if (body.code===403){
            commit('setRegisterError','Имя введено неверно!')
          }
          // bad ln
          else if (body.code===404){

            commit('setRegisterError','Фамилия введена неверно!')
          }
          // email exists
          else if (body.code===407){

            commit('setRegisterError','Данная почта уже зарегистрирована!')
          }
          // phine exists
          else if (body.code===408){

            commit('setRegisterError','Данный номер телефона уже зарегистрирован!')
          }
          // server error
          else if (responce.code===500){

            commit('setRegisterError','Неизвестная серверная ошибка. Попробуйте еще раз 😭')
          }
        }
      )
    },

    fetchCategory ({ state, commit }, id) {
      Vue.http.get(`categories/${id}`, {}).then(
        response => {
          console.log(response)

          var body = response.body
          var value = body['value']
          commit('setSelectedCategory', value)
        },
        error => {
          console.error(error)
        }
      )
    },

    fetchCategoriesPage ({ state, commit }, page) {
      if (page === state.categoriesPage) {
        return
      }
      commit('setCategoriesPage', page)
      var offsetValue = state.categoriesPageSize * (page - 1)
      var limitValue = state.categoriesPageSize
      Vue.http.get(`categories`, { params: { offset: offsetValue, limit: limitValue } }).then(
        response => {
          console.log(response)

          var body = response.body
          var count = body['@odata.count']
          var total = body['@odata.total']
          var value = body['value']

          var newCategories = []

          for (var i = 0; i < count; i++) {
            newCategories.push(value[i]['value'])
          }

          console.log(newCategories)

          commit('setCategories', newCategories)
          commit('setCategoriesPageTotal', ((total / state.categoriesPageSize) | 0) + 1)
        },
        error => {
          console.error(error)
        }
      )
    },

    fetchTopic ({ state, commit }, id) {
      Vue.http.get(`topics/${id}`, {}).then(
        response => {
          console.log(response)

          var body = response.body
          var value = body['value']
          commit('setSelectedTopic', value)
        },
        error => {
          console.error(error)
        }
      )
    },

    fetchTopicsPage ({ state, commit }, data) {
      console.log('Data: ' + data)
      var page = data.page
      var categoryId = data.categoryId
      if (page === state.topicsPage) {
        return
      }
      commit('setTopicsPage', page)
      var offsetValue = state.topicsPageSize * (page - 1)
      var limitValue = state.topicsPageSize
      Vue.http.get(`categories/${categoryId}/topics`, { params: { offset: offsetValue, limit: limitValue } }).then(
        response => {
          console.log(response)

          var body = response.body
          var count = body['@odata.count']
          var total = body['@odata.total']
          var value = body['value']

          var newTopics = []

          for (var i = 0; i < count; i++) {
            newTopics.push(value[i]['value'])
          }

          console.log(newTopics)

          commit('setTopics', newTopics)
          commit('setTopicsPageTotal', ((total / state.topicsPageSize) | 0) + 1)
        },
        error => {
          console.error(error)
        }
      )
    },

    createTopic ({ state, commit }, data) {
      console.log('Data: ' + data)
      if (state.identity === null) {
        return
      }
      var categoryId = data.categoryId
      var model = data.model
      return new Promise((resolve, reject) => {
        Vue.http.post(`categories/${categoryId}/topics`, model).then(
          response => {
            console.log(response)

            var body = response.body
            var value = body['value']

            console.log('Topic value: ' + value)
            commit('addCreatedTopic', value)
            resolve(value)
          },
          error => {
            console.error(error)
            reject(error.body)
          }
        )
      })
    },

    fetchCommentsPage ({ state, commit }, data) {
      var page = data.page
      var topicId = data.topicId
      if (page === state.commentsPage) {
        return
      }
      commit('setCommentsPage', page)
      var offsetValue = state.commentsPageSize * (page - 1)
      var limitValue = state.commentsPageSize

      console.log('fetchCommentsPage called')
      Vue.http.get(`topics/${topicId}/comments`, { params: { offset: offsetValue, limit: limitValue } }).then(
        response => {
          console.log(response)

          var body = response.body
          var count = body['@odata.count']
          var total = body['@odata.total']
          var value = body['value']

          var newComments = []

          for (var i = 0; i < count; i++) {
            newComments.push(value[i]['value'])
          }

          console.log(newComments)

          commit('setComments', newComments)
          commit('setCommentsPageTotal', ((total / state.commentsPageSize) | 0) + 1)
        },
        error => {
          console.error(error)
        }
      )
    },

    createComment ({ state, commit }, data) {
      if (state.identity === null) {
        return
      }
      var topicId = data.topicId
      var model = data.model
      Vue.http.post(`topics/${topicId}/comments`, model).then(
        response => {
          console.log(response)

          var body = response.body
          var value = body['value']
          commit('addCreatedComment', value)
        },
        error => {
          console.error(error)
        }
      )
    },

    fetchTags ({ state, commit }) {
      if (state.tags.length !== 0) {
        return
      }

      Vue.http.get(`tags`, { params: { } }).then(
        response => {
          console.log(response)

          var body = response.body
          var value = body['value']
          var newTags = []
          for (var tag of value) {
            newTags.push(tag['value'])
          }

          commit('setTags', newTags)
        },
        error => {
          console.log(error)
        }
      )
    },

    fetchPopularTags ({ state, commit }) {
      if (state.popularTags.length !== 0) {
        commit('invalidatePopularTags', null)
      }

      Vue.http.get(`tags/popular`, { params: { limit : 10} }).then(
        response => {
          console.log(response)

          var body = response.body
          var value = body['value']
          var newPopularTags = []

          for(var tag of value) {
            newPopularTags.push(tag['value'])
          }

          commit('setPopularTags', newPopularTags)
        },
        error => {
          console.error(error)
        }
      )
    },

    fetchIdentity ({ state, commit }, user_id) {
      if(user_id===null)
        return;
      Vue.http.get(`identities/`+user_id, { params: {} }).then(
        response => {
          console.log(response)
          var body = response.body
          var value = body['value']
          var newfirstname = value['first_name']
          var newlastname = value['last_name']
          var newemail = value['email']
          var newphonenumber = value['phone_number']
          commit('setUserFirstName', newfirstname)
          commit('setUserLastName', newlastname)
          commit('setUserEmail', newemail)
          commit('setUserPhoneNumber', newphonenumber)
        },
        error => {
          console.log(error)
        }
      )
    },

    fetchUserTopics ({ state, commit }, user_id) {
      if(user_id===null)
        return;
      Vue.http.get(`topics/`, { params: {subscriptions_of: user_id} }).then(
        response => {
          console.log(response)
          var body = response.body
          var value = body['value']
          var newusertopics = []
          for(var topic of value) {
            newusertopics.push(topic['value'])
          }
          console.log(newusertopics)
          commit('setUserTopics', newusertopics)
          commit('setUserTopicsTotal', newusertopics.length)
        },
        error => {
          console.log(error)
        }
      )
    },
    upvote ({ state, commit, dispatch }, payload) {
      dispatch('upvoteInternal', payload).then(
        (data) => {
          //do something when upvoted successfully
          //to know what was upvoted just use the payload information
        },
        (error) => {
          console.error(error)
        }
      )
    },

    downvote ({ state, commit, dispatch }, payload) {
      dispatch('downvoteInternal', payload).then(
        (data) => {
          //do something when downvoted successfully
          //to know what was downvoted just use the payload information
        },
        (error) => {
          console.error(error)
        }
      )
    },

    subscribe ({ state, commit, dispatch }, payload) {
      dispatch('subscribe', payload).then(
        (date) => {
          //do something when subscribed successfully
        },
        (error) => {
          console.error(error)
        }
      )
    },

    unsubscribe ({ state, commit, dispatch }, payload) {
      dispatch('unsubscribe', payload).then(
        (date) => {
          //do something when subscribed successfully
        },
        (error) => {
          console.error(error)
        }
      )
    },

    // Payload MUST contain next params:
    // 1. artifactId
    upvoteInternal ({ state, commit, dispatch }, payload) {
      if (payload.artifactId === undefined) {
        console.log('artifactId param in \'upvoteInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'applications/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}

      return new Promise((resolve, reject) => {
        Vue.http.post(`artifacts/${payload.artifactId}/upvote`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            resolve({})
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'upvoteInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // Payload MUST contain next params:
    // 1. artifactId
    downvoteInternal ({ state, commit, dispatch }, payload) {
      if (payload.artifactId === undefined) {
        console.log('artifactId param in \'downvoteInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'applications/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}

      return new Promise((resolve, reject) => {
        Vue.http.post(`artifacts/${payload.artifactId}/downvote`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            resolve({})
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'downvoteInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // Payload MUST contain next params:
    // 1. artifactId
    subscribeInternal ({ state, commit, dispatch }, payload) {
      if (payload.artifactId === undefined) {
        console.log('artifactId param in \'subscribeInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'applications/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}

      return new Promise((resolve, reject) => {
        Vue.http.post(`artifacts/${payload.artifactId}/subscribe`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            resolve({})
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'subscribeInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // Payload MUST contain next params:
    // 1. artifactId
    unsubscribeInternal ({ state, commit, dispatch }, payload) {
      if (payload.artifactId === undefined) {
        console.log('artifactId param in \'unsubscribeInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'applications/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}

      return new Promise((resolve, reject) => {
        Vue.http.post(`artifacts/${payload.artifactId}/unsubscribe`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            resolve({})
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'unsubscribeInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    fetchTagsInternal ({ state, commit, dispatch }, payload) {
      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.get(`tags`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchTagsInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var tags = body['value']
            var outTags = []

            for (tag of tags) {
              outTags.push(tag['value'])
            }

            resolve(outTags)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchTagsInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // Payload MIGHT include:
    // 1. offset
    // 2. limit
    fetchPopularTagsInternal ({ state, commit, dispatch }, payload) {
      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}
      if (payload.offset !== undefined) {
        queryParams.offset = payload.offset
      }

      if (payload.limit !== undefined) {
        queryParams.limit = payload.limit
      }

      return new Promise((resolve, reject) => {
        Vue.http.get(`tags/popular`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchTagsInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var tags = body['value']
            var outTags = []

            for (tag of tags) {
              outTags.push(tag['value'])
            }

            resolve(outTags)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchTagsInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. title – (String) the title of category being created
    // 2. description – (String) the description of category being created
    //
    createCategoryInternal ({ state, commit, dispatch }, payload) {
      if (payload.title === undefined) {
        console.log('title param in \'createCategoryInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.description === undefined) {
        console.log('description param in \'createCategoryInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}
      bodyValue['title'] = payload.title
      bodyValue['description'] = payload.description

      return new Promise((resolve, reject) => {
        Vue.http.post(`categories`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Created) {
              console.log(`Unexpected response code in \'createCategoryInternal\': Expected 201, Created; Found: ${response.status}`)
            }

            var body = response.body
            var category = body['value']
            resolve(category)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'createCategoryInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. categoryId – (UUID) ID of category being fetched
    //
    fetchCategoryInternal ({ state, commit, dispatch }, payload) {
      if (payload.categoryId === undefined) {
        console.log('No categoryId was supplied in the payload to action \'fetchCategoryInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.get(`categories/${payload.categoryId}`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchCategoryInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var category = body['value']
            resolve(category)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchCategoryInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MIGHT include next params:
    // 1. offset – (Int) the amount of records that should be skipped
    // 2. limit – (Int) the amount of records to be loaded
    //
    fetchCategoriesInternal ({ state, commit, dispatch }, payload) {
      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      if (payload.offset !== undefined) {
        queryParams.offset = payload.offset
      }

      if (payload.limit !== undefined) {
        queryParams.limit = payload.limit
      }

      return new Promise((resolve, reject) => {
        Vue.http.get(`categories`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchCategoriesInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var categories = body['value']
            var outCategories = []

            for (category of categories) {
              outCategories.push(category['value'])
            }

            resolve(outCategories)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchCategoriesInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. categoryId – (UUID) the ID of category that is being updated
    //
    // Payload MIGHT include next params:
    // 1. title – (String) new title of category
    // 2. description – (String) new description of category
    //
    updateCategoryInternal ({ state, commit, dispatch }, payload) {
      if (payload.categoryId === undefined) {
        console.log('categoryId param in \'updateCategoryInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.title === undefined && payload.description === undefined) {
        console.log('No params were passed in payload in \'updateCategoryInternal\', returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}

      if (payload.title !== undefined) {
        bodyValue['title'] = payload.title
      }

      if (payload.description !== undefined) {
        bodyValue['description'] = payload.description
      }

      return new Promise((resolve, reject) => {
        Vue.http.patch(`categories/${payload.categoryId}`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'updateCategoryInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var category = body['value']
            resolve(category)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'updateCategoryInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. categoryId – (UUID) the ID of category that is being deleted
    //
    deleteCategoryInternal ({ state, commit, dispatch }, payload) {
      if (payload.categoryId === undefined) {
        console.log('categoryId param in \'deleteCategoryInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
         Vue.http.delete(`categories/${payload.categoryId}`, { headers: headersValue, params: queryParams }).then(
           response => {
            if (response.status !== HttpStatuses.NoContent) {
              console.log(`Unexpected response code in \'deleteCategoryInternal\': Expected 204, NoContent; Found: ${response.status}`)
            }

            resolve({})
           },
           error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'deleteCategoryInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
           }
         )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. title – (String) the title of topic being created
    // 2. description – (String) the description of topic being created
    // 3. tagIds – (UUID[]) and array of tags to be attached to topic being created
    // 4. categoryId – (UUID) ID of category where the topic is being created
    //
    createTopicInternal ({ state, commit, dispatch }, payload) {
      if (payload.title === undefined) {
        console.log('title param in \'createTopicInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.description === undefined) {
        console.log('description param in \'createTopicInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.tagIds === undefined) {
        console.log('tagIds param in \'createTopicInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.categoryId === undefined) {
        console.log('categoryId param in \'createTopicInternal\' is undefined, returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}
      bodyValue['title'] = payload.title
      bodyValue['description'] = payload.description
      bodyValue['tag_ids'] = []
      for (tagId of payload.tagIds) {
        bodyValue['tag_ids'].push(tagId)
      }

      return new Promise((resolve, reject) => {
        Vue.http.post(`categories/${payload.categoryId}/topics`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Created) {
              console.log(`Unexpected response code in \'createTopicInternal\': Expected 201, Created; Found: ${response.status}`)
            }

            var body = response.body
            var topic = body['value']
            resolve(topic)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'createTopicInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. topicId – (UUID) the ID of topic that is being fetched
    //
    fetchTopicInternal ({ state, commit, dispatch }, payload) {
      if (payload.topicId === undefined) {
        console.log('No topicId was supplied in the payload to action \'fetchTopicInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.get(`topics/${payload.topicId}`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchTopicInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var topic = body['value']
            resolve(topic)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchTopicInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MIGHT include next params:
    // 1. offset – (Int) the amount of records that should be skipped
    // 2. limit – (Int) the amount of records to be loaded
    // 3. tagIds – (UUID[]) set of tag filters that would be applied to result set
    // 4. tagMode – (Enum) must be set along with tags of at least one element.
    //              Possible values are:
    // 4.1. all_of – any record in result set would have all of tags from the supplied array
    // 4.2. any_of – any record in result set would have at least one tag from supplied array
    // 5. votesGt – (Long) specifies the lower bound of votes for records in the result set
    // 6. votesLt – (Long) specifies the upper bound of votes for records in the result set
    // 7. votesOrder – (Enum) specifies the ordering of records by votes.
    //                 Possible values are:
    // 7.1. ASC – sort by ascending
    // 7.2. DESC – sort by descendingg
    // 8. query – (String) a string that WOULD present in titles of records in the result set as a substring
    // 9. subscriptionsOf – (UUID) applies subscribe filter to result set,
    // returning only the topics that the user with given ID has subscribed to
    //
    fetchTopicsInternal ({ state, commit, dispatch }, payload) {
      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      // Map all supplied params to queryParams object to use with VueResource

      if (payload.offset !== undefined) {
        queryParams.offset = payload.offset
      }

      if (payload.limit !== undefined) {
        queryParams.limit = payload.limit
      }

      if (payload.tags !== undefined && payload.tags.length !== 0) {
        queryParams.tag = []

        for (tagId of payload.tagIds) {
          queryParam.tag.push(tagId)
        }
      }

      if (payload.tagMode !== undefined) {
        queryParam.tag_mode = payload.tagMode
      }

      if (payload.votesGt !== undefined) {
        queryParams.votes_gt = payload.votesGt
      }

      if (payload.votesLt !== undefined) {
        queryParams.votes_lt = payload.votesLt
      }

      if (payload.votesOrder !== undefined) {
        queryParams.votes_order = payload.votesOrder
      }

      if (payload.query !== undefined) {
        queryParams.query = payload.query
      }

      if (payload.subscriptionsOf !== undefined) {
        queryParams.subscriptions_of = payload.subscriptionsOf
      }

      return new Promise((resolve, reject) => {
        Vue.http.get(`topics`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchTopicsInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var topics = body['value']

            var outTopics = []

            for (topic of topics) {
              outTopics.push(topic['value'])
            }

            resolve(outTopics)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchTopicsInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. categoryId
    //
    // Payload MIGHT include next params:
    // 1. offset – (Int) the amount of records that should be skipped
    // 2. limit – (Int) the amount of records to be loaded
    // 3. tagIds – (UUID[]) set of tag filters that would be applied to result set
    // 4. tagMode – (Enum) must be set along with tags of at least one element.
    //              Possible values are:
    // 4.1. all_of – any record in result set would have all of tags from the supplied array
    // 4.2. any_of – any record in result set would have at least one tag from supplied array
    // 5. votesGt – (Long) specifies the lower bound of votes for records in the result set
    // 6. votesLt – (Long) specifies the upper bound of votes for records in the result set
    // 7. votesOrder – (Enum) specifies the ordering of records by votes.
    //                 Possible values are:
    // 7.1. ASC – sort by ascending
    // 7.2. DESC – sort by descendingg
    // 8. query – (String) a string that WOULD present in titles of records in the result set as a substring
    //
    fetchTopicsByCategoryInternal ({ state, commit, dispatch }, payload) {
      if (payload.categoryId === undefined) {
        console.log('No categoryId was supplied in the payload to action \'fetchTopicsByCategoryInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      // Map all supplied params to queryParams object to use with VueResource

      if (payload.offset !== undefined) {
        queryParams.offset = payload.offset
      }

      if (payload.limit !== undefined) {
        queryParams.limit = payload.limit
      }

      if (payload.tags !== undefined && payload.tags.length !== 0) {
        queryParams.tag = []

        for (tagId of payload.tagIds) {
          queryParam.tag.push(tagId)
        }
      }

      if (payload.tagMode !== undefined) {
        queryParam.tag_mode = payload.tagMode
      }

      if (payload.votesGt !== undefined) {
        queryParams.votes_gt = payload.votesGt
      }

      if (payload.votesLt !== undefined) {
        queryParams.votes_lt = payload.votesLt
      }

      if (payload.votesOrder !== undefined) {
        queryParams.votes_order = payload.votesOrder
      }

      if (payload.query !== undefined) {
        queryParams.query = payload.query
      }

      return new Promise((resolve, reject) => {
        Vue.http.get(`categories/${payload.cateoryId}/topics`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchTopicsByCategoryInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var topics = body['value']

            var outTopics = []

            for (topic of topics) {
              outTopics.push(topic['value'])
            }

            resolve(outTopics)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchTopicsInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    // Payload MUST include next params:
    // 1. topicId – (UUID) the ID of topic being updated
    //
    // Payload MIGHT include next params:
    // 1. title – (String) the new title of topic
    // 2. description - (Strin) the new description of topic
    // 3. categoryId – the new category id of topic
    //
    updateTopicInternal ({ state, commit, dispatch }, payload) {
      if (payload.topicId === undefined) {
        console.log('No topicId param was supplied in the payload to action \'updateTopicInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.title === undefined && payload.description === undefined && payload.categoryId === undefined) {
        console.log('No params were supplied in the payload to action \'updateTopicInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}
      if (payload.title !== undefined) {
        bodyValue['title'] = payload.title
      }

      if (payload.description !== undefined) {
        bodyValue['description'] = payload.description
      }

      if (payload.categoryId !== undefined) {
        bodyValue['category_id'] = payload.categoryId
      }

      return new Promise((resolve, reject) => {
        Vue.http.patch(`topics/${payload.topicId}`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'updateTopicInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var topic = body['value']
            resolve(topic)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'updateTopicInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. topicId – (UUID) the ID of topic being deleted
    //
    deleteTopicInternal ({ state, commit, dispatch }, payload) {
      if (payload.topicId === undefined) {
        console.log('No topicId was supplied in the payload to action \'deleteTopicInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.delete(`topics/${payload.topicId}`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.NoContent) {
              console.log(`Unexpected response code in \'deleteTopicInternal\': Expected 204, NoContent; Found: ${response.status}`)
            }

            resolve({})
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'deleteTopicInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. topicId – (UUID) the ID of topic being commented
    // 2. contentText – (Strin) the actual text of comment
    //
    createCommentInTopicInternal ({ state, commit, dispatch }, payload) {
      if (payload.topicId === undefined) {
        console.log('No topicId was supplied in the payload to action \'createCommentInTopicInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.contentText === undefined) {
        console.log('No contentText was supplied in the payload to action \'createCommentInTopicInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}
      bodyValue['content_text'] = payload.contentText

      return new Promise((resolve, reject) => {
        Vue.http.post(`topics/${payload.topicId}/comments`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Created) {
              console.log(`Unexpected response code in \'createCommentInTopicInternal\': Expected 201, Created; Found: ${response.status}`)
            }

            var body = response.body
            var comment = body['value']
            resolve(comment)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'createCommentInTopicInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. commentId – (UUID) the ID of comment being fetched
    //
    fetchCommentInternal ({ state, commit, dispatch }, payload) {
      if (payload.commentId === undefined) {
        console.log('No commentId was supplied in the payload to action \'fetchCommentInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.get(`comments/${payload.commentId}`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchCommentInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var comment = body['value']
            resolve(comment)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchCommentInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. topicId – (UUID) the ID of comment being fetched
    //
    // Payload MIGHT include next params:
    // 1. offset – (Int) the amount of records that should be skipped
    // 2. limit – (Int) the amount of records to be loaded
    //
    fetchCommentsInTopicInternal ({ state, commit, dispatch }, payload) {
      if (payload.topicId === undefined) {
        console.log('No topicId was supplied in the payload to action \'fetchCommentsInTopicInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}
      if (payload.offset !== undefined) {
        queryParams.offset = payload.offset
      }

      if (payload.limit !== undefined) {
        queryParams.limit = payload.limit
      }

      return new Promise((resolve, reject) => {
        Vue.http.get(`topics/${payload.topicId}/comments`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'fetchCommentsInTopicInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var comments = body['value']
            var outComments = []

            for (comment of comments) {
              outComments.push(comment['value'])
            }

            resolve(outComments)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchCommentsInTopicInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. commentId – (UUID) the ID of comment being updated
    //
    // Payload MIGHT include next params:
    // 1. contentText
    //
    updateCommentInternal ({ state, commit, dispatch }, payload) {
      if (payload.commentId === undefined) {
        console.log('No commentId was supplied in the payload to action \'updateCommentInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.contentText === undefined) {
        console.log('No params were supplied to the payload in action \'updateCommentInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}

      if (payload.contentText !== undefined) {
        bodyValue['content_text'] = payload.contentText
      }

      return new Promise((resolve, reject) => {
        Vue.http.patch(`comments/${payload.commentId}`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'updateCommentInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var comment = body['value']
            resolve(comment)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'updateCommentInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. commentId – (UUID) the ID of comment being updated
    //
    deleteCommentInternal ({ state, commit, dispatch }, payload) {
      if (payload.commentId === undefined) {
        console.log('No commentId was supplied in the payload to action \'deleteCommentInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.delete(`comments/${payload.commentId}`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.NoContent) {
              console.log(`Unexpected response code in \'deleteCommentInternal\': Expected 204, NoContent; Found: ${response.status}`)
            }

            resolve({})
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'createIdentityInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. firstName – (String) the first name of user being created
    // 2. lastName – (String) the last name of user being created
    // 3. phoneNumber – (String) the phone number of user being created
    // 4. email – (String) the e-mail address of user being created
    // 5. password – (String) plain (non-encrypted) password of user beingg created
    //
    createIdentityInternal ({ state, commit, dispatch }, payload) {
      if (payload.firstName === undefined) {
        console.log('No firstName was supplied in the payload to action \'createIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.lastName === undefined) {
        console.log('No lastName was supplied in the payload to action \'createIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.phoneNumber === undefined) {
        console.log('No phoneNumber was supplied in the payload to action \'createIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.email === undefined) {
        console.log('No email was supplied in the payload to action \'createIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.password === undefined) {
        console.log('No password was supplied in the payload to action \'createIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}
      bodyValue['first_name'] = payload.firstName
      bodyValue['last_name'] = payload.lastName
      bodyValue['phone_number'] = payload.phoneNumber
      bodyValue['email'] = payload.email
      bodyValue['password'] = payload.password

      return new Promise((resolve, reject) => {
        Vue.http.post(`identities`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Created) {
              console.log(`Unexpected response code in \'createIdentityInternal\': Expected 201, Created; Found: ${response.status}`)
            }

            var body = response.body
            var identity = body['value']
            resolve(identity)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'createIdentityInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. identityId – (UUID) the ID of identity that is being fetched
    //
    fetchIdentityInternal ({ state, commit, dispatch }, payload) {
      if (payload.identityId === undefined) {
        console.log('No identityId was supplied in the payload to action \'fetchIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.get(`identities/${payload.identityId}`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Created) {
              console.log(`Unexpected response code in \'fetchIdentityInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var identity = body['value']
            resolve(identity)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchIdentityInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MIGHT include next params:
    // 1. offset – (Int) the amount of records that should be skipped
    // 2. limit – (Int) the amount of records to be loaded
    //
    fetchIdentitiesInternal ({ state, commit, dispatch }, payload) {
      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}
      if (payload.offset !== undefined) {
        queryParams.offset = payload.offset
      }

      if (payload.limit !== undefined) {
        queryParams.limit = payload.limit
      }

      return new Promise((resolve, reject) => {
        Vue.http.get(`identities`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Created) {
              console.log(`Unexpected response code in \'fetchIdentityInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var identities = body['value']

            var outIdentities = []
            for (identity of identities) {
              outIdentities.push(identity['value'])
            }

            resolve(outIdentities)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'fetchIdentitiesInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. identityId – (UUID) the ID of identity being updated
    //
    // Payload MIGHT include next params:
    // 1. firstName – (String) new first name of identity being updated
    // 2. lastName – (String) new last name of identity being updated
    // 3. phoneNumber – (String) new phone number of identity being updated
    // 4. email – (String) new email of identity being updated
    // 5. password – (String) new password of identity being updated
    //
    updateIdentityInternal ({ state, commit, dispatch }, payload) {
      if (payload.identityId === undefined) {
        console.log('No identityId was supplied in the payload to action \'updateIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      if (payload.firstName === undefined && payload.lastName === undefined
        && payload.phoneNumber === undefined && payload.email === undefined
        && payload.password === undefined) {

        console.log('No params were supplied in the payload to action \'updateIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Content-Type'] = 'application/json'
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      var bodyValue = {}
      bodyValue['id'] = payload.identityId

      if (payload.firstName !== undefined) {
        bodyValue['first_name'] = payload.firstName
      }

      if (payload.lastName !== undefined) {
        bodyValue['last_name'] = payload.lastName
      }

      if (payload.phoneNumber !== undefined) {
        bodyValue['phone_number'] = payload.phoneNumber
      }

      if (payload.email !== undefined) {
        bodyValue['email'] = payload.email
      }

      if (payload.password !== undefined) {
        bodyValue['password'] = payload.password
      }

      return new Promise((resolve, reject) => {
        Vue.http.patch(`identities/${payload.identityId}`, bodyValue, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.Ok) {
              console.log(`Unexpected response code in \'updateIdentityInternal\': Expected 200, Ok; Found: ${response.status}`)
            }

            var body = response.body
            var identity = body['value']
            resolve(identity)
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'updateIdentityInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    // NOTE: This action is for internal use only and the resultive promise
    // should never be used inside the components directly because
    // 'the state is only source of truth'
    //
    // This action is the way to get other actions look more clear,
    // e.g. dispatch this with needed set of params
    //
    // Payload MUST include next params:
    // 1. identityId – (UUID) the ID of identity being deleted
    //
    deleteIdentityInternal ({ state, commit, dispatch }, payload) {
      if (payload.identityId === undefined) {
        console.log('No identityId was supplied in the payload to action \'deleteIdentityInternal\'; returning...')
        return new Promise((resolve, reject) => { reject(new Error('Bad payload')) })
      }

      var headersValue = {}
      headersValue['Accept'] = 'application/json'

      var queryParams = {}

      return new Promise((resolve, reject) => {
        Vue.http.delete(`identities/${payload.identityId}`, { headers: headersValue, params: queryParams }).then(
          response => {
            if (response.status !== HttpStatuses.NoContent) {
              console.log(`Unexpected response code in \'deleteIdentityInternal\': Expected 204, NoContent; Found: ${response.status}`)
            }

            resolve({})
          },
          error => {
            if (error.status === HttpStatuses.Unauthorized) {
              // Session expired, invalidate it
              console.log('Session expired, invalidating')
              commit('invalidateSessionToken', null)
              dispatch('onSessionInvalidated', null)

              // Note: the retry logic must be handled by the caller
              reject(error)
            } else {
              // Some unexpected error occured – logging
              console.log('Error occured while processing \'deleteIdentityInternal\' action, logging...')
              console.error(error)
              reject(error)
            }
          }
        )
      })
    },

    uploadImage({state, commit, dispatch}, payload) {
      var formData = new FormData();

      // append Blob/File object
      formData.append('image', payload.fileInput, payload.fileInput.name);

      // POST /someUrl
      Vue.http.post(`identities/image`, formData, {
         headers: {
             'Content-Type': 'multipart/form-data'
         }
      }).then(response => {
          console.log(response)
        },
        error => {
          console.log(error)
        }
      )
    },
  },
  getters: {
    refreshToken: (state, getters) => {
      return state.refreshToken
    },
    sessionToken: (state, getters) => {
      return state.sessionToken
    },
    identity: (state, getters) => {
      return state.identity
    },

    loginState: (state, getters) => {
      return state.loginState
    },
    userFirstName: (state, getters) => {
      return state.userFirstName
    },
    userLastName: (state, getters) => {
      return state.userLastName
    },
    userEmail: (state, getters) => {
      return state.userEmail
    },
    userPhoneNumber: (state, getters) => {
      return state.userPhoneNumber
    },

    categoriesPage: (state, getters) => {
      return state.categoriesPage
    },
    categoriesPageTotal: (state, getters) => {
      return state.categoriesPageTotal
    },
    userTopicsTotal: (state, getters) => {
      return state.userTopiscTotal
    },
    categoriesPageDataSet: (state, getters) => {
      return state.categories
    },
    categoriesByIdentity: (state, getters) => (identity) => {
      return state.categories.filter(category => category.metadata.created_by_identity.id === identity)
    },
    categoryById: (state, getters) => (id) => {
      return state.categories.filter(category => category.metadata.id === id)
    },
    categorySelected: (state, getters) => {
      return state.selectedCategory
    },
    topicsPage: (state, getters) => {
      return state.topicsPage
    },
    topicsPageTotal: (state, getters) => {
      return state.topicsPageTotal
    },
    topicsPageDataSet: (state, getters) => {
      return state.topics
    },
    topicsByIdentity: (state, getters) => (identity) => {
      return state.topics.filter(topic => topic.metadata.created_by_identity.id === identity)
    },
    userTopics: (state, getters) => {
      return state.userTopics
    },
    topicsById: (state, getters) => (id) => {
      return state.topics.filter(topic => topic.metadata.id === id)
    },
    topicSelected: (state, getters) => {
      return state.selectedTopic
    },

    commentsPage: (state, getters) => {
      return state.commentsPage
    },
    commentsPageTotal: (state, getters) => {
      return state.commentsPageTotal
    },
    commentsPageDataSet: (state, getters) => {
      return state.comments
    },
    commentsByIdentity: (state, getters) => (identity) => {
      return state.comments.filter(comment => comment.metadata.created_by_identity.id === identity)
    },
    commentsById: (state, getters) => (id) => {
      return state.comments.filter(comment => comment.metadata.id === id)
    },
    commentSelected: (state, getters) => {
      return state.selectedComment
    },

    tags: (state, getters) => {
      return state.tags
    },
    tagNames: (state, getters) => {
      return state.tags.map(tag => tag.value)
    },
    tagsByName: (state, getters) => (name) => {
      return state.tags.filter(tag => tag.value === name)
    },
    tagsByNames: (state, getters) => (names) => {
      return state.tags.filter(tag => utils.indexOf(names, name => name === tag.name) !== -1)
    },
    popularTags: (state, getters) => {
      return state.popularTags
    },

    registerError: (state, getters) => {
      return state.registererror
    }
  },
  plugins: [createPersistedState()]
})

Vue.http.interceptors.push(function (request, next) {
  var bearer = store.getters.sessionToken
  if (bearer !== null) {
    request.headers.set('Authorization', bearer)
  } else {
    request.headers.delete('Authorization')
  }
  next(response => {
    if (response.code === 401) {
      store.commit('invalidateSessionToken', null)
    }
    return response
  })
})

export default store
