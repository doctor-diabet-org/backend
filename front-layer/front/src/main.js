// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import Buefy from 'buefy'
import App from './App'

import router from './router'
import store from './store'

import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

Vue.url.options.root = 'http://78.155.197.41:8080/api/'

Vue.use(Buefy)

Vue.use(Vuetify, {
  theme: {
    primary: '#3F51B5',
    secondary: '#303F9F',
    accent: '#CDDC39',
    error: '#CDDC39',
    info: '#757575',
    success: '#212121',
    warning: '#757575'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
  http: {
    headers: {
      Authorization: null
    }
  }
})
