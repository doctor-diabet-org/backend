package org.doctordiabet.domainlayer.usecase.roles;

import org.doctordiabet.domainlayer.entity.identity.RoleEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.List;

public interface RoleGrantCallback {

    void onSuccess(List<RoleEntity> entities);

    void onFailure(ExceptionBundle error);
}
