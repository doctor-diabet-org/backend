package org.doctordiabet.domainlayer.usecase.categories;

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.UUID;

public interface CategoryDeleteCallback {

    void onSuccess(UUID id);

    void onFailure(ExceptionBundle error);
}
