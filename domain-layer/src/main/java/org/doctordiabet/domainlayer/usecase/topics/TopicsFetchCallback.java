package org.doctordiabet.domainlayer.usecase.topics;

import org.doctordiabet.domainlayer.entity.topics.TopicEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.List;

public interface TopicsFetchCallback {

    void onSuccess(int totalCount, List<TopicEntity> entities);

    void onFailure(ExceptionBundle error);
}
