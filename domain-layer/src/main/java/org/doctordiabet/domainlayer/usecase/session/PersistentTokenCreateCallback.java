package org.doctordiabet.domainlayer.usecase.session;

import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface PersistentTokenCreateCallback {

    void onSuccess(PersistentTokenEntity entity);

    void onFailure(ExceptionBundle error);
}
