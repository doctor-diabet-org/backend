package org.doctordiabet.domainlayer.usecase.identities;

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface IdentityPwdValidateCallback {

    void onSuccess();

    void onFailure(ExceptionBundle error);
}
