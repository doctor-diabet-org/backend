package org.doctordiabet.domainlayer.usecase.roles;

import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface RelationshipGetCallback {

    void onSuccess(RelationshipEntity entity);

    void onFailure(ExceptionBundle error);
}
