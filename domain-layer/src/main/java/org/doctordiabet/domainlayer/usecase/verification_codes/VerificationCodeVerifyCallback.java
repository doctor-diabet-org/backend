package org.doctordiabet.domainlayer.usecase.verification_codes;

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface VerificationCodeVerifyCallback {

    void onSuccess(Boolean matches);

    void onFailure(ExceptionBundle error);
}
