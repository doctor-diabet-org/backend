package org.doctordiabet.domainlayer.usecase.comments;

import org.doctordiabet.domainlayer.entity.comments.CommentEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.List;

public interface CommentsFetchCallback {

    void onSuccess(int totalCount, List<CommentEntity> entities);

    void onFailure(ExceptionBundle error);
}
