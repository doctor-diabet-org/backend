package org.doctordiabet.domainlayer.usecase.identities;

import org.doctordiabet.domainlayer.entity.identity.IdentityEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface IdentityGetCallback {

    void onSuccess(IdentityEntity entity);

    void onFailure(ExceptionBundle error);
}
