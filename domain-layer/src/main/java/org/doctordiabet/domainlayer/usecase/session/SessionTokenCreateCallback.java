package org.doctordiabet.domainlayer.usecase.session;

import org.doctordiabet.domainlayer.entity.session.SessionTokenEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface SessionTokenCreateCallback {

    void onSuccess(SessionTokenEntity entity);

    void onFailure(ExceptionBundle error);
}
