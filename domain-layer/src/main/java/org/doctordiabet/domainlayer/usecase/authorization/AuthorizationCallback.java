package org.doctordiabet.domainlayer.usecase.authorization;

import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface AuthorizationCallback {

    void onSuccess(PersistentTokenEntity entity);

    void onFailure(ExceptionBundle error);
}
