package org.doctordiabet.domainlayer.usecase.comments;

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.UUID;

public interface CommentDeleteCallback {

    void onSuccess(UUID id);

    void onFailure(ExceptionBundle error);
}
