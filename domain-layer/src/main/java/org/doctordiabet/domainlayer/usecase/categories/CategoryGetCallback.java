package org.doctordiabet.domainlayer.usecase.categories;

import org.doctordiabet.domainlayer.entity.categories.CategoryEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface CategoryGetCallback {

    void onSuccess(CategoryEntity entity);

    void onFailure(ExceptionBundle error);
}
