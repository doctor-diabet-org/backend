package org.doctordiabet.domainlayer.usecase.comments;

import org.doctordiabet.domainlayer.entity.comments.CommentEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface CommentUpdateCallback {

    void onSuccess(CommentEntity entity);

    void onFailure(ExceptionBundle error);
}
