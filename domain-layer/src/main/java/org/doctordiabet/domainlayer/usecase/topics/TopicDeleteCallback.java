package org.doctordiabet.domainlayer.usecase.topics;

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.UUID;

public interface TopicDeleteCallback {

    void onSuccess(UUID id);

    void onFailure(ExceptionBundle error);
}
