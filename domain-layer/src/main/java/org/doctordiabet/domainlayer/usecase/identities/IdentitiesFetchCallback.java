package org.doctordiabet.domainlayer.usecase.identities;

import org.doctordiabet.domainlayer.entity.identity.IdentityEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.List;

public interface IdentitiesFetchCallback {

    void onSuccess(int totalCount, List<IdentityEntity> entities);

    void onFailure(ExceptionBundle error);
}
