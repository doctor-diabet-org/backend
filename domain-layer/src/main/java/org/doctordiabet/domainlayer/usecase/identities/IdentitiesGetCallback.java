package org.doctordiabet.domainlayer.usecase.identities;

import java.util.List;
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface IdentitiesGetCallback {

    void onSuccess(int totalCount, List<IdentityEntity> entities);

    void onFailure(ExceptionBundle error);
}
