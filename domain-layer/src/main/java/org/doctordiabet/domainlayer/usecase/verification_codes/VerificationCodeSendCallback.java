package org.doctordiabet.domainlayer.usecase.verification_codes;

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface VerificationCodeSendCallback {

    void onSuccess();

    void onFailure(ExceptionBundle error);
}
