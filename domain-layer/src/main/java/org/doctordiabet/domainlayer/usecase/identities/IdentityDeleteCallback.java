package org.doctordiabet.domainlayer.usecase.identities;

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.UUID;

public interface IdentityDeleteCallback {

    void onSuccess(UUID id);

    void onFailure(ExceptionBundle error);
}
