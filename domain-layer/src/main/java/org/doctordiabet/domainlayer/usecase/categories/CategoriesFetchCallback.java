package org.doctordiabet.domainlayer.usecase.categories;

import org.doctordiabet.domainlayer.entity.categories.CategoryEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

import java.util.List;

public interface CategoriesFetchCallback {

    void onSuccess(int totalCount, List<CategoryEntity> entities);

    void onFailure(ExceptionBundle error);
}
