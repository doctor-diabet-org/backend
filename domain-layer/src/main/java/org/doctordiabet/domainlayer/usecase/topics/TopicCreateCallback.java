package org.doctordiabet.domainlayer.usecase.topics;

import org.doctordiabet.domainlayer.entity.topics.TopicEntity;
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle;

public interface TopicCreateCallback {

    void onSuccess(TopicEntity entity);

    void onFailure(ExceptionBundle error);
}
