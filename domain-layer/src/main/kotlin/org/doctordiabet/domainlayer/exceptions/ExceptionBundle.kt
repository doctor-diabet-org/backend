package org.doctordiabet.domainlayer.exceptions

import java.io.Serializable

class ExceptionBundle(val source: ExceptionSource): Throwable() {

    private val data = HashMap<String, Any>()

    val accessor
        get() = when(source) {
            ExceptionSource.DATABASE -> DatabaseExceptionAccessor(this)
            ExceptionSource.NETWORK -> NetworkExceptionAccessor(this)
            ExceptionSource.BUSINESS -> BusinessExceptionAccessor(this)
            ExceptionSource.INTERNAL -> InternalExceptionAccessor(this)
            ExceptionSource.SMS -> SmsExceptionAccessor(this)
        }

    fun putString(key: String, value: String) = data.put(key, value)

    fun getString(key: String) = data.get(key) as? String ?: throw IllegalArgumentException("No data matching given key found, searched for: ${key}")

    fun putInt(key: String, value: Int) = data.put(key, value)

    fun getInt(key: String) = data.get(key) as? Int ?: throw IllegalArgumentException("No data matching given key found, searched for: ${key}")

    fun putDouble(key: String, value: Double) = data.put(key, value)

    fun getDouble(key: String) = data.get(key) as? Double ?: throw IllegalArgumentException("No data matching given key found, searched for: ${key}")

    fun putThrowable(key: String, value: Throwable) = data.put(key, value)

    fun getThrowable(key: String) = data.get(key) as? Throwable ?: throw IllegalArgumentException("No data matching given key found, searched for: ${key}")
}

enum class ExceptionSource: Serializable {
    DATABASE  ,
    NETWORK   ,
    BUSINESS  ,
    INTERNAL  ,
    SMS       ,
}

sealed class ExceptionAccessor<K> {

}

class DatabaseExceptionAccessor(private val bundle: ExceptionBundle): ExceptionAccessor<DatabaseExceptionAccessor.Key>() {

    enum class Key(val value: String) {
        CODE    ("code"),
        MESSAGE ("message"),
        CAUSE   ("cause");
    }

    companion object {

        const val CODE_REDIS = 0

        const val CODE_RESOURCE_DELETED    = 10
        const val CODE_RESOURCE_NOT_EXISTS = 11
    }

    var code: Int
        get() = bundle.getInt(Key.CODE.value)
        set(value) {
            bundle.putInt(Key.CODE.value, value)
        }

    var message: String
        get() = bundle.getString(Key.MESSAGE.value)
        set(value) {
            bundle.putString(Key.MESSAGE.value, value)
        }

    var cause: Throwable
        get() = bundle.getThrowable(Key.CAUSE.value)
        set(value) {
            bundle.putThrowable(Key.CAUSE.value, value)
        }
}

class NetworkExceptionAccessor(private val bundle: ExceptionBundle): ExceptionAccessor<NetworkExceptionAccessor.Key>() {

    enum class Key(val value: String) {
        CODE    ("code"),
        MESSAGE ("message");
    }

    var code: Int
        get() = bundle.getInt(Key.CODE.value)
        set(value) {
            bundle.putInt(Key.CODE.value, value)
        }

    var message: String
        get() = bundle.getString(Key.MESSAGE.value)
        set(value) {
            bundle.putString(Key.MESSAGE.value, value)
        }
}

class SmsExceptionAccessor(private val bundle: ExceptionBundle): ExceptionAccessor<SmsExceptionAccessor.Key>() {

    enum class Key(val value: String) {
        CODE    ("code"),
        BALANCE ("balance");
    }

    var code: Int
        get() = bundle.getInt(Key.CODE.value)
        set(value) {
            bundle.putInt(Key.CODE.value, value)
        }

    var balance: Double
        get() = bundle.getDouble(Key.BALANCE.value)
        set(value) {
            bundle.putDouble(Key.BALANCE.value, value)
        }
}

class BusinessExceptionAccessor(private val bundle: ExceptionBundle): ExceptionAccessor<BusinessExceptionAccessor.Key>() {

    companion object {

        //data inconsistency error codes
        const val CODE_ARTIFACT_DOES_NOT_EXIST              = 300
        const val CODE_ARTIFACT_DELETED                     = 301

        //data validation error codes
        const val CODE_CATEGORY_TITLE_BAD_LENGTH            = 400
        const val CODE_IDENTITY_BAD_EMAIL                   = 401
        const val CODE_IDENTITY_BAD_PHONE                   = 402
        const val CODE_IDENTITY_BAD_FIRST_NAME              = 403
        const val CODE_IDENTITY_BAD_LAST_NAME               = 404
        const val CODE_VERIFICATION_BAD_PHONE               = 405
        const val CODE_TOPIC_TITLE_BAD_LENGTH               = 406

        const val CODE_INCOMPATIBLE_PARAMS                  = 499

        //session validation error codes
        const val CODE_NO_PERSISTENT_TOKEN                  = 500
        const val CODE_NO_SESSION_TOKEN                     = 501
        const val CODE_NO_SESSION_IDENTITY                  = 502
        const val CODE_PWD_CHECK_FAILED                     = 503
        const val CODE_CODE_CHECK_FAILED                    = 504
        const val CODE_CODE_EXPIRED                         = 505

        //role validation error codes
        const val CODE_ACTION_FORBIDDEN                     = 600
        const val CODE_WRONG_PHONE_FORBIDDEN                = 601
    }

    enum class Key(val value: String) {
        CODE    ("code"),
        MESSAGE ("message");
    }

    var code: Int
        get() = bundle.getInt(Key.CODE.value)
        set(value) {
            bundle.putInt(Key.CODE.value, value)
        }

    var message: String
        get() = bundle.getString(Key.MESSAGE.value)
        set(value) {
            bundle.putString(Key.MESSAGE.value, value)
        }
}

class InternalExceptionAccessor(private val bundle: ExceptionBundle): ExceptionAccessor<InternalExceptionAccessor.Key>() {
    enum class Key(val value: String) {
        MESSAGE ("message"),
        CAUSE   ("cause");
    }

    var message: String
        get() = bundle.getString(Key.MESSAGE.value)
        set(value) {
            bundle.putString(Key.MESSAGE.value, value)
        }

    var cause: Throwable
        get() = bundle.getThrowable(Key.CAUSE.value)
        set(value) {
            bundle.putThrowable(Key.CAUSE.value, value)
        }
}
