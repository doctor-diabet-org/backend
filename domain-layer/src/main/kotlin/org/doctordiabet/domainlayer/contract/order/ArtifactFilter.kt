package org.doctordiabet.domainlayer.contract.order

sealed class ArtifactFilter {

    sealed class Votes(open val type: Type): ArtifactFilter() {

        data class Lt(val value: Long, override val type: Type): Votes(type)

        data class Gt(val value: Long, override val type: Type): Votes(type)

        data class Range(val lower: Long, val upper: Long, override val type: Type): Votes(type)

        data class Simple(override val type: Type): Votes(type)
    }

    sealed class Type {
        object ASC: Type()
        object DESC: Type()
    }
}