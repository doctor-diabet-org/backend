package org.doctordiabet.domainlayer.contract.uri

import java.net.URI
import java.util.*

class UriBuilder {

    enum class Schema(val value: String) {
        HTTP ("http"),
        HTTPS("https");
    }

    enum class Archetype(val value: String) {
        ARTIFACT("artifact")
    }

    enum class Type(val value: String) {
        CATEGORY("category"),
        TOPIC   ("topic"),
        COMMENT ("comment"),
        IDENTITY("identity");
    }

    internal lateinit var schema: Schema
    internal lateinit var archetype: Archetype
    internal lateinit var type: Type
    internal lateinit var id: UUID

    fun schema(function1: UriBuilder.() -> Schema) = apply { schema = function1() }

    fun archetype(function1: UriBuilder.() -> Archetype) = apply { archetype = function1() }

    fun type(function1: UriBuilder.() -> Type) = apply { type = function1() }

    fun id(function1: UriBuilder.() -> UUID) = apply { id = function1() }

    fun build() = URI.create("${schema.value}://${archetype.value}:${type.value}/${id}")
}