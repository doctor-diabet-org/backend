package org.doctordiabet.domainlayer.usecase.rbac

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.repository.roles.RolesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import org.doctordiabet.domainlayer.usecase.roles.RelationshipGetCallback
import java.util.*

class RelationshipGetUseCase(private val repository: RolesRepository): AbstractUseCase() {

    fun execute(identityId: UUID, artifactId: UUID, callback: RelationshipGetCallback): Unit = repository
            .relationshipOf(identityId, artifactId)
            .subscribe(object: DisposableSingleObserver<RelationshipEntity>() {
                override fun onSuccess(t: RelationshipEntity): Unit = t.let(callback::onSuccess)
                override fun onError(t: Throwable): Unit = t.parse().let(callback::onFailure)
            })

    fun executeBlocking(identityId: UUID, artifactId: UUID): RelationshipEntity = repository
            .relationshipOf(identityId, artifactId)
            .blockingGet()
}