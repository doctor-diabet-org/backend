package org.doctordiabet.domainlayer.usecase.identities

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.sql.Timestamp

class IdentitiesFetchUseCase(private val repository: IdentitiesRepository): AbstractUseCase() {

    fun execute(offset: Int, limit: Int, nameSubstring: String, lastModifiedTime: Timestamp, full: Boolean, callback: IdentitiesFetchCallback) = repository.read(offset, limit, nameSubstring, lastModifiedTime, true, full)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<IdentityEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<IdentityEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(offset: Int, limit: Int, lastModifiedTime: Timestamp, full: Boolean, callback: IdentitiesFetchCallback) = repository.read(offset, limit, lastModifiedTime, full)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<IdentityEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<IdentityEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}