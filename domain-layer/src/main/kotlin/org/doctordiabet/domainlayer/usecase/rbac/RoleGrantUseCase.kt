package org.doctordiabet.domainlayer.usecase.rbac

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.repository.roles.RolesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import org.doctordiabet.domainlayer.usecase.roles.RoleGrantCallback
import java.util.*

class RoleGrantUseCase(
        private val repository: RolesRepository
): AbstractUseCase() {

    fun execute(identityId: UUID, role: RoleEntity, callback: RoleGrantCallback): Unit = repository.grant(identityId, role)
            .subscribe(object: DisposableSingleObserver<List<RoleEntity>>() {
                override fun onSuccess(t: List<RoleEntity>) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}