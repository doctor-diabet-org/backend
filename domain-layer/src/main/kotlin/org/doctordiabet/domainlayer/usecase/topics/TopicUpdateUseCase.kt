package org.doctordiabet.domainlayer.usecase.topics

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.entity.topics.TopicUpdateModel
import org.doctordiabet.domainlayer.repository.topics.TopicsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class TopicUpdateUseCase(
        private val repository: TopicsRepository
): AbstractUseCase() {

    fun execute(model: TopicUpdateModel, callback: TopicUpdateCallback): Unit = repository.update(model)
            .subscribe(object: DisposableSingleObserver<TopicEntity>() {
                override fun onSuccess(t: TopicEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}