package org.doctordiabet.domainlayer.usecase.categories

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.contract.order.ArtifactFilter
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.repository.categories.CategoriesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.sql.Timestamp
import java.util.*

class CategoriesFetchUseCase(private val repository: CategoriesRepository): AbstractUseCase() {

    fun execute(offset: Int, limit: Int, lastModifiedTime: Timestamp, tagIds: List<UUID> = emptyList(), criteria: List<ArtifactFilter>, callback: CategoriesFetchCallback) = repository.read(offset, limit, lastModifiedTime, true, tagIds)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<CategoryEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<CategoryEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}