package org.doctordiabet.domainlayer.usecase.artifacts

import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class TagsAttachUseCase(private val repository: ArtifactsRepository): AbstractUseCase() {

    fun executeBlocking(artifactId: UUID, tagIds: List<UUID>): Unit =
            repository.attachTags(artifactId, tagIds)
                    .blockingGet()
}