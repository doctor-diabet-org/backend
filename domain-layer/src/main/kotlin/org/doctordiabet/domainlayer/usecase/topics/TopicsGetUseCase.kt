package org.doctordiabet.domainlayer.usecase.topics

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.repository.topics.TopicsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.sql.Timestamp
import java.util.*

class TopicsGetUseCase(
        private val repository: TopicsRepository
): AbstractUseCase() {

    fun execute(offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList(), callback: TopicsGetCallback): Unit = repository.read(offset, limit, unmodifiedSince, false, tagIds)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<TopicEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<TopicEntity>>)  {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(categoryId: UUID, offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList(), callback: TopicsGetCallback): Unit = repository.read(categoryId, offset, limit, unmodifiedSince, false, tagIds)
                .subscribe(object: DisposableSingleObserver<Pair<Int, List<TopicEntity>>>() {
                    override fun onSuccess(t: Pair<Int, List<TopicEntity>>) {
                        callback.onSuccess(t.first, t.second)
                    }
                    override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
                })
}