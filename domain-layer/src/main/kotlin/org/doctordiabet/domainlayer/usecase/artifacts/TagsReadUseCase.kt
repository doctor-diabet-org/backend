package org.doctordiabet.domainlayer.usecase.artifacts

import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class TagsReadUseCase(private val repository: ArtifactsRepository): AbstractUseCase() {

    fun executeBlocking(): List<TagEntity> =
            repository.readTags()
                    .blockingGet()
}