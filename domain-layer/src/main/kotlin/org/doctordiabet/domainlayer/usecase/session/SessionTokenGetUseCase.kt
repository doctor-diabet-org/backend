package org.doctordiabet.domainlayer.usecase.session

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.session.SessionTokenEntity
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.session.SessionsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class SessionTokenGetUseCase(private val repository: SessionsRepository): AbstractUseCase() {

    fun execute(sessionToken: String, callback: SessionTokenGetCallback): Unit = repository.readSession(sessionToken)
            .onErrorResumeNext(hookTokenNotFoundException())
            .flatMap { repository.readSession(it) }
            .map { it.first { it.sessionToken == sessionToken } }
            .subscribe(object: DisposableSingleObserver<SessionTokenEntity>() {
                override fun onSuccess(t: SessionTokenEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun executeBlocking(sessionToken: String): SessionTokenEntity = repository.readSession(sessionToken)
            .flatMap { repository.readSession(it) }
            .map { it.first { it.sessionToken == sessionToken } }
            .blockingGet()

    private fun hookTokenNotFoundException(): (Throwable) -> Single<UUID> = { t ->
        when {
            t is ExceptionBundle && t.source == ExceptionSource.BUSINESS -> {
                val accessor = t.accessor as BusinessExceptionAccessor
                when {
                    accessor.code == BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST -> Single.error<UUID>(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                        (this.accessor as BusinessExceptionAccessor).apply {
                            code = BusinessExceptionAccessor.CODE_NO_SESSION_TOKEN
                            message = "Not authorized"
                        }
                    })
                    accessor.code == BusinessExceptionAccessor.CODE_ARTIFACT_DELETED -> Single.error<UUID>(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                        (this.accessor as BusinessExceptionAccessor).apply {
                            code = BusinessExceptionAccessor.CODE_NO_SESSION_TOKEN
                            message = "Not authorized"
                        }
                    })
                    else -> Single.error<UUID>(t)
                }
            }
            else -> Single.error<UUID>(t)
        }
    }
}