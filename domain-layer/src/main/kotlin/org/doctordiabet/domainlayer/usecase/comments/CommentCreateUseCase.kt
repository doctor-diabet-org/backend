package org.doctordiabet.domainlayer.usecase.comments

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.comments.CommentCreateModel
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class CommentCreateUseCase(private val repository: CommentsRepository): AbstractUseCase() {

    fun execute(model: CommentCreateModel, callback: CommentCreateCallback): Unit = repository.create(model)
            .subscribe(object: DisposableSingleObserver<CommentEntity>() {
                override fun onSuccess(t: CommentEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}