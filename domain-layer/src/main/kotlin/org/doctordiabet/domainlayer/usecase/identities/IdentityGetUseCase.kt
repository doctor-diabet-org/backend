package org.doctordiabet.domainlayer.usecase.identities

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class IdentityGetUseCase(private val repository: IdentitiesRepository): AbstractUseCase() {

    enum class Type {
        EMAIL,
        PHONE
    }

    fun execute(id: UUID, full: Boolean = false, callback: IdentityGetCallback) = repository.read(id, full)
            .subscribe(object: DisposableSingleObserver<IdentityEntity>() {
                override fun onSuccess(t: IdentityEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(type: Type, value: String, full: Boolean = false, callback: IdentityGetCallback) = when(type) {
        Type.EMAIL -> repository.readByEmail(value, full)
        Type.PHONE -> repository.readByPhone(value, full)
    }
            .subscribe(object: DisposableSingleObserver<IdentityEntity>() {
                override fun onSuccess(t: IdentityEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}