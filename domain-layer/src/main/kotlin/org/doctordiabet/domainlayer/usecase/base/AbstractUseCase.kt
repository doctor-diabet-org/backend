package org.doctordiabet.domainlayer.usecase.base

import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.exceptions.InternalExceptionAccessor

abstract class AbstractUseCase {

    protected fun Throwable.parse(): ExceptionBundle = when(this) {
        is ExceptionBundle -> this
        else -> ExceptionBundle(ExceptionSource.INTERNAL).apply {
            val accessor = this.accessor as InternalExceptionAccessor
            accessor.message = message ?: localizedMessage ?: "N/A"
            accessor.cause = this
        }
    }
}