package org.doctordiabet.domainlayer.usecase.session

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.session.PersistentTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity
import org.doctordiabet.domainlayer.repository.session.SessionsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class PersistentTokenCreateUseCase(
        private val repository: SessionsRepository
): AbstractUseCase() {

    fun execute(model: PersistentTokenCreateModel, callback: PersistentTokenCreateCallback): Unit =
            repository.createPersistent(model)
            .subscribe(object: DisposableSingleObserver<PersistentTokenEntity>() {
                override fun onSuccess(t: PersistentTokenEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}