package org.doctordiabet.domainlayer.usecase.session

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.session.SessionsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class PersistentTokenGetUseCase(
        private val repository: SessionsRepository
): AbstractUseCase() {

    fun execute(identityId: UUID, callback: PersistentTokenGetCallback): Unit = repository.readPersistent(identityId)
            .onErrorResumeNext(hookTokenNotFoundException())
            .subscribe(object: DisposableSingleObserver<PersistentTokenEntity>() {
                override fun onSuccess(t: PersistentTokenEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)

            })

    private fun hookTokenNotFoundException(): (Throwable) -> Single<PersistentTokenEntity> = { t ->
        when {
            t is ExceptionBundle && t.source == ExceptionSource.BUSINESS -> {
                val accessor = t.accessor as BusinessExceptionAccessor
                when {
                    accessor.code == BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST -> Single.error<PersistentTokenEntity>(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                        (this.accessor as BusinessExceptionAccessor).apply {
                            code = BusinessExceptionAccessor.CODE_NO_PERSISTENT_TOKEN
                            message = "Not registered"
                        }
                    })
                    accessor.code == BusinessExceptionAccessor.CODE_ARTIFACT_DELETED -> Single.error<PersistentTokenEntity>(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                        (this.accessor as BusinessExceptionAccessor).apply {
                            code = BusinessExceptionAccessor.CODE_NO_PERSISTENT_TOKEN
                            message = "Not registered"
                        }
                    })
                    else -> Single.error<PersistentTokenEntity>(t)
                }
            }
            else -> Single.error<PersistentTokenEntity>(t)
        }
    }
}