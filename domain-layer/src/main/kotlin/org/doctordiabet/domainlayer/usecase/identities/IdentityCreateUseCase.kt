package org.doctordiabet.domainlayer.usecase.identities

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.identity.IdentityCreateModel
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.regex.Pattern

class IdentityCreateUseCase(private val repository: IdentitiesRepository): AbstractUseCase() {

    private val emailPattern = Pattern.compile("^[-a-z0-9!#\$%&'*+/=?^_`{|}~]+(?:\\.[-a-z0-9!#\$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])\$")

    private val phoneNumberPattern = Pattern.compile("^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}\$")

    private val namePattern = Pattern.compile("^[а-яА-ЯёЁa-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð']{2,61}\$")

    fun execute(model: IdentityCreateModel, callback: IdentityCreateCallback) = Single.create<IdentityCreateModel> { emitter ->
        try {
            validateEmail(model)
                    .also { validatePhoneNumber(model) }
                    .also { validateName(model) }
                    .let { emitter.onSuccess(model) }
        } catch(e: ExceptionBundle) {
            emitter.onError(e)
        }
    }
            .flatMap { repository.create(it, true) }
            .subscribe(object: DisposableSingleObserver<IdentityEntity>() {
                override fun onSuccess(t: IdentityEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    private fun validateEmail(model: IdentityCreateModel) {
        if(!emailPattern.matcher(model.email).matches()) {
            throw ExceptionBundle(ExceptionSource.BUSINESS).apply {
                (accessor as BusinessExceptionAccessor).apply {
                    code = BusinessExceptionAccessor.CODE_IDENTITY_BAD_EMAIL
                    message = "Email validation failed. Bad email supplied"
                }
            }
        }
    }

    private fun validatePhoneNumber(model: IdentityCreateModel) {
        model.phoneNumber?.let { phoneNumber ->
            if(!phoneNumberPattern.matcher(phoneNumber).matches()) {
                throw ExceptionBundle(ExceptionSource.BUSINESS).apply {
                    (accessor as BusinessExceptionAccessor).apply {
                        code = BusinessExceptionAccessor.CODE_IDENTITY_BAD_PHONE
                        message = "Phone validation failed. Bad phone number supplied"
                    }
                }
            }
        }
    }

    private fun validateName(model: IdentityCreateModel) {
        if(!namePattern.matcher(model.firstName).matches()) {
            throw ExceptionBundle(ExceptionSource.BUSINESS).apply {
                (accessor as BusinessExceptionAccessor).apply {
                    code = BusinessExceptionAccessor.CODE_IDENTITY_BAD_FIRST_NAME
                    message = "First name validation failed. Bad first name supplied"
                }
            }
        }

        if(!namePattern.matcher(model.lastName).matches()) {
            throw ExceptionBundle(ExceptionSource.BUSINESS).apply {
                (accessor as BusinessExceptionAccessor).apply {
                    code = BusinessExceptionAccessor.CODE_IDENTITY_BAD_LAST_NAME
                    message = "Last name validation failed. Bad last name supplied"
                }
            }
        }
    }
}