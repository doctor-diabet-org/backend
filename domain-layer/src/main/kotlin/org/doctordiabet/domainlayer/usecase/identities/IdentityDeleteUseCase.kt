package org.doctordiabet.domainlayer.usecase.identities

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.identity.IdentityDeleteModel
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class IdentityDeleteUseCase(private val repository: IdentitiesRepository): AbstractUseCase() {

    fun execute(model: IdentityDeleteModel, callback: IdentityDeleteCallback): Unit = repository.delete(model)
            .subscribe(object: DisposableSingleObserver<UUID>() {
                override fun onSuccess(t: UUID) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}