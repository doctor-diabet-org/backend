package org.doctordiabet.domainlayer.usecase.identities

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.sql.Timestamp

class IdentitiesGetUseCase(private val repository: IdentitiesRepository): AbstractUseCase() {

    fun execute(offset: Int, limit: Int, nameSubstring: String, unmodified: Timestamp, full: Boolean, callback: IdentitiesGetCallback) = repository.read(offset, limit, nameSubstring, unmodified, false, full)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<IdentityEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<IdentityEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(offset: Int, limit: Int, nameSubstring: String, full: Boolean, callback: IdentitiesGetCallback) = repository.read(offset, limit, nameSubstring, full)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<IdentityEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<IdentityEntity>>)  {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(offset: Int, limit: Int, unmodified: Timestamp, full: Boolean, callback: IdentitiesGetCallback) = repository.read(offset, limit, unmodified, false, full)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<IdentityEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<IdentityEntity>>)  {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(offset: Int, limit: Int, full: Boolean, callback: IdentitiesGetCallback) = repository.read(offset, limit, full)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<IdentityEntity>>>() {
                override fun onSuccess(t: Pair<Int,  List<IdentityEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}