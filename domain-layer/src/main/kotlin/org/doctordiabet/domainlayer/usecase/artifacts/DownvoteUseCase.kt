package org.doctordiabet.domainlayer.usecase.artifacts

import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class DownvoteUseCase(private val repository: ArtifactsRepository): AbstractUseCase() {

    fun executeBlocking(identityId: UUID, artifactId: UUID): Boolean =
            repository.downvote(identityId, artifactId)
                    .blockingGet()
}