package org.doctordiabet.domainlayer.usecase.identities

import org.doctordiabet.domainlayer.entity.identity.IdentityPrivateEntity
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class IdentityPwdValidateUseCase(private val repository: IdentitiesRepository): AbstractUseCase() {

    fun execute(pwd: String, identity: IdentityPrivateEntity, callback: IdentityPwdValidateCallback): Unit = when(repository.calculatePwdHash(pwd)) {
        identity.pwdHash -> callback.onSuccess()
        else -> callback.onFailure(ExceptionBundle(ExceptionSource.BUSINESS).apply {
            (accessor as BusinessExceptionAccessor).apply {
                code = BusinessExceptionAccessor.CODE_PWD_CHECK_FAILED
                message = "Password check failed, hashes do not match"
            }
        })
    }
}