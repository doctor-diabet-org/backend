package org.doctordiabet.domainlayer.usecase.session

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.session.SessionTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.SessionTokenEntity
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.session.SessionsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class SessionTokenCreateUseCase(
        private val repository: SessionsRepository
): AbstractUseCase() {

    fun execute(model: SessionTokenCreateModel, callback: SessionTokenCreateCallback): Unit =
            repository.readPersistent(model.identityId)
                    .flatMap { actual ->
                        if(actual.refreshToken == model.refreshToken) {
                            Single.just(actual)
                        } else {
                            Single.error(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                                (accessor as BusinessExceptionAccessor).apply {
                                    code = BusinessExceptionAccessor.CODE_NO_PERSISTENT_TOKEN
                                    message = "Not registered"
                                }
                            })
                        }
                    }
                    .flatMap {
                        repository.createSession(model)
                    }
                    .subscribe(object: DisposableSingleObserver<SessionTokenEntity>() {
                        override fun onSuccess(t: SessionTokenEntity) = t.let(callback::onSuccess)
                        override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
                    })
}