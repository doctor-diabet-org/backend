package org.doctordiabet.domainlayer.usecase.topics

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.topics.TopicDeleteModel
import org.doctordiabet.domainlayer.repository.topics.TopicsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class TopicDeleteUseCase(
        private val repository: TopicsRepository
): AbstractUseCase() {

    fun execute(model: TopicDeleteModel, callback: TopicDeleteCallback): Unit = repository.delete(model)
            .subscribe(object: DisposableSingleObserver<UUID>() {
                override fun onSuccess(t: UUID) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}