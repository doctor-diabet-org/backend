package org.doctordiabet.domainlayer.usecase.rbac

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import org.doctordiabet.domainlayer.repository.roles.RolesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import org.doctordiabet.domainlayer.usecase.roles.RolesGetCallback
import java.util.*

class RolesGetUseCase(
        private val repository: RolesRepository
): AbstractUseCase() {

    fun execute(identityId: UUID, callback: RolesGetCallback): Unit = repository.read(identityId)
            .subscribe(object: DisposableSingleObserver<List<RoleEntity>>() {
                override fun onSuccess(t: List<RoleEntity>) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun executeBlocking(identityId: UUID): List<RoleEntity> = repository.read(identityId)
                    .blockingGet()
}