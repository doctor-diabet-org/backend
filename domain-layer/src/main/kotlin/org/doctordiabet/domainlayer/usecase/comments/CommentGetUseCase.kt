package org.doctordiabet.domainlayer.usecase.comments

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class CommentGetUseCase(private val repository: CommentsRepository): AbstractUseCase() {

    fun execute(id: UUID, callback: CommentGetCallback): Unit = repository.read(id)
            .subscribe(object: DisposableSingleObserver<CommentEntity>() {
                override fun onSuccess(t: CommentEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}