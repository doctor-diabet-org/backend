package org.doctordiabet.domainlayer.usecase.comments

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.sql.Timestamp
import java.util.*

class CommentsFetchUseCase(private val repository: CommentsRepository): AbstractUseCase() {

    fun execute(offset: Int, limit: Int, modifiedSince: Timestamp, tagIds: List<UUID> = emptyList(), callback: CommentsFetchCallback): Unit = repository.read(offset, limit, modifiedSince, true, tagIds)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<CommentEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<CommentEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(artifactReferralId: UUID, offset: Int, limit: Int, modifiedSince: Timestamp, tagIds: List<UUID> = emptyList(), callback: CommentsFetchCallback): Unit = repository.read(artifactReferralId, offset, limit, modifiedSince, true, tagIds)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<CommentEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<CommentEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}