package org.doctordiabet.domainlayer.usecase.topics

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.topics.TopicCreateModel
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.topics.TopicsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class TopicCreateUseCase(
        private val repository: TopicsRepository
): AbstractUseCase() {

    companion object {

        private const val TOPIC_TITLE_LENGTH_MIN = 12
    }

    fun execute(model: TopicCreateModel, callback: TopicCreateCallback): Unit = Single.create<TopicCreateModel> { emitter ->
        try {
            preValidate(model).let { emitter.onSuccess(model) }
        } catch (e: ExceptionBundle) {
            emitter.onError(e)
        }
    }
            .flatMap { topic -> repository.create(topic) }
            .subscribe(object: DisposableSingleObserver<TopicEntity>() {
                override fun onSuccess(t: TopicEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    private fun preValidate(model: TopicCreateModel) {
        if(model.title.length < TOPIC_TITLE_LENGTH_MIN) {
            throw ExceptionBundle(ExceptionSource.BUSINESS).apply {
                (accessor as BusinessExceptionAccessor).apply {
                    code = BusinessExceptionAccessor.CODE_TOPIC_TITLE_BAD_LENGTH
                    message = "Topic title length constraint failed. Minimum length required: ${TOPIC_TITLE_LENGTH_MIN}"
                }
            }
        }
    }
}