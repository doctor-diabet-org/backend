package org.doctordiabet.domainlayer.usecase.verification_codes

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeVerifyModel
import org.doctordiabet.domainlayer.repository.verification_codes.VerificationCodesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class VerificationCodeVerifyUseCase(
        private val repository: VerificationCodesRepository
): AbstractUseCase() {

    fun execute(model: VerificationCodeVerifyModel, callback: VerificationCodeVerifyCallback): Unit = repository.verify(model)
            .subscribe(object: DisposableSingleObserver<Boolean>() {
                override fun onSuccess(t: Boolean) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}