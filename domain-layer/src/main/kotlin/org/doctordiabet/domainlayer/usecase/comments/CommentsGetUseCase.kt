package org.doctordiabet.domainlayer.usecase.comments

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.sql.Timestamp
import java.util.*

class CommentsGetUseCase(private val repository: CommentsRepository): AbstractUseCase() {

    fun execute(offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList(), callback: CommentsGetCallback): Unit = repository.read(offset, limit, unmodifiedSince, false, tagIds)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<CommentEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<CommentEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    fun execute(artifactReferralId: UUID, offset: Int, limit: Int, unmodifiedSince: Timestamp, tagIds: List<UUID> = emptyList(), callback: CommentsGetCallback): Unit = repository.read(artifactReferralId, offset, limit, unmodifiedSince, false, tagIds)
            .subscribe(object: DisposableSingleObserver<Pair<Int, List<CommentEntity>>>() {
                override fun onSuccess(t: Pair<Int, List<CommentEntity>>) {
                    callback.onSuccess(t.first, t.second)
                }
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}