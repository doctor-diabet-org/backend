package org.doctordiabet.domainlayer.usecase.artifacts

import io.reactivex.Single
import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class SubscribersGetUseCase(private val repository: ArtifactsRepository): AbstractUseCase() {

    fun executeBlocking(artifactId: UUID): List<UUID>
        = repository.subscribers(artifactId)
            .onErrorResumeNext { t -> Single.error(t.parse()) }
            .blockingGet()
}