package org.doctordiabet.domainlayer.usecase.categories

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.categories.CategoryDeleteModel
import org.doctordiabet.domainlayer.repository.categories.CategoriesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class CategoryDeleteUseCase(private val repository: CategoriesRepository): AbstractUseCase() {

    fun execute(model: CategoryDeleteModel, callback: CategoryDeleteCallback) = repository.delete(model)
            .subscribe(object: DisposableSingleObserver<UUID>() {
                override fun onSuccess(t: UUID) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}