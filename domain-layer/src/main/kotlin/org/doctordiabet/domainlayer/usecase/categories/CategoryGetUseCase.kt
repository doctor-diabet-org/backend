package org.doctordiabet.domainlayer.usecase.categories

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.repository.categories.CategoriesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class CategoryGetUseCase(private val repository: CategoriesRepository): AbstractUseCase() {

    fun execute(id: UUID, callback: CategoryGetCallback) = repository.read(id)
            .subscribe(object: DisposableSingleObserver<CategoryEntity>() {
                override fun onSuccess(t: CategoryEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}