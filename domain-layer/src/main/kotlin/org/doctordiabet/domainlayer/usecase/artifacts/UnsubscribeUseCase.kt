package org.doctordiabet.domainlayer.usecase.artifacts

import io.reactivex.Single
import org.doctordiabet.domainlayer.repository.artifacts.ArtifactsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class UnsubscribeUseCase(private val repository: ArtifactsRepository): AbstractUseCase() {

    fun executeBlocking(identityId: UUID, artifactId: UUID): Boolean
            = repository.unsubscribe(identityId, artifactId)
            .onErrorResumeNext { t -> Single.error(t.parse()) }
            .blockingGet()
}