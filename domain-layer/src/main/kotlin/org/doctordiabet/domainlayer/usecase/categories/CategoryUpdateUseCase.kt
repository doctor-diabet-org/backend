package org.doctordiabet.domainlayer.usecase.categories

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.entity.categories.CategoryUpdateModel
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.categories.CategoriesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class CategoryUpdateUseCase(private val repository: CategoriesRepository): AbstractUseCase() {

    companion object {

        private const val CATEGORY_TITLE_LENGTH_MIN = 12
    }

    fun execute(model: CategoryUpdateModel, callback: CategoryUpdateCallback) = Single.create<CategoryUpdateModel> { emitter ->
        try {
            preValidate(model)
            emitter.onSuccess(model)
        } catch (e: ExceptionBundle) {
            emitter.onError(e)
        }
    }
            .flatMap { repository.update(model) }
            .subscribe(object: DisposableSingleObserver<CategoryEntity>() {
                override fun onSuccess(t: CategoryEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    private fun preValidate(model: CategoryUpdateModel) {
        if(model.title != null && model.title.length < CATEGORY_TITLE_LENGTH_MIN) {
            throw ExceptionBundle(ExceptionSource.BUSINESS).apply {
                (accessor as BusinessExceptionAccessor).apply {
                    code = BusinessExceptionAccessor.CODE_CATEGORY_TITLE_BAD_LENGTH
                    message = "Category title length constraint failed. Minimum length required: ${CATEGORY_TITLE_LENGTH_MIN}"
                }
            }
        }
    }
}