package org.doctordiabet.domainlayer.usecase.authorization

import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.authentication.AuthenticateCodeModel
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.entity.session.PersistentTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeVerifyModel
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.repository.session.SessionsRepository
import org.doctordiabet.domainlayer.repository.verification_codes.VerificationCodesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class AuthorizeCodeUseCase(private val identities: IdentitiesRepository,
                           private val codes: VerificationCodesRepository,
                           private val sessions: SessionsRepository): AbstractUseCase() {

    fun execute(model: AuthenticateCodeModel, callback: AuthorizationCallback): Unit = identities.readByPhone(model.phoneNumber, true)
            .flatMap(model.validate())
            .flatMap(processVerificationResult())
            .subscribe(object: DisposableSingleObserver<PersistentTokenEntity>() {
                override fun onSuccess(t: PersistentTokenEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    private fun AuthenticateCodeModel.validate(): (IdentityEntity) -> SingleSource<Pair<IdentityEntity, Boolean>> = { identity ->
        codes.verify(VerificationCodeVerifyModel(identity.metadata.id, code))
                .map { validated -> Pair(identity, validated) }
                .onErrorResumeNext(hookCodeNotFoundException())
    }

    private fun processVerificationResult(): (Pair<IdentityEntity, Boolean>) -> SingleSource<PersistentTokenEntity> = { (identity, validated) ->
        when(validated) {
            true -> sessions.createPersistent(PersistentTokenCreateModel(identity.metadata.id))
            else -> Single.error(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                (accessor as BusinessExceptionAccessor).apply {
                    code = BusinessExceptionAccessor.CODE_CODE_CHECK_FAILED
                    message = "Code verification failed, codes do not match"
                }
            })
        }
    }

    private fun <T> hookCodeNotFoundException(): (Throwable) -> SingleSource<T> = { throwable ->
        when {
            throwable is ExceptionBundle && throwable.source == ExceptionSource.BUSINESS -> {
                val accessor = throwable.accessor as BusinessExceptionAccessor
                when {
                    accessor.code == BusinessExceptionAccessor.CODE_ARTIFACT_DELETED -> Single.error(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                        (this.accessor as BusinessExceptionAccessor).apply {
                            code = BusinessExceptionAccessor.CODE_CODE_EXPIRED
                            message = "Verification code expired or never existed, please, try again"
                        }
                    })
                    accessor.code == BusinessExceptionAccessor.CODE_ARTIFACT_DOES_NOT_EXIST -> Single.error(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                        (this.accessor as BusinessExceptionAccessor).apply {
                            code = BusinessExceptionAccessor.CODE_CODE_EXPIRED
                            message = "Verification code expired or never existed, please, try again"
                        }
                    })
                    else -> Single.error(throwable)
                }
            }
            else -> Single.error(throwable)
        }
    }
}