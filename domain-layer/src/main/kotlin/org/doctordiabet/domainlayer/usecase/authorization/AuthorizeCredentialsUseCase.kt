package org.doctordiabet.domainlayer.usecase.authorization

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.authentication.AuthenticateCredentialsModel
import org.doctordiabet.domainlayer.entity.identity.IdentityPrivateEntity
import org.doctordiabet.domainlayer.entity.session.PersistentTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.identities.IdentitiesRepository
import org.doctordiabet.domainlayer.repository.session.SessionsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class AuthorizeCredentialsUseCase(private val identities: IdentitiesRepository,
                                  private val sessions: SessionsRepository): AbstractUseCase() {

    fun execute(model: AuthenticateCredentialsModel, callback: AuthorizationCallback): Unit = identities.readByEmail(model.email, true)
            .map { it as IdentityPrivateEntity }
            .flatMap { identity: IdentityPrivateEntity ->
                when(identities.calculatePwdHash(model.password).equals(identity.pwdHash, ignoreCase = true)) {
                    true -> sessions.createPersistent(PersistentTokenCreateModel(identity.metadata.id))
                    else -> Single.error(ExceptionBundle(ExceptionSource.BUSINESS).apply {
                        (accessor as BusinessExceptionAccessor).apply {
                            code = BusinessExceptionAccessor.CODE_PWD_CHECK_FAILED
                            message = "Password check failed, hashes do not match"
                        }
                    })
                }
            }
            .subscribe(object: DisposableSingleObserver<PersistentTokenEntity>() {
                override fun onSuccess(t: PersistentTokenEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}