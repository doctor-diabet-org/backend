package org.doctordiabet.domainlayer.usecase.verification_codes

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeCreateModel
import org.doctordiabet.domainlayer.exceptions.BusinessExceptionAccessor
import org.doctordiabet.domainlayer.exceptions.ExceptionBundle
import org.doctordiabet.domainlayer.exceptions.ExceptionSource
import org.doctordiabet.domainlayer.repository.verification_codes.VerificationCodesRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.regex.Pattern

class VerificationCodeSendUseCase(
        private val repository: VerificationCodesRepository
): AbstractUseCase() {

    private val phoneNumberPattern = Pattern.compile("^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}\$")

    fun execute(model: VerificationCodeCreateModel, callback: VerificationCodeSendCallback): Unit = Single.create<VerificationCodeCreateModel> { emitter ->
        try {
            validatePhoneNumber(model)
                    .let { emitter.onSuccess(model) }
        } catch(e: ExceptionBundle) {
            emitter.onError(e)
        }
    }
            .flatMap { repository.create(model) }
            .subscribe(object: DisposableSingleObserver<Unit>() {
                override fun onSuccess(t: Unit) = callback.onSuccess()
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })

    private fun validatePhoneNumber(model: VerificationCodeCreateModel) {
        if(!phoneNumberPattern.matcher(model.phoneNumber).matches()) {
            throw ExceptionBundle(ExceptionSource.BUSINESS).apply {
                (accessor as BusinessExceptionAccessor).apply {
                    code = BusinessExceptionAccessor.CODE_VERIFICATION_BAD_PHONE
                    message = "Phone validation failed. Bad phone number supplied"
                }
            }
        }
    }
}