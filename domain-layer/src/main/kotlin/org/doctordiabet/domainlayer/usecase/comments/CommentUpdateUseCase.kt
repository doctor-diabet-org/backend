package org.doctordiabet.domainlayer.usecase.comments

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.entity.comments.CommentUpdateModel
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase

class CommentUpdateUseCase(private val repository: CommentsRepository): AbstractUseCase() {

    fun execute(model: CommentUpdateModel, callback: CommentUpdateCallback): Unit = repository.update(model)
            .subscribe(object: DisposableSingleObserver<CommentEntity>() {
                override fun onSuccess(t: CommentEntity) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}