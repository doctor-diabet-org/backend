package org.doctordiabet.domainlayer.usecase.comments

import io.reactivex.observers.DisposableSingleObserver
import org.doctordiabet.domainlayer.entity.comments.CommentDeleteModel
import org.doctordiabet.domainlayer.repository.comments.CommentsRepository
import org.doctordiabet.domainlayer.usecase.base.AbstractUseCase
import java.util.*

class CommentDeleteUseCase(private val repository: CommentsRepository): AbstractUseCase() {

    fun execute(model: CommentDeleteModel, callback: CommentDeleteCallback): Unit = repository.delete(model)
            .subscribe(object: DisposableSingleObserver<UUID>() {
                override fun onSuccess(t: UUID) = t.let(callback::onSuccess)
                override fun onError(e: Throwable) = e.parse().let(callback::onFailure)
            })
}