package org.doctordiabet.domainlayer.entity.topics

import java.util.*

data class TopicDeleteModel(
        val id: UUID,
        val modifyingByIdentity: UUID
)