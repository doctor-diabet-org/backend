package org.doctordiabet.domainlayer.entity.authentication

data class AuthenticateCodeModel(
        val phoneNumber: String,
        val code: String
)