package org.doctordiabet.domainlayer.entity.topics

import java.util.*

data class TopicUpdateModel(
        val id: UUID,
        val title: String? = null,
        val description: String? = null,
        val categoryId: UUID? = null,
        val modifyingByIdentity: UUID
)