package org.doctordiabet.domainlayer.entity.identity

enum class RoleEntity {
    GUEST         ,
    AUTHORIZED    ,
    SPECIALIST    ,
    MODERATOR     ,
    ADMINISTRATOR ;
}