package org.doctordiabet.domainlayer.entity.artifact

data class TagCreateModel(
        val value: String
)