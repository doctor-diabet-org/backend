package org.doctordiabet.domainlayer.entity.categories

import java.util.*

data class CategoryCreateModel(
        val creatingByIdentity: UUID,
        val title: String,
        val description: String
)