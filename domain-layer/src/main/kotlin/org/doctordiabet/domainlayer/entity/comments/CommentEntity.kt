package org.doctordiabet.domainlayer.entity.comments

import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity
import java.util.*

data class CommentEntity(
        val artifactReferralId: UUID,
        val contentText: String,
        override val metadata: ArtifactEntity.Metadata
): ArtifactEntity