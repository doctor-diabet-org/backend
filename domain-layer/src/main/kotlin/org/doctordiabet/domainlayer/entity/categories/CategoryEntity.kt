package org.doctordiabet.domainlayer.entity.categories

import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity

data class CategoryEntity(
        val title: String,
        val description: String,
        val topicsCount: Long,
        override val metadata: ArtifactEntity.Metadata
): ArtifactEntity