package org.doctordiabet.domainlayer.entity.artifact

import org.doctordiabet.domainlayer.entity.identity.IdentityLink
import java.sql.Timestamp
import java.util.*

interface ArtifactEntity {

    val metadata: Metadata

    enum class Type {
        EXISTING,
        DELETED
    }

    data class Metadata(
            val id: UUID,
            val createdTime: Timestamp,
            val createdByIdentity: IdentityLink,
            val lastModifiedTime: Timestamp,
            val lastModifiedByIdentity: IdentityLink,
            val type: Type,
            val tags: Set<TagEntity>,
            val votes: Int = 0
    )
}