package org.doctordiabet.domainlayer.entity.categories

import org.doctordiabet.domainlayer.entity.artifact.StandardUpdateModel
import java.util.*

data class CategoryDeleteModel(
        val id: UUID,
        override val modifyingByIdentity: UUID
): StandardUpdateModel