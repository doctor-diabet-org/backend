package org.doctordiabet.domainlayer.entity.artifact

import java.util.*

interface StandardUpdateModel {

    val modifyingByIdentity: UUID
}