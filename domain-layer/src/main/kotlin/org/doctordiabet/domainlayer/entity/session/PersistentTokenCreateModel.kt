package org.doctordiabet.domainlayer.entity.session

import java.util.*

data class PersistentTokenCreateModel(
        val identityId: UUID
)