package org.doctordiabet.domainlayer.entity.comments

import java.util.*

data class CommentCreateModel(
        val artifactReferralId: UUID,
        val contentText: String,
        val modifyingByIdentity: UUID
)