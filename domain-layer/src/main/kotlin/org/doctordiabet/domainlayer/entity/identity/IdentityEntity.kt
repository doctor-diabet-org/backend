package org.doctordiabet.domainlayer.entity.identity

import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity
import java.util.*

interface IdentityEntity {

    val firstName: String
    val lastName: String
    val metadata: ArtifactEntity.Metadata
}

data class IdentityPublicEntity(
        override val firstName: String,
        override val lastName: String,
        override val metadata: ArtifactEntity.Metadata
): IdentityEntity

data class IdentityPrivateEntity(
        override val firstName: String,
        override val lastName: String,
        val phoneNumber: String?,
        val email: String,
        val pwdHash: String,
        override val metadata: ArtifactEntity.Metadata
): IdentityEntity

data class IdentityLink(
        val firstName: String,
        val lastName: String,
        val id: UUID
)