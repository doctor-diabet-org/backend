package org.doctordiabet.domainlayer.entity.artifact

enum class RelationshipEntity {
    CREATOR     ,
    CONTRIBUTOR ,
    NONE        ;
}