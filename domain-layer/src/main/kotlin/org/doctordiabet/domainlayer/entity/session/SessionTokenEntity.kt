package org.doctordiabet.domainlayer.entity.session

import java.util.*

data class SessionTokenEntity(
        val identityId: UUID,
        val refreshToken: String,
        val sessionToken: String,
        val createdTime: Long,
        val expiration: Long,
        val expirationTime: Long
)
