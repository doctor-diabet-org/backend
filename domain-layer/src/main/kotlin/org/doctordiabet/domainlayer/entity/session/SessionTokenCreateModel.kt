package org.doctordiabet.domainlayer.entity.session

import java.util.*

data class SessionTokenCreateModel(
        val identityId: UUID,
        val refreshToken: String
)