package org.doctordiabet.domainlayer.entity.artifact

import java.net.URI

data class LinkEntity(
        val uri: URI
)