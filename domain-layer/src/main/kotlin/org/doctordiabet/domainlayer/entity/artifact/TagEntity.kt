package org.doctordiabet.domainlayer.entity.artifact

import java.util.*

data class TagEntity(
        val id: UUID,
        val value: String
)