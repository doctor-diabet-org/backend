package org.doctordiabet.domainlayer.entity.verification_codes

import java.util.*

data class VerificationCodeCreateModel(
        val identityId: UUID,
        val phoneNumber: String
)