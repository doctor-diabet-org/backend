package org.doctordiabet.domainlayer.entity.comments

import java.util.*

data class CommentDeleteModel(
        val id: UUID,
        val modifyingByIdentity: UUID
)