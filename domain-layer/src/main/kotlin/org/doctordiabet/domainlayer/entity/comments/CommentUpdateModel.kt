package org.doctordiabet.domainlayer.entity.comments

import java.util.*

data class CommentUpdateModel(
    val id: UUID,
    val contentText: String? = null,
    val modifyingByIdentity: UUID
)