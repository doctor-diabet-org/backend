package org.doctordiabet.domainlayer.entity.identity

import java.util.*

class IdentityDeleteModel(
        val id: UUID,
        val modifyingByIdentity: UUID
)