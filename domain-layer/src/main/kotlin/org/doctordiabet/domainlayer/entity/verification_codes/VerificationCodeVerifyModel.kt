package org.doctordiabet.domainlayer.entity.verification_codes

import java.util.*

data class VerificationCodeVerifyModel(
        val identityId: UUID,
        val code: String
)