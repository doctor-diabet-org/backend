package org.doctordiabet.domainlayer.entity.authentication

data class AuthenticateCredentialsModel(
        val email: String,
        val password: String
)