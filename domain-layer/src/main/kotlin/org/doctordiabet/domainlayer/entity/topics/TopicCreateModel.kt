package org.doctordiabet.domainlayer.entity.topics

import java.util.*

data class TopicCreateModel(
        val title: String,
        val description: String,
        val tagIds: List<UUID>,
        val categoryId: UUID,
        val creatingByIdentity: UUID
)