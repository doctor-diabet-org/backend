package org.doctordiabet.domainlayer.entity.topics

import org.doctordiabet.domainlayer.entity.artifact.ArtifactEntity
import java.util.*

data class TopicEntity(
        val title: String,
        val description: String,
        val categoryId: UUID,
        val commentsCount: Long,
        override val metadata: ArtifactEntity.Metadata
): ArtifactEntity