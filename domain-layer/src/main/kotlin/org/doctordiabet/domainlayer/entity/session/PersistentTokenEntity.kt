package org.doctordiabet.domainlayer.entity.session

import java.util.*

data class PersistentTokenEntity(
        val identityId: UUID,
        val refreshToken: String
)