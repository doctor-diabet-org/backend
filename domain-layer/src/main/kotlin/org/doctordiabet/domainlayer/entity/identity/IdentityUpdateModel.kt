package org.doctordiabet.domainlayer.entity.identity

import java.util.*

data class IdentityUpdateModel(
        val id: UUID,
        val modifyingByIdentity: UUID,
        val firstName: String? = null,
        val lastName: String? = null,
        val phoneNumber: String? = null,
        val email: String? = null,
        val password: String? = null
)