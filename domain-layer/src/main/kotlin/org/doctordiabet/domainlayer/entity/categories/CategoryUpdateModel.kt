package org.doctordiabet.domainlayer.entity.categories

import org.doctordiabet.domainlayer.entity.artifact.StandardUpdateModel
import java.util.*

data class CategoryUpdateModel(
        val id: UUID,
        val title: String? = null,
        val description: String? = null,
        override val modifyingByIdentity: UUID
): StandardUpdateModel