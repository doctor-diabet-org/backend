package org.doctordiabet.domainlayer.entity.identity

import java.util.*

data class IdentityCreateModel(
        val firstName: String,
        val lastName: String,
        val phoneNumber: String?,
        val email: String,
        val password: String,
        val creatingByIdentity: UUID? = null
)