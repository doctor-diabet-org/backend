package org.doctordiabet.domainlayer.repository.artifacts

import io.reactivex.Observable
import io.reactivex.Single
import org.doctordiabet.domainlayer.entity.artifact.TagCreateModel
import org.doctordiabet.domainlayer.entity.artifact.TagEntity
import java.util.*

interface ArtifactsRepository {

    fun subscribe(identityId: UUID, artifactId: UUID): Single<Boolean>

    fun unsubscribe(identityId: UUID, artifactId: UUID): Single<Boolean>

    fun subscribers(artifactId: UUID): Single<List<UUID>>

    fun createTags(models: List<TagCreateModel>): Single<List<TagEntity>>

    fun readTags(): Single<List<TagEntity>>

    fun attachTags(artifactId: UUID, tagIds: List<UUID>): Single<Unit>

    fun detachTags(artifactId: UUID, tagIds: List<UUID>): Single<Unit>

    fun upvote(identityId: UUID, artifactId: UUID): Single<Boolean>

    fun downvote(identityId: UUID, artifactId: UUID): Single<Boolean>

    fun sub(): Observable<Event>

    sealed class Event {
        data class Subscribe       (val identityId: UUID, val entityId: UUID, val ownerId: UUID): Event()
        data class Unsubscribe     (val identityId: UUID, val entityId: UUID, val ownerId: UUID): Event()

        data class IdentityUpvote  (val identityId: UUID, val entityId: UUID): Event()
        data class IdentityDownvote(val identityId: UUID, val entityId: UUID): Event()
        data class CategoryUpvote  (val identityId: UUID, val entityId: UUID): Event()
        data class CategoryDownvote(val identityId: UUID, val entityId: UUID): Event()
        data class TopicUpvote     (val identityId: UUID, val entityId: UUID): Event()
        data class TopicDownvote   (val identityId: UUID, val entityId: UUID): Event()
        data class CommentUpvote   (val identityId: UUID, val entityId: UUID): Event()
        data class CommentDownvote (val identityId: UUID, val entityId: UUID): Event()
    }
}