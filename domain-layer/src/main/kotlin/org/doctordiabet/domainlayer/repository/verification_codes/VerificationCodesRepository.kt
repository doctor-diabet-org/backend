package org.doctordiabet.domainlayer.repository.verification_codes

import io.reactivex.Single
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeCreateModel
import org.doctordiabet.domainlayer.entity.verification_codes.VerificationCodeVerifyModel
import java.util.*

interface VerificationCodesRepository {

    fun create(model: VerificationCodeCreateModel): Single<Unit>

    fun verify(model: VerificationCodeVerifyModel): Single<Boolean>
}