package org.doctordiabet.domainlayer.repository.roles

import io.reactivex.Single
import org.doctordiabet.domainlayer.entity.artifact.RelationshipEntity
import org.doctordiabet.domainlayer.entity.identity.RoleEntity
import java.util.*

interface RolesRepository {

    fun grant(identityId: UUID, role: RoleEntity): Single<List<RoleEntity>>

    fun read(identityId: UUID): Single<List<RoleEntity>>

    fun revoke(identityId: UUID, role: RoleEntity): Single<List<RoleEntity>>

    fun relationshipOf(identityId: UUID, artifactId: UUID): Single<RelationshipEntity>
}