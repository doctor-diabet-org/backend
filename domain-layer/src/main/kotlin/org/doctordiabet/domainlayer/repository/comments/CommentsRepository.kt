package org.doctordiabet.domainlayer.repository.comments

import io.reactivex.Observable
import io.reactivex.Single
import org.doctordiabet.domainlayer.entity.comments.CommentCreateModel
import org.doctordiabet.domainlayer.entity.comments.CommentDeleteModel
import org.doctordiabet.domainlayer.entity.comments.CommentEntity
import org.doctordiabet.domainlayer.entity.comments.CommentUpdateModel
import java.sql.Timestamp
import java.util.*

interface CommentsRepository {

    fun create(model: CommentCreateModel): Single<CommentEntity>

    fun read(id: UUID): Single<CommentEntity>

    fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentEntity>>>

    fun read(artifactReferralId: UUID, offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<CommentEntity>>>

    fun update(model: CommentUpdateModel): Single<CommentEntity>

    fun delete(model: CommentDeleteModel): Single<UUID>

    fun sub(): Observable<Event>

    sealed class Event {
        data class Create(val referral: Referral, val identityId: UUID, val entityId: UUID): Event()
        data class Update(val referral: Referral, val identityId: UUID, val entityId: UUID): Event()
        data class Delete(val referral: Referral, val identityId: UUID, val entityId: UUID): Event()
    }

    sealed class Referral {
        object Identity: Referral()
        object Category: Referral()
        object Topic: Referral()
        object Comment: Referral()
    }
}