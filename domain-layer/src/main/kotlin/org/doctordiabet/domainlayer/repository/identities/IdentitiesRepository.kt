package org.doctordiabet.domainlayer.repository.identities

import io.reactivex.Single
import org.doctordiabet.domainlayer.entity.identity.IdentityCreateModel
import org.doctordiabet.domainlayer.entity.identity.IdentityDeleteModel
import org.doctordiabet.domainlayer.entity.identity.IdentityEntity
import org.doctordiabet.domainlayer.entity.identity.IdentityUpdateModel
import java.sql.Timestamp
import java.util.*

interface IdentitiesRepository {

    fun create(model: IdentityCreateModel, full: Boolean = true): Single<IdentityEntity>

    fun read(id: UUID, full: Boolean = false): Single<IdentityEntity>

    fun readByPhone(phoneNumber: String, full: Boolean = false): Single<IdentityEntity>

    fun readByEmail(email: String, full: Boolean = false): Single<IdentityEntity>

    fun read(offset: Int, limit: Int, full: Boolean = false): Single<Pair<Int, List<IdentityEntity>>>

    fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, full: Boolean = false): Single<Pair<Int, List<IdentityEntity>>>

    fun read(offset: Int, limit: Int, nameSubstring: String, full: Boolean = false): Single<Pair<Int, List<IdentityEntity>>>

    fun read(offset: Int, limit: Int, nameSubstring: String, timestamp: Timestamp, modified: Boolean, full: Boolean): Single<Pair<Int, List<IdentityEntity>>>

    fun update(model: IdentityUpdateModel, full: Boolean = false): Single<IdentityEntity>

    fun delete(model: IdentityDeleteModel): Single<UUID>

    fun calculatePwdHash(pwd: String): String
}