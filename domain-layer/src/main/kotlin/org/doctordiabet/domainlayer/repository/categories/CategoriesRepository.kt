package org.doctordiabet.domainlayer.repository.categories

import io.reactivex.Observable
import io.reactivex.Single
import org.doctordiabet.domainlayer.contract.order.ArtifactFilter
import org.doctordiabet.domainlayer.entity.categories.CategoryCreateModel
import org.doctordiabet.domainlayer.entity.categories.CategoryDeleteModel
import org.doctordiabet.domainlayer.entity.categories.CategoryEntity
import org.doctordiabet.domainlayer.entity.categories.CategoryUpdateModel
import java.sql.Timestamp
import java.util.*

interface CategoriesRepository {

    fun create(model: CategoryCreateModel): Single<CategoryEntity>

    fun read(id: UUID): Single<CategoryEntity>

    fun read(offset: Int, limit: Int, tagIds: List<UUID> = emptyList(), criteria: List<ArtifactFilter> = emptyList()): Single<Pair<Int, List<CategoryEntity>>>

    fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID> = emptyList(), criteria: List<ArtifactFilter> = emptyList()): Single<Pair<Int, List<CategoryEntity>>>

    fun update(model: CategoryUpdateModel): Single<CategoryEntity>

    fun delete(model: CategoryDeleteModel): Single<UUID>

    fun sub(): Observable<Event>

    sealed class Event {
        data class Create(val identityId: UUID, val entityId: UUID): Event()
        data class Update(val identityId: UUID, val entityId: UUID): Event()
        data class Delete(val identityId: UUID, val entityId: UUID): Event()
    }
}