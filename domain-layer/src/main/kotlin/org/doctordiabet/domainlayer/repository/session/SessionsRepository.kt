package org.doctordiabet.domainlayer.repository.session

import io.reactivex.Maybe
import io.reactivex.Single
import org.doctordiabet.domainlayer.entity.session.PersistentTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.PersistentTokenEntity
import org.doctordiabet.domainlayer.entity.session.SessionTokenCreateModel
import org.doctordiabet.domainlayer.entity.session.SessionTokenEntity
import java.util.*

interface SessionsRepository {

    fun createPersistent(model: PersistentTokenCreateModel): Single<PersistentTokenEntity>

    fun createSession(model: SessionTokenCreateModel): Single<SessionTokenEntity>

    fun readPersistent(identityId: UUID): Single<PersistentTokenEntity>

    fun readSession(identityId: UUID): Single<List<SessionTokenEntity>>

    fun readSession(token: String): Single<UUID>
}