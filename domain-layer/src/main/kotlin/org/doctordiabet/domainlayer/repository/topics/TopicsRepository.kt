package org.doctordiabet.domainlayer.repository.topics

import io.reactivex.Observable
import io.reactivex.Single
import org.doctordiabet.domainlayer.entity.topics.TopicCreateModel
import org.doctordiabet.domainlayer.entity.topics.TopicDeleteModel
import org.doctordiabet.domainlayer.entity.topics.TopicEntity
import org.doctordiabet.domainlayer.entity.topics.TopicUpdateModel
import java.sql.Timestamp
import java.util.*

interface TopicsRepository {

    fun create(model: TopicCreateModel): Single<TopicEntity>

    fun read(id: UUID): Single<TopicEntity>

    fun read(offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<TopicEntity>>>

    fun read(categoryId: UUID, offset: Int, limit: Int, timestamp: Timestamp, modified: Boolean, tagIds: List<UUID> = emptyList()): Single<Pair<Int, List<TopicEntity>>>

    fun update(model: TopicUpdateModel): Single<TopicEntity>

    fun delete(model: TopicDeleteModel): Single<UUID>

    fun sub(): Observable<Event>

    sealed class Event {
        data class Create(val identityId: UUID, val entityId: UUID): Event()
        data class Update(val identityId: UUID, val entityId: UUID): Event()
        data class Delete(val identityId: UUID, val entityId: UUID): Event()
    }
}