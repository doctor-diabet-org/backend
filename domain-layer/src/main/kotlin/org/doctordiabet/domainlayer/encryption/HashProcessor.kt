package org.doctordiabet.domainlayer.encryption

interface HashProcessor {

    fun apply(value: String): String
}